# CoreDMS Documentation

This documentation may be changes while deploying. You need to review
each comments for each features in classes for latest changes.

**_This documentation has 3 sections:_**

- **Module Summary:** Description of the directory tree, features of the components, requirements.
- **Getting Started:** How to integrate module in new project, migrate module into existing projects.
- **Fix Issues:** Fix conflicting dependencies, versions, compatible, debugging, improve source code while deploying.


### A. Module Summary

##### 1. Module requirements.
This module using Kotlin programming language, excludes some class written in Java (required from Gradle processor like databinding adapters,...):

- Support minimum API: 19 and above.
- AndroidX (Jetpack) is required.
- Support RxJava: RxJava is using everywhere, any events, threads handler, local/network processors,...
- Build types available: `debug`, `staging`, `release`. You can change directly in `DMSCore` based on your PM. (For example: You don't have `staging` type, use `QC` instead, you can change it in module `build.gradle` file.).
- Support View Databinding.
- Support debugger: `Timber`, `Stetho`,... You can debug directly from Google Chrome in debug mode (by goto `chrome://inspect/#devices`).
- Included dependencies: Please see in `build.gradle` for dependencies and its version.


##### 2. **CoreDMS** has 3 main points: controller, entities, and utilities.
- **Controller:** Base activities, fragments, lifecycle observable classes.
- **Entity:** Base models, daos, rest resquests/responses, viewmodels, annotations.
- **Utilities:** Datasources (DataBindingSource, ViewModelSource), extensions, helpers classes, api data contributes, api exception parsers.


##### 3. Anatomy of a **CoreDMS** module,
Below is directories structure of **CoreDMS** module, the marks (a, b, c,...) with description is available for using in deploying app.

Other directories which no have marks is using for module processors, you can reuse but not recommendation.

```
├── api                     // a. Handle anythings with Restful. Main class: `DMSRest`.
│   ├── contribs            // a.1. Available supported entities/helpers can be reuse while processing api data.
│   │   ├── parsers         // a.1.1. When server response an exception, we will negotiate based on response body and select correct parser to get correct data to UI. You can discuss with backend developers and create custom parser to fit exception response body format.
│   │   └── responses       // a.1.2. Available response data can be parse from backend api.
│   ├── entities            // a.2. Any request/response data need to inherit from base `DMSRestRequest`, `DMSRestResponse`. When backend response error, you can use `DMSRestException` to check and debug error while requesting.
│   └── processors          // a.3. Classes with interact between Network and Datasource. If you familiar with Javascript, it like `Promise` classes.
├── behaviours
├── datasources             // b. Datasource annotations for Controllers.
├── delegates
├── entities                // c. Base entities, any models, objects,... interact with controllers using in this module need to inherit from this base.
├── extensions              // d. Kotlin extensions speed up your project development.
├── ui                      // e. Base UI controllers. Exclude `view` for widget, any activities, fragments need to inherit from this base.
│   ├── activity
│   ├── fragment
│   └── view
└── utils                   // d. App's helpers.
    ├── alert               // d.1. This helper already added into base activities, fragments with named: `alert`.
    ├── databinding
    ├── fragment
    ├── notification
    ├── permission          // d.2. This helper already added into base activities, fragments with named: `permission`.
    ├── preference          // d.3. This helper already added into app and call it everywhere with `DMSPreference.get()`.
    ├── recycleradapters
    └── socketio
```


### B. Getting Started

#### 1. Check your project requirements.

- Check supported minimum version: Current **CoreDMS** only support API 19 (Kitkat) and above. If you need to support lower version, negotiate libraries version and correct your project structure. See __Fix Issues__ section for more information.
- This module is using MVVM pattern, but have some changes based on dependencies. For example, your app need to custom `Presenter` to present data into UI and handle actions from UI. Presenter can interact with your Controller directly, so Presenter's class need to implement as inner class.
- Your root project `build.gradle` need to declare version of libraries consistency between your app and CoreDMS module.

#### 2. Integrate module into fresh new project.

**Edit your project's `build.gradle` file:**
Create new project, enable Kotlin programming language, set Android minimum of API to 19. Open your project's `build.gradle` file, add there lines above `clean` task:

```
ext {
    version_core_androidx_support 	= '1.0.0'
    version_arch_lifecycle 			= '2.0.0'
    version_arch_room 					= '2.0.0'
    version_arch_paging 				= '2.1.0-alpha01'
    version_3rd_retrofit 				= '2.4.0'
    version_3rd_google_services 		= '16.0.0'
    version_3rd_glide 					= '4.5.0'
}
```


From project's `build.gradle` file, navigate into `buildscript` tag, add (or replace) this line on top:

```
ext.kotlin_version = '1.2.71'
```


Preview your project's `build.gradle` file:

```
// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    ext.kotlin_version = '1.2.71'
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.2.1'
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
        classpath 'com.google.gms:google-services:4.1.0'
    }
}

allprojects {
    repositories {
        google()
        jcenter()
        maven { url "https://jitpack.io" }
    }
}

ext {
    version_core_androidx_support = '1.0.0'
    version_arch_lifecycle = '2.0.0'
    version_arch_room = '2.0.0'
    version_arch_paging = '2.1.0-alpha01'
    version_3rd_retrofit = '2.4.0'
    version_3rd_google_services = '16.0.0'
    version_3rd_glide = '4.5.0'
}

task clean(type: Delete) {
    delete rootProject.buildDir
}

```
---


**Edit your project's `gradle-wrapper.properties` file:**

Because we are using gradle plugin version 3.2.1 to compile/lint our source code, so we need to retrive gradle 4.6 and above.

Change this file is required, otherwise, your compiler will be can't find your gradle plugin to process source code.

```
distributionBase=GRADLE_USER_HOME
distributionPath=wrapper/dists
zipStoreBase=GRADLE_USER_HOME
zipStorePath=wrapper/dists
distributionUrl=https\://services.gradle.org/distributions/gradle-4.6-all.zip

```
---


**Declare CoreDMS module into your project's settings**
Navigate into your `settings.gradle`, add `:coredms` inside `app` key:

```
include ':app', ':coredms'
```
---


**Edit your app's `build.gradle` file:**

Enable databinding to your app module: Inside `android` tag, add this lines:

```
dataBinding {
    enabled = true
}
```


If you are using `google-services` plugin, some class of this library deploy over 65,536 methods... That is not good, we have to accept by adding `multiDexEnabled` into `android` tag.

```
android {
	...
	defaultConfig {
		...
		multiDexEnabled true
		...
	}
	...
}
```
---


**Import CoreDMS module, in your `dependencies` tag, add there lines:**

```
implementation project(':coredms')

kapt "androidx.lifecycle:lifecycle-compiler:$rootProject.ext.version_arch_lifecycle"
kapt "androidx.room:room-compiler:$rootProject.ext.version_arch_room"
kapt "com.github.bumptech.glide:compiler:$rootProject.ext.version_3rd_glide"

implementation 'com.android.support:multidex:1.0.3'
```

To use `kapt` you need to add `kotlin-kapt` plugin into top of your `build.gradle` file:

```
apply plugin: 'kotlin-kapt'
```

Preview your app's `build.gradle` file:

```
apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-android-extensions'
apply plugin: 'kotlin-kapt'


android {
    compileSdkVersion 28
    defaultConfig {
        minSdkVersion 19
        targetSdkVersion 28
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
        multiDexEnabled true
        // ... your rest of project
    }

    dataBinding {
        enabled = true
    }

    buildTypes {
        debug {
            debuggable = true
            applicationIdSuffix ".debug"
            versionNameSuffix "-DEBUG"
        }
        staging {
            initWith debug
            applicationIdSuffix ".staging"
            versionNameSuffix "-STAGING"
        }
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    implementation project(':coredms')
    implementation 'com.android.support:multidex:1.0.3'

    // implementation 'com.google.firebase:firebase-core:16.0.4'
    // implementation 'com.google.firebase:firebase-messaging:17.3.4'

    kapt "androidx.lifecycle:lifecycle-compiler:$rootProject.ext.version_arch_lifecycle"
    kapt "androidx.room:room-compiler:$rootProject.ext.version_arch_room"
    kapt "com.github.bumptech.glide:compiler:$rootProject.ext.version_3rd_glide"
    
   	 // ... your rest of project...
}


// apply plugin: 'com.google.gms.google-services'

```

All done! Press `Make Project` to build your project. You will see module named `coredms` in project structure after build successfully.

Otherwise, see **Fix Issues** section for more information.
---


**Init CoreDMS module**
To make CoreDMS work correctly, you need to init module in your App context.

Create new `Application` class and declare your App's class in `AndroidManifest.xml` file. Init `CoreDMS` inside `onCreate` method:

```
class App : Application() {

	override fun onCreate() {
		super.onCreate()
		
		// init default dms preference
		this@App.initialCoreDMS()
	}

}
```

**When your class inherited from Application class, we already added method named `App.initialCoreDMS `. You just call this method to setup CoreDMS. After setup, you will have:**

- Logging: `Timber` will enable, you can print into logcat directly when call `Timber.d(...)`, or log exception detail by `Timber.e(...)`, `Timber.wtf(...)`. The logger can be print into logcat in debug mode, or notify to Crashlytics in production build.
- Chrome Debugger: Your app can be debuggable in Google Chrome by enter `chrome://inspect/#devices`, you can: Edit your database, SharedPreference data, network tracking (like Charles) or more,...
- Preference: You can save/get data to/from SharedPreference by call `DMSPreference.get(...)`, `DMSPreference.set(...)`. You no need to care of the data type, we help you negotiate it and save in correct type.
- Image Processor: We already init Facebook Fresco. For more information about this module, Google it.
- App font: This is optional, you can skip it. Some project need to display language in correct font (for example, device not support Japanese language, we need to enable Japanese text in app), so we need to use custom font instead of System font.


### C. Fix Issues
