package com.seldatdirect.dms.core.utils.socketio

import java.lang.Exception

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSSocketListener {

    fun onSocketConnection(socket: DMSSocket, isConnected: Boolean) = Unit

    fun onSocketReConnecting(socket: DMSSocket) = Unit

    fun onSocketGotPayload(socket: DMSSocket, eventName: String, payload: Array<Any>?) = Unit

    fun onSocketConnectedFailed(socket: DMSSocket, exception: Exception) = Unit

    fun onSocketException(socket: DMSSocket, exception: Exception) = Unit

}
