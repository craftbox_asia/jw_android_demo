package com.seldatdirect.dms.core.utils.databinding;

import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.BindingAdapter;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class ToolbarBindingAdapter {

    @BindingAdapter("toolbarOnNavigationIconClicked")
    public static void toolbarOnNavigationIconClicked(Toolbar toolbar, @Nullable View.OnClickListener onClickListener) {
        toolbar.setNavigationOnClickListener(onClickListener);
    }

}
