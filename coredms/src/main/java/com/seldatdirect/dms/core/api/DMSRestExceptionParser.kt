package com.seldatdirect.dms.core.api

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSRestExceptionParser {

    fun getErrorMessage(responseBody: String): String?

}
