@file:Suppress("unused")

package com.seldatdirect.dms.core.api.entities

import android.content.Context
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


class DMSRestException : DMSJsonableObject {

    var httpStatusCode: Int = -1

    var httpBody: String? = null

    var type: ExceptionType = ExceptionType.Unknown

    var original: Throwable? = null


    fun getErrorMessage(context: Context): String {
        val defaultError = context.getString(R.string.dms_error_api_unknown)

        return when (this@DMSRestException.type) {
            ExceptionType.Server -> {
                // parse error response from backend and get actual message
                // otherwise (parsing error, no message,...) will return unknown error from local resource
                DMSRest.config.exceptionParsers.asSequence().mapNotNull { parser ->
                    parser.getErrorMessage(this@DMSRestException.httpBody ?: "{}")
                }.firstOrNull() ?: context.getString(R.string.dms_error_api_unknown_server)
            }
            ExceptionType.NetworkTimeout -> context.getString(R.string.dms_error_api_network_timeout)
            ExceptionType.NoInternet -> context.getString(R.string.dms_error_api_no_internet)
            else -> defaultError
        }
    }

    fun isUnAuthorization(): Boolean {
        return arrayOf(401, 403).contains(this@DMSRestException.httpStatusCode)
    }

    enum class ExceptionType {
        Server,
        NoInternet,
        NetworkTimeout,
        Unknown
    }


    companion object {

        fun customMessage(message: String?): DMSRestException {
            val instance = DMSRestException()
            instance.httpStatusCode = 400
            instance.httpBody = "{ \"message\": \"${ message ?: "unknown" }\" }"
            instance.type = ExceptionType.Server
            return instance
        }

    }

}
