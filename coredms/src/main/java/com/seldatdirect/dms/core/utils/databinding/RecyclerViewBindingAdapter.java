package com.seldatdirect.dms.core.utils.databinding;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.annotation.Nullable;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class RecyclerViewBindingAdapter {

    @BindingAdapter(value = {"recyclerViewGridLayoutAdapter", "recyclerViewGirdLayoutColumns"})
    public static void recyclerViewGridLayoutAdapter(RecyclerView recyclerView, @Nullable RecyclerView.Adapter adapter, int columns) {
        if (recyclerView == null || columns == 0 || adapter == null) {
            return;
        }

        recyclerView.setLayoutManager(new GridLayoutManager(recyclerView.getContext(), columns));
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter(value = {"recyclerViewLinearLayoutAdapter", "recyclerViewLinearLayoutIsHorizontal"})
    public static void recyclerViewLinearLayoutAdapter(RecyclerView recyclerView, @Nullable RecyclerView.Adapter adapter, boolean isHorizontal) {
        if (recyclerView == null || adapter == null) {
            return;
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        layoutManager.setOrientation(isHorizontal ? RecyclerView.HORIZONTAL : RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter("recyclerViewItemDecoration")
    public static void recyclerViewItemDecoration(RecyclerView recyclerView, @Nullable RecyclerView.ItemDecoration decoration) {
        if (recyclerView == null || decoration == null) {
            return;
        }

        for (int itemDecoratorIndex = 0; itemDecoratorIndex < recyclerView.getItemDecorationCount(); itemDecoratorIndex += 1) {
            recyclerView.removeItemDecorationAt(itemDecoratorIndex);
        }

        recyclerView.addItemDecoration(decoration);
        recyclerView.invalidateItemDecorations();
    }

    @BindingAdapter("recyclerViewCustomLayoutManager")
    public static void recyclerViewCustomLayoutManager(RecyclerView recyclerView, @Nullable RecyclerView.LayoutManager layoutManager) {
        if (recyclerView == null) {
            return;
        }

        try {
            if (recyclerView.getLayoutManager() == null || (recyclerView.getLayoutManager() != null && recyclerView.getLayoutManager() != layoutManager))
                recyclerView.setLayoutManager(layoutManager);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @BindingAdapter("recyclerViewIsInsideNestedLayout")
    public static void recyclerViewIsInsideNestedLayout(RecyclerView recyclerView, boolean isInsideNested) {
        if (recyclerView == null) {
            return;
        }

        recyclerView.setNestedScrollingEnabled(!isInsideNested);
    }

    @BindingAdapter("recyclerViewCustomAdapter")
    public static void recyclerViewCustomAdapter(RecyclerView recyclerView, @Nullable RecyclerView.Adapter adapter) {
        if (recyclerView == null) {
            return;
        }

        recyclerView.setAdapter(adapter);
    }

}
