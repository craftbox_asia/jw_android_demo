package com.seldatdirect.dms.core.api.contribs.parsers

import com.google.gson.Gson
import com.seldatdirect.dms.core.api.DMSRestExceptionParser
import timber.log.Timber
import java.io.Serializable

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [StringArrayDMSExceptionParser]
 *
 * This will parsing response body into data with format like
 * [
 *      "message",...
 * ]
 */
class StringArrayDMSExceptionParser : DMSRestExceptionParser {

    override fun getErrorMessage(responseBody: String): String? {
        return try {
            Gson().fromJson(responseBody, Data::class.java).firstOrNull()
        } catch (exception: Exception) {
            null
        }
    }


    class Data : ArrayList<String>(), Serializable

}
