@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.seldatdirect.dms.core.utils.permission

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.seldatdirect.dms.core.ui.activity.DMSActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSPermission]
 * Manage apps permissions, included helpers for runtime permissions request
 *
 * @property context Context
 * @constructor
 */
class DMSPermission(private val context: Context) {

    /**
     * List of declared permissions in manifest (included merged manifest)
     *
     * @return List<String>
     */
    fun getManifestUsesPermissions(): List<String> {
        return try {
            val info = this@DMSPermission.context.packageManager.getPackageInfo(
                    this@DMSPermission.context.packageName,
                    PackageManager.GET_PERMISSIONS
            )
            info.requestedPermissions?.toList() ?: emptyList()
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    /**
     * List of permission that not granted by user
     *
     * @return List<String>
     */
    fun getMissingPermissions(): List<String> {
        return this@DMSPermission.getManifestUsesPermissions()
                .filter { !this@DMSPermission.isPermissionGranted(it) }
    }

    /**
     * Send request permissions dialog and handle response in [DMSActivity.onRequestPermissionsResult]
     *
     * @param activity DMSActivity
     * @param permissions List<String>
     * @param customRequestCode Int
     */
    fun requestPermission(activity: DMSActivity, permissions: List<String>, customRequestCode: Int = REQUEST_CODE) {
        ActivityCompat.requestPermissions(activity, permissions.toTypedArray(), customRequestCode)
    }

    /**
     * Send request permissions dialog and handle response in [DMSFragment.onRequestPermissionsResult]
     *
     * @param fragment DMSFragment
     * @param permissions List<String>
     * @param customRequestCode Int
     */
    fun requestPermission(fragment: DMSFragment, permissions: List<String>, customRequestCode: Int = REQUEST_CODE) {
        fragment.requestPermissions(permissions.toTypedArray(), customRequestCode)
    }

    /**
     * Check if a permission granted by user
     *
     * @param permissionName String
     * @return Boolean
     */
    fun isPermissionGranted(permissionName: String): Boolean {
        return ContextCompat.checkSelfPermission(this@DMSPermission.context, permissionName) == PackageManager.PERMISSION_GRANTED
    }

    companion object {

        const val REQUEST_CODE: Int = 200

        val DAMAGE_PERMISSIONS = arrayOf(
                android.Manifest.permission.READ_CALENDAR,
                android.Manifest.permission.WRITE_CALENDAR,
                android.Manifest.permission.CAMERA,
                android.Manifest.permission.READ_CONTACTS,
                android.Manifest.permission.WRITE_CONTACTS,
                android.Manifest.permission.GET_ACCOUNTS,
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.RECORD_AUDIO,
                android.Manifest.permission.READ_PHONE_STATE,
                "android.permission.READ_PHONE_NUMBERS",
                android.Manifest.permission.CALL_PHONE,
                "android.permission.ANSWER_PHONE_CALLS",
                android.Manifest.permission.READ_CALL_LOG,
                android.Manifest.permission.WRITE_CALL_LOG,
                android.Manifest.permission.ADD_VOICEMAIL,
                android.Manifest.permission.USE_SIP,
                android.Manifest.permission.PROCESS_OUTGOING_CALLS,
                "android.permission.BODY_SENSORS",
                android.Manifest.permission.SEND_SMS,
                android.Manifest.permission.RECEIVE_SMS,
                android.Manifest.permission.READ_SMS,
                android.Manifest.permission.RECEIVE_WAP_PUSH,
                android.Manifest.permission.RECEIVE_MMS,
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

    }

}
