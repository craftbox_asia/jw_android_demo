package com.seldatdirect.dms.core.api.contribs.responses

import com.seldatdirect.dms.core.api.entities.DMSRestResponse

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


open class ListDMSRestResponse<T> : ArrayList<T>(), DMSRestResponse
