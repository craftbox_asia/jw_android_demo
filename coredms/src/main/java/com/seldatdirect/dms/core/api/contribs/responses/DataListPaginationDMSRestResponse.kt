@file:Suppress("unused")

package com.seldatdirect.dms.core.api.contribs.responses

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


class DataListPaginationDMSRestResponse<T> : DMSRestResponse {

    @SerializedName("data")
    var data: List<T>   = emptyList()

    @SerializedName("meta")
    var meta: Meta      = Meta()


    class Meta : DMSJsonableObject {

        @SerializedName("pagination")
        var pagination: Pagination  = Pagination()

    }

    class Pagination : DMSJsonableObject {

        @SerializedName("total")
        var total: Int      = 0

        @SerializedName("per_page")
        var perPage: String = "0"

        @SerializedName("current_page")
        var currentPage: Int    = 0

        @SerializedName("total_pages")
        var totalPages: Int      = 0


        fun getPerPage(): Int {
            return this@Pagination.perPage.toIntOrNull() ?: 0
        }

    }
}
