@file:Suppress("MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.cardview.widget.CardView
import com.seldatdirect.dms.core.R
import java.lang.Exception

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


class DMSCardView :  CardView {

    constructor(context: Context) : super(context) { this@DMSCardView.init(null) }
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) { this@DMSCardView.init(attrs) }
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { this@DMSCardView.init(attrs) }


    private lateinit var titleTextView: TextView
    private var containerView: FrameLayout? = null


    fun setTitle(title: String?) {
        this@DMSCardView.titleTextView.text = title
    }

    fun setTitleRes(@StringRes titleResId: Int) {
        this@DMSCardView.titleTextView.setText(titleResId)
    }


    private fun init(attrs: AttributeSet?) {
        LayoutInflater.from(this@DMSCardView.context)
                .inflate(R.layout.dms_widget_card_view, this@DMSCardView)
        this@DMSCardView.clipChildren   = false
        this@DMSCardView.clipToPadding  = false

        this@DMSCardView.titleTextView = this@DMSCardView.findViewById(android.R.id.title)
        this@DMSCardView.containerView = this@DMSCardView.findViewById(android.R.id.custom)

        attrs?.let { this@DMSCardView.parseAttrs(it) }
    }

    private fun parseAttrs(attributeSet: AttributeSet) {
        val typedValue = this@DMSCardView.context.resources
                .obtainAttributes(attributeSet, R.styleable.DMSCardView)

        // parse title text
        try {
            val titleResId = typedValue.getResourceId(R.styleable.DMSCardView_sectionCardTitle, View.NO_ID)
            this@DMSCardView.setTitleRes(titleResId)
        } catch (idNotFoundException: Exception) {
            idNotFoundException.printStackTrace()
        }
        if (this@DMSCardView.titleTextView.text.isNullOrBlank()) {
            typedValue.getString(R.styleable.DMSCardView_sectionCardTitle)
                    ?.let { this@DMSCardView.setTitle(it) }
        }

        // parse card attributes
        try {
            this@DMSCardView.cardElevation  = typedValue.getDimension(R.styleable.DMSCardView_sectionCardElevation, this@DMSCardView.resources.getDimension(R.dimen.dms_spacing_mini))
            this@DMSCardView.radius         = typedValue.getDimension(R.styleable.DMSCardView_sectionCardCornerRadius, this@DMSCardView.resources.getDimension(R.dimen.dms_spacing_mini))
        } catch (resourceNotFound: Exception) {
            resourceNotFound.printStackTrace()
        }

        typedValue.recycle()
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        if (this@DMSCardView.containerView == null) {
            super.addView(child, index, params)
            return
        }

        this@DMSCardView.containerView
                ?.addView(child, index, params)
    }

}
