@file:Suppress("PropertyName", "unused", "MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.entities

import androidx.room.ColumnInfo
import androidx.room.PrimaryKey

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSModel : DMSJsonableObject {

    @DMSAnnotations.DMSJsonableExcludeField
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "localDBId")
    open var localDBId: Long? = null

    @DMSAnnotations.DMSJsonableExcludeField
    @ColumnInfo(name = "localCreatedAt")
    open var localCreatedAt: Long? = System.currentTimeMillis()

    @DMSAnnotations.DMSJsonableExcludeField
    @ColumnInfo(name = "localUpdatedAt")
    open var localUpdatedAt: Long? = System.currentTimeMillis()


    open fun onCopyPrimaryKey(): Long? = this@DMSModel.localDBId

    open fun saveOrUpdate(): Long? {
        this@DMSModel.localDBId = this@DMSModel.localDBId ?: this@DMSModel.onCopyPrimaryKey()

        if (this@DMSModel.localCreatedAt == null) {
            this@DMSModel.localCreatedAt = System.currentTimeMillis()
        }

        this@DMSModel.localUpdatedAt = System.currentTimeMillis()

        return this@DMSModel.localDBId
    }

    /**
     * sometime layout don't detect empty field to display empty text
     * we only return string text if it not null and have text, otherwise return null
     *
     * @param field String?
     * @return String?
     */
    fun getStringOrNull(field: String?): String? {
        return if (field.isNullOrBlank()) null else field
    }


    companion object {

        inline fun <reified T : DMSModel> mergeFields(from: T, to: T) {
            from::class.java.declaredFields.forEach { field ->
                val isLocked = field.isAccessible
                field.isAccessible = true

                if (field.get(from) != null) {
                    field.set(to, field.get(from))
                }

                field.isAccessible = isLocked
            }
        }

    }

}
