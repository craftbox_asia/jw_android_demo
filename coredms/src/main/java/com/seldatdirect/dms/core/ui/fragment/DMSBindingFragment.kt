package com.seldatdirect.dms.core.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.seldatdirect.dms.core.datasources.DMSDataBindingSource

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSBindingFragment<T : ViewDataBinding>(@LayoutRes private val layoutResId: Int) : DMSFragment(), DMSDataBindingSource<T> {

    override lateinit var dataBinding: T


    final override fun getLayoutResId(savedInstanceState: Bundle?): Int? {
        return null
    }

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this@DMSBindingFragment.dataBinding = DataBindingUtil.inflate(inflater, this@DMSBindingFragment.layoutResId, container, false)
        this@DMSBindingFragment.dataBinding.setLifecycleOwner(this@DMSBindingFragment)
        return this@DMSBindingFragment.dataBinding.root
    }

}
