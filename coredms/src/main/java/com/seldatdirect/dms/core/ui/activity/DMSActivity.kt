@file:Suppress("MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.behaviours.DMSControllerBehaviour
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.delegates.DMSControllerDelegate
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.alert.DMSAlert
import com.seldatdirect.dms.core.utils.permission.DMSPermission
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSActivity : AppCompatActivity(), DMSControllerBehaviour, DMSControllerDelegate, DMSViewModel.Listener {

    override fun onResume() {
        super.onResume()
        Timber.d("onResume: " + this.localClassName)
    }

    protected val alert: DMSAlert by lazy {
        DMSAlert(this@DMSActivity)
    }

    protected val permission: DMSPermission by lazy {
        DMSPermission(this@DMSActivity)
    }

    protected val compositeDisposable = CompositeDisposable()

    private val onBackButtonPressedSubject = PublishSubject.create<Boolean>()

    @LayoutRes
    override fun getLayoutResId(savedInstanceState: Bundle?): Int? = null

    @MenuRes
    override fun getMenuResId(): Int? = null

    @StringRes
    override fun getTitleResId(): Int? = null

    open fun initUI() = Unit

    open fun initObservables() = Unit

    @Suppress("DEPRECATION")
    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DMSActivity.initUI()
        this@DMSActivity.initObservables()
    }

    fun requestPermission(vararg permissions: String) {
        this@DMSActivity.permission.requestPermission(this@DMSActivity, permissions.toList())
    }

    fun getAlertHelper(): DMSAlert {
        return this@DMSActivity.alert
    }

    open fun isRequiredUserPressBackButtonTwiceToExit(): Boolean = true

    final override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this@DMSActivity.alert.bind(this@DMSActivity.lifecycle)

        // init view model and bind automatically
        (this@DMSActivity as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            dmsViewModelSource.viewModel.bind(this@DMSActivity.lifecycle)
            dmsViewModelSource.viewModel.setAPICallback(this@DMSActivity)
        }

        // inflate layout resource if available
        this@DMSActivity.getLayoutResId(savedInstanceState)?.let { layoutResId ->
            this@DMSActivity.setContentView(layoutResId)

            this@DMSActivity.getTitleResId()?.let { titleStringResId ->
                this@DMSActivity.setTitle(titleStringResId)
            }

            val rootView = this@DMSActivity.findViewById<ViewGroup>(android.R.id.content).getChildAt(0)
            this@DMSActivity.onContentViewCreated(rootView, savedInstanceState)
        }

        // handle network state changed
        val networkActionFilter = IntentFilter()
        networkActionFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        this@DMSActivity.registerReceiver(this@DMSActivity.networkDetector, networkActionFilter)

        // handle on user press back button
        this@DMSActivity.compositeDisposable.add(
            this@DMSActivity.onBackButtonPressedSubject
                .debounce(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext { this@DMSActivity.alert.toast(R.string.dms_hint_back_to_exit) }
                .timeInterval(TimeUnit.MILLISECONDS)
                .skip(1)
                .filter { it.time() < 2000 }
                .subscribe { super.onBackPressed() }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this@DMSActivity.getMenuResId()?.let { menuResId ->
            this@DMSActivity.menuInflater.inflate(menuResId, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }

    override fun onBackPressed() {
        if (!this@DMSActivity.isRequiredUserPressBackButtonTwiceToExit()) {
            super.onBackPressed()
            return
        }

        this@DMSActivity.onBackButtonPressedSubject.onNext(true)
    }

    override fun onDestroy() {
        this@DMSActivity.unregisterReceiver(this@DMSActivity.networkDetector)
        this@DMSActivity.compositeDisposable.clear()
        super.onDestroy()
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        val taskNameResId = promise.taskNameResId ?: return

        (this@DMSActivity as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            if (dmsViewModelSource.apiAllowBehaviourWithTaskNameResIds().contains(taskNameResId)) {
                this@DMSActivity.alert.progressShow(
                    promise.taskNameResId
                        ?: R.string.dms_loading_default
                )
            }
        }
    }

    override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {

    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {

    }

    override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {
        val taskNameResId = promise.taskNameResId ?: return

        (this@DMSActivity as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            if (dmsViewModelSource.apiAllowBehaviourWithTaskNameResIds().contains(taskNameResId)) {
                val errorMessage = exception.getErrorMessage(this@DMSActivity)
                this@DMSActivity.alert.alert(errorMessage)
            }
        }
    }

    override fun onAPIDidFinishedAll() {
        this@DMSActivity.alert.progressDismiss()
    }

    private val networkDetector = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val cm = (context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)
                ?: return
            (this@DMSActivity as? DMSViewModelSource<*>)?.viewModel?.isNetworkAvailable?.postValue(cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected)
        }
    }
}
