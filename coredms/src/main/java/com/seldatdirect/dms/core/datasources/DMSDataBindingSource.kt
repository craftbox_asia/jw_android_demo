package com.seldatdirect.dms.core.datasources

import androidx.databinding.ViewDataBinding

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSDataBindingSource<T : ViewDataBinding> {

    val dataBinding: T

}
