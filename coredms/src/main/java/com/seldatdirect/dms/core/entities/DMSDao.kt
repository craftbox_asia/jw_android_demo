package com.seldatdirect.dms.core.entities

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSDao<T : DMSModel> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOne(item: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMany(items: List<T>): List<Long>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateOne(item: T): Int

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateMany(items: List<T>)

    @Delete
    fun delete(item: T)

}
