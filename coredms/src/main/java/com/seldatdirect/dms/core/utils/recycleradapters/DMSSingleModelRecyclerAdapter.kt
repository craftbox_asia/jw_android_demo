@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.seldatdirect.dms.core.utils.recycleradapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSSingleModelRecyclerAdapter]
 * RecyclerView adapter for single model
 * This helper support list ViewHolder based on single entity, included filters data
 *
 * @param M
 * @param S: DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<M>
 * @property originItems ArrayList<M>
 * @property actualItems ArrayList<M>
 */
abstract class DMSSingleModelRecyclerAdapter<M, S: DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<M>> : RecyclerView.Adapter<S>() {

    private val originItems: ArrayList<M> = ArrayList()
    protected val actualItems: ArrayList<M> = ArrayList()


    /**
     * Clear all old items (if available) and set new data into [RecyclerView]
     *
     * @param newItems List<M>?
     */
    fun updateItems(newItems: List<M>?) {
        this@DMSSingleModelRecyclerAdapter.originItems.clear()
        this@DMSSingleModelRecyclerAdapter.originItems.addAll(newItems ?: emptyList())
        this@DMSSingleModelRecyclerAdapter.filterItems()
    }

    /**
     * Trigger update new data based on original data by filter
     *
     * @see DMSSingleModelRecyclerAdapter.onRequestFilter
     */
    fun filterItems() {
        val willDisplayingItems = this@DMSSingleModelRecyclerAdapter.onRequestFilter(this@DMSSingleModelRecyclerAdapter.originItems)

        if (willDisplayingItems.isNotEmpty() && this@DMSSingleModelRecyclerAdapter.actualItems.isEmpty()) {
            this@DMSSingleModelRecyclerAdapter.actualItems.clear()
            this@DMSSingleModelRecyclerAdapter.actualItems.addAll(willDisplayingItems)
            this@DMSSingleModelRecyclerAdapter.notifyDataSetChanged()
            return
        }

        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun getOldListSize(): Int = this@DMSSingleModelRecyclerAdapter.actualItems.count()

            override fun getNewListSize(): Int = willDisplayingItems.count()

            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = this@DMSSingleModelRecyclerAdapter.actualItems[oldItemPosition]
                val newItem = willDisplayingItems[newItemPosition]
                return this@DMSSingleModelRecyclerAdapter.onCheckItemId(oldItem, newItem)
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                val oldItem = this@DMSSingleModelRecyclerAdapter.actualItems[oldItemPosition]
                val newItem = willDisplayingItems[newItemPosition]
                return this@DMSSingleModelRecyclerAdapter.onCheckItemContents(oldItem, newItem)
            }
        }, true)

        this@DMSSingleModelRecyclerAdapter.actualItems.clear()
        this@DMSSingleModelRecyclerAdapter.actualItems.addAll(willDisplayingItems)
        diffResult.dispatchUpdatesTo(this@DMSSingleModelRecyclerAdapter)
    }

    /**
     * Get entity by it's position
     *
     * @param position Int
     * @return M?
     */
    fun getItemByPosition(position: Int): M? {
        return this@DMSSingleModelRecyclerAdapter.actualItems.getOrNull(position)
    }

    /**
     * Return filtered data from original items into actual data
     * Override this method and apply your filters
     *
     * @param totalItems List<M>
     * @return List<M>
     */
    open fun onRequestFilter(totalItems: List<M>): List<M> {
        return totalItems
    }

    /**
     * When any request updates new data, to save device's memory,
     * This class just update data which need to changes by primary key.
     * Override this method to check difference between entity by it primary key
     *
     * @param oldItem M
     * @param newItem M
     * @return Boolean
     */
    open fun onCheckItemId(oldItem: M, newItem: M): Boolean {
        return false
    }

    /**
     * When any request updates new data, to save device's memory,
     * This class just update data which need to changes by fields.
     * Override this method to check difference between entity by it's fields
     *
     * @param oldItem M
     * @param newItem M
     * @return Boolean
     */
    open fun onCheckItemContents(oldItem: M, newItem: M): Boolean {
        return false
    }

    final override fun getItemCount(): Int {
        return this@DMSSingleModelRecyclerAdapter.actualItems.count()
    }

    override fun onBindViewHolder(holder: S, position: Int) {
        holder.bind(this@DMSSingleModelRecyclerAdapter.actualItems[position], position)
    }

    abstract class DMSSingleModelViewHolder<M>(parent: ViewGroup, @LayoutRes layoutResId: Int) : RecyclerView.ViewHolder(LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)) {

        /**
         * Override this method to update data when holder inflated from adapter
         *
         * @param item M
         * @param position Int
         */
        open fun bind(item: M, position: Int) = Unit

    }

}
