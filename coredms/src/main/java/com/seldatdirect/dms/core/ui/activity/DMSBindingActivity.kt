package com.seldatdirect.dms.core.ui.activity

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.seldatdirect.dms.core.datasources.DMSDataBindingSource

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSBindingActivity<T : ViewDataBinding>(@LayoutRes layoutResId: Int) : DMSActivity(),
        DMSDataBindingSource<T> {

    override val dataBinding: T by lazy {
        DataBindingUtil.setContentView<T>(this@DMSBindingActivity, layoutResId)
    }

    final override fun getLayoutResId(savedInstanceState: Bundle?): Int? {
        this@DMSBindingActivity.dataBinding.setLifecycleOwner(this@DMSBindingActivity)

        this@DMSBindingActivity.getTitleResId()?.let { titleStringResId ->
            this@DMSBindingActivity.setTitle(titleStringResId)
        }

        this@DMSBindingActivity.onContentViewCreated(this@DMSBindingActivity.dataBinding.root, savedInstanceState)
        return null
    }

}
