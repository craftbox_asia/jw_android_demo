@file:Suppress("unused")

package com.seldatdirect.dms.core.extensions

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import java.lang.Exception

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * display keyboard in active edittext
 *
 * @receiver EditText
 */
fun EditText.showKeyboard() {
    try {
        // this help edittext can be focus on perform click programtically
        this@showKeyboard.isFocusableInTouchMode = true

        this@showKeyboard.requestFocus()
        this@showKeyboard.performClick()

        val keyboardManager = this@showKeyboard.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboardManager.showSoftInput(this@showKeyboard, InputMethodManager.SHOW_IMPLICIT)
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
}


/**
 * hide keyboard if it shown in edittext
 *
 * @receiver EditText
 */
fun EditText.hideKeyboard() {
    try {
        val keyboardManager = this@hideKeyboard.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboardManager.hideSoftInputFromWindow(this@hideKeyboard.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        this@hideKeyboard.clearFocus()
    } catch (exception: Exception) {
        exception.printStackTrace()
    }
}
