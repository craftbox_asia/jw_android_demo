package com.seldatdirect.dms.core.utils.databinding;

import android.view.View;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import timber.log.Timber;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class DrawerLayoutBindingAdapter {

    @BindingAdapter(value = { "drawerLayoutIsOpen", "drawerLayoutDefaultNavigationView" })
    public static void drawerLayoutIsOpen(DrawerLayout drawerLayout, boolean drawerLayoutIsOpen, @IdRes int drawerLayoutDefaultNavigationView) {
        try {
            if (drawerLayoutIsOpen) {
                drawerLayout.openDrawer(drawerLayout.findViewById(drawerLayoutDefaultNavigationView));
            } else {
                drawerLayout.closeDrawer(drawerLayout.findViewById(drawerLayoutDefaultNavigationView));
            }
        } catch (Exception idResNotFoundException) {
            Timber.e(idResNotFoundException);
        }
    }

    @SuppressWarnings("EmptyMethod")
    @BindingAdapter("drawerLayoutOnStateChanged")
    public static void drawerLayoutOnStateChanged(DrawerLayout drawerLayout, @Nullable final DrawerLayoutNavigationStateChangesListener listener) {
        if (drawerLayout == null) {
            return;
        }

        if (listener == null) {
            return;
        }

        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                listener.onStateChanged(true);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                listener.onStateChanged(false);
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
            }
        });
    }


    public interface DrawerLayoutNavigationStateChangesListener {
        void onStateChanged(boolean isShown);
    }

}
