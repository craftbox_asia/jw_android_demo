package com.seldatdirect.dms.core.utils.databinding;

import com.google.android.material.navigation.NavigationView;

import javax.annotation.Nullable;

import androidx.databinding.BindingAdapter;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class NavigationViewBindingAdapter {

    @BindingAdapter("navigationViewOnNavigationItemSelected")
    public static void navigationViewOnNavigationItemSelected(NavigationView navigationView, @Nullable NavigationView.OnNavigationItemSelectedListener listener) {
        if (navigationView == null) {
            return;
        }

        navigationView.setNavigationItemSelectedListener(listener);
    }

}
