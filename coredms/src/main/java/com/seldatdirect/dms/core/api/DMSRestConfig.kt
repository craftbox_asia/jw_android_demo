@file:Suppress("unused")

package com.seldatdirect.dms.core.api

import com.facebook.stetho.rhino.JsRuntimeReplFactoryBuilder
import com.seldatdirect.dms.core.BuildConfig
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.contribs.parsers.MessageStringArrayDMSExceptionParser
import com.seldatdirect.dms.core.api.contribs.parsers.MessageStringDMSExceptionParser
import com.seldatdirect.dms.core.api.contribs.parsers.StringArrayDMSExceptionParser
import com.seldatdirect.dms.core.api.contribs.parsers.YiiDefaultDMSRestExceptionParser
import org.mozilla.javascript.BaseFunction

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


data class DMSRestConfig(
        var baseURL: String = "",
        var authorizationToken: String? = null,
        var authorizationTokenPrefix: String = "Bearer",
        var additionalHeaders: Map<String, String> = emptyMap(),
        var exceptionParsers: List<DMSRestExceptionParser> = listOf(
                YiiDefaultDMSRestExceptionParser(),
                MessageStringDMSExceptionParser(),
                MessageStringArrayDMSExceptionParser(),
                StringArrayDMSExceptionParser()
        ),
        var enabledDebug: Boolean = BuildConfig.DEBUG,
        var jsRuntimeDebuggerFunctions: Map<String, BaseFunction> = emptyMap()
) {

    /**
     * set default base url for api requests
     *
     * @param baseURL String
     * @return DMSRestConfig
     */
    fun updateAPIBaseURL(baseURL: String): DMSRestConfig {
        this@DMSRestConfig.baseURL = baseURL
        return this@DMSRestConfig
    }

    /**
     * add Authorization header for each request
     *
     * @see DMSRestConfig.updateAuthorizationTokenPrefix to set token's prefix (default: Bearer)
     *
     * @param authorizationToken String?
     * @return DMSRestConfig
     */
    fun updateAuthorizationToken(authorizationToken: String?): DMSRestConfig {
        this@DMSRestConfig.authorizationToken = authorizationToken
        return this@DMSRestConfig
    }

    /**
     * set token's prefix for Authorization header
     *
     * @param authorizationTokenPrefix String
     * @return DMSRestConfig
     */
    fun updateAuthorizationTokenPrefix(authorizationTokenPrefix: String = "Bearer"): DMSRestConfig {
        this@DMSRestConfig.authorizationTokenPrefix = authorizationTokenPrefix
        return this@DMSRestConfig
    }

    /**
     * add additional headers for each requests
     *
     * @param additionalHeaders Map<String, String>
     * @return DMSRestConfig
     */
    fun updateAdditionalHeaders(additionalHeaders: Map<String, String>): DMSRestConfig {
        this@DMSRestConfig.additionalHeaders = additionalHeaders
        return this@DMSRestConfig
    }

    /**
     * setup error body parsers
     * when error response from server ([DMSRestException.ExceptionType.Server]), the exception will
     * includes [DMSRestException.httpBody], the parsers will parse this body into object that readble
     * by user.
     *
     * References:
     * @see DMSRestException.getErrorMessage this will negotiate parsers to parsing response into error message then
     * display to user interface
     *
     * @param exceptionParsers List<DMSRestExceptionParser>
     * @return DMSRestConfig
     */
    fun updateExceptionParsers(exceptionParsers: List<DMSRestExceptionParser>): DMSRestConfig {
        this@DMSRestConfig.exceptionParsers = exceptionParsers
        return this@DMSRestConfig
    }

    /**
     * enable logigng each requests,responses into logcat
     *
     * @param enabledDebug Boolean
     * @return DMSRestConfig
     */
    fun updateEnabledDebug(enabledDebug: Boolean): DMSRestConfig {
        this@DMSRestConfig.enabledDebug = enabledDebug
        return this@DMSRestConfig
    }

    /**
     * allow debugging app via mozilla or chrome
     *
     * @param functions Map<String, BaseFunction>
     * @return DMSRestConfig
     */
    fun updateCustomJSDebuggerFunctions(functions: Map<String, BaseFunction> = this@DMSRestConfig.jsRuntimeDebuggerFunctions): DMSRestConfig {
        this@DMSRestConfig.jsRuntimeDebuggerFunctions = functions
        return this@DMSRestConfig
    }

}
