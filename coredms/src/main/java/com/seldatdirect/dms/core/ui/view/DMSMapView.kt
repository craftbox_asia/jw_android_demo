@file:Suppress("MissingPermission", "unused")

package com.seldatdirect.dms.core.ui.view

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.os.Build
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import androidx.room.Ignore
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.gson.annotations.SerializedName
import com.google.maps.DirectionsApi
import com.google.maps.DirectionsApiRequest
import com.google.maps.GeoApiContext
import com.google.maps.android.ui.IconGenerator
import com.google.maps.model.TravelMode
import com.kaopiz.kprogresshud.KProgressHUD
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.behaviours.DMSLifecycleBehaviour
import com.seldatdirect.dms.core.entities.DMSAnnotations
import com.seldatdirect.dms.core.entities.DMSJsonableObject
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.joda.time.DateTime
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


class DMSMapView : FrameLayout, DMSLifecycleBehaviour {

    constructor(context: Context) : super(context) {
        this@DMSMapView.initial(this@DMSMapView.context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        this@DMSMapView.initial(this@DMSMapView.context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this@DMSMapView.initial(this@DMSMapView.context, attrs)
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        this@DMSMapView.initial(this@DMSMapView.context, attrs)
    }


    open class DMSMapMarkerItem(val location: LatLng) : DMSJsonableObject {

        @SerializedName("title")
        var title: String? = null

        @DMSAnnotations.DMSJsonableExcludeField
        @Ignore
        var isUserLocation: Boolean = false

        @DMSAnnotations.DMSJsonableExcludeField
        @Ignore
        internal var mapViewMarker: Marker? = null


        fun withTitle(title: String?): DMSMapMarkerItem {
            this@DMSMapMarkerItem.title = title
            return this@DMSMapMarkerItem
        }

        fun copy(): DMSMapMarkerItem {
            return try {
                DMSRest.getDefaultGsonParser().fromJson(this@DMSMapMarkerItem.toJSON(), DMSMapMarkerItem::class.java)
            } catch (exception: java.lang.Exception) {
                exception.printStackTrace()
                this@DMSMapMarkerItem
            }
        }
    }

    open class DMSMapDirectionItem(var from: DMSMapMarkerItem, var to: DMSMapMarkerItem) : DMSJsonableObject {
        var waypoints = listOf<LatLng>()

        @DMSAnnotations.DMSJsonableExcludeField
        @Ignore
        internal var mapViewPolyline: Polyline? = null

        fun copy(): DMSMapDirectionItem {
            return try {
                DMSRest.getDefaultGsonParser().fromJson(
                        this@DMSMapDirectionItem.toJSON(),
                        DMSMapDirectionItem::class.java
                )
            } catch (exception: java.lang.Exception) {
                exception.printStackTrace()
                this@DMSMapDirectionItem
            }
        }
    }

    interface OnGotMapDirection {
        fun onGotDirections(mapView: DMSMapView, directions: List<DMSMapDirectionItem>)
        fun onGotException(mapView: DMSMapView, exception: Throwable)
    }


    override var lifecycle: Lifecycle? = null
    private var onDirectionListener: OnGotMapDirection? = null

    private val disposables: CompositeDisposable = CompositeDisposable()
    private val fetchDirectionTask: PublishSubject<List<DMSMapDirectionItem>> = PublishSubject.create()
    private val drawDirectionTask: PublishSubject<List<DMSMapDirectionItem>> = PublishSubject.create()

    private lateinit var mapView: MapView
    private lateinit var googleMap: GoogleMap
    private var loadingView: KProgressHUD? = null

    private var isMapLoaded: Boolean = false
    private var isStartFromUserLocation = false
    private var mapViewBoundPadding: Int? = null
    private var mapViewPolylineColor: Int? = null
    private var mapViewPolylineWidth: Int? = null
    private var mapViewZoom: Float = 12.0f

    private val directions: ArrayList<DMSMapDirectionItem> = ArrayList()


    fun setOnDirectionListener(listener: OnGotMapDirection?) {
        this@DMSMapView.onDirectionListener = listener
    }

    /**
     * Draw markers and directions between
     *
     * @param directions List<DMSMapDirectionItem>
     */
    fun setDirections(directions: List<DMSMapDirectionItem>, isStartFromUserLocation: Boolean = this@DMSMapView.isStartFromUserLocation) {
        this@DMSMapView.loadingView?.dismiss()
        this@DMSMapView.loadingView = KProgressHUD.create(this@DMSMapView.context, KProgressHUD.Style.SPIN_INDETERMINATE)
        this@DMSMapView.loadingView?.setCancellable(false)
        this@DMSMapView.loadingView?.setLabel(this@DMSMapView.context.getString(R.string.dms_loading_get_directions))
        this@DMSMapView.loadingView?.show()

        this@DMSMapView.isStartFromUserLocation = isStartFromUserLocation
        val userLatLng = this@DMSMapView.getCurrentUserLocation()

        // this will draw direction from user to secondth marker in directions list
        if (this@DMSMapView.isStartFromUserLocation && userLatLng != null && directions.isNotEmpty()) {
            val directionsIncludedUser = ArrayList<DMSMapDirectionItem>()

            val userLocation = DMSMapMarkerItem(userLatLng)
            directions[0].from = userLocation
            directions[0].from.isUserLocation = true

            directionsIncludedUser.addAll(directions)
            this@DMSMapView.fetchDirectionTask.onNext(directionsIncludedUser)
            return
        }

        this@DMSMapView.fetchDirectionTask.onNext(directions)
    }

    /**
     * create empty direction from locations then fetch directions automatically
     *
     * @param markers List<DMSMapMarkerItem>
     */
    fun createDirectionsWithMapMarkers(markers: List<DMSMapMarkerItem>, isStartFromUserLocation: Boolean = this@DMSMapView.isStartFromUserLocation) {
        val directions = ArrayList<DMSMapDirectionItem>()

        markers.forEachIndexed { markerIndex, markerItem ->
            if (markerIndex < markers.count() - 1) {
                val nextMarker = markers[markerIndex + 1]
                nextMarker.mapViewMarker = null
                directions.add(DMSMapDirectionItem(
                        from = markerItem,
                        to = nextMarker
                ))
            }
        }

        this@DMSMapView.setDirections(directions, isStartFromUserLocation)
    }

    private fun initial(context: Context, attrs: AttributeSet?) {
        attrs?.let(this@DMSMapView::parseAttributes)

        this@DMSMapView.mapView = MapView(context, attrs)
        this@DMSMapView.mapView.alpha = 0.0f

        this@DMSMapView.addView(this@DMSMapView.mapView, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
        this@DMSMapView.mapView.getMapAsync(this@DMSMapView.onMapReadyCallback)
    }

    private fun parseAttributes(attributeSet: AttributeSet) {
        val typedArray = this@DMSMapView.context.obtainStyledAttributes(attributeSet, R.styleable.DMSMapView, 0, 0)
        this@DMSMapView.mapViewZoom = typedArray.getDimension(R.styleable.DMSMapView_dms_mapView_zoom, this@DMSMapView.mapViewZoom)
        this@DMSMapView.mapViewPolylineWidth = typedArray.getDimensionPixelSize(R.styleable.DMSMapView_dms_mapView_polylineWidth, -1)
        this@DMSMapView.mapViewPolylineColor = typedArray.getColor(R.styleable.DMSMapView_dms_mapView_polylineColor, -1)
        this@DMSMapView.mapViewBoundPadding = typedArray.getDimensionPixelSize(R.styleable.DMSMapView_dms_mapView_boundPadding, -1)

        if (this@DMSMapView.mapViewPolylineWidth == -1) {
            this@DMSMapView.mapViewPolylineWidth = null
        }

        if (this@DMSMapView.mapViewPolylineColor == -1) {
            this@DMSMapView.mapViewPolylineColor = null
        }

        if (this@DMSMapView.mapViewBoundPadding == -1) {
            this@DMSMapView.mapViewBoundPadding = null
        }

        typedArray.recycle()
    }

    private val onMapReadyCallback = OnMapReadyCallback {
        this@DMSMapView.googleMap = it

        this@DMSMapView.getCurrentUserLocation()?.let { focusLocation ->
            this@DMSMapView.googleMap.moveCamera(CameraUpdateFactory
                    .newLatLngZoom(focusLocation, this@DMSMapView.mapViewZoom))
        }

        this@DMSMapView.mapView.clearAnimation()
        this@DMSMapView.mapView.animate()
                .alpha(1.0f)
                .setDuration(300)
                .start()

        if (this@DMSMapView.isLocationPermissionAllowed()) {
            this@DMSMapView.googleMap.uiSettings.isMyLocationButtonEnabled = true
            this@DMSMapView.googleMap.isMyLocationEnabled = true
        }

        this@DMSMapView.googleMap.setOnMapLoadedCallback(this@DMSMapView.onMapLoadedCallback)
    }

    private val onMapLoadedCallback = GoogleMap.OnMapLoadedCallback {
        this@DMSMapView.isMapLoaded = true
    }


    private fun getCurrentUserLocation(): LatLng? {
        val locationManager = this@DMSMapView.getLocationManager() ?: return null

        val location = locationManager.allProviders
                .mapNotNull {
                    if (!this@DMSMapView.isLocationPermissionAllowed()) {
                        return null
                    }
                    locationManager.getLastKnownLocation(it)
                }
                .firstOrNull()
                ?: return null

        return LatLng(location.latitude, location.longitude)
    }

    private fun getLocationManager(): LocationManager? {
        if (!this@DMSMapView.isLocationPermissionAllowed()) {
            return null
        }

        return this@DMSMapView.context.applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }

    private fun isLocationPermissionAllowed(): Boolean {
        return arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION)
                .all { ContextCompat.checkSelfPermission(this@DMSMapView.context, it) == PackageManager.PERMISSION_GRANTED }
    }

    private fun getMarkerOptions(iconText: String? = null): MarkerOptions {
        val instance = MarkerOptions()

        if (iconText.isNullOrBlank()) {
            return instance
        }

        val markerIcon = IconGenerator(this@DMSMapView.context)
        markerIcon.setColor(Color.WHITE)
        markerIcon.setTextAppearance(this@DMSMapView.context, R.style.TextAppearance_AppCompat_Body2)

        return instance
                .icon(BitmapDescriptorFactory.fromBitmap(markerIcon.makeIcon(iconText)))
    }

    private fun getDirectionRequest(from: LatLng, to: LatLng, mode: TravelMode = TravelMode.DRIVING): DirectionsApiRequest {
        val request = DirectionsApi.newRequest(GeoApiContext()
                .setQueryRateLimit(3)
                .setApiKey(this@DMSMapView.context.getString(R.string.dms_google_map_api)))
        request.mode(mode)
        request.departureTime(DateTime())
        request.origin(com.google.maps.model.LatLng(from.latitude, from.longitude))
        request.destination(com.google.maps.model.LatLng(to.latitude, to.longitude))
        return request
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@DMSMapView.mapView.onCreate(null)

        this@DMSMapView.disposables.add(this@DMSMapView.fetchDirectionTask.debounce(300, TimeUnit.MILLISECONDS, Schedulers.io()).subscribe { directionsList ->
            val waypointSplited = directionsList.chunked(23)
            val drawedDirections = ArrayList<DMSMapDirectionItem>()

            waypointSplited.forEach {
                if (it.count() < 2)
                    return@forEach

                try {

                    val request = DirectionsApi.newRequest(GeoApiContext()
                            .setQueryRateLimit(3)
                            .setApiKey(this@DMSMapView.context.getString(R.string.dms_google_map_api)))
                    request.mode(TravelMode.DRIVING)
                    request.departureTime(DateTime())
                    request.origin(com.google.maps.model.LatLng(it.first().from.location.latitude, it.first().from.location.longitude))
                    request.destination(com.google.maps.model.LatLng(it.last().to.location.latitude, it.last().to.location.longitude))
                    request.optimizeWaypoints(false)

                    val wayPoints = ArrayList<com.google.maps.model.LatLng>()

                    it.forEachIndexed { indexed, item ->
                        when (indexed) {
                            0 -> {
                                wayPoints.add(com.google.maps.model.LatLng(item.to.location.latitude, item.to.location.longitude))
                            }
                            (it.count() - 1) -> {
                                wayPoints.add(com.google.maps.model.LatLng(item.from.location.latitude, item.from.location.longitude))
                            }
                            else -> {
                                wayPoints.add(com.google.maps.model.LatLng(item.from.location.latitude, item.from.location.longitude))
                                wayPoints.add(com.google.maps.model.LatLng(item.to.location.latitude, item.to.location.longitude))
                            }
                        }
                    }


                    request.waypoints(*(wayPoints.toTypedArray()))

                    val locationResponse = request.await()
                    val waypointsList = locationResponse.routes.firstOrNull()?.overviewPolyline?.decodePath()
                            ?: emptyList()

                    it.first().waypoints = waypointsList.map {
                        @Suppress("NestedLambdaShadowedImplicitParameter")
                        LatLng(it.lat, it.lng)
                    }

                    drawedDirections.addAll(it)
                } catch (exception: Exception) {
                    this@DMSMapView.onDirectionListener?.onGotException(this@DMSMapView, exception)
                } finally {

                    // only draw directions when all fetched from API (included failed items)
                    if (drawedDirections.count() == directionsList.count()) {
                        this@DMSMapView.onDirectionListener?.onGotDirections(this@DMSMapView, drawedDirections.filter { !it.from.isUserLocation })
                        this@DMSMapView.drawDirectionTask.onNext(drawedDirections)
                    }
                }
            }

            /*
            directions.forEachIndexed { index, directionItem ->
                try {

                    val request = DirectionsApi.newRequest(GeoApiContext()
                            .setQueryRateLimit(3)
                            .setApiKey(this@DMSMapView.context.getString(R.string.dms_google_map_api)))
                    request.mode(TravelMode.DRIVING)
                    request.departureTime(DateTime())
                    request.origin(com.google.maps.model.LatLng(directionItem.from.location.latitude, directionItem.from.location.longitude))
                    request.destination(com.google.maps.model.LatLng(directionItem.to.location.latitude, directionItem.to.location.longitude))

                    val locationResponse = request.await()

                    val waypointsList = locationResponse.routes.firstOrNull()?.overviewPolyline?.decodePath()
                            ?: emptyList()

                    directionItem.waypoints = waypointsList.map { LatLng(it.lat, it.lng) }
                    directions[index] = directionItem
                } catch (exception: Exception) {
                    this@DMSMapView.onDirectionListener?.onGotException(this@DMSMapView, exception)
                } finally {
                    fetchedItems += 1

                    // only draw directions when all fetched from API (included failed items)
                    if (fetchedItems == directionsList.count()) {
                        this@DMSMapView.onDirectionListener?.onGotDirections(this@DMSMapView, directions.filter { !it.from.isUserLocation })
                        this@DMSMapView.drawDirectionTask.onNext(directions)
                    }
                }
            }
            */
        })

        this@DMSMapView.disposables.add(this@DMSMapView.drawDirectionTask.debounce(300, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe { directionItems ->
            this@DMSMapView.loadingView?.dismiss()
            this@DMSMapView.loadingView = null

            // clean up all makers, polylines
            this@DMSMapView.directions.forEach { directionItem ->
                directionItem.mapViewPolyline?.remove()
                directionItem.mapViewPolyline = null

                directionItem.from.mapViewMarker?.remove()
                directionItem.from.mapViewMarker = null

                directionItem.to.mapViewMarker?.remove()
                directionItem.to.mapViewMarker = null
            }
            this@DMSMapView.directions.clear()

            val latLngBounds = LatLngBounds.builder()

            directionItems.forEachIndexed { directionIndex, directionItem ->
                val polylineOptions = PolylineOptions()
                        .width(this@DMSMapView.mapViewPolylineWidth?.toFloat()
                                ?: this@DMSMapView.context.resources.getDimension(R.dimen.dms_spacing_mini))
                        .color(this@DMSMapView.mapViewPolylineColor
                                ?: this@DMSMapView.getRandomColor())


                directionItem.waypoints.forEach { polylineOptions.add(it); latLngBounds.include(it) }

                directionItem.mapViewPolyline = this@DMSMapView.googleMap.addPolyline(polylineOptions)

                if (directionIndex == 0) {
                    directionItem.from.mapViewMarker = this@DMSMapView.googleMap.addMarker(
                            // only draw index label from secondary marker and above
                            this@DMSMapView.getMarkerOptions(null)
                                    .position(directionItem.from.location)
                                    .title(directionItem.from.title))
                    latLngBounds.include(directionItem.from.location)
                }

                directionItem.to.mapViewMarker = this@DMSMapView.googleMap.addMarker(
                        // only draw index label from secondary marker and above
                        this@DMSMapView.getMarkerOptions("${directionIndex + 1}")
                                .position(directionItem.to.location)
                                .title(directionItem.to.title))

                latLngBounds.include(directionItem.to.location)
                this@DMSMapView.directions.add(directionItem)
            }

            try {
                this@DMSMapView.googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                        latLngBounds.build(),
                        this@DMSMapView.mapViewBoundPadding
                                ?: this@DMSMapView.context.resources.getDimensionPixelSize(R.dimen.dms_textsize_xxxlarge)
                ))
            } catch (_: IllegalStateException) {
                Timber.e("Skip bounding map because not included any points!")
            }
        })
    }

    private fun getRandomColor(): Int {
        val colorResIds = this@DMSMapView.context.resources.getIntArray(R.array.dms_color_palletes)
        return colorResIds.getOrNull(Random().nextInt(colorResIds.count()))
                ?: Color.parseColor("#ff009688")
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun lifecycleOnStart() {
        super.lifecycleOnStart()
        this@DMSMapView.mapView.onStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun lifecycleOnResume() {
        super.lifecycleOnResume()
        this@DMSMapView.mapView.onResume()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun lifecycleOnPause() {
        this@DMSMapView.mapView.onPause()
        super.lifecycleOnPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun lifecycleOnStop() {
        this@DMSMapView.mapView.onStop()
        super.lifecycleOnStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun lifecycleOnDestroy() {
        this@DMSMapView.loadingView?.dismiss()
        this@DMSMapView.loadingView = null

        this@DMSMapView.mapView.onDestroy()
        super.lifecycleOnDestroy()
    }

}
