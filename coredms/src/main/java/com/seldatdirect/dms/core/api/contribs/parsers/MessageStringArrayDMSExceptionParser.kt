package com.seldatdirect.dms.core.api.contribs.parsers

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.DMSRestExceptionParser
import timber.log.Timber
import java.io.Serializable

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [MessageStringArrayDMSExceptionParser]
 *
 * This will parsing response body into data with format like
 * {
 *      "message": ["message",...]
 * }
 */
class MessageStringArrayDMSExceptionParser : DMSRestExceptionParser {

    override fun getErrorMessage(responseBody: String): String? {
        return try {
            val message = Gson().fromJson(responseBody, Data::class.java).message
            return message.firstOrNull()
        } catch (exception: Exception) {
            null
        }
    }


    class Data : Serializable {

        @SerializedName("message")
        var message: List<String> = emptyList()

    }

}
