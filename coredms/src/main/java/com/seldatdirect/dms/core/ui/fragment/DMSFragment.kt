@file:Suppress("MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.ui.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.behaviours.DMSControllerBehaviour
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.delegates.DMSControllerDelegate
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.alert.DMSAlert

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSFragment : Fragment(), DMSControllerBehaviour, DMSControllerDelegate, DMSViewModel.Listener {

    @LayoutRes
    override fun getLayoutResId(savedInstanceState: Bundle?): Int? = null

    @MenuRes
    override fun getMenuResId(): Int? = null

    @StringRes
    override fun getTitleResId(): Int? = null

    open fun initUI() = Unit

    open fun initObservables() = Unit

    @Suppress("DEPRECATION")
    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DMSFragment.initUI()
        this@DMSFragment.initObservables()
    }

    open fun isBackable(): Boolean = true

    val alert: DMSAlert by lazy {
        DMSAlert(this@DMSFragment.requireActivity())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // init view model and bind automatically
        (this@DMSFragment as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            dmsViewModelSource.viewModel.bind(this@DMSFragment.lifecycle)
            dmsViewModelSource.viewModel.setAPICallback(this@DMSFragment)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this@DMSFragment.getLayoutResId(savedInstanceState)?.let { layoutResId ->
            return inflater.inflate(layoutResId, container, false)
        }
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this@DMSFragment.onContentViewCreated(view, savedInstanceState)
        this@DMSFragment.alert.bind(this@DMSFragment.lifecycle)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val title = this@DMSFragment.getTitleResId()
        if (title != null)
            this@DMSFragment.activity?.setTitle(title)
        else
            this@DMSFragment.activity?.title = null
        this@DMSFragment.setHasOptionsMenu(this@DMSFragment.getMenuResId() != null)
        this@DMSFragment.activity?.invalidateOptionsMenu()

        // handle network state changed
        val networkActionFilter = IntentFilter()
        networkActionFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        try {
            this@DMSFragment.activity?.registerReceiver(this@DMSFragment.networkDetector, networkActionFilter)
        } catch (_: Exception) {
        }
    }

    override fun onDestroy() {
        try {
            this@DMSFragment.activity?.unregisterReceiver(this@DMSFragment.networkDetector)
        } catch (_: Exception) {
        }
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        this@DMSFragment.getMenuResId()?.let { unwrappedMenuResId ->
            inflater?.inflate(unwrappedMenuResId, menu)
        }
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        val taskNameResId = promise.taskNameResId ?: return

        (this@DMSFragment as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            if (dmsViewModelSource.apiAllowBehaviourWithTaskNameResIds().contains(taskNameResId)) {
                this@DMSFragment.alert.progressShow(
                    promise.taskNameResId
                        ?: R.string.dms_loading_default
                )
            }
        }
    }

    override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {

    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {

    }

    override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {
        val taskNameResId = promise.taskNameResId ?: return

        (this@DMSFragment as? DMSViewModelSource<*>)?.let { dmsViewModelSource ->
            if (dmsViewModelSource.apiAllowBehaviourWithTaskNameResIds().contains(taskNameResId)) {
                this@DMSFragment.context?.let { unwrappedContext ->
                    val errorMessage = exception.getErrorMessage(unwrappedContext)
                    this@DMSFragment.alert.toast(errorMessage)
                }
            }
        }
    }

    override fun onAPIDidFinishedAll() {
        this@DMSFragment.alert.progressDismiss()
    }

    private val networkDetector = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val cm = (context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager)
                ?: return
            (this@DMSFragment as? DMSViewModelSource<*>)?.viewModel?.isNetworkAvailable?.postValue(cm.activeNetworkInfo != null && cm.activeNetworkInfo.isConnected)
        }
    }


    companion object {

        inline fun <reified T : DMSFragment> create(fragmentClass: Class<T>, bundle: Bundle? = null): T {
            val instance = fragmentClass.newInstance()
            instance.arguments = bundle ?: Bundle()
            return instance
        }

    }
}
