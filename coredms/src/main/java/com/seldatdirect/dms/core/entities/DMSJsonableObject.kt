package com.seldatdirect.dms.core.entities

import com.google.gson.Gson
import com.seldatdirect.dms.core.api.DMSRest
import timber.log.Timber
import java.io.Serializable

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSJsonableObject : Serializable {

    fun toJSON(): String {
        return try {
            DMSRest.getDefaultGsonParser()
                    .toJson(this@DMSJsonableObject)
        } catch (exception: Exception) {
            Timber.e(exception)
            "{}"
        }
    }


    companion object {

        inline fun <reified T : DMSJsonableObject> create(clazz: Class<T>, jsonString: String?): T {
            return try {
                Gson().fromJson(jsonString ?: "{}", clazz)
            } catch (exception: Exception) {
                exception.printStackTrace()
                clazz.newInstance()
            }
        }

    }

}
