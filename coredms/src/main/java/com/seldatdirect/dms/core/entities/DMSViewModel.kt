package com.seldatdirect.dms.core.entities

import android.annotation.SuppressLint
import android.os.Looper
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.behaviours.DMSLifecycleBehaviour
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressLint("CheckResult")
abstract class DMSViewModel : ViewModel(), DMSLifecycleBehaviour {

    interface Listener {
        fun onAPIWillRequest(promise: DMSRestPromise<*, *>)
        fun onAPIDidFinished(promise: DMSRestPromise<*, *>)
        fun onAPIDidFinishedAll()
        fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?)
        fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException)
    }


    val isNetworkBusy: MutableLiveData<Boolean>         = MutableLiveData()
        get() {
            if (field.value == null)
                field.postValue(false)
            return field
        }

    val isNetworkAvailable: MutableLiveData<Boolean>    = MutableLiveData()
        get() {
            if (field.value == null)
                field.postValue(true)
            return field
        }


    fun <M, O : Observable<M>> request(apiServiceMethod: O): DMSRestPromise<M, O> {
        val instance = DMSRestPromise(apiServiceMethod)
        instance.doBeforeRequest { promise ->
            this@DMSViewModel.requestQueues.add(promise)
            this@DMSViewModel.apiCallbacks.forEach { it.onAPIWillRequest(promise) }
        }
        instance.then { promise, responseData -> this@DMSViewModel.apiCallbacks.forEach { it.onAPIResponseSuccess(promise, responseData) } }
        instance.catch { promise, exception -> this@DMSViewModel.apiCallbacks.forEach {
            it.onAPIResponseError(promise, exception) }
        }
        instance.doAfterResponse { promise ->
            this@DMSViewModel.requestQueues.removeAll { it.identifier == promise.identifier }
            this@DMSViewModel.apiCallbacks.forEach {
                it.onAPIDidFinished(promise)
                if (this@DMSViewModel.requestQueues.isEmpty()) {
                    it.onAPIDidFinishedAll()
                }
            }
        }

        Timber.d("Generated DMSRestPromise: $instance")
        return instance
    }

    fun setAPICallback(apiCallback: Listener) {
        this@DMSViewModel.apiCallbacks.add(apiCallback)
    }

    /**
     * Notify to controller that viewmodle response exception manually
     *
     * @param promise DMSRestPromise<*, *>
     * @param exceptionDetails String?
     */
    fun postCustomException(promise: DMSRestPromise<*, *>, exceptionDetails: String?) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            Observable.just(exceptionDetails)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { customMessage ->
                        this@DMSViewModel.apiCallbacks.forEach { listener ->
                            listener.onAPIResponseError(
                                    promise,
                                    DMSRestException.customMessage(customMessage)
                            )
                        }
                    }
            return
        }
        this@DMSViewModel.apiCallbacks.forEach { listener ->
            listener.onAPIResponseError(
                    promise,
                    DMSRestException.customMessage(exceptionDetails)
            )
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@DMSViewModel.requestQueues.addOnListChangedCallback(this@DMSViewModel.onRequestQueuesChanged)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun lifecycleOnStart() {
        super.lifecycleOnStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun lifecycleOnResume() {
        super.lifecycleOnResume()
        this@DMSViewModel.apiCallbacks.forEach { apiCallback ->
            if (this@DMSViewModel.requestQueues.isEmpty()) {
                apiCallback.onAPIDidFinishedAll()
                return@forEach
            }

            this@DMSViewModel.requestQueues.lastOrNull()?.let { promise ->
                apiCallback.onAPIWillRequest(promise)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun lifecycleOnPause() {
        super.lifecycleOnPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun lifecycleOnStop() {
        super.lifecycleOnStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun lifecycleOnDestroy() {
        this@DMSViewModel.apiCallbacks.clear()
        this@DMSViewModel.requestQueues.removeOnListChangedCallback(this@DMSViewModel.onRequestQueuesChanged)
        this@DMSViewModel.compositeDisposable.clear()
        super.lifecycleOnDestroy()
    }

    private val onRequestQueuesChanged = object : ObservableList.OnListChangedCallback<ObservableArrayList<*>>() {
        override fun onItemRangeMoved(sender: ObservableArrayList<*>?, fromPosition: Int, toPosition: Int, itemCount: Int) { }
        override fun onItemRangeChanged(sender: ObservableArrayList<*>?, positionStart: Int, itemCount: Int) { }
        override fun onChanged(sender: ObservableArrayList<*>?) { }
        override fun onItemRangeRemoved(sender: ObservableArrayList<*>?, positionStart: Int, itemCount: Int) {
            this@DMSViewModel.isNetworkBusy.postValue(this@DMSViewModel.requestQueues.isNotEmpty())
        }
        override fun onItemRangeInserted(sender: ObservableArrayList<*>?, positionStart: Int, itemCount: Int) {
            this@DMSViewModel.isNetworkBusy.postValue(this@DMSViewModel.requestQueues.isNotEmpty())
        }
    }

    override var lifecycle: Lifecycle?              = null
    private var apiCallbacks: ArrayList<Listener>   = ArrayList()
    protected val compositeDisposable               = CompositeDisposable()
    private val requestQueues: ObservableArrayList<DMSRestPromise<*, *>> = ObservableArrayList()

}
