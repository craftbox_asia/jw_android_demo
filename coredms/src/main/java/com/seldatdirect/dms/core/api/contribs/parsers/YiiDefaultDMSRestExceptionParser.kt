@file:Suppress("unused")

package com.seldatdirect.dms.core.api.contribs.parsers

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.DMSRestExceptionParser
import timber.log.Timber
import java.io.Serializable
import java.lang.Exception

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [YiiDefaultDMSRestExceptionParser]
 *
 * This will parsing response body into data with format like
 * {
 *      "name": "name",
 *      "message": "message",
 *      "code": 0,
 *      "type": "type"
 * }
 */
class YiiDefaultDMSRestExceptionParser : DMSRestExceptionParser {

    override fun getErrorMessage(responseBody: String): String? {
        return try {
            val message = Gson().fromJson(responseBody, Data::class.java).message
            if (message.isBlank()) return null
            return message
        } catch (exception: Exception) {
            null
        }
    }


    class Data : Serializable {

        @SerializedName("name")
        var name: String = ""

        @SerializedName("message")
        var message: String = ""

        @SerializedName("code")
        var code: Int = 0

        @SerializedName("status")
        var status: Int = -1

        @SerializedName("type")
        var type: String = ""

    }

}
