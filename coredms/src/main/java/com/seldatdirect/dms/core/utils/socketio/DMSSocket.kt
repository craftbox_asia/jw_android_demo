@file:Suppress("MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.utils.socketio

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.seldatdirect.dms.core.behaviours.DMSLifecycleBehaviour
import io.socket.client.IO
import io.socket.client.Socket
import org.json.JSONObject
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSSocket]
 * Handle socket.io using in app
 */
class DMSSocket : DMSLifecycleBehaviour {

    /**
     * Register callbacks
     *
     * @param listener DMSSocketListener?
     */
    fun setSocketListener(listener: DMSSocketListener?) {
        this@DMSSocket.listener = listener
    }

    /**
     * Check socket is connected into server
     *
     * @return Boolean
     */
    fun isConnected(): Boolean {
        return this@DMSSocket.mSocket.connected()
    }

    /**
     * Connect socket into server when needed
     *
     * @param usersEventsList List<String> list of custom events need to
     *  handle via [DMSSocketListener.onSocketGotPayload]
     */
    fun connect(usersEventsList: List<String> = this@DMSSocket.usersEventsList) {

        if (this@DMSSocket.isConnected()) {
            return
        }

        this@DMSSocket.usersEventsList = usersEventsList
        this@DMSSocket.mSocket.off()

        this@DMSSocket.mSocket.on(Socket.EVENT_CONNECT) { _ ->
            this@DMSSocket.listener?.onSocketConnection(this@DMSSocket, true)
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_DISCONNECT) { _ ->
            this@DMSSocket.listener?.onSocketConnection(this@DMSSocket, false)
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_CONNECT_ERROR) { payload ->
            this@DMSSocket.listener?.onSocketConnectedFailed(this@DMSSocket, (payload?.firstOrNull() as? Exception) ?: Exception())
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_RECONNECT_ERROR) { payload ->
            this@DMSSocket.listener?.onSocketConnectedFailed(this@DMSSocket, (payload?.firstOrNull() as? Exception) ?: Exception())
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_CONNECT_TIMEOUT) { payload ->
            this@DMSSocket.listener?.onSocketConnectedFailed(this@DMSSocket, (payload?.firstOrNull() as? Exception) ?: Exception())
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_ERROR) { payload ->
            this@DMSSocket.listener?.onSocketException(this@DMSSocket, (payload?.firstOrNull() as? Exception) ?: Exception())
        }

        this@DMSSocket.mSocket.on(Socket.EVENT_RECONNECTING) { _ ->
            this@DMSSocket.listener?.onSocketReConnecting(this@DMSSocket)
        }

        this@DMSSocket.usersEventsList.forEach { userEventName ->
            if (userEventName.isNotBlank()) {
                this@DMSSocket.mSocket.on(userEventName) { payload ->
                    this@DMSSocket.listener
                            ?.onSocketGotPayload(this@DMSSocket, userEventName, payload)
                }
            }
        }

        this@DMSSocket.mSocket.connect()
    }

    /**
     * emit data into socket when needed (socket connected)
     * this method is not recommented, use @see [sendJSONObject] instead
     *
     * @param eventName String
     * @param payloads Array<out Any>
     */
    fun send(eventName: String, vararg payloads: Any) {
        if (!this@DMSSocket.isConnected()) {
            return
        }

        this@DMSSocket.mSocket.emit(eventName, payloads)
    }

    /**
     * emit a JSONObject
     * DMS parsers required json data to parsing correctly
     * Need to convert any data into JSONObject
     *
     * @param eventName String socet event need to emit
     * @param payload JSONObject
     */
    fun sendJSONObject(eventName: String, payload: JSONObject) {
        if (!this@DMSSocket.isConnected()) {
            return
        }

        Timber.d("Emiting event `$eventName`, data: ${ payload.toString(3) }")
        this@DMSSocket.mSocket.emit(eventName, payload)
    }

    /**
     * Disconnect from server when needed
     */
    fun disconnect() {
        if (!this@DMSSocket.isConnected()) {
            return
        }

        this@DMSSocket.mSocket.off()
        this@DMSSocket.mSocket.disconnect()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun lifecycleOnStart() {
        super.lifecycleOnStart()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun lifecycleOnResume() {
        super.lifecycleOnResume()

        if (DMSSocket.DefaultConfig.isAutoPauseConnection) {
            this@DMSSocket.connect(this@DMSSocket.usersEventsList)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun lifecycleOnPause() {
        if (DMSSocket.DefaultConfig.isAutoPauseConnection) {
            this@DMSSocket.disconnect()
        }

        super.lifecycleOnPause()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun lifecycleOnStop() {
        super.lifecycleOnStop()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun lifecycleOnDestroy() {
        this@DMSSocket.disconnect()
        super.lifecycleOnDestroy()
    }


    override var lifecycle: Lifecycle? = null
    private var listener: DMSSocketListener? = null
    private var usersEventsList: List<String> = emptyList()

    private val mSocket: Socket by lazy {
        val socketURL: String? = DMSSocket.DefaultConfig.baseURL
        val socketOptions = IO.Options()
        socketOptions.reconnection = true

        DMSSocket.DefaultConfig.socketPath?.let { unwrappedPath ->
            if (unwrappedPath.isNotBlank()) socketOptions.path = unwrappedPath
        }

        return@lazy IO.socket(socketURL, socketOptions)
    }


    companion object {

        val DefaultConfig: DMSSocketConfig = DMSSocketConfig()

    }

}
