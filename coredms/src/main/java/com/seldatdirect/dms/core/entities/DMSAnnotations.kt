package com.seldatdirect.dms.core.entities

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


object DMSAnnotations {

    /**
     * Add this annotation into a field to skip its value
     *
     * @see com.seldatdirect.dms.core.api.DMSRest.getDefaultGsonParser
     */
    @Target(AnnotationTarget.FIELD)
    annotation class DMSJsonableExcludeField

}
