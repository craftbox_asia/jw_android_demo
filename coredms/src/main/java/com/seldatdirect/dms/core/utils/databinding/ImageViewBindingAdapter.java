package com.seldatdirect.dms.core.utils.databinding;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.annotation.DrawableRes;
import androidx.databinding.BindingAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import timber.log.Timber;

import javax.annotation.Nullable;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class ImageViewBindingAdapter {

    @SuppressLint("CheckResult")
    @BindingAdapter(value = {"imageViewAsyncSrc", "imageViewAsyncSrcOnFailedDrawable"}, requireAll = false)
    public static void imageViewAsyncSrc(final ImageView sender, @Nullable String photoURL, @DrawableRes int onFailed) {
        if (sender == null) {
            return;
        }

        if (photoURL == null || photoURL.isEmpty()) {
            return;
        }

        try {
            String thumbnailURL = "https://images1-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&resize_w=50&url=" + photoURL;
            RequestBuilder<Drawable> thumbnailRequestBuilder = Glide.with(sender.getContext()).load(thumbnailURL);

            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(android.R.color.black);

            // skip cache photo if it already in local file...
            // this will use for display local file instead of url
            if (photoURL.startsWith("/")) {
                requestOptions
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .signature(new ObjectKey(String.valueOf(System.currentTimeMillis())));
            }

            Glide.with(sender).load(photoURL)
                    .thumbnail(thumbnailRequestBuilder)
                    .error(onFailed)
                    .apply(requestOptions)
                    .into(sender);
        } catch (Exception exception) {
            Timber.e(exception);
        }
    }

    @BindingAdapter("imageViewResourceId")
    public static void imageViewResourceId(ImageView view, @DrawableRes int resourceId) {
        if (view == null) {
            return;
        }

        try {
            view.setImageResource(resourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
