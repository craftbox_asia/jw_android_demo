@file:Suppress("UNCHECKED_CAST")

package com.seldatdirect.dms.core.utils.preference

import android.content.Context
import android.content.SharedPreferences
import com.seldatdirect.dms.core.api.DMSRest
import timber.log.Timber
import java.io.Serializable

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSPreference]
 * Manage default apps [SharedPreferences]
 *
 * @property sharedPref SharedPreferences
 * @constructor
 */
class DMSPreference(appContext: Context) {

    private val sharedPref: SharedPreferences = appContext.getSharedPreferences(
            appContext.applicationContext.packageName,
            Context.MODE_PRIVATE
    )

    /**
     * Put data into shared preference
     * This method will negotiate data type automatically
     *
     * @param key String preference key need to store
     * @param data Any? data need to store, set to null to remove value by its key
     */
    fun set(key: String, data: Any?) {
        if (key.isBlank()) throw Exception("Preference key is blank!")
        val editor = this@DMSPreference.sharedPref.edit()

        if (data == null) {
            editor.remove(key)
            editor.apply()
            return
        }

        when (data) {
            is Int -> editor.putInt(key, data)
            is Long -> editor.putLong(key, data)
            is Boolean -> editor.putBoolean(key, data)
            is Float -> editor.putFloat(key, data)
            is Double -> editor.putFloat(key, data.toFloat())
            is String -> editor.putString(key, data)
            is Serializable -> {
                try { editor.putString(key, DMSRest.getDefaultGsonParser().toJson(data)) }
                catch (exception: Exception) { Timber.e(exception) }
            }
        }

        editor.apply()
    }

    /**
     * Retrieve data by preference key and data class
     *
     * @param key String preference key need to retrieve value
     * @param dataType Class<T> class type of data need to get
     * @param defaultValue T? if reference value is null or parsing error, will return default value (optional)
     * @return T?
     */
    fun <T> get(key: String, dataType: Class<T>, defaultValue: T? = null): T? {
        if (key.isBlank()) throw Exception("Preference key is blank!")

        if (!this@DMSPreference.sharedPref.contains(key)) {
            return null
        }

        return when (dataType) {
            Int::class.java -> this@DMSPreference.sharedPref.getInt(key, (defaultValue as? Int) ?: -1) as? T
            Long::class.java -> this@DMSPreference.sharedPref.getLong(key, (defaultValue as? Long) ?: -1) as? T
            Boolean::class.java -> this@DMSPreference.sharedPref.getBoolean(key, (defaultValue as? Boolean) ?: false) as? T
            Float::class.java -> this@DMSPreference.sharedPref.getFloat(key, (defaultValue as? Float) ?: 0.0f) as? T
            Double::class.java -> this@DMSPreference.sharedPref.getFloat(key, (defaultValue as? Float) ?: 0.0f) as? T
            String::class.java -> this@DMSPreference.sharedPref.getString(key, (defaultValue as? String) ?: "") as? T
            else -> try { DMSRest.getDefaultGsonParser().fromJson<T>(this@DMSPreference.sharedPref.getString(key, "{}"), dataType) } catch (exception: Exception) { Timber.e(exception); defaultValue }

        }
    }


    companion object {

        private var preference: DMSPreference? = null


        internal fun init(appContext: Context) {
            this@Companion.preference = DMSPreference(appContext)
        }

        fun default(): DMSPreference {
            return this@Companion.preference ?: throw Exception(
                    "CoreDMS is not install already.\n"
                            + "Call `this.initialCoreDMS()` inside onCreate method in your Application subclass."
            )
        }

    }

}
