@file:Suppress("unused")

package com.seldatdirect.dms.core.api.processors

import android.annotation.SuppressLint
import androidx.annotation.StringRes
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.ui.activity.DMSActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


open class DMSRestPromise<M, O: Observable<M>>(private val observable: O) : DisposableObserver<M>() {

    val identifier: String = UUID.randomUUID().toString()
    @StringRes var taskNameResId: Int? = null


    /**
     * task name resource id help displaying loading status
     * this also identify promise type from [DMSViewModelSource] callbacks
     *
     * When an activity or fragment implement DMSViewModelSource, any api call from [DMSViewModel.request]
     * will trigger callbacks to [DMSViewModelSource], we can identify which api method by this identify by [DMSRestPromise.taskNameResId]
     *
     * References:
     * @see DMSActivity.onAPIResponseSuccess which implemented from [DMSViewModel.Listener]
     * @see DMSFragment.onAPIResponseSuccess which implemented from [DMSViewModel.Listener]
     * @see DMSViewModel.request
     * @see DMSViewModel.Listener
     *
     * @param resId Int?
     * @return DMSRestPromise<M, O>
     */
    fun setTaskNameResId(@StringRes resId: Int?): DMSRestPromise<M, O> {
        this@DMSRestPromise.taskNameResId = resId
        return this@DMSRestPromise
    }

    /**
     * response successfully with status code 2xx
     * this will call in [AndroidSchedulers.mainThread] or user custom thead in [DMSRestPromise.submit]
     *
     * @param block (promise: DMSRestPromise<M, O>, responseData: M) -> Unit
     * @return DMSRestPromise<M, O>
     */
    fun then(block: (promise: DMSRestPromise<M, O>, responseData: M) -> Unit): DMSRestPromise<M, O> {
        this@DMSRestPromise.onSuccessListeners.add(block)
        return this@DMSRestPromise
    }

    /**
     * response error
     * to identify which error, see [DMSRestException.type]
     * to get error message from server ([DMSRestException.ExceptionType.Server]), @see [DMSRestException.getErrorMessage]
     * this will call in [AndroidSchedulers.mainThread] or user custom thead in [DMSRestPromise.submit]
     *
     * @param block (promise: DMSRestPromise<M, O>, exception: DMSRestException) -> Unit
     * @return DMSRestPromise<M, O>
     */
    fun catch(block: (promise: DMSRestPromise<M, O>, exception: DMSRestException) -> Unit): DMSRestPromise<M, O> {
        this@DMSRestPromise.onExceptionListeners.add(block)
        return this@DMSRestPromise
    }

    /**
     * this callback will trigger when user start request into backend api
     * this will call in [AndroidSchedulers.mainThread] or user custom thead in [DMSRestPromise.submit]
     *
     * @param block (promise: DMSRestPromise<M, O>) -> Unit
     * @return DMSRestPromise<M, O>
     */
    fun doBeforeRequest(block: (promise: DMSRestPromise<M, O>) -> Unit): DMSRestPromise<M, O> {
        this@DMSRestPromise.onBeforeRequestListeners.add(block)
        return this@DMSRestPromise
    }

    /**
     * this callback will trigger when data responsed from backend (throw any exceptions)
     * this will call in [AndroidSchedulers.mainThread] or user custom thead in [DMSRestPromise.submit]
     *
     * @param block (promise: DMSRestPromise<M, O>) -> Unit
     * @return DMSRestPromise<M, O>
     */
    fun doAfterResponse(block: (promise: DMSRestPromise<M, O>) -> Unit): DMSRestPromise<M, O> {
        this@DMSRestPromise.onAfterResponseListeners.add(block)
        return this@DMSRestPromise
    }

    /**
     * send request into server
     *
     * @param subscribeThread Scheduler
     * @param observableThread Scheduler
     */
    @SuppressLint("CheckResult")
    fun submit(subscribeThread: Scheduler = Schedulers.io(), observableThread: Scheduler = AndroidSchedulers.mainThread()) {
        this@DMSRestPromise.observable
                .observeOn(observableThread)
                .subscribeOn(subscribeThread)
                .subscribeWith(this@DMSRestPromise)
    }

    /**
     * !!!DON'T CALL THIS!!!
     * this is override method, [DMSRestPromise] has been using this
     */
    @Deprecated("Don't call this method directly! It used in DMSRestPromise")
    final override fun onStart() {
        super.onStart()
        this@DMSRestPromise.onBeforeRequestListeners.forEach { it.invoke(this@DMSRestPromise) }
    }

    /**
     * !!!DON'T CALL THIS!!!
     * this is override method, [DMSRestPromise] has been using this
     */
    @Deprecated("Don't call this method directly! It used in DMSRestPromise")
    final override fun onError(throwable: Throwable) {
        throwable.printStackTrace()

        val exception = DMSRestException()
        exception.original = throwable

        when (throwable) {
            is HttpException -> {
                exception.type = DMSRestException.ExceptionType.Server
                exception.httpBody = throwable.response().errorBody()?.string()
                exception.httpStatusCode = throwable.response().code()
            }
            is SocketTimeoutException -> {
                exception.type = DMSRestException.ExceptionType.NetworkTimeout
            }
            is IOException -> {
                exception.type = DMSRestException.ExceptionType.NoInternet
            }
            else -> {
                exception.type = DMSRestException.ExceptionType.Unknown
            }
        }

        this@DMSRestPromise.onExceptionListeners.forEach { it.invoke(this@DMSRestPromise, exception) }
        this@DMSRestPromise.onAfterResponseListeners.forEach { it.invoke(this@DMSRestPromise) }
    }

    /**
     * !!!DON'T CALL THIS!!!
     * this is override method, [DMSRestPromise] has been using this
     */
    @Deprecated("Don't call this method directly! It used in DMSRestPromise")
    final override fun onNext(t: M) {
        this@DMSRestPromise.onSuccessListeners.forEach { it.invoke(this@DMSRestPromise, t) }
        this@DMSRestPromise.onAfterResponseListeners.forEach { it.invoke(this@DMSRestPromise) }
    }

    /**
     * !!!DON'T CALL THIS!!!
     * this is override method, [DMSRestPromise] has been using this
     */
    @Deprecated("Don't call this method directly! It used in DMSRestPromise")
    final override fun onComplete() { }

    override fun toString(): String {
        return "DMSRestPromise(Identifier: ${ this@DMSRestPromise.identifier })"
    }


    private val onBeforeRequestListeners : ArrayList<(promise: DMSRestPromise<M, O>) -> Unit> = ArrayList()
    private val onAfterResponseListeners : ArrayList<(promise: DMSRestPromise<M, O>) -> Unit> = ArrayList()
    private val onSuccessListeners : ArrayList<(promise: DMSRestPromise<M, O>, responseData: M) -> Unit> = ArrayList()
    private val onExceptionListeners : ArrayList<(promise: DMSRestPromise<M, O>, exception: DMSRestException) -> Unit> = ArrayList()
}
