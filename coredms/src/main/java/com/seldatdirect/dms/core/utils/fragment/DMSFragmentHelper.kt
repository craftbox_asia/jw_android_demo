@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.seldatdirect.dms.core.utils.fragment

import android.annotation.SuppressLint
import android.os.Build
import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.transition.*
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit


/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSFragmentHelper]
 * Manage fragments for default container
 *
 * @property fm FragmentManager
 * @property containerViewId Int
 * @constructor
 */
class DMSFragmentHelper(private val fm: FragmentManager, @IdRes private val containerViewId: Int) {

    private var onFragmentUpdated: ((fragment: DMSFragment?) -> Unit)? = null

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()


    private val notifyFragmentChangedSubject: PublishSubject<DMSFragment> by lazy {
        val instance = PublishSubject.create<DMSFragment>()
        this@DMSFragmentHelper.compositeDisposable.add(
            instance.debounce(
                100,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@DMSFragmentHelper.onFragmentUpdated?.invoke(it)
            })
        return@lazy instance
    }

    fun setOnFragmentChangesListener(block: ((fragment: DMSFragment?) -> Unit)?) {
        this@DMSFragmentHelper.onFragmentUpdated = block
    }

    /**
     * setup root fragment
     * warning: just call once when initial fragment helper
     *
     * @param fragment T instance of fragment need to inflate
     */
    fun <T : DMSFragment> initialRootFragment(fragment: T) {
        // if fragment already initialized, skip it
        if (this@DMSFragmentHelper.fm.findFragmentById(this@DMSFragmentHelper.containerViewId) != null) {
            return
        }

        this@DMSFragmentHelper.getFragmentTransaction(false)
            .replace(
                this@DMSFragmentHelper.containerViewId,
                fragment,
                fragment::class.java.simpleName
            )
            .commitNow()

        this@DMSFragmentHelper.notifyFragmentChangedSubject.onNext(fragment)
    }

    /**
     * show a fragment
     *
     * @param fragment T instance of fragment need to show
     * @param replaceRootFragment Boolean true if need to clear all fragments in backstack, otherwise, keep previous fragments and LIFO
     */
    fun <T : DMSFragment> pushFragment(
        fragment: T,
        replaceRootFragment: Boolean = false,
        hasAnimation: Boolean = true,
        sharedElements: Map<String, View> = emptyMap()
    ) {
        // if fragment current displaying, skip inflate it
        if (this@DMSFragmentHelper.fm.findFragmentById(this@DMSFragmentHelper.containerViewId)?.tag == fragment::class.java.simpleName) {
            return
        }

        // remove all fragments in backstacks
        if (replaceRootFragment) {
            this@DMSFragmentHelper.popToRoot()
        }

        if (!sharedElements.isEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fragment.sharedElementEnterTransition = DetailsTransition()
            fragment.enterTransition = Fade()
            this@DMSFragmentHelper.getCurrentFragment()?.exitTransition = Fade()
            fragment.sharedElementReturnTransition = DetailsTransition()
        }

        val transaction =
            this@DMSFragmentHelper.getFragmentTransaction(hasAnimation && !replaceRootFragment && sharedElements.isEmpty())

        sharedElements.keys.forEach { transitionName ->
            sharedElements[transitionName]?.let { unwrappedView ->
                transaction.addSharedElement(unwrappedView, transitionName)
            }
        }

        transaction.replace(
            this@DMSFragmentHelper.containerViewId,
            fragment,
            fragment::class.java.simpleName
        )

        if (!replaceRootFragment) {
            transaction
                .addToBackStack(fragment::class.java.simpleName)
        }

        transaction.commit()
        this@DMSFragmentHelper.notifyFragmentChangedSubject.onNext(fragment)
    }

    @SuppressLint("CheckResult")
            /**
             * clear all fragments in backstack, then back to root fragment
             */
    fun popToRoot() {
        this@DMSFragmentHelper.fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)

        Observable.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe { _ ->
            this@DMSFragmentHelper.getCurrentFragment()
                ?.let { this@DMSFragmentHelper.notifyFragmentChangedSubject.onNext(it) }
        }
    }

    @SuppressLint("CheckResult")
            /**
             * back to previous fragment when available
             * @return Boolean true if has previous fragment and pop successfully
             */
    fun popFragmentIfNeeded(): Boolean {
        if (this@DMSFragmentHelper.fm.backStackEntryCount > 0) {
            val isSuccess = this@DMSFragmentHelper.fm.popBackStackImmediate()

            if (isSuccess) {
                Observable.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe { _ ->
                    this@DMSFragmentHelper.getCurrentFragment()
                        ?.let { this@DMSFragmentHelper.notifyFragmentChangedSubject.onNext(it) }
                }
            }

            return isSuccess
        }
        return false
    }

    /**
     * check instance of fragment is displaying on container or not
     *
     * @param fragmentClass Class<T> instance of fragment need to check
     * @return Boolean true if it inflated and displaying
     */
    fun <T : DMSFragment> isDisplaying(fragmentClass: Class<T>): Boolean {
        return this@DMSFragmentHelper.fm.findFragmentById(this@DMSFragmentHelper.containerViewId)?.tag == fragmentClass.simpleName
    }

    /**
     * check if current fragment can backable
     * @see DMSFragment.isBackable
     *
     * @return Boolean
     */
    fun isCurrentFragmentBackable(): Boolean {
        return (this@DMSFragmentHelper.fm.findFragmentById(this@DMSFragmentHelper.containerViewId) as? DMSFragment)?.isBackable() == true
    }

    private fun getFragmentTransaction(hasAnimation: Boolean = true): FragmentTransaction {
        val fragmentTransaction = this@DMSFragmentHelper.fm.beginTransaction()

        if (hasAnimation) {
            fragmentTransaction.setCustomAnimations(
                R.anim.dms_anim_enter_fade_in,
                R.anim.dms_anim_exit_fade_out,
                R.anim.dms_anim_exit_fade_in,
                R.anim.dms_anim_enter_fade_out
            )
        }

        return fragmentTransaction
    }

    fun getCurrentFragment(): DMSFragment? {
        return this@DMSFragmentHelper.fm.findFragmentById(this@DMSFragmentHelper.containerViewId) as? DMSFragment
    }


    class DetailsTransition : TransitionSet() {
        init {
            ordering = TransitionSet.ORDERING_TOGETHER
            addTransition(ChangeBounds()).addTransition(ChangeTransform()).addTransition(ChangeImageTransform())
        }
    }

}
