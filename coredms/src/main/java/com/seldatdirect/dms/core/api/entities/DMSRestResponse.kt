package com.seldatdirect.dms.core.api.entities

import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSRestResponse : DMSJsonableObject
