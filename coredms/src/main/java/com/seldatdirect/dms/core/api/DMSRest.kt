@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.annotations.Expose
import com.seldatdirect.dms.core.entities.DMSAnnotations
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSRest] help connect between client with backend API
 *
 * This object included:
 * @see DMSRestConfig default api configurations (base url, access token, etc.)
 * @see Retrofit default retrofit with default configs.
 * @see Gson default gson parser for any requests, responses.
 *
 * To create new request, call [DMSRest.get] with service interface
 * To create new default JSON parser, call [DMSRest.getDefaultGsonParser]
 * To get custom retrofit (not recommended), call [DMSRest.getRetrofit]
 */
object DMSRest {

    val config: DMSRestConfig = DMSRestConfig()


    /**
     * create new retrofit with default configs ([DMSRest.config])
     * because config may be change in runtime (example user's access token will update after user login),
     * so need to create each retrofit for each requests is better than using instance
     *
     * @return Retrofit
     */
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(this@DMSRest.config.baseURL)
            .client(this@DMSRest.getHTTPClient())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(this@DMSRest.getDefaultGsonParser()))
            .build()
    }

    /**
     * create new service interface with default configs ([DMSRest.config])
     *
     * @param apiService Class<T>
     * @return T
     */
    fun <T> get(apiService: Class<T>): T {
        return this@DMSRest.getRetrofit().create(apiService)
    }

    /**
     * get defult gson parser instance
     *
     * @return Gson
     */
    fun getDefaultGsonParser(): Gson {
        val gsonBuilder = GsonBuilder()
            .setLenient()
            .serializeNulls()

        gsonBuilder.setExclusionStrategies(object : ExclusionStrategy {
            override fun shouldSkipClass(clazz: Class<*>?): Boolean {
                return false
            }

            override fun shouldSkipField(field: FieldAttributes?): Boolean {
                val unwrappedField = field ?: return false
                return (unwrappedField.getAnnotation(DMSAnnotations.DMSJsonableExcludeField::class.java) != null)
                        || (unwrappedField.getAnnotation(Expose::class.java) != null)
            }

        })

        return gsonBuilder.create()
    }

    internal fun getHTTPClient(isEmbedAppInterceptor: Boolean = true): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()

        if (this@DMSRest.config.enabledDebug) {
            val okHttpDebugging = HttpLoggingInterceptor()
            okHttpDebugging.level = HttpLoggingInterceptor.Level.BODY

            okHttpClient.addInterceptor(okHttpDebugging)
            okHttpClient.addNetworkInterceptor(StethoInterceptor())
        }

        if (isEmbedAppInterceptor) {
            okHttpClient.addInterceptor { chain ->
                val requestBuilder = chain.request().newBuilder()

                // set users access token if available
                this@DMSRest.config.authorizationToken?.let { unwrappedToken ->
                    if (unwrappedToken.isNotBlank()) {
                        requestBuilder.addHeader(
                            "Authorization",
                            "${this@DMSRest.config.authorizationTokenPrefix} $unwrappedToken"
                        )
                    }
                }

                // notify to backend to return correct localization
                /*
                requestBuilder.addHeader(
                        "X-localization",
                        if (arrayOf("he", "iw").contains(Locale.getDefault().language))
                            "hb" else
                            "en"
                )
                */

                // add custom users headers (of course that will override headers above)
                this@DMSRest.config.additionalHeaders.forEach { entry ->
                    requestBuilder.addHeader(entry.key, entry.value)
                }

                val requester = requestBuilder.build()
                return@addInterceptor chain.proceed(requester)
            }
        }

        okHttpClient.connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)

        return okHttpClient.build()
    }

}
