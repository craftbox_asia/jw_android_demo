package com.seldatdirect.dms.core.api.contribs.responses

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestResponse

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [MessageFieldDMSRestResponse]
 * this class will wrap respons data into message field
 *
 * @param T
 * @property message T?
 */
class MessageFieldDMSRestResponse<T> : DMSRestResponse {

    @SerializedName("message")
    var message: T? = null

}
