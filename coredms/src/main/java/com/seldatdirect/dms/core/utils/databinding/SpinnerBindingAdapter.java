package com.seldatdirect.dms.core.utils.databinding;

import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class SpinnerBindingAdapter {

    @BindingAdapter("spinnerAdapter")
    public static void spinnerAdapter(Spinner spinner, @Nullable ArrayAdapter adapter) {
        spinner.setAdapter(adapter);
    }

}
