@file:Suppress("unused")

package com.seldatdirect.dms.core.extensions

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


fun String.copyToClipboard(context: Context, overrideLabel: String = "default"): Boolean {
    val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clipData: ClipData = ClipData.newPlainText(overrideLabel, this@copyToClipboard)
    clipboardManager.primaryClip = clipData

    return clipboardManager.hasPrimaryClip()
}

fun String.convertDateFormat(currentFormat: String, newFormat: String): String {
    return try {
        val currentDate = SimpleDateFormat(currentFormat, Locale.US).parse(this@convertDateFormat)
        SimpleDateFormat(newFormat, Locale.US).format(currentDate)
    } catch(invalidFormatException: Exception) {
        Timber.e(invalidFormatException)
        this@convertDateFormat
    }
}
