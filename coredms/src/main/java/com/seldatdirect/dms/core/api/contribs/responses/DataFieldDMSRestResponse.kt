package com.seldatdirect.dms.core.api.contribs.responses

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestResponse
import com.seldatdirect.dms.core.entities.DMSAnnotations

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DataFieldDMSRestResponse]
 * this class will wrap respons data into data field
 *
 *
 * @param T
 * @property data T?
 */
open class DataFieldDMSRestResponse<T> : DMSRestResponse {

    @SerializedName("data")
    var data: T? = null

    @SerializedName("message")
    @DMSAnnotations.DMSJsonableExcludeField
    var message: String? = null

}
