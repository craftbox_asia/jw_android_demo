package com.seldatdirect.dms.core.behaviours

import android.os.Bundle

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSControllerBehaviour {

    fun getLayoutResId(savedInstanceState: Bundle?): Int?
    fun getMenuResId(): Int?
    fun getTitleResId(): Int?

}
