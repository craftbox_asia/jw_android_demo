@file:Suppress("unused")

package com.seldatdirect.dms.core.extensions

import android.net.Uri
import android.webkit.MimeTypeMap
import android.content.ContentResolver
import android.content.Context
import java.io.File
import java.io.FileOutputStream


/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


fun Uri.getMimeType(context: Context): String? {
    return if (this@getMimeType.scheme == ContentResolver.SCHEME_CONTENT) {
        val cr = context.applicationContext.contentResolver
        cr.getType(this@getMimeType)
    } else {
        val fileExtension = MimeTypeMap.getFileExtensionFromUrl(this@getMimeType.toString())
        MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase())
    }
}

fun Uri.createTempFile(context: Context): File? {
    try {
        val mimeType = this@createTempFile.getMimeType(context)
        val fileExt = mimeType?.split("/")?.lastOrNull() ?: "png"
        val fileName = "mobile_${ System.currentTimeMillis() }.$fileExt"

        val inputStream = context.contentResolver.openInputStream(this@createTempFile)
        val tempFile = File(context.cacheDir, fileName)

        if (inputStream == null) {
            return null
        }

        val fileOutputStream = FileOutputStream(tempFile)
        val buffer = ByteArray(4 * 1024)
        var read: Int = inputStream.read(buffer)

        while (read != -1) {
            fileOutputStream.write(buffer, 0, read)
            read = inputStream.read(buffer)
        }

        fileOutputStream.flush()
        fileOutputStream.close()
        inputStream.close()

        return tempFile
    } catch (exception: Exception) {
        exception.printStackTrace()
        return null
    }
}
