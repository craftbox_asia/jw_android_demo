package com.seldatdirect.dms.core.utils.databinding;

import androidx.annotation.ColorInt;
import androidx.databinding.BindingAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class SwipeRefreshLayoutBindingAdapter {

    @BindingAdapter("swipeRefreshLayoutIsRefresing")
    public static void swipeRefreshLayoutIsRefresing(SwipeRefreshLayout refreshLayout, boolean isRefreshing) {
        if (refreshLayout == null) {
            return;
        }

        refreshLayout.setRefreshing(isRefreshing);
    }

    @BindingAdapter("swipeRefreshLayoutOnRefresh")
    public static void swipeRefreshLayoutOnRefresh(SwipeRefreshLayout refreshLayout, SwipeRefreshLayout.OnRefreshListener listener) {
        if (refreshLayout == null) {
            return;
        }

        refreshLayout.setOnRefreshListener(listener);
    }

    @BindingAdapter("swipeRefreshLayoutTintColor")
    public static void swipeRefreshLayoutTintColor(SwipeRefreshLayout refreshLayout, @ColorInt int tintColor) {
        if (refreshLayout == null) {
            return;
        }

        refreshLayout.setColorSchemeColors(tintColor);
    }

}
