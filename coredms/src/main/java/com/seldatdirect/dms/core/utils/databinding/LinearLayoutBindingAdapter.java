package com.seldatdirect.dms.core.utils.databinding;

import android.widget.LinearLayout;

import androidx.annotation.ColorInt;
import androidx.databinding.BindingAdapter;

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


public class LinearLayoutBindingAdapter {

    @BindingAdapter("linearLayoutSetBackgroundColorInt")
    public static void linearLayoutSetBackgroundColorInt(LinearLayout layout, @ColorInt int color) {
        if (layout == null) {
            return;
        }

        try {
            layout.setBackgroundColor(color);
        } catch (Exception invalidColor) {
            invalidColor.printStackTrace();
        }
    }

}
