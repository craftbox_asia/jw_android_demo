package com.seldatdirect.dms.core.utils.databinding;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import javax.annotation.Nullable;

import androidx.databinding.BindingAdapter;

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


@SuppressWarnings({"unused", "WeakerAccess"})
public class BottomNavigationBindingAdapter {

    @BindingAdapter("bottomNavigationOnMenuItemSelected")
    public static void bottomNavigationOnMenuItemSelected(BottomNavigationView navigationView, @Nullable BottomNavigationView.OnNavigationItemSelectedListener listener) {
        if (navigationView == null) {
            return;
        }

        navigationView.setOnNavigationItemSelectedListener(listener);
    }

}
