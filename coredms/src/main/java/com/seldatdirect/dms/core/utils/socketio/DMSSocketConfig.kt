@file:Suppress("unused")

package com.seldatdirect.dms.core.utils.socketio

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


data class DMSSocketConfig(
        var baseURL: String = "",
        var socketPath: String? = null,
        var isAutoPauseConnection: Boolean = true
) {

    fun updateSocketBaseURL(baseURL: String): DMSSocketConfig {
        this@DMSSocketConfig.baseURL = baseURL
        return this@DMSSocketConfig
    }

    fun updateSocketPath(path: String?): DMSSocketConfig {
        this@DMSSocketConfig.socketPath = path
        return this@DMSSocketConfig
    }

    fun setAutoPauseConnection(isAutoPause: Boolean): DMSSocketConfig {
        this@DMSSocketConfig.isAutoPauseConnection = isAutoPause
        return this@DMSSocketConfig
    }

}
