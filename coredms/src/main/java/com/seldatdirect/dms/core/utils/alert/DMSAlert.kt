@file:Suppress("MemberVisibilityCanBePrivate", "unused")

package com.seldatdirect.dms.core.utils.alert

import android.annotation.SuppressLint
import android.content.Context
import android.os.Looper
import android.widget.Toast
import androidx.annotation.IntRange
import androidx.annotation.StringRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.SingleChoiceListener
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.kaopiz.kprogresshud.KProgressHUD
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.behaviours.DMSLifecycleBehaviour
import com.seldatdirect.dms.core.ui.activity.DMSActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


/**
 * [DMSAlert]
 * Manage UI alert messages
 * This class already embedded in [DMSActivity.alert], [DMSFragment.alert]
 *
 * @property context Context
 * @constructor
 */
class DMSAlert(private val context: Context) : DMSLifecycleBehaviour {

    override var lifecycle: Lifecycle? = null
    private var progressHUD: KProgressHUD? = null
    private var alertDialog: MaterialDialog? = null


    /**
     * display short toast
     * @param message String message need to display
     * @param isLongToast Boolean true if need to expand toasts duration
     */
    fun toast(message: String, isLongToast: Boolean = false) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        Toast.makeText(
            this@DMSAlert.context,
            message,
            if (isLongToast) Toast.LENGTH_LONG else Toast.LENGTH_SHORT
        ).show()
    }

    /**
     * display short toast
     * @see toast(String, Boolean)
     *
     * @param messageResId Int string resource id need to get message
     * @param isLongToast Boolean true if need to expand toasts duration
     */
    fun toast(@StringRes messageResId: Int, isLongToast: Boolean = false) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.toast(this@DMSAlert.context.getString(messageResId), isLongToast)
    }

    /**
     * display hud progress
     *
     * @param message String bottom text need to display as status
     * @param percent Int? progress need to display, set to null (default) mean show progress as indeterminate
     */
    fun progressShow(message: String, @IntRange(from = 0, to = 100) percent: Int? = null) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        if (this@DMSAlert.progressHUD == null) {
            this@DMSAlert.progressHUD =
                    KProgressHUD.create(this@DMSAlert.context, KProgressHUD.Style.SPIN_INDETERMINATE)
                        .setCancellable(false)
        }

        this@DMSAlert.progressHUD?.setLabel(message)

        if (percent != null && percent in 0..100) {
            this@DMSAlert.progressHUD?.setStyle(KProgressHUD.Style.PIE_DETERMINATE)
            this@DMSAlert.progressHUD?.setProgress(percent)
        } else {
            this@DMSAlert.progressHUD?.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        }

        if (this@DMSAlert.progressHUD?.isShowing != true) {
            try {
                this@DMSAlert.progressHUD?.show()
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }
    }

    /**
     * display hud progress
     * @see progressShow(String, Int?)
     *
     * @param messageResId Int bottom string resource id need to display as status
     * @param percent Int? progress need to display, set to null (default) mean show progress as indeterminate
     */
    fun progressShow(@StringRes messageResId: Int, @IntRange(from = 0, to = 100) percent: Int? = null) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.progressShow(this@DMSAlert.context.getString(messageResId), percent)
    }

    /**
     * hide progress if it shown
     *
     * @see progressShow
     */
    fun progressDismiss() {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.releaseProgressHUDIfNeeded()
    }

    fun alert(errorMessage: String, closeButtonText: String? = null, onClose: (() -> Unit)? = null) {
        if (Looper.getMainLooper() != Looper.myLooper()) return
        this@DMSAlert.releaseAlertDialogIfNeeded()

        this@DMSAlert.alertDialog = MaterialDialog(this@DMSAlert.context)
            .message(text = errorMessage)
            .positiveButton(text = closeButtonText ?: this@DMSAlert.context.getString(android.R.string.ok))
        this@DMSAlert.alertDialog?.setOnDismissListener {
            onClose?.invoke()
            this@DMSAlert.releaseAlertDialogIfNeeded()
        }
        this@DMSAlert.alertDialog?.show()
    }

    /**
     * show quick alert dialog
     *
     * @param messageResId Int string resource id need to display as message content
     * @param closeBtnResId Int? string resource id for close button text
     * @param onClose (() -> Unit)? block callback when user close dialog
     */
    fun alert(@StringRes messageResId: Int, @StringRes closeBtnResId: Int? = null, onClose: (() -> Unit)? = null) {
        if (Looper.getMainLooper() != Looper.myLooper()) return
        this@DMSAlert.alert(
            errorMessage = this@DMSAlert.context.getString(messageResId),
            closeButtonText = if (closeBtnResId != null) this@DMSAlert.context.getString(closeBtnResId) else null,
            onClose = onClose
        )
    }

    /**
     * show list of selectable items as an alert dialog
     *
     * @param items List<String> list of items need to display
     * @param isRequired Boolean set to true if need user select at least an item
     * @param selectButtonResId Int string resource for positive button text
     * @param titleResId Int? string resource for dialog title
     * @param onSelected (itemIndex: Int) -> Unit block callback when user selected item, return index in items list
     */
    @SuppressLint("CheckResult")
    fun singleChoice(
        items: List<String>,
        isRequired: Boolean = true, @StringRes selectButtonResId: Int = android.R.string.ok, @StringRes titleResId: Int? = null,
        onSelected: (itemIndex: Int) -> Unit
    ) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.releaseAlertDialogIfNeeded()
        this@DMSAlert.alertDialog = MaterialDialog(this@DMSAlert.context)

        if (isRequired) {
            alertDialog?.cancelable(false)
            alertDialog?.noAutoDismiss()
        }

        var itemIndex = 0

        alertDialog?.listItemsSingleChoice(
            items = items,
            waitForPositiveButton = isRequired,
            selection = object : SingleChoiceListener {
                override fun invoke(dialog: MaterialDialog, index: Int, text: String) {
                    itemIndex = index
                }
            })

        alertDialog?.positiveButton(res = selectButtonResId, click = {
            onSelected.invoke(itemIndex)
            this@DMSAlert.releaseAlertDialogIfNeeded()
        })

        titleResId?.let { unwrappedTitle ->
            alertDialog?.title(res = unwrappedTitle)
        }

        this@DMSAlert.alertDialog?.setOnDismissListener { this@DMSAlert.releaseAlertDialogIfNeeded() }
        this@DMSAlert.alertDialog?.show()
    }

    /**
     * display confirmation alert dialog
     *
     * @param title String dialog title text
     * @param cancelRes Int string resource id for negative button text
     * @param confirmRes Int string resource id for positive button text
     * @param onConfirm () -> Unit block callback when user select confirm button
     */
    fun confirm(
        title: String, @StringRes cancelRes: Int = R.string.dms_action_cancel, @StringRes confirmRes: Int = R.string.dms_action_confirm,
        onConfirm: () -> Unit
    ) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.alertDialog = MaterialDialog(this@DMSAlert.context)
            .message(text = title)
            .negativeButton(res = cancelRes, click = { it.dismiss() })
            .positiveButton(res = confirmRes, click = { onConfirm.invoke() })

        this@DMSAlert.alertDialog?.setOnDismissListener { this@DMSAlert.releaseAlertDialogIfNeeded() }
        this@DMSAlert.alertDialog?.show()
    }

    fun confirm(
        @StringRes titleResId: Int, @StringRes cancelRes: Int = R.string.dms_action_cancel, @StringRes confirmRes: Int = R.string.dms_action_confirm,
        onConfirm: () -> Unit
    ) {
        this@DMSAlert.confirm(this@DMSAlert.context.getString(titleResId), cancelRes, confirmRes, onConfirm)
    }

    fun showDatePicker(
        fragment: DMSFragment,
        preSelectedDate: Date? = Date(System.currentTimeMillis()),
        onDateSelected: (date: Date) -> Unit
    ) {
        this@DMSAlert.showDatePicker(fragment.childFragmentManager, preSelectedDate, onDateSelected)
    }

    fun showDatePicker(activity: DMSActivity, preSelectedDate: Date?, onDateSelected: (date: Date) -> Unit) {
        this@DMSAlert.showDatePicker(activity.supportFragmentManager, preSelectedDate, onDateSelected)
    }

    private fun showDatePicker(fm: FragmentManager, preSelectedDate: Date?, onDateSelected: (date: Date) -> Unit) {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        val calendar = Calendar.getInstance(Locale.getDefault())
        calendar.time = preSelectedDate ?: Date(System.currentTimeMillis())

        var datePicker: DatePickerDialog? = DatePickerDialog.newInstance({ view, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            onDateSelected.invoke(calendar.time)
            view.dismiss()
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))

        datePicker?.setOnDismissListener { datePicker = null }
        datePicker?.setStyle(DialogFragment.STYLE_NORMAL, R.style.ThemeOverlay_MaterialComponents_Dialog)
        datePicker?.show(fm, DatePickerDialog::class.java.simpleName)
    }

    private fun releaseAlertDialogIfNeeded() {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.alertDialog?.let { unwrapped ->
            if (unwrapped.isShowing) {
                unwrapped.dismiss()
            }
        }
        this@DMSAlert.alertDialog = null
    }

    private fun releaseProgressHUDIfNeeded() {
        if (Looper.getMainLooper() != Looper.myLooper()) return

        this@DMSAlert.progressHUD?.let { unwrapped ->
            if (unwrapped.isShowing) {
                unwrapped.dismiss()
            }
        }
        this@DMSAlert.progressHUD = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun lifecycleOnDestroy() {
        super.lifecycleOnDestroy()
        this@DMSAlert.releaseAlertDialogIfNeeded()
        this@DMSAlert.releaseProgressHUDIfNeeded()
    }

}
