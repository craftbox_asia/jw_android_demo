package com.seldatdirect.dms.core.api.contribs.responses

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


class YiiBackendStatusDMSRestResponse : DataFieldDMSRestResponse<YiiBackendStatusDMSRestResponse.ResponseData>() {

    class ResponseData : DMSJsonableObject {

        @SerializedName("msg")
        var msg: String? = null

        @SerializedName("table")
        var table: String? = null

        @SerializedName("id")
        var id: String? = null

    }

}
