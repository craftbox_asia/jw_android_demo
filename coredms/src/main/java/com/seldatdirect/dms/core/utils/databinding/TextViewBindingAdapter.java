package com.seldatdirect.dms.core.utils.databinding;

import android.text.TextUtils;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Nullable;

import androidx.databinding.BindingAdapter;
import timber.log.Timber;
import uk.co.chrisjenx.calligraphy.TypefaceUtils;

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


public class TextViewBindingAdapter {

    @BindingAdapter("textViewFontPath")
    public static void textViewFontPath(@Nullable TextView view, @Nullable  String fontPath) {
        if (view == null || fontPath == null || TextUtils.isEmpty(fontPath)) {
            return;
        }

        try {
            view.setTypeface(TypefaceUtils.load(view.getContext().getAssets(), fontPath));
        } catch (Exception exception) {
            Timber.e(exception);
        }
    }

    @BindingAdapter(value = { "textViewSetDateText", "textViewSetDatePattern" })
    public static void textViewSetDateText(@Nullable  TextView view, @Nullable Date date, @Nullable String pattern) {
        if (view == null || date == null) {
            return;
        }

        if (pattern == null || TextUtils.isEmpty(pattern)) {
            pattern = "dd/MM/yyyy";
        }

        try {
            SimpleDateFormat dateFormatter = new SimpleDateFormat(pattern, Locale.US);
            view.setText(dateFormatter.format(date));
        } catch (Exception exception) {
            Timber.e(exception);
        }
    }

}
