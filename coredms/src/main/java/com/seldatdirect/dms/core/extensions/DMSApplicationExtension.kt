package com.seldatdirect.dms.core.extensions

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory
import com.facebook.stetho.Stetho
import com.facebook.stetho.rhino.JsRuntimeReplFactoryBuilder
import com.seldatdirect.dms.core.BuildConfig
import com.seldatdirect.dms.core.R
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import org.mozilla.javascript.BaseFunction
import org.mozilla.javascript.Context
import org.mozilla.javascript.Scriptable
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


fun Application.initialCoreDMS() {

    if (BuildConfig.DEBUG) {
        // enable debug via chrome
        val jsRuntimeReplBuilder = JsRuntimeReplFactoryBuilder(this@initialCoreDMS)

        DMSRest.config.jsRuntimeDebuggerFunctions.forEach { mapEntry ->
            jsRuntimeReplBuilder.addFunction(mapEntry.key, mapEntry.value)
        }

        jsRuntimeReplBuilder.addFunction("functions", object : BaseFunction() {
            override fun call(cx: Context?, scope: Scriptable?, thisObj: Scriptable?, args: Array<out Any>?): Any {
                return DMSRest.config.jsRuntimeDebuggerFunctions.keys.joinToString("(); ")
            }
        })

        Stetho.initialize(
                Stetho.newInitializerBuilder(this@initialCoreDMS).enableWebKitInspector {
                    return@enableWebKitInspector Stetho.DefaultInspectorModulesBuilder(this@initialCoreDMS)
                            .runtimeRepl(jsRuntimeReplBuilder.build())
                            .finish()
                }
                        .build()
        )

        // enable logging in debug mode
        Timber.plant(object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                val className = super.createStackElementTag(element)
                val methodName = element.methodName
                val lineNumber = element.lineNumber
                return "$className->$methodName(L:$lineNumber)"
            }
        })
    }

    // init default dms preference
    DMSPreference.init(this@initialCoreDMS)

    // Facebook fresco
    val frescoImageConfig = OkHttpImagePipelineConfigFactory
            .newBuilder(this@initialCoreDMS, DMSRest.getHTTPClient(false))
            .build()
    Fresco.initialize(this@initialCoreDMS, frescoImageConfig)

    // enable default app font
    CalligraphyConfig.initDefault(
            CalligraphyConfig.Builder()
                    .setDefaultFontPath(this@initialCoreDMS.getString(R.string.dms_font_arimo_regular))
                    .setFontAttrId(R.attr.fontPath)
                    .build()
    )
}
