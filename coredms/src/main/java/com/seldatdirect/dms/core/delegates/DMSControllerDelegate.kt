package com.seldatdirect.dms.core.delegates

import android.os.Bundle
import android.view.View

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSControllerDelegate {

    fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?)

}
