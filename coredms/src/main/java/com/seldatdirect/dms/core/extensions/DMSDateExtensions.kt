package com.seldatdirect.dms.core.extensions

import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */
 
 
fun Date.formatToString(pattern: String): String? {
    return try {
        SimpleDateFormat(pattern, Locale.US).format(this@formatToString)
    } catch (illagelFormatPatternException: Exception) {
        Timber.e(illagelFormatPatternException)
        null
    }
}
