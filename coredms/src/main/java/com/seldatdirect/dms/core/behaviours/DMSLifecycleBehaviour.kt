@file:Suppress("MemberVisibilityCanBePrivate")
package com.seldatdirect.dms.core.behaviours

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSLifecycleBehaviour : LifecycleObserver {

    var lifecycle: Lifecycle?


    fun bind(lifecycle: Lifecycle) {
        if (this@DMSLifecycleBehaviour.lifecycle != null) {
            return
        }

        this@DMSLifecycleBehaviour.lifecycle = lifecycle
        this@DMSLifecycleBehaviour.lifecycle?.addObserver(this@DMSLifecycleBehaviour)
    }

    fun unbindIfNeeded() {
        this@DMSLifecycleBehaviour.lifecycle?.removeObserver(this@DMSLifecycleBehaviour)
        this@DMSLifecycleBehaviour.lifecycle = null
    }


    fun lifecycleOnCreate() {
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onCreate")
    }

    fun lifecycleOnStart() {
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onStart")
    }

    fun lifecycleOnResume() {
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onResume")
    }

    fun lifecycleOnPause() {
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onPause")
    }

    fun lifecycleOnStop() {
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onStop")
    }

    fun lifecycleOnDestroy() {
        this@DMSLifecycleBehaviour.unbindIfNeeded()
        Timber.i("${this@DMSLifecycleBehaviour::class.java.simpleName} onDestroy")
    }

}
