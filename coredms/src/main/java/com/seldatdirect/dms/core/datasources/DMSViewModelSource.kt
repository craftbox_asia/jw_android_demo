package com.seldatdirect.dms.core.datasources

import com.seldatdirect.dms.core.entities.DMSViewModel

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


interface DMSViewModelSource<T : DMSViewModel> {

    val viewModel: T


    /**
     * Sometime, a view model can be use in many views
     * For example, an activity can be inflate many fragments, fragments may be interact with single model's data
     * So, it also trigger behaviours into fragments's UI in same time (loading, error handler,...)
     *
     * Return list of tasks using in specific controller to sure controller just handle their actions
     * You can override this feature by disable calling to super in DMSViewModelBehaviour
     *
     * Reference:
     * @see com.seldatdirect.dms.core.ui.activity.DMSActivity.onAPIWillRequest
     * @see com.seldatdirect.dms.core.ui.fragment.DMSFragment.onAPIWillRequest
     * ...
     *
     * @return List<Int>
     */
    fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return emptyList()
    }

}
