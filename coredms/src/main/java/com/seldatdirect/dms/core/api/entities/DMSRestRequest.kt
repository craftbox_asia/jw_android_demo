@file:Suppress("unused")

package com.seldatdirect.dms.core.api.entities

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.seldatdirect.dms.core.BR
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */


abstract class DMSRestRequest : BaseObservable(), DMSJsonableObject {

    val isRequestValid: Boolean
        @Bindable get() = this@DMSRestRequest.validates()


    abstract fun validates(): Boolean


    /**
     * validate request and notify to view
     */
    protected fun notifyValidation() {
        this@DMSRestRequest.notifyPropertyChanged(BR.requestValid)
    }

}
