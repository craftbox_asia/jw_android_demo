@file:Suppress("MemberVisibilityCanBePrivate")

package com.seldatdirect.dms.core.utils.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import com.seldatdirect.dms.core.R

/**
 * Created by @dphans (https://github.com/dphans)
 * October 2018
 */


object DMSNotification {

    @RequiresApi(Build.VERSION_CODES.O)
    fun registerNotificationChannel(context: Context, @StringRes channelIdRes: Int, @StringRes channelNameRes: Int, important: Int = NotificationManager.IMPORTANCE_DEFAULT) {
        val notificationManager = this@DMSNotification.getNotificationManager(context)
        val channelId   = context.getString(channelIdRes)
        val channelName = context.getString(channelNameRes)

        val notificationChannel = NotificationChannel(channelId, channelName, important)
        notificationManager.createNotificationChannel(notificationChannel)
    }

    fun getNotificationBuilder(context: Context, @StringRes channelIdRes: Int, title: String, message: String, @DrawableRes smallIconRes: Int? = null, @DrawableRes largeIconRes: Int? = null): NotificationCompat.Builder {
        return NotificationCompat.Builder(context, context.getString(channelIdRes))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(NotificationCompat.BigTextStyle())
                .setSmallIcon(smallIconRes ?: R.drawable.dms_ic_notification_default)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, largeIconRes ?: R.mipmap.ic_launcher))
    }

    fun notify(context: Context, builder: NotificationCompat.Builder, id: Int) {
        this@DMSNotification.getNotificationManager(context).notify(id, builder.build())
    }

    fun getNotificationManager(context: Context): NotificationManager {
        return context.applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    fun cancel(context: Context, notificationId: Int) {
        this@DMSNotification.getNotificationManager(context).cancel(notificationId)
    }

}
