package asia.craftbox.android.jobway.entities.responses.test

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.contribs.responses.ListDMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.responses.test
 * Date: Jan 18, 2019 - 11:12 AM
 */


class HOLLANDTestResponse : ListDMSRestResponse<HOLLANDTestResponse.Item>() {

    class Item {
        @SerializedName("group")
        var group: Group? = null

        @SerializedName("questions")
        var questions: List<Question> = emptyList()
    }


    class Group : DMSJsonableObject {

        @SerializedName("id")
        var id: Long? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("cd")
        var cd: String? = null

    }

    class Question : DMSJsonableObject {

        @SerializedName("id")
        var id: Long? = null

        @SerializedName("name")
        var name: String? = null

    }

}
