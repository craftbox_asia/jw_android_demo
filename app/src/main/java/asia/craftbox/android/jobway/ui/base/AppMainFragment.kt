package asia.craftbox.android.jobway.ui.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.ViewDataBinding
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.base
 * Date: Feb 18, 2019 - 11:27 AM
 */


abstract class AppMainFragment<T : ViewDataBinding>(@LayoutRes layoutResId: Int) : DMSBindingFragment<T>(layoutResId) {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (this@AppMainFragment.activity as? MainActivity)
            ?.dataBinding
            ?.toolbarDisabledHeaderCover = true
        Timber.d(this.toString())
    }

    override fun onResume() {
        super.onResume()
        Timber.d(this.toString())
    }

}
