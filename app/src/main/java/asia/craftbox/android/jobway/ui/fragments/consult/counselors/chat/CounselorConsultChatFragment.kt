package asia.craftbox.android.jobway.ui.fragments.consult.counselors.chat

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultChatBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BaseDMSViewModel
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import timber.log.Timber


class CounselorConsultChatFragment :
    AppMainFragment<FragmentCounselorConsultChatBinding>(R.layout.fragment_counselor_consult_chat) {


    val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultChatFragment)[BaseDMSViewModel::class.java]
    }

    val presenter by lazy {
        CounselorConsultChatPresenter(this)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        dataBinding.viewModel = viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
        val counselorModel = ShareData.getInstance().currentCounselor ?: return
        loadUIFromData(counselorModel)

    }

    override fun initUI() {
        super.initUI()

        val chatRecyclerLayout = LinearLayoutManager(this.requireContext())
        chatRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultChatRecyclerView.layoutManager = chatRecyclerLayout
        dataBinding.counselorConsultChatRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultChatRecyclerView.adapter = presenter.counselorConsultChatAdapter

//        dataBinding.refreshLayout.setOnRefreshListener {
//            getListMessageAPI(true)
//        }
    }


    private fun loadUIFromData(counselorModel: CounselorModel) {
        presenter.currentCounselorModel = counselorModel
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.nameTextView.text = counselorModel.getFullName()
    }
}
