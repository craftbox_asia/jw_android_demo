package asia.craftbox.android.jobway.ui.fragments.uni.admission

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUniDetailAdmissionBinding
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniViewModel
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.admission
 * Date: Feb 14, 2019 - 9:43 PM
 */


class AdmissionUniFragment :
    DMSBindingFragment<FragmentUniDetailAdmissionBinding>(R.layout.fragment_uni_detail_admission),
    DMSViewModelSource<DetailsUniViewModel> {

    override val viewModel: DetailsUniViewModel by lazy {
        (this@AdmissionUniFragment.activity as DetailsUniActivity).viewModel
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@AdmissionUniFragment.dataBinding.viewModel = this@AdmissionUniFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initUI() {
        super.initUI()
        // optimize webview contents
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.displayZoomControls = false
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.builtInZoomControls = true
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.javaScriptEnabled = true
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.loadWithOverviewMode = true
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.useWideViewPort = true
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.setSupportZoom(false)
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.settings.setAppCacheEnabled(true)
        this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView.isEnabled = false
    }

    override fun initObservables() {
        super.initObservables()
        this@AdmissionUniFragment.viewModel.getUni().observe(this@AdmissionUniFragment, Observer {
            val uniItem = it ?: return@Observer
            val recruitment = uniItem.recruitment?: ""
            recruitment.attachJWHtmlIntoWebView(this@AdmissionUniFragment.dataBinding.uniDetailAdmissionWebView)
        })
    }

}
