package asia.craftbox.android.jobway.ui.fragments.uni.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.entities.services.UniAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.entities.DMSViewModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.search
 * Date: Feb 18, 2019 - 4:05 PM
 */


class SearchUniDatasource : PageKeyedDataSource<Int, UniModel>() {

    private val keywordMutable = MutableLiveData<String>()
    private var listener: DMSViewModel.Listener? = null


    fun setKeyword(keyword: String?) {
        this@SearchUniDatasource.keywordMutable.postValue(keyword ?: "")
    }

    fun getKeyword(): LiveData<String> {
        return this@SearchUniDatasource.keywordMutable
    }

    fun setListener(block: DMSViewModel.Listener? = null) {
        this@SearchUniDatasource.listener = block
    }

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, UniModel>) {
        val keyword = this@SearchUniDatasource.getKeyword().value ?: return
        if (keyword.count() < 3) return

        DMSRestPromise(
            DMSRest.get(UniAPIService::class.java).searchUni(
                filter = keyword.trim(),
                page = 1
            )
        )
            .then { promise, res ->
                val uniItems = res.data?.data ?: emptyList()
                val currentPage = res.data?.meta?.currentPage ?: 1
                val totalPage = res.data?.meta?.totalPages ?: 1
                val nextPage = if (currentPage + 1 < totalPage) currentPage + 1 else null

                callback.onResult(uniItems, null, nextPage)
                this@SearchUniDatasource.listener?.onAPIResponseSuccess(promise, res)
            }
            .catch { promise, exception ->
                this@SearchUniDatasource.listener?.onAPIResponseError(promise, exception)
            }
            .doBeforeRequest { this@SearchUniDatasource.listener?.onAPIWillRequest(it) }
            .doAfterResponse {
                this@SearchUniDatasource.listener?.onAPIDidFinished(it)
                this@SearchUniDatasource.listener?.onAPIDidFinishedAll()
            }
            .setTaskNameResId(R.string.task_api_uni_search)
            .submit()
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, UniModel>) {}

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, UniModel>) {
        val keyword = this@SearchUniDatasource.getKeyword().value ?: return
        if (keyword.count() < 3) return

        DMSRestPromise(
            DMSRest.get(UniAPIService::class.java).searchUni(
                filter = keyword.trim(),
                page = params.key
            )
        )
            .then { promise, res ->
                val uniItems = res.data?.data ?: emptyList()
                val currentPage = res.data?.meta?.currentPage ?: 1
                val totalPage = res.data?.meta?.totalPages ?: 1
                val nextPage = if (currentPage + 1 < totalPage) currentPage + 1 else null

                callback.onResult(uniItems, nextPage)
                this@SearchUniDatasource.listener?.onAPIResponseSuccess(promise, res)
            }
            .catch { promise, exception ->
                this@SearchUniDatasource.listener?.onAPIResponseError(promise, exception)
            }
            .doBeforeRequest { this@SearchUniDatasource.listener?.onAPIWillRequest(it) }
            .doAfterResponse {
                this@SearchUniDatasource.listener?.onAPIDidFinished(it)
                this@SearchUniDatasource.listener?.onAPIDidFinishedAll()
            }
            .setTaskNameResId(R.string.task_api_uni_search)
            .submit()
    }

}
