package asia.craftbox.android.jobway.ui.fragments.news.list

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.services.NewsAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list
 * Date: Feb 13, 2019 - 2:39 PM
 */


class ListNewsViewModel : DMSViewModel() {

    private val newsPerPage = 10
    private var currentPage = 0
    private var totalPages = Int.MAX_VALUE


    private val fetchNewsListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@ListNewsViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                if ((this@ListNewsViewModel.currentPage + 1) <= this@ListNewsViewModel.totalPages) {
                    this@ListNewsViewModel.request(DMSRest.get(NewsAPIService::class.java).getPosts(page = this@ListNewsViewModel.currentPage + 1))
                        .setTaskNameResId(R.string.task_api_news_list_retrieve)
                        .then { _, data ->
                            this@ListNewsViewModel.currentPage = data.data?.meta?.currentPage ?: 1
                            this@ListNewsViewModel.totalPages = data.data?.meta?.totalPages ?: Int.MAX_VALUE

                            data.data?.data?.forEach { it.saveOrUpdate() }
                        }
                        .catch { _, exc ->
                            Timber.wtf(exc.original)
                        }
                        .submit()
                }
            })
        return@lazy instance
    }

    private val fetchHotNewsListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@ListNewsViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@ListNewsViewModel.request(DMSRest.get(NewsAPIService::class.java).hotPosts())
                    .setTaskNameResId(R.string.task_api_news_hot_list_retrieve)
                    .then { _, data ->
                        AppData.get().getNews().cleanupHotNews()
                        data.data?.forEach {
                            it.hotNews = 1
                            it.saveOrUpdate()
                        }
                    }
                    .catch { _, exc ->
                        Timber.wtf(exc.original)
                    }
                    .submit()
            })
        return@lazy instance
    }

    private val newsOnRequestFetching = object : PagedList.BoundaryCallback<NewsModel>() {

        override fun onItemAtEndLoaded(itemAtEnd: NewsModel) {
            super.onItemAtEndLoaded(itemAtEnd)
            this@ListNewsViewModel.fetchNewsListTask.onNext(true)
        }

    }

    fun doFetchNews() {
        this@ListNewsViewModel.currentPage = 0
        this@ListNewsViewModel.totalPages = Int.MAX_VALUE
        this@ListNewsViewModel.fetchNewsListTask.onNext(true)
    }

    fun getHotNews(): LiveData<List<NewsModel>> {
        return AppData.get().getNews().getHotNews()
    }

    fun getLatestNews(): LiveData<PagedList<NewsModel>> {
        val factory = AppData.get().getNews().getLatestNewsPaginated()
        return factory.toLiveData(
            pageSize = this@ListNewsViewModel.newsPerPage,
            boundaryCallback = this@ListNewsViewModel.newsOnRequestFetching
        )
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@ListNewsViewModel.doFetchNews()
        this@ListNewsViewModel.fetchHotNewsListTask.onNext(true)
    }

}
