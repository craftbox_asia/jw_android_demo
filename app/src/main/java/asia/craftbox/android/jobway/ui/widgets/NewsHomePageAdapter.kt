package asia.craftbox.android.jobway.ui.widgets

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ItemPagerHomeNewsBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.ui.activities.news.details.DetailsNewsActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import kotlinx.android.synthetic.main.item_pager_home_news.*
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Jan 16, 2019 - 4:57 PM
 */


class NewsHomePageAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val items = ArrayList<FragmentItem>()

    fun updateItems(items: List<FragmentItem>) {
        this@NewsHomePageAdapter.items.clear()
        this@NewsHomePageAdapter.items.addAll(items)
        this@NewsHomePageAdapter.notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return this@NewsHomePageAdapter.items[position]
    }

    override fun getCount(): Int {
        return this@NewsHomePageAdapter.items.count()
    }


     class FragmentItem : Fragment() {

        private var dataBinding: ItemPagerHomeNewsBinding? = null
        private val presenter = FragmentItemPresenter()


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            this@FragmentItem.dataBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.item_pager_home_news,
                container,
                false
            )
            return this@FragmentItem.dataBinding!!.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            this@FragmentItem.dataBinding?.presenter = this@FragmentItem.presenter
            super.onViewCreated(view, savedInstanceState)

            try {
                val bundle = this@FragmentItem.arguments ?: return
                this@FragmentItem.presenter.title.set(bundle.getString(TITLE_STRING))
                this@FragmentItem.presenter.subTitle.set(bundle.getString(SUBTITLE_STRING))
                this@FragmentItem.presenter.imageSource.set(bundle.getString(COVER_PHOTO))
                this@FragmentItem.presenter.modelType = bundle.getString(MODEL_TYPE)
                this@FragmentItem.presenter.modelId = bundle.getLong(MODEL_ID)
                this@FragmentItem.presenter.modelViews.set( getString(R.string.common_label_post_views) + ": " + bundle.getInt(MODEL_VIEWS)?.toString())
                this@FragmentItem.presenter.isEdu = bundle.getBoolean(IS_EDU)
                if(this@FragmentItem.presenter.isEdu){
                    new_description.visibility = GONE
                    edu_description.visibility = VISIBLE
                    val uni: UniModel = bundle.get(UNI_OBJ) as UniModel
                    if (uni != null) {
                        this@FragmentItem.presenter.uniType.set(uni.uniTypeName + " - " + uni.uniClassName)
                        this@FragmentItem.presenter.uniCode.set(uni.uniCode)
                    }
                } else{
                    new_description.visibility = VISIBLE
                    edu_description.visibility = GONE
                }

                view.findViewById<CardView?>(R.id.pager_home_news_card_view)?.setOnClickListener {
                    this@FragmentItem.parentFragment?.activity?.let { mainActivity ->
                        when (this@FragmentItem.presenter.modelType) {
                            NewsModel::class.java.simpleName -> {
                                val newsActivity = Intent(mainActivity, DetailsNewsActivity::class.java)
                                newsActivity.putExtra(
                                    DetailsNewsActivity.INTENT_DATA_NEWS_ID,
                                    this@FragmentItem.presenter.modelId
                                )
                                mainActivity.startActivity(newsActivity)
                            }
                            UniModel::class.java.simpleName -> {
                                val uniActivity = Intent(mainActivity, DetailsUniActivity::class.java)
                                uniActivity.putExtra(
                                    DetailsUniActivity.INTENT_DATA_UNI_MODEL_ID,
                                    this@FragmentItem.presenter.modelId
                                )
                                mainActivity.startActivity(uniActivity)
                            }
                        }
                    }
                }

            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }


        class FragmentItemPresenter {

            val title = ObservableField<String>()
            val subTitle = ObservableField<String>()
            val imageSource = ObservableField<String>()
            var modelType = NewsModel::class.java.simpleName
            var modelId: Long = -1
            var modelViews = ObservableField<String>()
            var isEdu: Boolean = false
            var uniType = ObservableField<String>()
            var uniCode = ObservableField<String>()
        }


        companion object {
            internal const val TITLE_STRING = "TITLE_STRING"
            internal const val SUBTITLE_STRING = "SUBTITLE_STRING"
            internal const val COVER_PHOTO = "COVER_PHOTO"
            internal const val MODEL_TYPE = "MODEL_TYPE"
            internal const val MODEL_ID = "MODEL_ID"
            internal const val MODEL_VIEWS = "MODEL_VIEWS"
            internal const val IS_EDU = "IS_EDU"
            internal const val UNI_OBJ = "UNI_OBJ"

            fun create(
                title: String,
                views: Int,
                subtitle: String,
                coverPhoto: String,
                modelType: String,
                modelId: Long,
                isEdu: Boolean,
                uni: UniModel?
            ): FragmentItem {
                val instance = FragmentItem()
                instance.arguments = Bundle()
                instance.arguments?.putString(TITLE_STRING, title)
                instance.arguments?.putString(SUBTITLE_STRING, subtitle)
                instance.arguments?.putString(COVER_PHOTO, coverPhoto)
                instance.arguments?.putString(MODEL_TYPE, modelType)
                instance.arguments?.putLong(MODEL_ID, modelId)
                instance.arguments?.putInt(MODEL_VIEWS, views)
                instance.arguments?.putBoolean(IS_EDU, isEdu)
                instance.arguments?.putSerializable(UNI_OBJ, uni)
                return instance
            }
        }
    }
}
