package asia.craftbox.android.jobway.ui.fragments.consult.counselors.subtract_point

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose.CounselorConsultComposeFragment
import asia.craftbox.android.jobway.ui.fragments.earn_point.EarnPointFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorConsultSubtractionPointPresenter(private val fragment: CounselorConsultSubtractionPointFragment) : BaseObservable() {

    fun closeButtonClicked(sender: View?) {
        fragment.dismiss()
    }

    fun agreeButtonClicked(sender: View?) {
        val success = ShareData.getInstance().canSendMessage()
        if (success) {
            val mainActivity = fragment.activity as? MainActivity
            val composeFragment = DMSFragment.create(CounselorConsultComposeFragment::class.java)
            fragment.dismiss()
            mainActivity?.getFragmentHelper()?.pushFragment(composeFragment)
        } else {
            fragment.dismiss()
        }
    }

    fun getMorePointButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        val composeFragment = DMSFragment.create(EarnPointFragment::class.java)
        fragment.dismiss()
        mainActivity?.getFragmentHelper()?.pushFragment(composeFragment)
    }
}
