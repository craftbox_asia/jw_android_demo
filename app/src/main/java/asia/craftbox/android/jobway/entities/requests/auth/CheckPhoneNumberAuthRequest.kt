package asia.craftbox.android.jobway.entities.requests.auth

import android.util.Patterns
import androidx.databinding.Bindable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.auth
 * Date: Jan 09, 2019 - 2:42 PM
 */


@Suppress("UNUSED_PARAMETER")
class CheckPhoneNumberAuthRequest : DMSRestRequest() {

    @SerializedName("phone")
    var phone: String? = null
        @Bindable get

    @Expose(serialize = false, deserialize = false)
    var countryCode: String = "84"
        @Bindable get

    override fun validates(): Boolean {
        val phoneNum = this@CheckPhoneNumberAuthRequest.phone ?: return false
        val cntCode = this@CheckPhoneNumberAuthRequest.countryCode

        // phone number is valid pattern
        if (!Patterns.PHONE.matcher(phoneNum).matches()) {
            return false
        }

        // phone number at least 10 characters and has one of prefixs: 0, countryCode, +countryCode
        // or may be just phone number, not include country code
        return (phoneNum.count() >= 10 && (arrayOf("0", cntCode, "+$cntCode").any { phoneNum.startsWith(it) }))
                || (phoneNum.count() >= 9 && !phoneNum.startsWith("0"))
    }

}
