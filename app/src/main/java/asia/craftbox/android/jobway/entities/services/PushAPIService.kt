package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.requests.notification.UpdateTokenNotificationRequest
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.services
 * Date: Jan 18, 2019 - 10:47 AM
 */


interface PushAPIService {

    @POST("notification/v1/update-notification-token")
    @Headers("Content-Type: application/json")
    fun updateToken(@Body request: UpdateTokenNotificationRequest): Observable<Unit>

}
