package asia.craftbox.android.jobway.ui.fragments.pages.home.adapters

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.annotation.*
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderHomeMainItemBinding
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home.adapters
 * Date: Dec 28, 2018 - 10:38 PM
 */


class HomePageMenuAdapter(context: Context) :
    DMSSingleModelRecyclerAdapter<HomePageMenuAdapter.MenuItem, HomePageMenuAdapter.HomePageMenuViewHolder>() {

    data class MenuItem(
        @IdRes val menuId: Int,
        @DrawableRes val iconRes: Int,
        @StringRes val titleRes: Int,
        @ColorInt val colorInt: Int,
        var badgeCounts: Int = 0
    )

    enum class MenuItemMapped(@IdRes val itemId: Int, @DrawableRes val drawableRes: Int, @StringRes val stringRes: Int, @ColorInt val color: Int) {
        Consulting(
            R.id.main_item_consulting,
            R.drawable.ic_main_item_consulting,
            R.string.main_item_consulting,
            Color.parseColor("#FD97CA")
        ),
        News(
            R.id.main_item_news,
            R.drawable.ic_main_item_news,
            R.string.main_item_news,
            Color.parseColor("#FD7575")
        ),
        ExploreSelf(
            R.id.main_item_explore_self,
            R.drawable.ic_main_item_explore_self,
            R.string.main_item_explore_self,
            Color.parseColor("#FFC543")
        ),
        Job(
            R.id.main_item_job,
            R.drawable.ic_main_item_job,
            R.string.main_item_job,
            Color.parseColor("#A4A9FB")
        ),
        EduInfo(
            R.id.main_item_uni_info,
            R.drawable.ic_main_item_uni_info,
            R.string.main_item_uni_info,
            Color.parseColor("#5BDDC5")
        ),
        Gift(
            R.id.main_item_gift,
            R.drawable.ic_main_item_gift,
            R.string.main_item_gift,
            Color.parseColor("#7FDC72")
        );

        companion object {
            fun getItemById(@IdRes itemId: Int): MenuItemMapped? {
                return MenuItemMapped.values().firstOrNull { it.itemId == itemId }
            }
        }
    }


    private var onMenuItemClicked: ((menuItem: MenuItem) -> Unit)? = null


    fun setOnMenuClickListener(block: ((menuItem: MenuItem) -> Unit)? = null) {
        this@HomePageMenuAdapter.onMenuItemClicked = block
    }


    fun setBadgeCounts(@IdRes menuItemId: Int, badgeCounts: Int) {
        try {
            val index = this@HomePageMenuAdapter.actualItems.indexOfFirst { it.menuId == menuItemId }
            if (index >= 0 && index < this@HomePageMenuAdapter.actualItems.count()) {
                this@HomePageMenuAdapter.actualItems[index].badgeCounts = badgeCounts
                this@HomePageMenuAdapter.notifyItemChanged(index)
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomePageMenuViewHolder {
        return HomePageMenuViewHolder(parent)
    }

    override fun onCheckItemContents(oldItem: MenuItem, newItem: MenuItem): Boolean {
        return oldItem.menuId == newItem.menuId
                && oldItem.iconRes == newItem.iconRes
                && oldItem.titleRes == newItem.titleRes
                && oldItem.badgeCounts == newItem.badgeCounts
    }

    override fun onCheckItemId(oldItem: MenuItem, newItem: MenuItem): Boolean {
        return oldItem.menuId == newItem.menuId
    }

    private fun inflateMenu(context: Context, @MenuRes menuResId: Int) {
        val popupMenu = PopupMenu(context, null)
        popupMenu.inflate(menuResId)

        val menuItems = popupMenu.menu?.children?.mapNotNull { menuItem ->
            val menuMappedItem = MenuItemMapped.getItemById(menuItem.itemId)
                ?: return@mapNotNull null
            return@mapNotNull MenuItem(
                menuId = menuItem.itemId,
                iconRes = menuMappedItem.drawableRes,
                titleRes = menuMappedItem.stringRes,
                colorInt = menuMappedItem.color
            )
        }?.toList()

        this@HomePageMenuAdapter.updateItems(menuItems)
    }

    init {
        this@HomePageMenuAdapter.inflateMenu(
            context,
            R.menu.home_main_menu
        )
    }


    inner class HomePageMenuViewHolder(parent: ViewGroup) :
        DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<HomePageMenuAdapter.MenuItem>(
            parent,
            R.layout.viewholder_home_main_item
        ) {

        private val dataBinding =
            DataBindingUtil.bind<ViewholderHomeMainItemBinding>(this@HomePageMenuViewHolder.itemView)


        override fun bind(item: MenuItem, position: Int) {
            super.bind(item, position)

            this@HomePageMenuViewHolder.dataBinding?.presenter = HomePageMenuPresenter(item, position)
            this@HomePageMenuViewHolder.dataBinding?.invalidateAll()
        }


        inner class HomePageMenuPresenter(val item: MenuItem, val pos: Int) {

            fun onMenuItemClicked(sender: View?) {
                this@HomePageMenuAdapter.onMenuItemClicked?.invoke(
                    this@HomePageMenuPresenter.item
                )
            }

            private fun getContext(): Context {
                return this@HomePageMenuViewHolder.itemView.context
            }

            fun getTitle(): String? {
                return try {
                    val stringRes = this@HomePageMenuPresenter.item.titleRes
                    this@HomePageMenuPresenter.getContext().getString(stringRes)
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    null
                }
            }

            @ColorInt
            fun getBackgroundColor(): Int? {
                return this@HomePageMenuPresenter.item.colorInt
            }

        }

    }

}
