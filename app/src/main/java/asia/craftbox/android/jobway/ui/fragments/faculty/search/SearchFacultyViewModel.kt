package asia.craftbox.android.jobway.ui.fragments.faculty.search

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import asia.craftbox.android.jobway.EventBus.FacultyResponseEvent
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.entities.services.JobAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit

class SearchFacultyViewModel: DMSViewModel() {

    private val searchDebounceTask by lazy {
        val instance = PublishSubject.create<String>()
        this@SearchFacultyViewModel.compositeDisposable.add(
            instance.debounce(300, TimeUnit.MILLISECONDS).subscribe {
                this@SearchFacultyViewModel.request(DMSRest.get(JobAPIService::class.java).searchFaculty(
                    filter = it.trim(),
                    page = 1
                )).then{ _, responseData ->
                    Log.i("data", responseData.toString())
                    EventBus.getDefault().post(FacultyResponseEvent(responseData.data?.data!!))
                }.submit()
            })
        return@lazy instance
    }

    fun doSearch(keyword: CharSequence?) {
        if (keyword.isNullOrEmpty() || keyword?.toString()!!.count() < 3) {
            return
        }
        this@SearchFacultyViewModel.searchDebounceTask.onNext(keyword?.toString() ?: "")
    }
}