package asia.craftbox.android.jobway.entities.models

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Ignore
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSJsonableObject
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Jan 09, 2019 - 4:09 PM
 */


@Entity
class ProfileModel : AppModel() {

    @SerializedName("role_id")
    var roleId: Long? = null

    @SerializedName("city_id")
    var cityId: Long? = null

    @SerializedName("status")
    var status: Int = 0

    @SerializedName("coin")
    var coin: Int = 0

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("personal_code")
    var personalCode: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("date_of_birth")
    var dateOfBirth: String? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("role")
    @Embedded(prefix = "ROLE_")
    var role: Role? = null

    @SerializedName("user_results")
    @Ignore
    var userResults: UserResults? = null


    fun getDisplayName(): String {
        return arrayOf(
            this@ProfileModel.firstName,
            this@ProfileModel.lastName
        ).filterNotNull()
            .joinToString(" ")
    }

    fun isFinishedTest(): Boolean {
        return AppData.get().getTest()
            .countTestsByUserIdFlatten(this@ProfileModel.id ?: -1) > 1
    }

    fun isFinishedProfile(): Boolean {
        return AppData.get().getTest()
            .countProfilesByUserIdFlatten(this@ProfileModel.id ?: -1) > 0
    }

    fun countCompletedItems(): Int {
        return AppData.get().getTest()
            .countCompletedItems(this@ProfileModel.id ?: -1)
    }

    fun getStringifyProfileProgress(): String? {
        return "${this@ProfileModel.countCompletedItems()}/2"
    }

    fun getProfileResult(): ResultProfileModel? {
        return AppData.get().getTest().getProfileResultByUserIdFlatten(this@ProfileModel.id)
    }

    fun isStudent(): Boolean {
        return role?.cd?.toLowerCase().equals("st")
    }

    fun isMaster() : Boolean {
        if (role == null) return false
        if (role!!.cd == null) return false
        val cd = role!!.cd!!.toUpperCase()
        return (cd == CounselorRole.EX.toString() || cd == CounselorRole.CO.toString())
    }

    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getProfile().getProfileByIdFlatten(this@ProfileModel.onCopyPrimaryKey())
            ?: this@ProfileModel

        DMSModel.mergeFields(this@ProfileModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()
        AppData.get().getProfile().insertOne(instance)

        // update test entities
        AppData.get().getTest().cleanUpResultTests()
        instance.userResults?.let { exploreData ->
            exploreData.holland?.userId = instance.id
            exploreData.holland?.type = CreateTestRequest.TestType.Holland.typeCode
            exploreData.holland?.saveOrUpdate()

            exploreData.mbti?.userId = instance.id
            exploreData.mbti?.type = CreateTestRequest.TestType.MBTI.typeCode
            exploreData.mbti?.saveOrUpdate()

            exploreData.profile?.saveOrUpdate()
        }

        return instance.onCopyPrimaryKey()
    }


    class Role : DMSJsonableObject {

        @SerializedName("cd")
        var cd: String? = null

        @SerializedName("name")
        var name: String? = null

    }

    class UserResults : DMSJsonableObject {

        @SerializedName("HOLLAND")
        var holland: ResultTestModel? = null

        @SerializedName("MBTI")
        var mbti: ResultTestModel? = null

        @SerializedName("PROFILE")
        var profile: ResultProfileModel? = null

    }

}
