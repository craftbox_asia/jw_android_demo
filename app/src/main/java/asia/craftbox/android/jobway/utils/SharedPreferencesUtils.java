package asia.craftbox.android.jobway.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesUtils {

    private static Context mContext;
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;

    private static void instance(Context context){
        if (pref == null){
            prefInstance(context);
        }

        editor = pref.edit();
    }

    private static void prefInstance(Context context) {
        mContext = context;
        if (pref == null) {
            pref = mContext.getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        }
    }

    public static void putBoolValue(Context context,String key, Boolean value){
        if (editor == null) {
            instance(context);
        }
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolValue(Context context,String key) {
        if (pref == null) {
            prefInstance(context);
        }
        return pref.getBoolean(key, false);
    }

    public static void clearAll(Context context) {
        if (editor == null) {
            instance(context);
        }
        editor.clear();
        editor.commit();
    }
}
