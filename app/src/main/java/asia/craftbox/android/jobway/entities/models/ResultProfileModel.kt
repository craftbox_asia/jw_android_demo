package asia.craftbox.android.jobway.entities.models

import androidx.room.Entity
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Jan 25, 2019 - 2:51 PM
 */


/**
{"id":8,"user_id":31,"hobbies":null,"advantage":null,"defect":null,"career":null,"job_idol":null,"affect_people":null,"be_evaluate":null,"be_advice":null,"ac":1}
 */
@Entity
class ResultProfileModel : DMSModel() {

    @SerializedName("user_id")
    var userId: Long? = null

    @SerializedName("hobbies")
    var hobbies: String? = null

    @SerializedName("advantage")
    var advantage: String? = null

    @SerializedName("defect")
    var defect: String? = null

    @SerializedName("career")
    var career: String? = null

    @SerializedName("job_idol")
    var jobIdol: String? = null

    @SerializedName("affect_people")
    var affectPeople: String? = null

    @SerializedName("be_evaluate")
    var beEvaluate: String? = null

    @SerializedName("be_advice")
    var beAdvice: String? = null

    @SerializedName("ac")
    var ac: Int = 0


    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getTest().getProfileResultByUserIdFlatten(this@ResultProfileModel.userId)
            ?: this@ResultProfileModel

        DMSModel.mergeFields(this@ResultProfileModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()

        return AppData.get().getTest().insertOneResultProfile(instance)
    }

}
