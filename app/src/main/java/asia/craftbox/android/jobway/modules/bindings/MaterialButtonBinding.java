package asia.craftbox.android.jobway.modules.bindings;

import android.content.res.ColorStateList;
import androidx.databinding.BindingAdapter;
import com.google.android.material.button.MaterialButton;

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.modules.bindings
 * Date: Jan 29, 2019 - 9:44 PM
 */

public class MaterialButtonBinding {

    @BindingAdapter("materialButtonBackgroundTint")
    public static void materialButtonBackgroundTint(MaterialButton button, int colorInt) {
        button.setBackgroundTintList(
                new ColorStateList(
                        new int[][]{new int[]{}},
                        new int[]{colorInt}
                )
        );
    }

}
