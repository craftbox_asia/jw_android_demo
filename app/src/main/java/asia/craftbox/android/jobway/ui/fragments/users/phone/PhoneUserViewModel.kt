package asia.craftbox.android.jobway.ui.fragments.users.phone

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.requests.auth.CheckPhoneNumberAuthRequest
import asia.craftbox.android.jobway.entities.requests.auth.RegisterAuthRequest
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.users.phone
 * Date: Jan 13, 2019 - 8:19 PM
 */


class PhoneUserViewModel : DMSViewModel() {

    private val registerAuthRequest = RegisterAuthRequest()
    private val screenStateMutable = MutableLiveData<PhoneUserState>()


    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@PhoneUserViewModel.screenStateMutable.postValue(PhoneUserState.EnterPhoneNumber)
    }

    fun getPhoneRequest(): RegisterAuthRequest {
        return this@PhoneUserViewModel.registerAuthRequest
    }

    fun getScreenState(): LiveData<PhoneUserState> {
        return this@PhoneUserViewModel.screenStateMutable
    }

    fun doCheckPhoneNumber() {
        this@PhoneUserViewModel.checkPhoneTask.onNext(this@PhoneUserViewModel.registerAuthRequest)
    }

    private val checkPhoneTask by lazy {
        val instance = PublishSubject.create<RegisterAuthRequest>()
        this@PhoneUserViewModel.compositeDisposable.add(
            instance.throttleFirst(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread()).subscribe { requester ->
                val checkPhoneRequest = CheckPhoneNumberAuthRequest()
                checkPhoneRequest.phone = requester.getValidPhoneNumber()

                this@PhoneUserViewModel.request(
                    DMSRest.get(AuthAPIService::class.java).checkPhoneNumber(
                        checkPhoneRequest
                    )
                )
                    .then { _, response ->
                        val isAccountRegistered = response.data?.isExist ?: false

                        if (!isAccountRegistered) {
                            this@PhoneUserViewModel.screenStateMutable.postValue(PhoneUserState.EnterProfileInfo)
                        }
                    }
                    .setTaskNameResId(R.string.task_api_auth_check_phone_number)
                    .submit()
            }
        )
        return@lazy instance
    }

}
