package asia.craftbox.android.jobway.ui.activities.uni.details

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityUniDetailBinding
import asia.craftbox.android.jobway.ui.activities.uni.details.adapters.DetailsUniPagerAdapter
import asia.craftbox.android.jobway.ui.fragments.uni.admission.AdmissionUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.gallery.GalleryUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.intro.IntroUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.speciality.SpecialityUniFragment
import com.google.android.material.tabs.TabLayout
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.uni.details
 * Date: Feb 14, 2019 - 8:35 PM
 */


class DetailsUniActivity : DMSBindingActivity<ActivityUniDetailBinding>(R.layout.activity_uni_detail),
    DMSViewModelSource<DetailsUniViewModel> {

    private val presenter by lazy {
        DetailsUniPresenter()
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@DetailsUniActivity)[DetailsUniViewModel::class.java]
    }

    private val fragmentHelper by lazy {
        DMSFragmentHelper(this@DetailsUniActivity.supportFragmentManager, R.id.uni_detail_container)
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            val uniId = this@DetailsUniActivity.intent.getLongExtra(INTENT_DATA_UNI_MODEL_ID, -1)
            this@DetailsUniActivity.viewModel.setupUniWithId(uniId)
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        }
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DetailsUniActivity.dataBinding.presenter = this@DetailsUniActivity.presenter
        this@DetailsUniActivity.dataBinding.viewModel = this@DetailsUniActivity.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)

        if (savedInstanceState == null) {
            this@DetailsUniActivity.fragmentHelper.initialRootFragment(
                DMSFragment.create(
                    IntroUniFragment::class.java
                )
            )
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun initUI() {
        super.initUI()

        val tabLayout = this@DetailsUniActivity.dataBinding.uniDetailTabLayout

        val introTab = tabLayout.newTab()
            .setText(R.string.title_fragment_uni_detail_intro)
            .setTag("${R.string.title_fragment_uni_detail_intro}")
        tabLayout.addTab(introTab)

        val facultyTab = tabLayout.newTab()
            .setText(R.string.title_fragment_uni_detail_speciality)
            .setTag("${R.string.title_fragment_uni_detail_speciality}")
        tabLayout.addTab(facultyTab)

        val admissionTab = tabLayout.newTab()
            .setText(R.string.title_fragment_uni_detail_admission)
            .setTag("${R.string.title_fragment_uni_detail_admission}")
        tabLayout.addTab(admissionTab)

        /**
        val galleryTab = tabLayout.newTab()
        .setText(R.string.title_fragment_uni_detail_gallery)
        .setTag("${R.string.title_fragment_uni_detail_gallery}")
        tabLayout.addTab(galleryTab)
         */

        introTab.select()
        tabLayout.addOnTabSelectedListener(this@DetailsUniActivity.presenter)
    }


    inner class DetailsUniPresenter : TabLayout.OnTabSelectedListener {

        internal var pagerAdapter: DetailsUniPagerAdapter? = null


        fun getContext(): Context? {
            return this@DetailsUniActivity
        }

        override fun onTabReselected(p0: TabLayout.Tab?) {

        }

        override fun onTabUnselected(p0: TabLayout.Tab?) {

        }

        fun onBackButtonClicked(sender: View?) {
            return this@DetailsUniActivity.finish()
        }

        override fun onTabSelected(p0: TabLayout.Tab?) {
            val tabItem = p0 ?: return
            val fragment = when (tabItem.tag) {
                "${R.string.title_fragment_uni_detail_intro}" -> DMSFragment.create(IntroUniFragment::class.java)
                "${R.string.title_fragment_uni_detail_speciality}" -> DMSFragment.create(SpecialityUniFragment::class.java)
                "${R.string.title_fragment_uni_detail_admission}" -> DMSFragment.create(AdmissionUniFragment::class.java)
                "${R.string.title_fragment_uni_detail_gallery}" -> DMSFragment.create(GalleryUniFragment::class.java)
                else -> null
            } ?: return
            this@DetailsUniActivity.fragmentHelper.pushFragment(
                fragment = fragment,
                replaceRootFragment = true
            )
        }
    }


    companion object {
        const val INTENT_DATA_UNI_MODEL_ID: String = "INTENT_DATA_UNI_MODEL_ID"
    }
}
