package asia.craftbox.android.jobway.entities.requests.auth

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.auth
 * Date: Jan 15, 2019 - 10:38 PM
 */


class CheckInvitationCodeRequest : DMSRestRequest() {

    @SerializedName("code")
    var code: String? = null


    override fun validates(): Boolean {
        return !this@CheckInvitationCodeRequest.code.isNullOrBlank()
    }

}
