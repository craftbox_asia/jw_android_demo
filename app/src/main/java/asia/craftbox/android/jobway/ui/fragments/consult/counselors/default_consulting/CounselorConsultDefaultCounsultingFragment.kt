package asia.craftbox.android.jobway.ui.fragments.consult.counselors.default_consulting

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultDefaultCounsultingBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.exts.fromHTML
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BaseDMSViewModel
import asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history.CounselorConsultHistoryDetailViewModel
import asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history.CounselorHistoryDetailFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter


class CounselorConsultDefaultCounsultingFragment :
    AppMainFragment<FragmentCounselorConsultDefaultCounsultingBinding>(R.layout.fragment_counselor_consult_default_counsulting) {


    val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultDefaultCounsultingFragment)[BaseDMSViewModel::class.java]
    }

    val presenter by lazy {
        CounselorConsultDefaultCounsultingPresenter(this)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        dataBinding.viewModel = viewModel
        super.onContentViewCreated(rootView, savedInstanceState)

        val counselorModel = ShareData.getInstance().currentCounselor ?: return
        loadUIFromData(counselorModel)
        gotoCounselorHistoryDetailFragment()
    }

    private fun gotoCounselorHistoryDetailFragment() {
        val userId = getCurrentUserId() ?: return
        alert.progressShow(R.string.task_api_get_list_history)
        val historyDetailViewModel = CounselorConsultHistoryDetailViewModel()
        historyDetailViewModel.getListHistory(userId, 1) {
            val size = it?.data?.size ?: 0
            alert.progressDismiss()
            if (size > 0) {
                ShareData.getInstance().isClickedNotification = false
                val mainActivity = activity as? MainActivity
                val historyDetailFragment = DMSFragment.create(CounselorHistoryDetailFragment::class.java)
                mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
                mainActivity?.getFragmentHelper()?.pushFragment(historyDetailFragment)
            }
        }
    }
    private fun getCurrentUserId(): Int? {
        if (ShareData.getInstance().isFromHistoryFrament(context!!)) {
            return ShareData.getInstance().currentHistoryMessage?.userId
        }
        return ShareData.getInstance().currentCounselor?.id?.toInt()
    }

    private fun loadUIFromData(counselorModel: CounselorModel) {
        presenter.currentCounselorModel = counselorModel
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.titleTextView.text = counselorModel.getFullName()
        val format = String.format(getString(R.string.text_hello_user_name), "<b>${ShareData.getInstance().getDisplayName()}</b>")
        dataBinding.nameTextView.text = format.fromHTML()
    }
}
