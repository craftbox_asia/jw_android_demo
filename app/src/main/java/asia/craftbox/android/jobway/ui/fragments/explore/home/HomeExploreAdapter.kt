package asia.craftbox.android.jobway.ui.fragments.explore.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import asia.craftbox.android.jobway.R

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.explore.home
 * Date: Jan 25, 2019 - 7:24 PM
 */


class HomeExploreAdapter(private val context: Context) : PagerAdapter() {

    data class ItemDetail(
        val drawableResId: Int,
        val titleResId: Int,
        val contentsResId: Int
    )


    private val itemsList = arrayOf(
        ItemDetail(R.drawable.multiple_choice_1, R.string.explore_home_title, R.string.explore_home_subtitle),
        ItemDetail(
            R.drawable.multiple_choice_2,
            R.string.explore_home_title_mbti,
            R.string.explore_home_subtitle_mbti
        ),
        ItemDetail(
            R.drawable.multiple_choice_3,
            R.string.explore_home_title_holland,
            R.string.explore_home_subtitle_holland
        ),
        ItemDetail(
            R.drawable.multiple_choice_4,
            R.string.explore_home_title_holland,
            R.string.explore_home_recommand_holland
        )
    )


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return this@HomeExploreAdapter.itemsList.count()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(this@HomeExploreAdapter.context).inflate(
            R.layout.layout_explore_home_item,
            container,
            false
        )
        container.addView(view)

        val viewDetail = this@HomeExploreAdapter.itemsList[position]
        view.findViewById<ImageView?>(R.id.explore_home_item_image)?.setImageResource(viewDetail.drawableResId)
        view.findViewById<TextView?>(R.id.explore_home_item_title)?.setText(viewDetail.titleResId)
        view.findViewById<TextView?>(R.id.explore_home_item_subtitle)?.setText(viewDetail.contentsResId)

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        (`object` as? View)?.let { container.removeView(it) }
    }

}
