package asia.craftbox.android.jobway.ui.fragments.uni.list.adapters

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderUniItemBinding
import asia.craftbox.android.jobway.entities.models.UniModel
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list.adapters
 * Date: Feb 14, 2019 - 7:37 PM
 */


class ListUniAdapter : PagedListAdapter<UniModel, ListUniAdapter.ViewHolder>(UniModel.DIFF) {

    private var onUniItemClickedBlock: ((item: UniModel, pos: Int) -> Unit)? = null

    fun setOnUniItemClicked(block: ((item: UniModel, pos: Int) -> Unit)?) {
        this@ListUniAdapter.onUniItemClickedBlock = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this@ListUniAdapter.getItem(position)?.let { unwrappedUni ->
            holder.bind(unwrappedUni, position)
        }
    }


    inner class ViewHolder(parent: ViewGroup) :
        DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<UniModel>(parent, R.layout.viewholder_uni_item) {

        private val dataBinding = DataBindingUtil.bind<ViewholderUniItemBinding>(this@ViewHolder.itemView)


        override fun bind(item: UniModel, position: Int) {
            super.bind(item, position)

            this@ViewHolder.dataBinding?.presenter = Presenter(item, position)
            this@ViewHolder.dataBinding?.invalidateAll()
        }


        inner class Presenter(val item: UniModel, private val position: Int) {

            fun getItemUniCode(): Spanned? {
                val localText = this@ViewHolder.itemView.context.getString(R.string.label_uni_uni_code)
                return this@Presenter.getHtml("$localText: <b>${this@Presenter.item.uniCode}</b>")
            }

            fun getItemUniClassName(): Spanned? {
                val localText = this@ViewHolder.itemView.context.getString(R.string.label_uni_name)
                val combineUniTypeNUniLevel = this@Presenter.item.uniTypeName + " - " + this@Presenter.item.uniClassName
                return this@Presenter.getHtml("$localText: <b>$combineUniTypeNUniLevel</b>")
            }

            fun onContainerClicked(@Suppress("UNUSED_PARAMETER") sender: View?) {
                this@ListUniAdapter.onUniItemClickedBlock?.invoke(
                    this@Presenter.item,
                    this@Presenter.position
                )
            }

            private fun getHtml(source: String) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                @Suppress("DEPRECATION")
                Html.fromHtml(source)
            }

        }

    }

}
