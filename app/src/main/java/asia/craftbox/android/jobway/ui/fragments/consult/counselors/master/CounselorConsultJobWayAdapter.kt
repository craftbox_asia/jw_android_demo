package asia.craftbox.android.jobway.ui.fragments.consult.counselors.master

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.CounselorModel
import com.makeramen.roundedimageview.RoundedImageView
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import kotlinx.android.synthetic.main.item_consult_master.view.*

class CounselorConsultJobWayAdapter(private val items: ArrayList<CounselorModel>, val context: Context) :
    RecyclerView.Adapter<CounselorConsultJobWayAdapter.ViewHolder>() {

    private var onContainClickedCallback: ((item: CounselorModel, pos: Int) -> Unit)? = null

    fun setCallback(callback: ((item: CounselorModel, pos: Int) -> Unit)?) {
        onContainClickedCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_consult_master, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val isLastItem = position == items.count() - 1
        holder.hideLastItemLineView(isLastItem)
        val item = items[position]
        holder.title_text_view?.text = item.getFullName()
        holder.description_text_view?.text = item.getDescriptionJob()
        holder.loadAvatar(item.avatar)

        holder.contain_linear_layout.setOnClickListener {
            this@CounselorConsultJobWayAdapter.onContainClickedCallback?.let { it(item, position)}
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rounded_image_view: RoundedImageView? = view.rounded_image_view
        val title_text_view: TextView? = view.title_text_view
        val description_text_view: TextView? = view.description_text_view
        val line_view: View? = view.line_view
        val contain_linear_layout: LinearLayout = view.contain_linear_layout

        fun hideLastItemLineView(hide: Boolean) {
            if (hide) {
                line_view?.visibility = View.INVISIBLE
            } else {
                line_view?.visibility = View.VISIBLE
            }
        }
        fun loadAvatar(avatar: String?) {
            ImageViewBindingAdapter.imageViewAsyncSrc(rounded_image_view, avatar, 0)
        }
    }
}