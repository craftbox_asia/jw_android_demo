package asia.craftbox.android.jobway.ui.fragments.faculty.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentFacultyListBinding
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.faculty.list.adapters.ListFacultyAdapter
import asia.craftbox.android.jobway.ui.fragments.faculty.search.SearchFacultyFragment
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import kotlinx.android.synthetic.main.fragment_faculty_list.*
import android.util.Log
import android.widget.AbsListView
import android.widget.ProgressBar
import asia.craftbox.android.jobway.EventBus.FacultyResponseEvent
import asia.craftbox.android.jobway.ui.fragments.faculty.search.SearchFacultyActivity
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list
 * Date: Feb 14, 2019 - 3:53 PM
 */


class ListFacultyFragment : AppMainFragment<FragmentFacultyListBinding>(R.layout.fragment_faculty_list),
    DMSViewModelSource<ListFacultyViewModel> {

    private val presenter by lazy {
        ListFacultyPresenter(this@ListFacultyFragment)
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@ListFacultyFragment)[ListFacultyViewModel::class.java]
    }

    var adapter: ListFacultyAdapter? = null
    lateinit var progressBar: ProgressBar
    var firstLoad: Boolean = true
    var stoploading : Boolean = false

    override fun getTitleResId(): Int? {
        return R.string.title_fragment_faculties_list
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ListFacultyFragment.dataBinding.presenter = this@ListFacultyFragment.presenter
        this@ListFacultyFragment.dataBinding.viewModel = this@ListFacultyFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
        progressBar = rootView.findViewById(R.id.progress_bar) as ProgressBar
        viewModel.doFetchFacultyList(progressBar)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: FacultyResponseEvent) {
        Log.i("aaa", event.res.toString())
        if (event.res.size < viewModel.itemPerPage){
            stoploading = true
        }
        if (firstLoad){
            adapter = ListFacultyAdapter(this@ListFacultyFragment.context!!, ArrayList(event.res))
            faculty_list_recycler_view.adapter = adapter
            firstLoad = false
            setupScrollListener()
        } else{
            adapter?.addMore(ArrayList(event.res))
        }

    }

    private fun setupScrollListener() {
        faculty_list_recycler_view.setOnScrollListener(object : AbsListView.OnScrollListener {
            override fun onScrollStateChanged(view: AbsListView, scrollState: Int) {

            }

            override fun onScroll(
                view: AbsListView, firstVisibleItem: Int,
                visibleItemCount: Int, totalItemCount: Int
            ) {
                if (firstLoad || stoploading)
                {
                    return
                }
                if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0 && !viewModel.loadingData) {
                    viewModel.fetchFacultyTask(progressBar!!)
                }
            }
        })
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_faculty_list_retrieve
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_faculty_list_retrieve) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    inner class ListFacultyPresenter(private val fragment:ListFacultyFragment) : BasePresenter<FragmentFacultyListBinding>(fragment = fragment) {

        fun onSearchContainerClicked(sender: View?) {
            val context = getMainActivity() as Context
            val intent = Intent(context , SearchFacultyActivity::class.java)
            context.startActivity(intent)
        }

        private fun getMainActivity(): MainActivity? {
            return this@ListFacultyFragment.activity as? MainActivity
        }
    }

}
