package asia.craftbox.android.jobway.ui.widgets

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import asia.craftbox.android.jobway.R
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Feb 14, 2019 - 9:18 AM
 */


class AppChipGroup : ChipGroup {

    private val chipItems = HashMap<String, Chip>()
    @Suppress("PrivatePropertyName")
    private val CHIP_TAG_PREFIX = AppChipGroup::class.java.simpleName.toUpperCase()


    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    fun addChipItem(text: String?) {
        if (text.isNullOrBlank()) {
            return
        }

        val chipItem = Chip(this@AppChipGroup.context)
        val chipTag = "${CHIP_TAG_PREFIX}_${System.currentTimeMillis()}"
        chipItem.tag = chipTag
        chipItem.text = text

        chipItem.setTextColor(Color.WHITE)
        chipItem.setChipBackgroundColorResource(R.color.colorCustomPink)
        chipItem.setChipCornerRadiusResource(R.dimen.app_dimen_input_radius)

        this@AppChipGroup.chipItems[chipTag] = chipItem
        this@AppChipGroup.addView(chipItem)
    }

    fun removeChip(byTag: String? = null, all: Boolean = false) {
        if (byTag != null && !byTag.isNullOrBlank()) {
            try {
                this@AppChipGroup.chipItems[byTag]?.let { this@AppChipGroup.removeView(it) }
            } catch (viewNotFound: Exception) {
                Timber.wtf(viewNotFound.localizedMessage)
            } finally {
                this@AppChipGroup.chipItems.remove(byTag)
            }
            return
        }

        if (all) {
            try {
                this@AppChipGroup.chipItems.values.forEach { this@AppChipGroup.removeView(it) }
            } catch (viewNotFound: Exception) {
                Timber.wtf(viewNotFound.localizedMessage)
            } finally {
                this@AppChipGroup.chipItems.clear()
            }
        }
    }

}
