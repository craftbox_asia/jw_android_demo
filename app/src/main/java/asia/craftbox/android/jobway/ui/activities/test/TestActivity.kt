package asia.craftbox.android.jobway.ui.activities.test

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityTestBinding
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.fragments.test.detail.DetailTestFragment
import asia.craftbox.android.jobway.ui.fragments.test.result.ResultTestFragment
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.test
 * Date: Jan 18, 2019 - 2:57 PM
 */


class TestActivity : DMSBindingActivity<ActivityTestBinding>(R.layout.activity_test),
    DMSViewModelSource<TestViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@TestActivity).get(TestViewModel::class.java)
    }

    private val presenter by lazy {
        TestPresenter(this@TestActivity)
    }

    private val fragmentHelper by lazy {
        DMSFragmentHelper(this@TestActivity.supportFragmentManager, R.id.test_container)
    }

    override fun getTitleResId(): Int? {
        return R.string.test_main
    }

    internal fun getFMHelper(): DMSFragmentHelper {
        return this@TestActivity.fragmentHelper
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
            gotoDetailTestFragment()
    }

    override fun onBackPressed() {
        if (!this@TestActivity.fragmentHelper.isCurrentFragmentBackable()) {
            return
        }

        if (this@TestActivity.fragmentHelper.popFragmentIfNeeded()) {
            return
        }

        super.onBackPressed()
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_test_get_holland,
            R.string.task_api_test_get_mbti,
            R.string.task_api_test_submit_test
        )
    }

    override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {
        if (arrayOf(
                R.string.task_api_test_get_holland,
                R.string.task_api_test_get_mbti
            ).contains(promise.taskNameResId)
        ) {
            val errorMessage = exception.getErrorMessage(this@TestActivity)
            this@TestActivity.alert.alert(errorMessage) {
                this@TestActivity.finish()
            }
            return
        }

        super.onAPIResponseError(promise, exception)
    }

    private fun gotoDetailTestFragment(bundle: Bundle? = null) {
        this@TestActivity.fragmentHelper.initialRootFragment(
            DMSFragment.create(
                fragmentClass = DetailTestFragment::class.java,
                bundle = bundle
            )
        )
    }

}
