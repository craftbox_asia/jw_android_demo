package asia.craftbox.android.jobway.ui.fragments.consult.counselors.master

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultMasterBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselor.CounselorConsultFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert.CounselorConsultExpertFragment
import asia.craftbox.android.jobway.ui.fragments.consult.question.detail_question.CounselorConsultDetailQuestionFragment
import asia.craftbox.android.jobway.utils.Utils
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import kotlinx.android.synthetic.main.layout_home_support.view.*
import android.net.Uri.fromParts
import android.content.Intent
import android.net.Uri


class CounselorConsultMasterFragment : AppMainFragment<FragmentCounselorConsultMasterBinding>(R.layout.fragment_counselor_consult_master),
    DMSViewModelSource<CounselorConsultMasterViewModel> {

    override fun getTitleResId(): Int? {
        return R.string.title_consult_counselor
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultMasterFragment)[CounselorConsultMasterViewModel::class.java]
    }

    private val presenter by lazy {
        CounselorConsultMasterPresenter(this@CounselorConsultMasterFragment)
    }

    var isCalledApi = false

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_auth_list_counselor,
            R.string.task_api_master_faqs
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_auth_list_counselor) {
            return
        }
        if (promise.taskNameResId == R.string.task_api_master_faqs) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@CounselorConsultMasterFragment.dataBinding.viewModel = this@CounselorConsultMasterFragment.viewModel
        this@CounselorConsultMasterFragment.dataBinding.presenter = this@CounselorConsultMasterFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
        if (!isCalledApi) {
            isCalledApi = true
        } else {
            return
        }

        viewModel.getFaqListAPI {data: List<QuestionModel>? ->
            val diffList = Utils.instance().getDiffItems(presenter.questionList, data)
            if (diffList.isEmpty()) {
                return@getFaqListAPI
            }
            presenter.questionList.addAll(diffList)
            parentFrament().addQuestionList(diffList)
            presenter.counselorConsultQuestionAdapter?.notifyDataSetChanged()
        }
        viewModel.getCounselorListAPI { data: List<CounselorModel>? ->
            var diffList = Utils.instance().getDiffItems(presenter.exList, data)
            diffList = Utils.instance().getDiffItems(presenter.coList, diffList)
            if (diffList.isEmpty()) {
                return@getCounselorListAPI
            }
            val _exList = diffList.filter { it.isExpert() }
            presenter.exList.addAll(_exList)
            presenter.counselorConsultMasterAdapter?.notifyDataSetChanged()

            val _coList = diffList.filter { it.isCo() }
            presenter.coList.addAll(_coList)
            presenter.counselorConsultJobWayAdapter?.notifyDataSetChanged()
        }
    }

    override fun initUI() {
        super.initUI()

        val jobWayRecyclerLayout = LinearLayoutManager(this.requireContext())
        jobWayRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultJobWayRecyclerView.layoutManager = jobWayRecyclerLayout
        dataBinding.counselorConsultJobWayRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultJobWayRecyclerView.adapter = presenter.counselorConsultJobWayAdapter

        val masterRecyclerLayout = LinearLayoutManager(this.requireContext())
        masterRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultMasterRecyclerView.layoutManager = masterRecyclerLayout
        dataBinding.counselorConsultMasterRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultMasterRecyclerView.adapter = presenter.counselorConsultMasterAdapter

        val questionRecyclerLayout = LinearLayoutManager(this.requireContext())
        questionRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultPopularQuestionRecyclerView.layoutManager = questionRecyclerLayout
        dataBinding.counselorConsultPopularQuestionRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultPopularQuestionRecyclerView.adapter = presenter.counselorConsultQuestionAdapter

        presenter.counselorConsultJobWayAdapter?.setCallback { counselorModel, position ->
            ShareData.getInstance().currentCounselor = counselorModel
            val expertFragment = CounselorConsultExpertFragment()
            expertFragment.show(childFragmentManager, "CounselorConsultExpertFragment")
        }
        presenter.counselorConsultMasterAdapter?.setCallback { counselorModel, position ->
            ShareData.getInstance().currentCounselor = counselorModel
            val expertFragment = CounselorConsultExpertFragment()
            expertFragment.show(childFragmentManager, "CounselorConsultExpertFragment")
        }
        presenter.counselorConsultQuestionAdapter?.setCallback { item, pos ->
            val mainActivity = activity as? MainActivity
            val bundle = Bundle()
            bundle.putSerializable(JobWayConstant.INTENT_QUESTION_MODEL, item)
            bundle.putInt(JobWayConstant.INTENT_QUESTION_MODEL_INDEX, pos)
            val detailQuestionFragment = DMSFragment.create(CounselorConsultDetailQuestionFragment::class.java, bundle)
            mainActivity?.getFragmentHelper()?.pushFragment(detailQuestionFragment)
        }

        dataBinding.layoutHomeSupport.support_service_ll.setOnClickListener {
            val phone = resources.getString(R.string.value_support_phone_primary)
            val intent = Intent(Intent.ACTION_DIAL, fromParts("tel", phone, null))
            startActivity(intent)
        }
    }

    private fun parentFrament() : CounselorConsultFragment {
        return parentFragment as CounselorConsultFragment
    }
}
