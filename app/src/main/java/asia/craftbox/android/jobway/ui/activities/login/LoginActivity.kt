package asia.craftbox.android.jobway.ui.activities.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityLoginBinding
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialActivity
import asia.craftbox.android.jobway.ui.fragments.users.phone.PhoneUserFragment
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper

/**
 * Created by @dphans (https://github.com/dphans)
 * December 2018
 */


class LoginActivity : DMSBindingActivity<ActivityLoginBinding>(R.layout.activity_login),
    DMSViewModelSource<LoginViewModel> {

    private val presenter: LoginPresenter = LoginPresenter(this@LoginActivity)
    override val viewModel by lazy { ViewModelProviders.of(this@LoginActivity)[LoginViewModel::class.java] }

    private val fragmentUtil by lazy {
        DMSFragmentHelper(
            this@LoginActivity.supportFragmentManager,
            R.id.login_container
        )
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@LoginActivity.dataBinding.presenter = this@LoginActivity.presenter
        this@LoginActivity.dataBinding.viewModel = this@LoginActivity.viewModel

        if (savedInstanceState == null) {
            this@LoginActivity.fragmentUtil.initialRootFragment(
                DMSFragment.create(PhoneUserFragment::class.java)
            )
        }

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun onBackPressed() {
        if (!this@LoginActivity.fragmentUtil.isCurrentFragmentBackable()) {
            return
        }

        super.onBackPressed()
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun getFragmentHelper(): DMSFragmentHelper {
        return this@LoginActivity.fragmentUtil
    }

    override fun initUI() {
        super.initUI()

        // display tutorial before displaying login form
        this@LoginActivity.startActivity(
            Intent(
                this@LoginActivity,
                TutorialActivity::class.java
            )
        )
    }

}
