package asia.craftbox.android.jobway.ui.fragments.faculty.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import asia.craftbox.android.jobway.databinding.FragmentFacultyDetailBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import com.bumptech.glide.Glide
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import kotlinx.android.synthetic.main.fragment_faculty_detail.*

class DetailFacultyFragment: AppMainFragment<FragmentFacultyDetailBinding>(R.layout.fragment_faculty_detail),
    DMSViewModelSource<DetailFacultyDescriptionViewModel>{

    private var facObj: FacultyModel = FacultyModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        facObj = arguments!!.getSerializable(FACULTY_OBJ) as FacultyModel
        this@DetailFacultyFragment.viewModel.setupWithFacultyId(facObj.id!!)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        //Init Fragment
        super.onContentViewCreated(rootView, savedInstanceState)
        this@DetailFacultyFragment.dataBinding.viewModel = this@DetailFacultyFragment.viewModel
        this@DetailFacultyFragment.dataBinding.presenter = this@DetailFacultyFragment.presenter
        if (!facObj.getFeartureImage().isNullOrBlank()) {
            context?.let { Glide.with(it).load(facObj.getFeartureImage()).into(image_faculty_detail) }
        }
        faculty_name_job.text = facObj.getFacNameToString()
        val summary = facObj!!.des ?: ""
        if(!summary.isNullOrBlank()) {
            summary.attachJWHtmlIntoWebView(this@DetailFacultyFragment.dataBinding.facultyDetailWebView)
        }else{
            viewModel.handleFetchFacultyDetail()
        }
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@DetailFacultyFragment)[DetailFacultyDescriptionViewModel::class.java]
    }

    val presenter by lazy {
        FacultyDescriptionDetailPresenter(this@DetailFacultyFragment)
    }
    override fun getTitleResId(): Int? {
        return R.string.title_fragment_faculties_list
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initUI() {
        super.initUI()
        // optimize webview contents
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.displayZoomControls = false
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.builtInZoomControls = true
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.javaScriptEnabled = true
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.loadWithOverviewMode = true
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.useWideViewPort = true
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.setSupportZoom(false)
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.settings.setAppCacheEnabled(true)
        this@DetailFacultyFragment.dataBinding.facultyDetailWebView.isEnabled = false
    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {
        super.onAPIResponseSuccess(promise, responseData)
         var data = responseData as DataFieldDMSRestResponse<FacultyModel>
        if (data.data != null ) {
            if(!data.data!!.getFeartureImage().isNullOrBlank()) {
                context?.let { Glide.with(it).load(data.data!!.getFeartureImage()).into(image_faculty_detail) }
            }
            faculty_name_job.text = data.data!!.getFacNameToString()
            val summary = data.data!!.summary ?: ""
            summary.attachJWHtmlIntoWebView(this@DetailFacultyFragment.dataBinding.facultyDetailWebView)
        }
    }

    companion object{
        val FACULTY_OBJ = "FULL_FACULTY_OBJ"

        fun newInstance(obj: FacultyModel): DMSFragment {
            var fragment = DetailFacultyFragment()
            var args = Bundle()
            args.putSerializable(FACULTY_OBJ, obj)
            fragment.arguments = args
            return  fragment
        }
    }
}
