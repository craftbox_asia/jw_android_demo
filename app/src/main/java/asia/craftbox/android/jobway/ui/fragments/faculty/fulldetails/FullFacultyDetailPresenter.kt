package asia.craftbox.android.jobway.ui.fragments.faculty.fulldetails

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDescriptionDetailActivity
import asia.craftbox.android.jobway.ui.activities.faculty.FullFacultyDetailActivity

class FullFacultyDetailPresenter (private val fragment: FullFacultyDetailFragment) : BaseObservable() {

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? FullFacultyDetailActivity
        mainActivity?.finish()
    }
}