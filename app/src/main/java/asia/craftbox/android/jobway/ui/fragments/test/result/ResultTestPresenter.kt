package asia.craftbox.android.jobway.ui.fragments.test.result

import android.content.Context
import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.ui.fragments.test.detail.DetailTestFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.test.result
 * Date: Jan 21, 2019 - 11:46 AM
 */


class ResultTestPresenter(private val fragment: ResultTestFragment) : BaseObservable() {

    fun getContext(): Context? {
        return this@ResultTestPresenter.fragment.context
    }

    fun onReTestButtonClicked(sender: View?) {
        this@ResultTestPresenter.fragment.viewModel.doResetForm()
        val testActivity = this@ResultTestPresenter.fragment.activity as? TestActivity ?: return
        testActivity.getFMHelper().pushFragment(
            fragment = DMSFragment.create(DetailTestFragment::class.java),
            replaceRootFragment = true
        )
    }

    fun onBackIconPressed() {
        this@ResultTestPresenter.fragment.activity?.finish()
    }

}
