package asia.craftbox.android.jobway.entities.responses.master

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestResponse

class MasterResponse <T> : DMSRestResponse {

    @SerializedName("data")
    var data: List<T> = emptyList()

}