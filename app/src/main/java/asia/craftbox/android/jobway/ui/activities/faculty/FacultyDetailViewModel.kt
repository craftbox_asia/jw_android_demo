package asia.craftbox.android.jobway.ui.activities.faculty

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.entities.services.JobAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class FacultyDetailViewModel : DMSViewModel(){
    private val itemPerPage = 10
    private var currentPage: Int = 0
    private var totalPage: Int = Int.MAX_VALUE
    private var lastID : Long = -1
    var facultyId: Long? = -1

    private val facultyPagedData by lazy {
        val dataFactory = AppData.get().getFaculty().getFacultyDetailPagination(facultyId?.toInt())
        val pageListBuilder = LivePagedListBuilder(dataFactory, this@FacultyDetailViewModel.itemPerPage)
        pageListBuilder.setBoundaryCallback(this@FacultyDetailViewModel.onFacultyPagingBoundary)
        return@lazy pageListBuilder.build()
    }

    private val onFacultyPagingBoundary = object : PagedList.BoundaryCallback<FacultyModel>() {

        override fun onItemAtEndLoaded(itemAtEnd: FacultyModel) {
            super.onItemAtEndLoaded(itemAtEnd)
            if (lastID.equals(itemAtEnd.id))
                return
            lastID = itemAtEnd.id!!
            this@FacultyDetailViewModel.fetchFacultyTask.onNext(true)
        }
    }

    private val fetchFacultyTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@FacultyDetailViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                if (this@FacultyDetailViewModel.currentPage < (this@FacultyDetailViewModel.totalPage - 1)) {
                    this@FacultyDetailViewModel.request(DMSRest.get(JobAPIService::class.java).getJobsListByFacultyIdPaginate(fcId = facultyId!! ,page = this@FacultyDetailViewModel.currentPage + 1))
                        .then { _, responseData ->
                            this@FacultyDetailViewModel.currentPage = responseData.data?.meta?.currentPage ?: 1
                            this@FacultyDetailViewModel.totalPage = responseData.data?.meta?.totalPages ?: 1

                            responseData.data?.data?.forEach {
                                it.subFacultyId = facultyId?.toInt()
                                it.saveOrUpdate()
                            }
                        }
                        .setTaskNameResId(R.string.task_api_faculty_list_retrieve)
                        .submit()
                }
            })
        return@lazy instance
    }

    private val fetchHotUniListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@FacultyDetailViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@FacultyDetailViewModel.request(DMSRest.get(JobAPIService::class.java).getHotFaculties(id = facultyId!!))
                    .then { _, responseData ->
                        AppData.get().getFaculty().cleanupHotFaculty()
                        responseData.data?.forEach {
                            it.hotFaculty = 1
                            it.saveOrUpdate()
                        }
                    }
                    .setTaskNameResId(R.string.task_api_uni_list_retrieve)
                    .submit()
            })
        return@lazy instance
    }

    fun setupFacultyWithId(facultyId: Long) {
        this@FacultyDetailViewModel.facultyId = facultyId
    }

    fun getFacultyPagination(): LiveData<PagedList<FacultyModel>> {
        return this@FacultyDetailViewModel.facultyPagedData
    }

    fun getHotFaculties(): LiveData<List<FacultyModel>> {
        return AppData.get().getFaculty().hotFaculty()
    }

    fun doFetchFacultyList() {
        this@FacultyDetailViewModel.currentPage = 0
        this@FacultyDetailViewModel.totalPage = Int.MAX_VALUE
        this@FacultyDetailViewModel.fetchFacultyTask.onNext(true)
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@FacultyDetailViewModel.doFetchFacultyList()
        this@FacultyDetailViewModel.fetchHotUniListTask.onNext(true)
    }
}