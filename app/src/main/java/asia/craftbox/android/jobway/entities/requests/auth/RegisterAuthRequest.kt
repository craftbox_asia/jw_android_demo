package asia.craftbox.android.jobway.entities.requests.auth

import android.util.Patterns
import androidx.databinding.Bindable
import asia.craftbox.android.jobway.BR
import asia.craftbox.android.jobway.JobWayConstant
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.auth
 * Date: Jan 09, 2019 - 3:09 PM
 */


@Suppress("UNUSED_PARAMETER")
class RegisterAuthRequest : DMSRestRequest() {

    @SerializedName("first_name")
    var firstName: String? = null
        @Bindable get

    @SerializedName("last_name")
    var lastName: String? = null
        @Bindable get

    @SerializedName("phone")
    var phone: String? = null
        @Bindable get


    fun getValidPhoneNumber(): String? {
        val phoneNum = this@RegisterAuthRequest.phone ?: return null

        if (!this@RegisterAuthRequest.isPhoneNumberValid()) {
            return null
        }

        return when {
            phoneNum.count() == 9 -> "+${this@RegisterAuthRequest.getCountryCode()}$phoneNum"
            phoneNum.startsWith("+${this@RegisterAuthRequest.getCountryCode()}") -> phoneNum
            phoneNum.startsWith(this@RegisterAuthRequest.getCountryCode()) -> "+$phoneNum"
            phoneNum.startsWith("0") -> {
                val trimmedPhoneNum = phoneNum.substring(1, phoneNum.count())
                "+${this@RegisterAuthRequest.getCountryCode()}$trimmedPhoneNum"
            }
            else -> "+${this@RegisterAuthRequest.getCountryCode()}$phoneNum"
        }
    }

    fun onPhoneEditTextChanged(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@RegisterAuthRequest.phone = sender?.toString()
        this@RegisterAuthRequest.notifyPropertyChanged(BR.phone)
        this@RegisterAuthRequest.notifyValidation()
    }

    fun onFirstNameEditTextChanged(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@RegisterAuthRequest.firstName = sender?.toString()
        this@RegisterAuthRequest.notifyPropertyChanged(BR.firstName)
        this@RegisterAuthRequest.notifyValidation()
    }

    fun onLastNameEditTextChanged(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@RegisterAuthRequest.lastName = sender?.toString()
        this@RegisterAuthRequest.notifyPropertyChanged(BR.lastName)
        this@RegisterAuthRequest.notifyValidation()
    }

    @Bindable
    fun isPhoneNumberValid(): Boolean {
        val phoneNum = this@RegisterAuthRequest.phone ?: return false
        val cntCode = this@RegisterAuthRequest.getCountryCode()

        // phone number is valid pattern
        if (!Patterns.PHONE.matcher(phoneNum).matches()) {
            return false
        }

        // phone number at least 10 characters and has one of prefixs: 0, countryCode, +countryCode
        // or may be just phone number, not include country code
        return (phoneNum.count() >= 10 && (arrayOf("0", cntCode, "+$cntCode").any { phoneNum.startsWith(it) }))
                || (phoneNum.count() >= 9 && !phoneNum.startsWith("0"))
    }

    @Bindable
    fun isUserFullnameValid(): Boolean {
        return !this@RegisterAuthRequest.lastName.isNullOrBlank()
                && !this@RegisterAuthRequest.firstName.isNullOrBlank()
    }

    private fun getCountryCode(): String {
        return "${JobWayConstant.DEFAULT_COUNTRY_PREFIX_VN}"
    }

    override fun validates(): Boolean {
        this@RegisterAuthRequest.notifyPropertyChanged(BR.phoneNumberValid)
        this@RegisterAuthRequest.notifyPropertyChanged(BR.userFullnameValid)
        return this@RegisterAuthRequest.isPhoneNumberValid() && this@RegisterAuthRequest.isUserFullnameValid()
    }

}
