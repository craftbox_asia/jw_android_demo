package asia.craftbox.android.jobway.ui.fragments.users.verify

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.BR
import asia.craftbox.android.jobway.EventBus.PhoneVerificationFail
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ContentDialogInvitationCodeBinding
import asia.craftbox.android.jobway.databinding.FragmentUserVerifyBinding
import asia.craftbox.android.jobway.entities.requests.auth.CheckInvitationCodeRequest
import asia.craftbox.android.jobway.entities.requests.auth.RegisterAuthRequest
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.ui.activities.login.LoginActivity
import asia.craftbox.android.jobway.ui.activities.splash.SplashActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.users.verify
 * Date: Jan 13, 2019 - 8:16 PM
 */


class VerifyUserFragment : DMSBindingFragment<FragmentUserVerifyBinding>(R.layout.fragment_user_verify),
    DMSViewModelSource<VerifyUserViewModel> {

    private val presenter by lazy { Presenter() }
    override val viewModel by lazy { ViewModelProviders.of(this@VerifyUserFragment)[VerifyUserViewModel::class.java] }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val fragmentBundle = this@VerifyUserFragment.arguments ?: return

        try {
            val registerRequest = fragmentBundle.getSerializable(INTENT_USER_REGISTER_REQUEST)
                    as? RegisterAuthRequest ?: return
            val isRegisteredPhone = fragmentBundle.getBoolean(INTENT_IS_REGISTERED)
            this@VerifyUserFragment.viewModel.setRequest(registerRequest, isRegisteredPhone)

            if (!isRegisteredPhone) {
                this@VerifyUserFragment.presenter.isNeedToSubmitInvitationCode = true
            }
        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }

    override fun isBackable(): Boolean {
        this@VerifyUserFragment.alert.confirm(R.string.confirmation_sms_verfication_cancellation) {
            val loginActivity = this@VerifyUserFragment.activity as? LoginActivity ?: return@confirm
            loginActivity.getFragmentHelper().popToRoot()
        }
        return false
    }

    override fun onDestroyView() {
        this@VerifyUserFragment.presenter.subjects.clear()
        super.onDestroyView()
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@VerifyUserFragment.dataBinding.viewModel = this@VerifyUserFragment.viewModel
        this@VerifyUserFragment.dataBinding.presenter = this@VerifyUserFragment.presenter

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()
        this@VerifyUserFragment.viewModel.setOnVerificationSuccessBlock {
            this@VerifyUserFragment.presenter.updateOTPCode(
                it.smsCode,
                0, 0, 0
            )
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        this@VerifyUserFragment.activity?.let {
            this@VerifyUserFragment.viewModel.doResendVerificationCode(it)
        }
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_auth_login,
            R.string.task_api_auth_register,
            R.string.task_firebase_auth_verification,
            R.string.task_api_auth_send_invitation_code
        )
    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {
        if (promise.taskNameResId == R.string.task_api_auth_login) {
            if (!this@VerifyUserFragment.isAdded) return
            val loginActivity = this@VerifyUserFragment.activity as? LoginActivity ?: return

            if (this@VerifyUserFragment.presenter.isNeedToSubmitInvitationCode) {
                var invitationDialog: MaterialDialog? = MaterialDialog(loginActivity)
                    .noAutoDismiss()
                    .cancelable(false)
                    .customView(
                        viewRes = R.layout.content_dialog_invitation_code,
                        noVerticalPadding = true
                    )
                invitationDialog?.setOnShowListener {
                    invitationDialog?.getCustomView()?.let { unwrappedCustomView ->
                        var dialogBinding =
                            DataBindingUtil.bind<ContentDialogInvitationCodeBinding>(unwrappedCustomView)
                        dialogBinding?.invalidateAll()
                        dialogBinding?.dialogInvitationSubmit?.isEnabled = false

                        fun doDismissDialog() {
                            dialogBinding?.unbind()
                            dialogBinding = null
                            invitationDialog?.dismiss()
                        }

                        dialogBinding?.dialogInvitationCode?.addTextChangedListener(object : TextWatcher {
                            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                            override fun afterTextChanged(s: Editable?) {}
                            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                                dialogBinding?.dialogInvitationSubmit?.isEnabled = !s?.toString().isNullOrBlank()
                            }
                        })

                        dialogBinding?.dialogInvitationSubmit?.setOnClickListener {
                            val requester = CheckInvitationCodeRequest()
                            requester.code = dialogBinding?.dialogInvitationCode?.text?.toString()

                            this@VerifyUserFragment.viewModel.request(
                                DMSRest.get(AuthAPIService::class.java).checkInvitationCode(requester)
                            )
                                .then { _, _ -> doDismissDialog() }
                                .setTaskNameResId(R.string.task_api_auth_send_invitation_code)
                                .submit()
                        }

                        dialogBinding?.dialogInvitationDismiss?.setOnClickListener {
                            doDismissDialog()
                        }

                        return@let true
                    }
                }
                invitationDialog?.setOnDismissListener {
                    invitationDialog = null
                    loginActivity.startActivity(Intent(loginActivity, SplashActivity::class.java))
                    loginActivity.finish()
                }
                invitationDialog?.show()
                return
            }

            loginActivity.startActivity(Intent(loginActivity, SplashActivity::class.java))
            loginActivity.finish()
            return
        }

        super.onAPIResponseSuccess(promise, responseData)
    }

    override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {
        if (promise.taskNameResId == R.string.task_firebase_auth_verification) {
            this@VerifyUserFragment.alert.alert(R.string.error_firebase_auth_phone_verification) {
                val loginActivity = this@VerifyUserFragment.activity as? LoginActivity ?: return@alert
                loginActivity.getFragmentHelper().popToRoot()
            }
            return
        }

        super.onAPIResponseError(promise, exception)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event:PhoneVerificationFail){
        this@VerifyUserFragment.alert.alert(R.string.error_firebase_auth_phone_verification) {
            val loginActivity = this@VerifyUserFragment.activity as? LoginActivity ?: return@alert
            loginActivity.getFragmentHelper().popToRoot()
        }
    }


    @Suppress("UNUSED_PARAMETER")
    inner class Presenter : BaseObservable() {

        internal val subjects = CompositeDisposable()
        internal var isNeedToSubmitInvitationCode = false


        var otpCode: String? = null
            @Bindable get() = if (field.isNullOrBlank()) null else field

        val alertMessage = ObservableField<String>("")
        val alertIsError = ObservableField(false)
        val alertBackgroundColor = ObservableField(Color.WHITE)
        val alertTextColor = ObservableField(Color.WHITE)


        fun getSubtitleText(): Spanned? {
            val context = this@VerifyUserFragment.context ?: return null
            val phoneNumber = this@VerifyUserFragment.viewModel.getRequest().getValidPhoneNumber() ?: return null

            return try {
                val textContent = context.getString(R.string.hint_login_verification, "<b>$phoneNumber</b>")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(textContent, Html.FROM_HTML_MODE_LEGACY)
                } else {
                    @Suppress("DEPRECATION")
                    Html.fromHtml(textContent)
                }
            } catch (exception: Exception) {
                Timber.e(exception)
                null
            }
        }

        fun updateOTPCode(sender: CharSequence?, before: Int, after: Int, count: Int) {
            val unwrappedSender = sender?.toString() ?: ""

            if (unwrappedSender.count() > 6) {
                this@Presenter.otpCode = unwrappedSender.substring(0, 6)
            } else {
                this@Presenter.otpCode = unwrappedSender
            }

            this@Presenter.notifyPropertyChanged(BR.otpCode)
        }

        fun doVerifyOTP() {
            if (!this@Presenter.alertMessage.get().isNullOrBlank()) {
                this@Presenter.alertMessage.set("")
            }

            this@Presenter.verifyOTPTask.onNext(this@Presenter.otpCode ?: "")
        }


        private val verifyOTPTask by lazy {
            val instance = PublishSubject.create<String>()
            this@Presenter.subjects.add(
                instance.throttleFirst(
                    1000,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread()
                ).subscribe { code ->
                    this@VerifyUserFragment.alert.progressShow(R.string.status_auth_sms_verification_checking)

                    val verificationCode = code ?: return@subscribe
                    val verificationId = this@VerifyUserFragment.viewModel.getVerificationId() ?: return@subscribe
                    if (verificationCode.count() != 6) return@subscribe

                    try {
                        val lastCredential = this@VerifyUserFragment.viewModel.getCredential()
                        val phoneAuthCredential =
                            lastCredential ?: PhoneAuthProvider.getCredential(verificationId, verificationCode)
                        FirebaseAuth.getInstance().signInWithCredential(phoneAuthCredential)
                            .addOnSuccessListener {
                                this@Presenter.alertIsError.set(false)
                                this@Presenter.alertBackgroundColor.set(Color.parseColor("#DFF3BD"))
                                this@Presenter.alertTextColor.set(Color.parseColor("#4D8B00"))
                                this@Presenter.alertMessage.set(
                                    this@VerifyUserFragment.context
                                        ?.getString(R.string.success_auth_sms_verification) ?: ""
                                )

                                Observable.just(true).observeOn(AndroidSchedulers.mainThread()).subscribe {
                                    this@VerifyUserFragment.alert.progressDismiss()
                                }
                                this@VerifyUserFragment.viewModel.doSubmitAuth()
                            }
                            .addOnFailureListener {
                                Timber.wtf(it)

                                Observable.just(true).observeOn(AndroidSchedulers.mainThread()).subscribe {
                                    this@VerifyUserFragment.alert.progressDismiss()
                                }

                                this@Presenter.updateOTPCode(null, 0, 0, 0)
                                this@Presenter.alertIsError.set(true)
                                this@Presenter.alertBackgroundColor.set(Color.parseColor("#F74E64"))
                                this@Presenter.alertTextColor.set(Color.parseColor("#FFFFFF"))
                                this@Presenter.alertMessage.set(
                                    this@VerifyUserFragment.context
                                        ?.getString(R.string.error_auth_sms_verification_invalid) ?: ""
                                )
                            }
                    } catch (exception: Exception) {
                        Timber.e(exception)
                    }
                })
            return@lazy instance
        }

        fun doResendVerificationCode() {
            this@VerifyUserFragment.activity?.let {
                this@VerifyUserFragment.viewModel.doResendVerificationCode(it)
            }
        }

        fun getContext(): Context? {
            return this@VerifyUserFragment.context
        }

    }


    companion object {
        const val INTENT_USER_REGISTER_REQUEST: String = "INTENT_USER_REGISTER_REQUEST"
        const val INTENT_IS_REGISTERED: String = "INTENT_IS_REGISTERED"
    }

}
