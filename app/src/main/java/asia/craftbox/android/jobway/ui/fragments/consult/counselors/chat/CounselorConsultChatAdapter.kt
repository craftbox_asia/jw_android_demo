package asia.craftbox.android.jobway.ui.fragments.consult.counselors.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel

class CounselorConsultChatAdapter(private val items: ArrayList<MessageModel>, val context: Context) :
    RecyclerView.Adapter<CounselorConsultChatAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_consult_chat, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val isLastItem = position == items.count() - 1
//        holder.hideLastItemLineView(isLastItem)
//        val item = items[position]
//        holder.left_title_text_view?.text = item.userName
//        holder.right_title_text_view?.text = item.getNatureTime(context)
//        holder.description_text_view?.text = item.body
//        holder.loadAvatar(item.avatar)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//        val rounded_image_view: RoundedImageView? = view.rounded_image_view
//        val left_title_text_view: TextView? = view.left_title_text_view
//        val right_title_text_view: TextView? = view.right_title_text_view
//        val description_text_view: TextView? = view.description_text_view
//        val line_view: View? = view.line_view
//
//        fun hideLastItemLineView(hide: Boolean) {
//            if (hide) {
//                line_view?.visibility = View.INVISIBLE
//            } else {
//                line_view?.visibility = View.VISIBLE
//            }
//        }
//        fun loadAvatar(avatar: String?) {
//            ImageViewBindingAdapter.imageViewAsyncSrc(rounded_image_view, avatar, 0)
//        }
    }
}