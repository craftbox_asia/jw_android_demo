package asia.craftbox.android.jobway.ui.base

import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityDefaultBinding
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.base
 * Date: Jan 17, 2019 - 3:43 PM
 */


abstract class DefaultActivity : DMSBindingActivity<ActivityDefaultBinding>(R.layout.activity_default) {

    private val fragmentHelper by lazy {
        DMSFragmentHelper(this@DefaultActivity.supportFragmentManager, R.id.default_container)
    }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DefaultActivity.fragmentHelper.toString()
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    fun getFragmentUtils(): DMSFragmentHelper {
        return this@DefaultActivity.fragmentHelper
    }

}
