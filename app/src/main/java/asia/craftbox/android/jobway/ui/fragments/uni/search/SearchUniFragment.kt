package asia.craftbox.android.jobway.ui.fragments.uni.search

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUniSearchBinding
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.fragments.uni.list.adapters.ListUniAdapter
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.extensions.hideKeyboard
import com.seldatdirect.dms.core.extensions.showKeyboard
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.search
 * Date: Feb 18, 2019 - 2:59 PM
 */


class SearchUniFragment : DMSBindingFragment<FragmentUniSearchBinding>(R.layout.fragment_uni_search),
    DMSViewModelSource<SearchUniViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@SearchUniFragment)[SearchUniViewModel::class.java]
    }

    private val presenter by lazy {
        SearchUniPresenter()
    }


    @SuppressLint("CheckResult")
    override fun onResume() {
        super.onResume()

        Observable.timer(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .filter { this@SearchUniFragment.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) }
            .subscribe {
                this@SearchUniFragment.dataBinding.uniSearchSearchEditText
                    .showKeyboard()
            }
    }

    override fun onPause() {
        this@SearchUniFragment.dataBinding.uniSearchSearchEditText
            .hideKeyboard()
        super.onPause()
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@SearchUniFragment.dataBinding.presenter = this@SearchUniFragment.presenter
        this@SearchUniFragment.dataBinding.viewModel = this@SearchUniFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()

        this@SearchUniFragment.presenter.searchUniAdapter = ListUniAdapter()

        val uniRecyclerLayout = LinearLayoutManager(this@SearchUniFragment.requireContext())

        this@SearchUniFragment.dataBinding.uniSearchRecyclerView.layoutManager = uniRecyclerLayout
        this@SearchUniFragment.dataBinding.uniSearchRecyclerView.adapter =
            this@SearchUniFragment.presenter.searchUniAdapter

        this@SearchUniFragment.presenter.searchUniAdapter?.setOnUniItemClicked { item, _ ->
            val uniDetail = Intent(
                this@SearchUniFragment.requireContext().applicationContext,
                DetailsUniActivity::class.java
            )

            uniDetail.putExtra(DetailsUniActivity.INTENT_DATA_UNI_MODEL_ID, item.id)
            this@SearchUniFragment.activity?.startActivity(uniDetail)
        }

        this@SearchUniFragment.dataBinding.uniSearchSearchEditText.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                this@SearchUniFragment.dataBinding.uniSearchSearchEditText
                    .hideKeyboard()
            }
            return@setOnKeyListener keyCode == KeyEvent.KEYCODE_ENTER
        }
    }

    override fun initObservables() {
        super.initObservables()

        this@SearchUniFragment.viewModel.getDatasource().observe(this@SearchUniFragment, Observer {
            it.setListener(this@SearchUniFragment.presenter)
        })

        this@SearchUniFragment.viewModel.getSearchResult().observe(this@SearchUniFragment, Observer {
            this@SearchUniFragment.presenter.searchUniAdapter?.submitList(it)
        })
    }


    inner class SearchUniPresenter : DMSViewModel.Listener {

        var searchUniAdapter: ListUniAdapter? = null
        val loadingState = ObservableField<Boolean>(false)


        fun onBackButtonPressed(sender: View?) {
            this@SearchUniFragment.activity?.onBackPressed()
        }


        override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
            if (promise.taskNameResId == R.string.task_api_uni_search) {
                this@SearchUniPresenter.loadingState.set(true)
            }
        }

        override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {
            if (promise.taskNameResId == R.string.task_api_uni_search) {
                this@SearchUniPresenter.loadingState.set(false)
            }
        }

        override fun onAPIDidFinishedAll() {

        }

        override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {

        }

        override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {

        }

    }

}
