package asia.craftbox.android.jobway.ui.fragments.explore.home

import androidx.lifecycle.LiveData
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.explore.home
 * Date: Jan 18, 2019 - 2:51 PM
 */


class HomeExploreViewModel : DMSViewModel() {

    private val userProfile by lazy {
        val profileId = DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
        return@lazy AppData.get().getProfile().getProfileById(profileId)
    }


    fun getProfile(): LiveData<ProfileModel> {
        return this@HomeExploreViewModel.userProfile
    }

}
