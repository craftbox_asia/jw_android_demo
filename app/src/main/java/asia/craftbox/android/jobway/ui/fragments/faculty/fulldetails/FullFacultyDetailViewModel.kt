package asia.craftbox.android.jobway.ui.fragments.faculty.fulldetails

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.services.JobAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel

class FullFacultyDetailViewModel : DMSViewModel(){

    private var facultyId: Long = 0

    fun setupWithFacultyId(id: Long) {
        facultyId = id
    }

    private val fetchFacultyDetailTask by lazy {
        request(DMSRest.get(JobAPIService::class.java).getJobByFacultyId(fcId =  facultyId))
            .then { _, responseData ->
            }
            .setTaskNameResId(R.string.task_api_faculty_list_retrieve)
            .submit()
    }

    fun handleFetchFacultyDetail() {
        this@FullFacultyDetailViewModel.fetchFacultyDetailTask
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@FullFacultyDetailViewModel.fetchFacultyDetailTask
    }
}