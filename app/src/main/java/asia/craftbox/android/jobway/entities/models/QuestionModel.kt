package asia.craftbox.android.jobway.entities.models

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
class QuestionModel : AppModel() {

    @SerializedName("question")
    var question: String? = null

    @SerializedName("answer")
    var answer: String? = null

    @SerializedName("updated_at")
    var updatedAt: String? = null



    companion object {
        val DIFF = object : DiffUtil.ItemCallback<QuestionModel>() {
            override fun areItemsTheSame(oldItem: QuestionModel, newItem: QuestionModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: QuestionModel, newItem: QuestionModel): Boolean {
                return false
            }
        }
    }

}
