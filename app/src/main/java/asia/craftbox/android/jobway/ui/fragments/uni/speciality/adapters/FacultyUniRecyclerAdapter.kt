package asia.craftbox.android.jobway.ui.fragments.uni.speciality.adapters

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderUniFacultyItemBinding
import asia.craftbox.android.jobway.entities.models.UniFacultyModel
import asia.craftbox.android.jobway.ui.activities.faculty.FullFacultyDetailActivity
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.speciality.adapters
 * Date: Feb 27, 2019 - 11:39 AM
 */


class FacultyUniRecyclerAdapter :
    PagedListAdapter<UniFacultyModel, FacultyUniRecyclerAdapter.ViewHolder>(UniFacultyModel.DIFF) {

    var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this@FacultyUniRecyclerAdapter.getItem(position)?.let { unwrappedItem ->
            holder.bind(unwrappedItem, position)
        }
    }


    inner class ViewHolder(parent: ViewGroup) : DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<UniFacultyModel>(
        parent,
        R.layout.viewholder_uni_faculty_item
    ) {

        private val dataBinding = DataBindingUtil.bind<ViewholderUniFacultyItemBinding>(this@ViewHolder.itemView)

        override fun bind(item: UniFacultyModel, position: Int) {
            super.bind(item, position)

            this@ViewHolder.dataBinding?.presenter = Presenter(item, position)
            this@ViewHolder.dataBinding?.invalidateAll()
        }

        inner class Presenter(val item: UniFacultyModel, private val position: Int) {

            fun onItemClicked(sender: View?) {
                val intent = Intent(context, FullFacultyDetailActivity::class.java)
                intent.putExtra(FullFacultyDetailActivity.FACULTY_DETAIL_BY_FAC_ID, item.id)
                context?.startActivity(intent)
            }
        }
    }
}
