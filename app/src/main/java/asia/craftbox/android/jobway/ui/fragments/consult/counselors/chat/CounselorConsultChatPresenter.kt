package asia.craftbox.android.jobway.ui.fragments.consult.counselors.chat

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import timber.log.Timber


class CounselorConsultChatPresenter(private val fragment: CounselorConsultChatFragment) : BaseObservable() {

    var currentCounselorModel: CounselorModel? = null
    val mainActivity = fragment.activity as? MainActivity

    var counselorConsultChatAdapter: CounselorConsultChatAdapter? = null
    var historyList: ArrayList<MessageModel> = ArrayList()

    init {
        historyList.add(MessageModel())
        historyList.add(MessageModel())
        historyList.add(MessageModel())
        historyList.add(MessageModel())
        historyList.add(MessageModel())
        historyList.add(MessageModel())

        counselorConsultChatAdapter =
            CounselorConsultChatAdapter(historyList, fragment.context!!
            )
    }

    fun onBackButtonClicked(sender: View?) {
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onExploreButtonClicked(sender: View?) {
        Timber.d("onExploreButtonClicked")
    }

    fun onContinueConsultButtonClicked(sender: View?) {
        Timber.d("onContinueConsultButtonClicked")
    }

}
