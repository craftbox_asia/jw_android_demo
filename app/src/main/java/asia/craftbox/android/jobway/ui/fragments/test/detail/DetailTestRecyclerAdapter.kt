package asia.craftbox.android.jobway.ui.fragments.test.detail

import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderTestHollandBinding
import asia.craftbox.android.jobway.databinding.ViewholderTestMbtiBinding
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.test.detail
 * Date: Jan 19, 2019 - 7:25 PM
 */


class DetailTestRecyclerAdapter :
    DMSSingleModelRecyclerAdapter<CreateTestRequest.QuestionData, DetailTestRecyclerAdapter.ViewHolder>() {

    private var updateAnswerListener: ((item: CreateTestRequest.QuestionData, pos: Int, sender: View?) -> Unit)? = null
    private var type = CreateTestRequest.TestType.MBTI
    private var originalItems: List<CreateTestRequest.QuestionData> = emptyList()

    fun setOnUserDidUpdateAnswerListener(block: ((item: CreateTestRequest.QuestionData, pos: Int, sender: View?) -> Unit)?) {
        this@DetailTestRecyclerAdapter.updateAnswerListener = block
    }

    fun updateItems(items: List<CreateTestRequest.QuestionData>, type: CreateTestRequest.TestType) {
        this@DetailTestRecyclerAdapter.originalItems = items
        this@DetailTestRecyclerAdapter.type = type
        this@DetailTestRecyclerAdapter.doFetchPagination()
    }

    fun isCurrentPageValidated(): Boolean {
        return this@DetailTestRecyclerAdapter.originalItems.filterIndexed { index, _ ->
            return@filterIndexed index <= (this@DetailTestRecyclerAdapter.originalItems.count() - 1)
        }.all { !it.value.isNullOrBlank() }
    }

    private fun getCurrentPageOffset(): Int {
        return when (this@DetailTestRecyclerAdapter.type) {
            CreateTestRequest.TestType.MBTI -> 0
            CreateTestRequest.TestType.Holland -> 0
        }
    }

    private fun doFetchPagination() {
        try {
            val currentIndex = when (this@DetailTestRecyclerAdapter.type) {
                CreateTestRequest.TestType.MBTI -> 0
                CreateTestRequest.TestType.Holland -> 0
            }
            this@DetailTestRecyclerAdapter.updateItems(
                this@DetailTestRecyclerAdapter.originalItems.subList(
                    currentIndex,
                    this@DetailTestRecyclerAdapter.originalItems.count()
                )
            )
            this@DetailTestRecyclerAdapter.updateItems(
                this@DetailTestRecyclerAdapter.originalItems.subList(
                    currentIndex,
                    originalItems.count()
                )
            )
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DetailTestRecyclerAdapter.ViewHolder {
        return if (viewType == VIEWTYPE_TEST_MBTI) {
            ViewHolder(parent, R.layout.viewholder_test_mbti)
        } else {
            ViewHolder(parent, R.layout.viewholder_test_holland)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = this@DetailTestRecyclerAdapter.getItemByPosition(position) ?: return VIEWTYPE_TEST_MBTI
        return if (item.mbtiQuestionValue != null) {
            VIEWTYPE_TEST_MBTI
        } else {
            VIEWTYPE_TEST_HOLLAND
        }
    }

    inner class ViewHolder(parent: ViewGroup, @LayoutRes val layoutRes: Int) :
        DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<CreateTestRequest.QuestionData>(parent, layoutRes) {

        private val mbtiBinding = if (this@ViewHolder.layoutRes == R.layout.viewholder_test_mbti)
            DataBindingUtil.bind<ViewholderTestMbtiBinding>(this@ViewHolder.itemView)
        else
            null

        private val hollandBinding = if (this@ViewHolder.layoutRes == R.layout.viewholder_test_holland)
            DataBindingUtil.bind<ViewholderTestHollandBinding>(this@ViewHolder.itemView)
        else
            null


        override fun bind(item: CreateTestRequest.QuestionData, position: Int) {
            super.bind(item, position)

            if (this@ViewHolder.layoutRes == R.layout.viewholder_test_mbti) {
                this@ViewHolder.mbtiBinding?.presenter = Presenter(item, position)
                this@ViewHolder.mbtiBinding?.invalidateAll()
                return
            }

            if (this@ViewHolder.layoutRes == R.layout.viewholder_test_holland) {
                this@ViewHolder.hollandBinding?.presenter = Presenter(item, position)
                this@ViewHolder.hollandBinding?.invalidateAll()
            }
        }


        inner class Presenter(val item: CreateTestRequest.QuestionData, private val pos: Int) {

            fun getQuestionNumber(): String? {
                val context = this@ViewHolder.itemView.context ?: return null
                return "${context.getString(R.string.common_question_number)} ${(this@Presenter.pos + this@DetailTestRecyclerAdapter.getCurrentPageOffset()) + 1}:"
            }

            fun getMBTIAnswer(answerPos: Int): String? {
                return this@Presenter.item.mbtiQuestionValue?.answers?.getOrNull(answerPos)?.name
            }

            fun getMBTIAnswerChecked(answerPos: Int): Boolean {
                return !this@Presenter.item.value.isNullOrBlank() && this@Presenter.item.mbtiQuestionValue?.answers?.getOrNull(
                    answerPos
                )?.value == this@Presenter.item.value
            }

            fun getHOLLANDAnswerChecked(answerPos: Int): Boolean {
                return !this@Presenter.item.value.isNullOrBlank() && this@Presenter.item.value == "$answerPos"
            }

            fun onHollandCheckerChanged(radioGroup: RadioGroup?, itemId: Int) {
                this@Presenter.item.value = when (itemId) {
                    R.id.test_holland_radio_button_a -> "0"
                    R.id.test_holland_radio_button_b -> "1"
                    R.id.test_holland_radio_button_c -> "2"
                    R.id.test_holland_radio_button_d -> "3"
                    R.id.test_holland_radio_button_e -> "4"
                    else -> null
                }
                this@DetailTestRecyclerAdapter.updateAnswerListener?.invoke(
                    this@Presenter.item,
                    this@Presenter.pos,
                    radioGroup
                )
            }

            fun onMBTICheckedChanged(index: Int, isChecked: Boolean) {
                if (isChecked) {
                    val answerValue = this@Presenter.item.mbtiQuestionValue?.answers?.getOrNull(index)?.value
                    this@Presenter.item.value = answerValue
                    this@ViewHolder.mbtiBinding?.invalidateAll()

                    this@DetailTestRecyclerAdapter.updateAnswerListener?.invoke(
                        this@Presenter.item,
                        this@Presenter.pos,
                        null
                    )
                }
            }

            fun onHOLLANDCheckedChanged(index: Int, isChecked: Boolean) {
                if (isChecked) {
                    this@Presenter.item.value = "$index"
                    this@ViewHolder.hollandBinding?.invalidateAll()

                    this@DetailTestRecyclerAdapter.updateAnswerListener?.invoke(
                        this@Presenter.item,
                        this@Presenter.pos,
                        null
                    )
                }
            }

        }

    }


    companion object {
        const val VIEWTYPE_TEST_MBTI: Int = 1
        const val VIEWTYPE_TEST_HOLLAND: Int = 2
    }

}
