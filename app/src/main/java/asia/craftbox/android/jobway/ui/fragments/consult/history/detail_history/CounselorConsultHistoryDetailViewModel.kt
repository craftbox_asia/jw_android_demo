package asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.responses.notification.NotificationMessagesResponse
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel


class CounselorConsultHistoryDetailViewModel : DMSViewModel() {

    fun getListHistory(userId: Int, page: Int, callback: ((NotificationMessagesResponse<MessageModel>?) -> Unit)) {
        this.request(DMSRest.get(NotificationAPIService::class.java).getListHistory(userId, page))
            .then { promise, res ->
                callback.invoke(res.data)
            }
            .setTaskNameResId(R.string.task_api_get_list_history)
            .submit()
    }

    fun viewProfileByIdApi(uid: Int, callback: ((ProfileModel?) -> Unit)) {
        request(DMSRest.get(AuthAPIService::class.java).viewProfileById(uid))
            .then { _, responseData ->
                callback.invoke(responseData.data)
            }
            .setTaskNameResId(R.string.task_api_auth_view_profile_by_id)
            .submit()
    }
}
