package asia.craftbox.android.jobway.ui.fragments.consult.question.frequently_question

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultQuestionBinding
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselor.CounselorConsultFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.master.CounselorConsultMasterViewModel
import asia.craftbox.android.jobway.ui.fragments.consult.question.detail_question.CounselorConsultDetailQuestionFragment
import asia.craftbox.android.jobway.utils.Utils
import com.seldatdirect.dms.core.extensions.hideKeyboard
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorConsultQuestionFragment :
    AppMainFragment<FragmentCounselorConsultQuestionBinding>(R.layout.fragment_counselor_consult_question) {

    override fun getTitleResId(): Int? {
        return R.string.text_requently_question
    }

    val presenter by lazy {
        CounselorConsultQuestionPresenter(this)
    }

    var textSearch: String = ""

    @SuppressLint("ClickableViewAccessibility")
    override fun initUI() {
        super.initUI()

        if (ShareData.getInstance().isCurrentUserIsMaster()) {
            getFaqListAPI()
        }
        val questionRecyclerLayout = LinearLayoutManager(this.requireContext())
        questionRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultPopularQuestionRecyclerView.layoutManager = questionRecyclerLayout
        dataBinding.counselorConsultPopularQuestionRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultPopularQuestionRecyclerView.adapter = presenter.counselorConsultQuestionAdapter
        presenter.search()

        presenter.counselorConsultQuestionAdapter.setCallback { item, pos ->
            val mainActivity = activity as? MainActivity
            val bundle = Bundle()
            bundle.putSerializable(JobWayConstant.INTENT_QUESTION_MODEL, item)
            bundle.putInt(JobWayConstant.INTENT_QUESTION_MODEL_INDEX, pos)
            val detailQuestionFragment = DMSFragment.create(CounselorConsultDetailQuestionFragment::class.java, bundle)
            mainActivity?.getFragmentHelper()?.pushFragment(detailQuestionFragment)
        }

        dataBinding.searchHistoryEditText.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                textSearch = p0.toString()
                presenter.search()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
        dataBinding.searchHistoryEditText.setOnTouchListener { view, motionEvent ->
            val DRAWABLE_RIGHT = 2
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if (motionEvent.rawX >= (dataBinding.searchHistoryEditText.right - dataBinding.searchHistoryEditText.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    dataBinding.searchHistoryEditText.hideKeyboard()
                    textSearch = dataBinding.searchHistoryEditText.text.toString().trim()
                    presenter.search()
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }
    }

    override fun onResume() {
        super.onResume()
        reloadData()
    }

    private fun reloadData() {
        presenter.counselorConsultQuestionAdapter?.notifyDataSetChanged()
    }

    private fun getFaqListAPI() {
        val masterViewModel = CounselorConsultMasterViewModel()
        masterViewModel.getFaqListAPI {
            val diffList = Utils.instance().getDiffItems(parentFrament().presenter.questionList, it)
            if (diffList.isEmpty()) {
                return@getFaqListAPI
            }
            parentFrament().addQuestionList(diffList)
            presenter.search()
        }
    }

    private fun parentFrament() : CounselorConsultFragment {
        return parentFragment as CounselorConsultFragment
    }
}
