package asia.craftbox.android.jobway.ui.fragments.faculty.detail

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDescriptionDetailActivity

class FacultyDescriptionDetailPresenter (private val fragment: DetailFacultyFragment) : BaseObservable() {

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? FacultyDescriptionDetailActivity
        mainActivity?.finish()
    }
}