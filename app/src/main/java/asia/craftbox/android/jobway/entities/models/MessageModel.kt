package asia.craftbox.android.jobway.entities.models

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.modules.exts.timeAgo
import asia.craftbox.android.jobway.modules.exts.toDate
import com.google.gson.annotations.SerializedName

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 13, 2019 - 3:11 PM
 */


@Entity
class MessageModel : AppModel() {

    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("read")
    var read: Int = 0

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("user_name")
    var userName: String? = null

    @SerializedName("files")
    var files: List<String>? = null

    var type = JobWayConstant.TYPE_MESSAGE

    fun getNatureTime(context: Context?): String? {
        val ctx = context ?: return null
        val publishTimeString = this@MessageModel.createdAt ?: return null
        val publishDate = publishTimeString.toDate("yyyy-MM-dd kk:mm:ss") ?: return null
        return publishDate.timeAgo(ctx)
    }

    fun isExistInBodyOrUsername(text: String) : Boolean {
        if (body == null && userName == null) return false
        return (body!!.contains(text, ignoreCase = true) || userName!!.contains(text, ignoreCase = true))
    }

    fun isRead(): Boolean {
        return read != 0
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<MessageModel>() {
            override fun areItemsTheSame(oldItem: MessageModel, newItem: MessageModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: MessageModel, newItem: MessageModel): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.localUpdatedAt == newItem.localUpdatedAt
            }
        }
    }
}
