package asia.craftbox.android.jobway.entities.models

import androidx.room.Entity
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Jan 25, 2019 - 2:25 PM
 */


@Entity
class ProfileResumeModel : DMSModel() {

    @SerializedName("key")
    var key: String? = null

    @SerializedName("text")
    var text: String? = null

    @SerializedName("hint")
    var hint: String? = null


    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getTest()
            .getResumeByKeyFlatten(this@ProfileResumeModel.key ?: "")
            ?: this@ProfileResumeModel

        DMSModel.mergeFields(this@ProfileResumeModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()
        return AppData.get().getTest().insertOneResume(instance)
    }

}
