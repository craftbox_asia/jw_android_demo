package asia.craftbox.android.jobway.ui.fragments.uni.list.adapters

import android.content.Intent
import android.os.Bundle
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ItemPagerUniHotBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.fragments.uni.list.ListUniFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list.adapters
 * Date: Feb 18, 2019 - 6:25 PM
 */


class ListHotUniAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val items = ArrayList<FragmentItem>()
    private var onItemSelectedBlock: ((newsItem: NewsModel) -> Unit)? = null


    fun setOnItemSelected(block: ((newsItem: NewsModel) -> Unit)?) {
        this@ListHotUniAdapter.onItemSelectedBlock = block
    }


    fun updateItems(items: List<FragmentItem>) {
        this@ListHotUniAdapter.items.clear()
        this@ListHotUniAdapter.items.addAll(items)
        this@ListHotUniAdapter.notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return this@ListHotUniAdapter.items[position]
    }

    override fun getCount(): Int {
        return this@ListHotUniAdapter.items.count()
    }


    class FragmentItem : Fragment() {

        private var dataBinding: ItemPagerUniHotBinding? = null


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            this@FragmentItem.dataBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.item_pager_uni_hot,
                container,
                false
            )
            return this@FragmentItem.dataBinding!!.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            try {
                val bundle = this@FragmentItem.arguments ?: return
                val newsItem = bundle.getSerializable(UNI_MODEL) as? UniModel ?: return

                this@FragmentItem.dataBinding?.presenter = FragmentItemPresenter(newsItem)
                this@FragmentItem.dataBinding?.invalidateAll()

                val onItemClicked = View.OnClickListener {
                    (this@FragmentItem.parentFragment as? ListUniFragment)?.let { listUniFragment ->
                        listUniFragment.activity?.let { mainActivity ->
                            val uniActivity = Intent(mainActivity, DetailsUniActivity::class.java)
                            uniActivity.putExtra(DetailsUniActivity.INTENT_DATA_UNI_MODEL_ID, newsItem.id)
                            mainActivity.startActivity(uniActivity)
                        }
                    }
                }

                view.findViewById<View?>(R.id.pager_uni_latest_card_view)
                    ?.setOnClickListener(onItemClicked)
            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }


        inner class FragmentItemPresenter(val item: UniModel) {

            fun getCollegeType(): Spanned? {
                return this@FragmentItemPresenter.item.getItemUniClassName(context)
            }

            fun getCollegeCode(): Spanned? {
                return this@FragmentItemPresenter.item.getItemUniCode(context)
            }

        }


        companion object {
            internal const val UNI_MODEL = "UNI_MODEL"

            fun create(uniItem: UniModel): FragmentItem {
                val instance = FragmentItem()
                instance.arguments = Bundle()
                instance.arguments?.putSerializable(UNI_MODEL, uniItem)
                return instance
            }
        }

    }

}
