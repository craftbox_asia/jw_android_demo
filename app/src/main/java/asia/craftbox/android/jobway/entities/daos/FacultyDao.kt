package asia.craftbox.android.jobway.entities.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import asia.craftbox.android.jobway.entities.models.FacultyModel
import com.seldatdirect.dms.core.entities.DMSDao

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.daos
 * Date: Feb 27, 2019 - 5:15 PM
 */


@Dao
interface FacultyDao : DMSDao<FacultyModel> {

    @Query("SELECT * FROM FacultyModel WHERE FacultyModel.id = :facId")
    fun getFacultyByIdFlatten(facId: Long?): FacultyModel?

    @Query("SELECT * FROM FacultyModel ORDER BY FacultyModel.id DESC")
    fun getFacultyPagination(): DataSource.Factory<Int, FacultyModel>

    @Query("SELECT * FROM FacultyModel WHERE FacultyModel.subFacultyId = :facId ORDER BY FacultyModel.id DESC")
    fun getFacultyDetailPagination(facId: Int?): DataSource.Factory<Int, FacultyModel>

    @Query("UPDATE FacultyModel SET hotFaculty = 0 WHERE 1")
    fun cleanupHotFaculty()

    @Query("UPDATE FacultyModel SET hotFaculty = 0 WHERE 1")
    fun cleanupFaculty()

    @Query("SELECT COUNT(*) FROM FacultyModel WHERE hotFaculty = 1 AND id = :id")
    fun countHotFacultyByIdFlatten(id: Long?): Int

    @Query("SELECT * FROM FacultyModel WHERE hotFaculty = 1")
    fun hotFaculty(): LiveData<List<FacultyModel>>
}
