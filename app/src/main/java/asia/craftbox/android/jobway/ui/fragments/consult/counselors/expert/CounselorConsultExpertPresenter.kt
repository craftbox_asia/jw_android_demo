package asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.default_consulting.CounselorConsultDefaultCounsultingFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert_bio.CounselorConsultExpertBioFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.warning_popup.CounselorConsultWarningPopupFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.preference.DMSPreference


class CounselorConsultExpertPresenter(private val fragment: CounselorConsultExpertFragment) : BaseObservable() {

    var currentCounselorModel: CounselorModel? = null

    fun closeButtonClicked(sender: View?) {
        fragment.dismiss()
    }

    fun infoButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        val expertBioFragment = DMSFragment.create(CounselorConsultExpertBioFragment::class.java)
        fragment.dismiss()
        mainActivity?.getFragmentHelper()?.pushFragment(expertBioFragment)
    }

    fun counsulorButtonClicked(sender: View?) {
        val profileId = DMSPreference.default().get(JobWayConstant.Preference.AuthProfileId.name, Long::class.java) ?: return
        val profile = AppData.get().getProfile().getProfileByIdFlatten(profileId) ?: return
        if (!profile.isFinishedTest() || !profile.isFinishedProfile()) {
            val mainActivity = fragment.activity as? MainActivity
            val popupFragment = DMSFragment.create(CounselorConsultWarningPopupFragment::class.java)
            fragment.dismiss()
            mainActivity?.getFragmentHelper()?.pushFragment(popupFragment)
        } else {
            val mainActivity = fragment.activity as? MainActivity
            val defaultCounsultingFragment = DMSFragment.create(CounselorConsultDefaultCounsultingFragment::class.java)
            fragment.dismiss()
            mainActivity?.getFragmentHelper()?.pushFragment(defaultCounsultingFragment)
        }
    }
}
