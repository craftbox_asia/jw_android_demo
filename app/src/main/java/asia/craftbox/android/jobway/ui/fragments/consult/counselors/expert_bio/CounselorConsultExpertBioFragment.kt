package asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert_bio

import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultExpertBioBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import timber.log.Timber


class CounselorConsultExpertBioFragment :
    AppMainFragment<FragmentCounselorConsultExpertBioBinding>(R.layout.fragment_counselor_consult_expert_bio) {
    var currentCounselorModel: CounselorModel? = null

    var isShowWarningPopup: Boolean? = false

    override fun getTitleResId(): Int? {
        return R.string.title_consult_counselor
    }

    val presenter by lazy {
        CounselorConsultExpertBioPresenter(this)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        super.onContentViewCreated(rootView, savedInstanceState)

        val counselorModel = ShareData.getInstance().currentCounselor ?: return
        loadUIFromData(counselorModel)
    }

    override fun initUI() {
        super.initUI()

        val profileId = DMSPreference.default().get(JobWayConstant.Preference.AuthProfileId.name, Long::class.java)
            ?: return


        val profile = AppData.get().getProfile().getProfileByIdFlatten(profileId) ?: return
        isShowWarningPopup = profile.isFinishedTest() && profile.isFinishedProfile()

    }

    private fun loadUIFromData(counselorModel: CounselorModel) {
        currentCounselorModel = counselorModel
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.nameTextView.text = counselorModel.getFullName()
        dataBinding.jobTextView.text = counselorModel.getDescriptionJob()
        counselorModel.getSummary().attachJWHtmlIntoWebView(dataBinding.descriptionWebView, "#f8f9fb")

        val starImageViewList = arrayListOf(
            dataBinding.star1ImageView, dataBinding.star2ImageView,
            dataBinding.star3ImageView, dataBinding.star4ImageView, dataBinding.star5ImageView
        )
        val star = counselorModel.getStar()
        if (star <= 0) {
            return
        }
        for (i in 0 until 5) {
            val imageView = starImageViewList[i]
            if (i < star) {
                imageView.setImageResource(R.drawable.ic_star_orange_24dp)
            } else {
                imageView.setImageResource(R.drawable.ic_star_border_orange_24dp)
            }
        }
    }
}
