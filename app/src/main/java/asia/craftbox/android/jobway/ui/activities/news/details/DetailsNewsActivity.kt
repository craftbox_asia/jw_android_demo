package asia.craftbox.android.jobway.ui.activities.news.details

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.EventBus.NewsEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityNewsDetailsBinding
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.news.list.adapters.ListHotNewsAdapter
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import kotlinx.android.synthetic.main.activity_news_details.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber
import android.R.attr.button
import android.graphics.drawable.Drawable
import asia.craftbox.android.jobway.EventBus.PostBookmarkEvent


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.news.details
 * Date: Feb 13, 2019 - 5:31 PM
 */


class DetailsNewsActivity : DMSBindingActivity<ActivityNewsDetailsBinding>(R.layout.activity_news_details),
    DMSViewModelSource<DetailsNewsViewModel> {

    var isFav : Boolean = true
    private val presenter by lazy {
        DetailsNewsPresenter()
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@DetailsNewsActivity)[DetailsNewsViewModel::class.java]
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DetailsNewsActivity.dataBinding.presenter = this@DetailsNewsActivity.presenter
        this@DetailsNewsActivity.dataBinding.viewModel = this@DetailsNewsActivity.viewModel

        try {
            val bundle = this@DetailsNewsActivity.intent ?: return
            val newsId = bundle.getLongExtra(INTENT_DATA_NEWS_ID, -1)
            this@DetailsNewsActivity.viewModel.setupNewsWithId(newsId)
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        } finally {
            super.onContentViewCreated(rootView, savedInstanceState)
        }
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_news_detail
        )
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initUI() {
        super.initUI()

        // optimize webview contents
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.displayZoomControls = false
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.builtInZoomControls = true
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.javaScriptEnabled = true
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.loadWithOverviewMode = true
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.useWideViewPort = true
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.setSupportZoom(false)
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.settings.setAppCacheEnabled(true)
//        this@DetailsNewsActivity.dataBinding.newsDetailWebView.isEnabled = false

        // setup hot news
        this@DetailsNewsActivity.presenter.relatedNewsAdapter =
            ListHotNewsAdapter(this@DetailsNewsActivity.supportFragmentManager)
        this@DetailsNewsActivity.dataBinding.newsDetailRelatedViewPager.adapter = this@DetailsNewsActivity
            .presenter.relatedNewsAdapter
        this@DetailsNewsActivity.dataBinding.newsDetailRelatedViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@DetailsNewsActivity.dataBinding.newsDetailRelatedViewPager)
            .pagerMargin(this@DetailsNewsActivity.resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()

        this@DetailsNewsActivity.dataBinding.newsDetailScrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                this@DetailsNewsActivity.viewModel.doIncrementViews()
            }
        }

        bookmark_text_view.setOnClickListener { v ->
            this@DetailsNewsActivity.viewModel.doBookmark()
            if (isFav) {
                isFav = false
                setDeactive()
            }else{
                isFav = true
                setActive()
            }
        }

    }

    fun setDeactive(){
        setDrawble(R.drawable.ic_hear_white)
        bookmark_text_view.text = getString(R.string.un_book_mark_text)
    }

    fun setActive() {
        setDrawble(R.drawable.ic_heart_red)
        bookmark_text_view.text = getString(R.string.book_marked_text)
    }

    fun setDrawble(drawbleId: Int){
        val image = this@DetailsNewsActivity.getResources().getDrawable(drawbleId)
        val h = image.getIntrinsicHeight()
        val w = image.getIntrinsicWidth()
        image.setBounds(0, 0, w, h)
        bookmark_text_view.setCompoundDrawables(image, null, null, null)
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: PostBookmarkEvent) {
        if (event != null) {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: NewsEvent) {
        if (event != null) {
            isFav = event.news.isFav!!
            if (isFav) {
                setActive()
            }
            else{
                setDeactive()
            }
            btn_news_detail_share.setOnClickListener { v ->
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    var shareMessage =
                        getString(R.string.home_section_news) + " - " + event.news.title + getString(R.string.share_link_app_job_way) + "\n\n"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    startActivity(Intent.createChooser(shareIntent, "Jobway invitation"))
                } catch (e: Exception) {

                }
            }
        }
    }

    override fun initObservables() {
        super.initObservables()

        // post detail
        this@DetailsNewsActivity.viewModel.getNewsItem().observe(this@DetailsNewsActivity, Observer {
            val newsItem = it ?: return@Observer
            val content = newsItem.content ?: ""
            content.attachJWHtmlIntoWebView(this@DetailsNewsActivity.dataBinding.newsDetailWebView)

            // generate tags
            val tagsItems = newsItem.tags.map { tagItem -> tagItem.trim() }
            val tagsContainer = this@DetailsNewsActivity.dataBinding.newsDetailsAppChipGroup
            tagsContainer.removeChip(all = true)
            tagsItems.forEach { tagText -> tagsContainer.addChipItem(tagText) }
            this@DetailsNewsActivity.dataBinding.newsDetailsTagsContainer.visibility =
                if (tagsItems.isEmpty()) View.GONE else View.VISIBLE
        })

        // related news
        this@DetailsNewsActivity.viewModel.getHotNews().observe(this@DetailsNewsActivity, Observer {
            if (it.isNotEmpty()) {
                val hotNewsFragments = it.map { newsItem -> ListHotNewsAdapter.FragmentItem.create(newsItem) }
                this@DetailsNewsActivity.presenter.relatedNewsAdapter?.updateItems(hotNewsFragments)

                this@DetailsNewsActivity.dataBinding.newsDetailRelatedViewPager.post {
                    try {
                        val pager = this@DetailsNewsActivity.dataBinding.newsDetailRelatedViewPager
                        val fragment = pager.adapter?.instantiateItem(pager, 0) as Fragment
                        fragment.view?.let { fragmentView -> ViewCompat.setElevation(fragmentView, 8.0f) }
                    } catch (illegalStateException: Exception) {
                        Timber.e(illegalStateException.localizedMessage)
                    }
                }

                this@DetailsNewsActivity.dataBinding.newsDetailsRelatedContainer.visibility = View.VISIBLE
            } else {
                this@DetailsNewsActivity.dataBinding.newsDetailsRelatedContainer.visibility = View.GONE
            }
        })
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_news_detail) {
            return
        }

        super.onAPIWillRequest(promise)
    }

    override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {
        if (promise.taskNameResId == R.string.task_api_news_detail) {
            return
        }

        super.onAPIResponseError(promise, exception)
    }


    inner class DetailsNewsPresenter {
        var relatedNewsAdapter: ListHotNewsAdapter? = null


        fun getContext(): Context? {
            return this@DetailsNewsActivity
        }

        fun onBackButtonClicked(sender: View?) {
            finish()
        }
    }


    companion object {
        const val INTENT_DATA_NEWS_ID = "INTENT_DATA_NEWS_ID"
    }

}
