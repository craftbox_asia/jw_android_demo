package asia.craftbox.android.jobway.ui.fragments.pages.home.adapters

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.GridLayoutManager

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home.adapters
 * Date: Dec 28, 2018 - 11:16 PM
 */


class HomePageMenuLayoutManager : GridLayoutManager {

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)
    constructor(context: Context?, spanCount: Int) : super(context, spanCount)
    constructor(context: Context?, spanCount: Int, orientation: Int, reverseLayout: Boolean) : super(context, spanCount, orientation, reverseLayout)


    override fun isAutoMeasureEnabled(): Boolean {
        return true
    }

}
