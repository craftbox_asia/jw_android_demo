package asia.craftbox.android.jobway.ui.fragments.pages.home

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import asia.craftbox.android.jobway.EventBus.ProfileEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentPageHomeBinding
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsFragment
import asia.craftbox.android.jobway.ui.fragments.pages.home.adapters.HomePageMenuAdapter
import asia.craftbox.android.jobway.ui.fragments.pages.home.adapters.HomePageMenuDecoration
import asia.craftbox.android.jobway.ui.fragments.pages.home.adapters.HomePageMenuLayoutManager
import asia.craftbox.android.jobway.ui.fragments.uni.list.ListUniFragment
import asia.craftbox.android.jobway.ui.widgets.NewsHomePageAdapter
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import org.greenrobot.eventbus.EventBus
import kotlin.math.roundToInt

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home
 * Date: Dec 28, 2018 - 10:11 PM
 */


class HomePagePresenter(private val fragment: HomePageFragment) : BasePresenter<FragmentPageHomeBinding>(fragment) {

    val mainMenuAdapter by lazy {
        HomePageMenuAdapter(this@HomePagePresenter.fragment.requireContext())
    }

    val mainMenuDecoration by lazy {
        val context = this@HomePagePresenter.fragment.requireContext()
        val spacing = context.resources.getDimension(R.dimen.dms_spacing_large)
        return@lazy HomePageMenuDecoration(spacing.roundToInt())
    }

    var newsAdapter: NewsHomePageAdapter? = null
    var eduInfoAdapter: NewsHomePageAdapter? = null

    private val toolbarTitleStyled =
        Transformations.map((this@HomePagePresenter.fragment.activity as MainActivity).viewModel.getUserProfile()) { profile ->
            if (profile != null)
            EventBus.getDefault().post(ProfileEvent(profile))
            val nameMerged = arrayOf(profile?.firstName, profile?.lastName).joinToString(" ")
            this@HomePagePresenter.fragment.requireActivity().getString(
                R.string.title_fragment_page_home_welcome,
                nameMerged
            )
        }

    fun getToolbarTitle(): LiveData<String?> {
        return this@HomePagePresenter.toolbarTitleStyled
    }

    fun getMainMenuLayoutManager(): HomePageMenuLayoutManager {
        return HomePageMenuLayoutManager(this@HomePagePresenter.fragment.requireContext(), 3)
    }

    fun setDrawerOpen(isOpen: Boolean) {
        (this@HomePagePresenter.fragment.activity as? MainActivity)?.let { mainActivity ->
            mainActivity.presenter.setDrawerOpen(isOpen)
        }
    }

    fun onReadMoreNews(sender: View?) {
        (this@HomePagePresenter.fragment.activity as? MainActivity)?.let { mainActivity ->
            mainActivity.getFragmentHelper().pushFragment(
                fragment = DMSFragment.create(ListNewsFragment::class.java),
                replaceRootFragment = false
            )
        }
    }

    fun onReadMoreUni(sender: View?) {
        (this@HomePagePresenter.fragment.activity as? MainActivity)?.let { mainActivity ->
            mainActivity.getFragmentHelper().pushFragment(
                fragment = DMSFragment.create(ListUniFragment::class.java),
                replaceRootFragment = false
            )
        }
    }
}
