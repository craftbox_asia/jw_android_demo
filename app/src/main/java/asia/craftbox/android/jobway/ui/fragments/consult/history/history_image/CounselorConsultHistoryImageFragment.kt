package asia.craftbox.android.jobway.ui.fragments.consult.history.history_image

import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultHistoryImageBinding
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import timber.log.Timber


class CounselorConsultHistoryImageFragment : AppMainFragment<FragmentCounselorConsultHistoryImageBinding>(R.layout.fragment_counselor_consult_history_image) {

    private val presenter by lazy {
        CounselorConsultHistoryImagePresenter(this)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        super.onContentViewCreated(rootView, savedInstanceState)
        val fragmentBundle = arguments ?: return
        try {
            val link =
                fragmentBundle.getString(JobWayConstant.INTENT_STRING_URL) ?: return
            ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.imageView, link, 0)
        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }
}
