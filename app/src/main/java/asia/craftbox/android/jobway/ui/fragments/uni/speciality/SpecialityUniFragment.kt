package asia.craftbox.android.jobway.ui.fragments.uni.speciality

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUniDetailFacultyBinding
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniViewModel
import asia.craftbox.android.jobway.ui.fragments.uni.speciality.adapters.FacultyUniRecyclerAdapter
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.admission
 * Date: Feb 14, 2019 - 9:43 PM
 */


class SpecialityUniFragment :
    DMSBindingFragment<FragmentUniDetailFacultyBinding>(R.layout.fragment_uni_detail_faculty),
    DMSViewModelSource<DetailsUniViewModel> {

    private val presenter by lazy {
        SpecialityUniPresenter()
    }

    override val viewModel: DetailsUniViewModel by lazy {
        (this@SpecialityUniFragment.activity as DetailsUniActivity).viewModel
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@SpecialityUniFragment.dataBinding.presenter = this@SpecialityUniFragment.presenter
        this@SpecialityUniFragment.dataBinding.viewModel = this@SpecialityUniFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initObservables() {
        super.initObservables()

        this@SpecialityUniFragment.viewModel.getJobsList().observe(this@SpecialityUniFragment, Observer {
            this@SpecialityUniFragment.presenter.adapter.submitList(it)
            this@SpecialityUniFragment.presenter.adapter
        })

        this@SpecialityUniFragment.viewModel.getJobsDatasource()?.observe(this@SpecialityUniFragment, Observer {
            it?.setListener(this@SpecialityUniFragment)
        })
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_uni_jobs_listing
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_uni_jobs_listing) {
            // disable display hud when paginating
            Timber.wtf("Starting paginate jobs list...")
            return
        }

        super.onAPIWillRequest(promise)
    }

    override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {
        super.onAPIDidFinished(promise)

        if (promise.taskNameResId == R.string.task_api_uni_jobs_listing) {
            Timber.wtf("Finishing paginate jobs list...")
        }
    }


    inner class SpecialityUniPresenter {

        val adapter = FacultyUniRecyclerAdapter()
        val divider by lazy {
            DividerItemDecoration(
                this@SpecialityUniFragment.requireContext(),
                DividerItemDecoration.VERTICAL
            )
        }

    }

}
