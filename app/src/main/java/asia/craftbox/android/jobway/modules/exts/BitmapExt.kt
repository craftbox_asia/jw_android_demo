package asia.craftbox.android.jobway.modules.exts

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.Environment
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


fun Bitmap.compress(quality: Int = 50): Bitmap {
    if (this.width > 1000 || this.height > 1000) {
        val stream = ByteArrayOutputStream()
        this.compress(Bitmap.CompressFormat.JPEG, quality, stream)
        val byteArray = stream.toByteArray()
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
    }
    return this
}

fun Bitmap.resize(maxWidth: Float, maxHeight: Float): Bitmap {
    if (maxHeight > 0 && maxWidth > 0) {
        val ratioBitmap = width / height.toFloat()
        val ratioMax = maxWidth / maxHeight

        var finalWidth = maxWidth
        var finalHeight = maxHeight
        if (ratioMax > ratioBitmap) {
            finalWidth = maxHeight * ratioBitmap
        } else {
            finalHeight = maxWidth / ratioBitmap
        }
        return Bitmap.createScaledBitmap(this, finalWidth.toInt(), finalHeight.toInt(), true)
    }
    return this
}

fun Bitmap.save(context: Context): String {
    val bytes = ByteArrayOutputStream()
    compress(Bitmap.CompressFormat.JPEG, 100, bytes)
    val directory = File(
        (Environment.getExternalStorageDirectory()).toString() + "/Jobway")
    Log.d("fee", directory.toString())
    if (!directory.exists()) {
        directory.mkdirs()
    }
    try {
        Log.d("heel",directory.toString())
        val f = File(directory, ((Calendar.getInstance().timeInMillis).toString() + ".jpg"))
        f.createNewFile()
        val fo = FileOutputStream(f)
        fo.write(bytes.toByteArray())
        MediaScannerConnection.scanFile(context, arrayOf(f.path), arrayOf("image/jpeg"), null)
        fo.close()
        Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

        return f.getAbsolutePath()
    }
    catch (e1: IOException) {
        e1.printStackTrace()
    }

    return ""
}