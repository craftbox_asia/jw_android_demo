package asia.craftbox.android.jobway.ui.fragments.consult.counselors.master

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.entities.services.MasterService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel


class CounselorConsultMasterViewModel : DMSViewModel() {

    fun getCounselorListAPI(callback: ((List<CounselorModel>?) -> Unit)) {
        request(DMSRest.get(AuthAPIService::class.java).listCounselor())
            .then { _, responseData ->
                callback.invoke(responseData.data ?: ArrayList())
            }
            .setTaskNameResId(R.string.task_api_auth_list_counselor)
            .submit()
    }

    fun getFaqListAPI(callback: ((List<QuestionModel>?) -> Unit)) {
        request(DMSRest.get(MasterService::class.java).getFaqs())
            .then { _, responseData ->
                callback.invoke(responseData.data?.data ?: ArrayList())
            }
            .setTaskNameResId(R.string.task_api_master_faqs)
            .submit()
    }
}
