package asia.craftbox.android.jobway.ui.fragments.test.detail;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import asia.craftbox.android.jobway.R;
import com.makeramen.roundedimageview.RoundedImageView;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TestTutorialAdapter extends PagerAdapter {

    private Context mContext;
    TestTutorialItem testTutorialItemMBTI ;
    TestTutorialItem testTutorialItemHolland ;

    private static final List<TestTutorialItem> items = new ArrayList<>();
    
    public TestTutorialAdapter(Context context) {
        super();
        mContext = context;
        testTutorialItemMBTI = new TestTutorialItem(R.drawable.ic_group_mbti, mContext.getString(R.string.title_test_MBTI), mContext.getString(R.string.content_test_MBTI));
        testTutorialItemHolland = new TestTutorialItem(R.drawable.ic_group_holland, mContext.getString(R.string.title_test_HOLLAND), mContext.getString(R.string.content_test_HOLLAND));
        items.add(testTutorialItemMBTI);
        items.add(testTutorialItemHolland);
    }

    @Override
    public void startUpdate(@NonNull ViewGroup container) {
        super.startUpdate(container);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NotNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View item = LayoutInflater.from(container.getContext()).inflate(R.layout.test_tutorial_pager_adapter, container, false);
        TestTutorialItem dataItem = items.get(position);
        TextView textView = item.findViewById(R.id.title_test);
        textView.setText(dataItem.title);
        RoundedImageView imageView = item.findViewById(R.id.image_test_type);
        imageView.setImageResource(dataItem.imageUrl);
        TextView textViewContent = item.findViewById(R.id.content_test);
        textViewContent.setText(dataItem.content);
        container.addView(item);
        return item;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        super.destroyItem(container, position, object);
    }
}
