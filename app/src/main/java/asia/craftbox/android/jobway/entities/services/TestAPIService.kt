package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.ProfileResumeModel
import asia.craftbox.android.jobway.entities.models.ResultTestModel
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.entities.responses.test.HOLLANDTestResponse
import asia.craftbox.android.jobway.entities.responses.test.MBTITestResponse
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.services
 * Date: Jan 18, 2019 - 11:21 AM
 */


interface TestAPIService {

    @GET("api/v1/tests/mbti-questions")
    @Headers("Content-Type: application/json")
    fun getMBTIQuestions(): Observable<DataFieldDMSRestResponse<MBTITestResponse>>

    @GET("api/v1/tests/holland-questions")
    @Headers("Content-Type: application/json")
    fun getHOLLANDQuestions(): Observable<DataFieldDMSRestResponse<HOLLANDTestResponse>>

    @GET("master-service/v1/resume")
    @Headers("Content-Type: application/json")
    fun getResumeItems(): Observable<DataFieldDMSRestResponse<List<ProfileResumeModel>>>

    @POST("api/v1/tests")
    @Headers("Content-Type: application/json")
    fun submit(@Body request: CreateTestRequest?): Observable<DataFieldDMSRestResponse<ResultTestModel>>

}
