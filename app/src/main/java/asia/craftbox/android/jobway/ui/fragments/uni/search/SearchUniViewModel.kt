package asia.craftbox.android.jobway.ui.fragments.uni.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import asia.craftbox.android.jobway.entities.models.UniModel
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.search
 * Date: Feb 18, 2019 - 4:02 PM
 */


class SearchUniViewModel : DMSViewModel() {

    private val searchKeywordMutable = MutableLiveData<String>()
    private val searchDataFactory = SearchUniFactory()
    private val searchResultTransform =
        Transformations.switchMap(this@SearchUniViewModel.searchKeywordMutable) { keyword ->
            this@SearchUniViewModel.searchDataFactory.updateKeyword(keyword)
            val config = PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .build()
            return@switchMap LivePagedListBuilder(
                this@SearchUniViewModel.searchDataFactory,
                config
            ).build()
        }

    private val searchDebounceTask by lazy {
        val instance = PublishSubject.create<String>()
        this@SearchUniViewModel.compositeDisposable.add(
            instance.debounce(300, TimeUnit.MILLISECONDS).subscribe {
                this@SearchUniViewModel.searchKeywordMutable.postValue(it)
            })
        return@lazy instance
    }


    fun getSearchResult(): LiveData<PagedList<UniModel>> {
        return this@SearchUniViewModel.searchResultTransform
    }

    fun getDatasource(): LiveData<SearchUniDatasource> {
        return this@SearchUniViewModel.searchDataFactory.getDatasource()
    }

    fun doSearch(keyword: CharSequence?) {
        this@SearchUniViewModel.searchDebounceTask.onNext(keyword?.toString() ?: "")
    }


    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@SearchUniViewModel.searchKeywordMutable.postValue("")
    }


    inner class SearchUniFactory : DataSource.Factory<Int, UniModel>() {

        private val searchDatasource = MutableLiveData<SearchUniDatasource>()


        override fun create(): DataSource<Int, UniModel> {
            val dataSource = SearchUniDatasource()
            this@SearchUniViewModel.searchKeywordMutable.value?.let { dataSource.setKeyword(it) }
            this@SearchUniFactory.searchDatasource.postValue(dataSource)
            return dataSource
        }

        fun updateKeyword(keyword: String?) {
            this@SearchUniFactory.searchDatasource.value
                ?.setKeyword(keyword)
        }

        fun getDatasource(): LiveData<SearchUniDatasource> {
            return this@SearchUniFactory.searchDatasource
        }

    }

}
