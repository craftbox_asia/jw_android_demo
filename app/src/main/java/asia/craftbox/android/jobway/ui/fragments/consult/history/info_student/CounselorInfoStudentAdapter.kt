package asia.craftbox.android.jobway.ui.fragments.consult.history.info_student

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.exts.toBold
import kotlinx.android.synthetic.main.item_student_info.view.*

class CounselorInfoStudentAdapter(private val totalTitles: ArrayList<ArrayList<String>>, private val totalDescriptions: ArrayList<ArrayList<String>>, val context: Context) :
    RecyclerView.Adapter<CounselorInfoStudentAdapter.ViewHolder>() {

    var tempTotalTitles = arrayListOf<String>()
    var tempTotalDescriptions = arrayListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_student_info, parent, false)
        )
    }

    override fun getItemCount(): Int {
        tempTotalTitles.clear()
        tempTotalDescriptions.clear()
        for (index in 0 until totalTitles.size) {
            tempTotalTitles.addAll(totalTitles[index])
            tempTotalDescriptions.addAll(totalDescriptions[index])
        }
        return tempTotalTitles.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val size0 = totalTitles[0].size
        val size1 = totalTitles[1].size
        val size2 = totalTitles[2].size
        val size3 = totalTitles[3].size

        if (position < size0) {
            holder.title_text_view?.visibility = View.VISIBLE
            holder.title_text_view?.text = tempTotalTitles[position]

            holder.header_text_view?.visibility = View.INVISIBLE

            holder.description_text_view?.visibility = View.VISIBLE
            holder.description_text_view?.text = tempTotalDescriptions[position].toBold()

            holder.sub_description_text_view?.visibility = View.GONE
        }
        else if (position == size0 || position == size0 + size1 || position == size0 + size1 + size2) {
            holder.title_text_view?.visibility = View.INVISIBLE

            holder.header_text_view?.visibility = View.VISIBLE
            holder.header_text_view?.text = tempTotalTitles[position].toBold()

            holder.description_text_view?.visibility = View.GONE
            holder.sub_description_text_view?.visibility = View.GONE
        }
        else if (position < size0 + size1 + size2) {
            holder.title_text_view?.visibility = View.VISIBLE
            holder.title_text_view?.text = tempTotalTitles[position]

            holder.header_text_view?.visibility = View.INVISIBLE

            holder.description_text_view?.visibility = View.GONE

            holder.sub_description_text_view?.visibility = View.VISIBLE
            holder.sub_description_text_view?.text = tempTotalDescriptions[position].toBold()
        }
        else {
            holder.title_text_view?.visibility = View.VISIBLE
            holder.title_text_view?.text = tempTotalTitles[position]

            holder.header_text_view?.visibility = View.INVISIBLE

            holder.description_text_view?.visibility = View.VISIBLE
            holder.description_text_view?.text = tempTotalDescriptions[position].toBold()

            holder.sub_description_text_view?.visibility = View.GONE
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title_text_view: TextView? = view.title_text_view
        val header_text_view: TextView? = view.header_text_view
        val description_text_view: TextView? = view.description_text_view
        val sub_description_text_view: TextView? = view.sub_description_text_view
        val line_view: View? = view.line_view

        fun hideLastItemLineView(hide: Boolean) {
            if (hide) {
                line_view?.visibility = View.INVISIBLE
            } else {
                line_view?.visibility = View.VISIBLE
            }
        }
    }
}