package asia.craftbox.android.jobway.entities.requests.news

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.news
 * Date: Feb 15, 2019 - 2:07 PM
 */


class IncrementNewsRequest : DMSRestRequest() {

    @SerializedName("id")
    var id: Long = -1


    override fun validates(): Boolean {
        return this@IncrementNewsRequest.id.toInt() != -1
    }

}
