package asia.craftbox.android.jobway.libs.coverflow.core;

import android.view.View;


/**
 * Created by yuweichen on 16/4/29.
 */
public interface PageItemClickListener {

    void onItemClick(View view, int position);
}
