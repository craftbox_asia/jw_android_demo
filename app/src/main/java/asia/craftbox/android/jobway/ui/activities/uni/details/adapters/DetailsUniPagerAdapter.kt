package asia.craftbox.android.jobway.ui.activities.uni.details.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.fragments.uni.admission.AdmissionUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.gallery.GalleryUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.intro.IntroUniFragment
import asia.craftbox.android.jobway.ui.fragments.uni.speciality.SpecialityUniFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.uni.details.adapters
 * Date: Feb 14, 2019 - 9:44 PM
 */


class DetailsUniPagerAdapter(private val ctx: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = listOf(
        DMSFragment.create(IntroUniFragment::class.java),
        DMSFragment.create(SpecialityUniFragment::class.java),
        DMSFragment.create(AdmissionUniFragment::class.java),
        DMSFragment.create(GalleryUniFragment::class.java)
    )

    override fun getCount(): Int {
        return this@DetailsUniPagerAdapter.fragments.count()
    }

    override fun getItem(position: Int): Fragment {
        return this@DetailsUniPagerAdapter.fragments[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> this@DetailsUniPagerAdapter.ctx.getString(R.string.title_fragment_uni_detail_intro)
            1 -> this@DetailsUniPagerAdapter.ctx.getString(R.string.title_fragment_uni_detail_speciality)
            2 -> this@DetailsUniPagerAdapter.ctx.getString(R.string.title_fragment_uni_detail_admission)
            3 -> this@DetailsUniPagerAdapter.ctx.getString(R.string.title_fragment_uni_detail_gallery)
            else -> super.getPageTitle(position)
        }
    }

}
