package asia.craftbox.android.jobway.entities.requests.news

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

class BookmarkRquest : DMSRestRequest() {

    @SerializedName("id")
    var id: Long = -1


    override fun validates(): Boolean {
        return this@BookmarkRquest.id.toInt() != -1
    }
}