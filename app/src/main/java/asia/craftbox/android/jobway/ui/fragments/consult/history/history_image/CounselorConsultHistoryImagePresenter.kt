package asia.craftbox.android.jobway.ui.fragments.consult.history.history_image

import android.view.View
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultHistoryImageBinding
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter


class CounselorConsultHistoryImagePresenter(private val fragment: CounselorConsultHistoryImageFragment) : BasePresenter<FragmentCounselorConsultHistoryImageBinding>(fragment) {

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

}
