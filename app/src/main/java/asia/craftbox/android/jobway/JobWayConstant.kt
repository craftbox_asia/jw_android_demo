package asia.craftbox.android.jobway

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway
 * Date: Dec 29, 2018 - 9:52 PM
 */


object JobWayConstant {

    const val DEFAULT_AUTH_SMS_REMAINING_TIME_SECONDS: Int = 120
    const val SUPPORT_SERVICE_PHONE_NUMBER: String = "900"
    const val DEFAULT_COUNTRY_PREFIX_VN: Int = 84

    const val INTENT_QUESTION_MODEL = "INTENT_QUESTION_MODEL"
    const val INTENT_QUESTION_MODEL_INDEX = "INTENT_QUESTION_MODEL_INDEX"
    const val INTENT_STRING_URL = "INTENT_STRING_URL"

    const val DEFAULT_SUBTRACTION_COIN: Int = 50

    const val EVENT_BUS_CLICKED_ON_NOTIFICATION_MESSAGE = "EVENT_BUS_CLICKED_ON_NOTIFICATION_MESSAGE"
    const val TYPE_MESSAGE = "msg"
    const val TYPE_NEWS = "news"
    const val PICK_CAMERA = 1
    const val PICK_GALARY = 2


    enum class Preference(val type: Class<*>, val isSensitive: Boolean = true) {
        AuthAccessToken(String::class.java, true),
        AuthProfileId(Long::class.java, true),
        PrefPushNotificationToken(String::class.java, true),

        AuthSMSVerificationCode(String::class.java, false),
        AuthLoginUserRequest(String::class.java, false),
        AuthSMSVerificationTimestamp(Long::class.java, false)
    }
}
