package asia.craftbox.android.jobway.ui.fragments.consult.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.entities.models.NewsModel

class ConsultingFragmentPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private var items = ArrayList<Fragment>()
    private var onItemSelectedBlock: ((newsItem: NewsModel) -> Unit)? = null


    fun updateItems(items: List<Fragment>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return items[position]
    }

    override fun getCount(): Int {
        return items.count()
    }

}
