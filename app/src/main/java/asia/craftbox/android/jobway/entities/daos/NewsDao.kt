package asia.craftbox.android.jobway.entities.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import asia.craftbox.android.jobway.entities.models.AttachmentModel
import asia.craftbox.android.jobway.entities.models.NewsModel
import com.seldatdirect.dms.core.entities.DMSDao

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.daos
 * Date: Feb 13, 2019 - 3:45 PM
 */


@Dao
interface NewsDao : DMSDao<NewsModel> {

    @Query("SELECT * FROM NewsModel WHERE NewsModel.hotNews =0 ORDER BY NewsModel.id DESC")
    fun getLatestNewsPaginated(): DataSource.Factory<Int, NewsModel>

    @Query("SELECT * FROM NewsModel WHERE NewsModel.favNews = 1 ORDER BY NewsModel.id DESC")
    fun getBookmarkNewsPaginated(): DataSource.Factory<Int, NewsModel>

    @Query("SELECT * FROM NewsModel WHERE NewsModel.hotNews = 0 ORDER BY NewsModel.id DESC LIMIT :limit")
    fun getLatestNewsLimited(limit: Int = 10): LiveData<List<NewsModel>>

    @Query("SELECT * FROM NewsModel WHERE NewsModel.hotNews = 1 ORDER BY NewsModel.id DESC")
    fun getHotNews(): LiveData<List<NewsModel>>

    @Query("SELECT * FROM NewsModel WHERE NewsModel.id = :id")
    fun getNewsByIdFlatten(id: Long?): NewsModel?

    @Query("SELECT * FROM AttachmentModel WHERE AttachmentModel.id = :id")
    fun getAttachmentByIdFlatten(id: Long?): AttachmentModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOneAttachment(item: AttachmentModel): Long

    @Query("UPDATE NewsModel SET hotNews = 0 WHERE 1")
    fun cleanupHotNews()

    @Query("UPDATE NewsModel SET favNews = 0 WHERE 1")
    fun cleanBookmarkNews()

}
