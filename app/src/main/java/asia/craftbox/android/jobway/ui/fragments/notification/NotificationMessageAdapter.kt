package asia.craftbox.android.jobway.ui.fragments.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel
import com.makeramen.roundedimageview.RoundedImageView
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import kotlinx.android.synthetic.main.item_consult_history.view.*
import kotlinx.android.synthetic.main.item_consult_master.view.contain_linear_layout
import kotlinx.android.synthetic.main.item_consult_master.view.description_text_view
import kotlinx.android.synthetic.main.item_consult_master.view.line_view
import kotlinx.android.synthetic.main.item_consult_master.view.rounded_image_view

class NotificationMessageAdapter(private val items: ArrayList<MessageModel>, val context: Context) :
    RecyclerView.Adapter<NotificationMessageAdapter.ViewHolder>() {

    private var onContainClickedCallback: ((item: MessageModel, pos: Int) -> Unit)? = null

    fun setCallback(callback: ((item: MessageModel, pos: Int) -> Unit)?) {
        onContainClickedCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_consult_history, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val isLastItem = position == items.count() - 1
        holder.hideLastItemLineView(isLastItem)
        val item = items[position]
        holder.left_title_text_view?.text = item.userName
        holder.right_title_text_view?.text = item.getNatureTime(context)
        holder.description_text_view?.text = item.body
        holder.loadAvatar(item.avatar)

        holder.contain_linear_layout.setOnClickListener {
            this@NotificationMessageAdapter.onContainClickedCallback?.let { it(item, position) }
        }
        var bgColor = ContextCompat.getColor(context, R.color.color_unread_background)
        if (item.isRead()) {
            bgColor = ContextCompat.getColor(context, R.color.colorWhite)
        }
        holder.contain_linear_layout?.setBackgroundColor(bgColor)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val rounded_image_view: RoundedImageView? = view.rounded_image_view
        val left_title_text_view: TextView? = view.left_title_text_view
        val right_title_text_view: TextView? = view.right_title_text_view
        val description_text_view: TextView? = view.description_text_view
        val line_view: View? = view.line_view
        val contain_linear_layout: LinearLayout = view.contain_linear_layout

        fun hideLastItemLineView(hide: Boolean) {
            if (hide) {
                line_view?.visibility = View.INVISIBLE
            } else {
                line_view?.visibility = View.VISIBLE
            }
        }
        fun loadAvatar(avatar: String?) {
            ImageViewBindingAdapter.imageViewAsyncSrc(rounded_image_view, avatar, 0)
        }
    }
}