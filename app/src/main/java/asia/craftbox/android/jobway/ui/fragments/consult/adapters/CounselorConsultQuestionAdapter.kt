package asia.craftbox.android.jobway.ui.fragments.consult.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.QuestionModel
import kotlinx.android.synthetic.main.item_consult_master.view.description_text_view
import kotlinx.android.synthetic.main.item_consult_master.view.line_view
import kotlinx.android.synthetic.main.item_consult_master.view.title_text_view
import kotlinx.android.synthetic.main.item_consult_question.view.*

class CounselorConsultQuestionAdapter(private val items: List<QuestionModel>, val context: Context) :
    RecyclerView.Adapter<CounselorConsultQuestionAdapter.ViewHolder>() {

    private var onContainClickedCallback: ((item: QuestionModel, pos: Int) -> Unit)? = null

    fun setCallback(callback: ((item: QuestionModel, pos: Int) -> Unit)?) {
        onContainClickedCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_consult_question, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val isLastItem = position == items.count() - 1
        holder.hideLastItemLineView(isLastItem)
        val item = items[position]
        holder.title_text_view?.text = String.format(context.getString(R.string.text_question_at_index), position + 1)
        holder.description_text_view?.text = item.question
        holder.root_item_consult_question?.setOnClickListener {
            onContainClickedCallback?.let { it(item, position)}
        }
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root_item_consult_question: FrameLayout? = view.root_item_consult_question
        val title_text_view: TextView? = view.title_text_view
        val description_text_view: TextView? = view.description_text_view
        val line_view: View? = view.line_view

        fun hideLastItemLineView(hide: Boolean) {
            if (hide) {
                line_view?.visibility = View.INVISIBLE
            } else {
                line_view?.visibility = View.VISIBLE
            }
        }
    }
}