package asia.craftbox.android.jobway.modules.exts

import android.content.Context
import asia.craftbox.android.jobway.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 * September 2018
 */

private const val SECOND_MILLIS = 1000
private const val MINUTE_MILLIS = 60 * SECOND_MILLIS
private const val HOUR_MILLIS = 60 * MINUTE_MILLIS
private const val DAY_MILLIS = 24 * HOUR_MILLIS


fun Date.parse(pattern: String = "yyyy-MM-dd"): String? {
    return try {
        SimpleDateFormat(pattern, Locale.US).format(this@parse)
    } catch (exception: Exception) {
        exception.printStackTrace()
        null
    }
}

fun Date.timeAgo(context: Context): String? {
    val timeInMillis = this@timeAgo.time
    val currentTimeInMillis = System.currentTimeMillis()
    val diff = currentTimeInMillis - timeInMillis

    return when {
        diff < MINUTE_MILLIS -> context.getString(R.string.common_formatted_datetime_just_now)
        diff < (2 * MINUTE_MILLIS) -> context.getString(R.string.common_formatted_datetime_an_minute_ago)
        diff < (50 * MINUTE_MILLIS) -> context.getString(
            R.string.common_formatted_datetime_x_minutes_ago,
            (diff / MINUTE_MILLIS)
        )
        diff < (90 * MINUTE_MILLIS) -> context.getString(R.string.common_formatted_datetime_an_hour_ago)
        diff < (24 * HOUR_MILLIS) -> context.getString(
            R.string.common_formatted_datetime_x_hours_ago,
            (diff / HOUR_MILLIS)
        )
        diff < (48 * HOUR_MILLIS) -> context.getString(R.string.common_formatted_datetime_yesterday)
        else -> context.getString(R.string.common_formatted_datetime_x_days_ago, (diff / DAY_MILLIS))
    }
}

