package asia.craftbox.android.jobway.ui.fragments.bookmark

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentBookmarkListBinding
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.activities.news.details.DetailsNewsActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.bookmark.ListBookmarkViewModel
import asia.craftbox.android.jobway.ui.fragments.news.list.adapters.ListLatestNewsAdapter
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import timber.log.Timber


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list
 * Date: Feb 13, 2019 - 2:38 PM
 */


class ListBookmarkNewsFragment : AppMainFragment<FragmentBookmarkListBinding>(R.layout.fragment_bookmark_list),
    DMSViewModelSource<ListBookmarkViewModel> {

    private val presenter by lazy {
        ListBookmarkNewsPresenter(this)
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@ListBookmarkNewsFragment)[ListBookmarkViewModel::class.java]
    }


    override fun getTitleResId(): Int? {
        return R.string.title_bookmark_list
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ListBookmarkNewsFragment.dataBinding.presenter = this@ListBookmarkNewsFragment.presenter
        this@ListBookmarkNewsFragment.dataBinding.viewModel = this@ListBookmarkNewsFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()

        // setup latest news
        val latestLayoutMgr = LinearLayoutManager(this@ListBookmarkNewsFragment.requireContext())
        latestLayoutMgr.isAutoMeasureEnabled = true

        this@ListBookmarkNewsFragment.presenter.latestNewsAdapter = ListLatestNewsAdapter()
        this@ListBookmarkNewsFragment.dataBinding.newsListLatestRecycleView.adapter =
            this@ListBookmarkNewsFragment.presenter.latestNewsAdapter
        this@ListBookmarkNewsFragment.dataBinding.newsListLatestRecycleView.layoutManager = latestLayoutMgr
        this@ListBookmarkNewsFragment.dataBinding.newsListLatestRecycleView.isNestedScrollingEnabled = false

        this@ListBookmarkNewsFragment.presenter.latestNewsAdapter?.setOnItemSelected {
            (this@ListBookmarkNewsFragment.activity as? MainActivity)?.let { mainActivity ->
                val newsActivity = Intent(mainActivity, DetailsNewsActivity::class.java)
                newsActivity.putExtra(DetailsNewsActivity.INTENT_DATA_NEWS_ID, it.id)
                mainActivity.startActivity(newsActivity)
            }
        }
    }

    override fun initObservables() {
        super.initObservables()

        this@ListBookmarkNewsFragment.viewModel.getLatestNews().observe(this@ListBookmarkNewsFragment, Observer {
            this@ListBookmarkNewsFragment.presenter.latestNewsAdapter?.submitList(it)
        })
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_news_list_retrieve,
            R.string.task_api_news_hot_list_retrieve
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (arrayOf(
                R.string.task_api_news_list_retrieve,
                R.string.task_api_news_hot_list_retrieve
            ).contains(promise.taskNameResId)
        ) {
            return
        }

        super.onAPIWillRequest(promise)
    }


    inner class ListBookmarkNewsPresenter(private val fragment: ListBookmarkNewsFragment): BasePresenter<FragmentBookmarkListBinding>(fragment) {

        var latestNewsAdapter: ListLatestNewsAdapter? = null

        fun setDrawerOpen(isOpen: Boolean) {
            (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
        }

    }

}