package asia.craftbox.android.jobway.ui.activities.faculty;

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityFullFacultyDetailBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.ui.fragments.faculty.fulldetails.FullFacultyDetailFragment
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper

class FullFacultyDetailActivity : DMSBindingActivity<ActivityFullFacultyDetailBinding>(R.layout.activity_full_faculty_detail),
    DMSViewModelSource<FullFacultyDetailActivityViewModel> {
    private val presenter: FullFacultyDetailActivityPresenter = FullFacultyDetailActivityPresenter(this@FullFacultyDetailActivity)
    override val viewModel by lazy { ViewModelProviders.of(this@FullFacultyDetailActivity)[FullFacultyDetailActivityViewModel::class.java] }

    private val fragmentUtil by lazy {
        DMSFragmentHelper(
            this@FullFacultyDetailActivity.supportFragmentManager,
            R.id.full_faculty_detail_container
        )
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@FullFacultyDetailActivity.dataBinding.presenter = this@FullFacultyDetailActivity.presenter
        this@FullFacultyDetailActivity.dataBinding.viewModel = this@FullFacultyDetailActivity.viewModel

        if (savedInstanceState == null) {

            val  facId = intent.getLongExtra(FACULTY_DETAIL_BY_FAC_ID, -1)
            if (facId.compareTo(-1) != 0) {
                this@FullFacultyDetailActivity.fragmentUtil.initialRootFragment(
                    fragment = FullFacultyDetailFragment.newInstance(facId)
                )
            }
            else {
                val facObj = intent.getSerializableExtra(FACULTY_DETAIL_BY_OBJECT) as FacultyModel
                this@FullFacultyDetailActivity.fragmentUtil.initialRootFragment(
                    fragment = FullFacultyDetailFragment.newInstance(facObj)
                )
            }
        }

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    companion object{
        val FACULTY_DETAIL_BY_OBJECT = "FACULTY_DETAIL_BY_OBJECT"
        val FACULTY_DETAIL_BY_FAC_ID = "FACULTY_DETAIL_BY_FAC_ID"
    }
}