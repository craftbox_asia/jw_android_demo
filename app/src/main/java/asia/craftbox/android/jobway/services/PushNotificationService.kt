package asia.craftbox.android.jobway.services

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.annotation.StringRes
import androidx.core.app.NotificationCompat
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.requests.notification.UpdateTokenNotificationRequest
import asia.craftbox.android.jobway.entities.services.PushAPIService
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.services
 * Date: Jan 08, 2019 - 4:24 PM
 */


class PushNotificationService : FirebaseMessagingService() {

    override fun onNewToken(optionalToken: String?) {
        super.onNewToken(optionalToken)
        Timber.wtf(optionalToken)
        DMSPreference.default().set(
            JobWayConstant.Preference.PrefPushNotificationToken.name,
            optionalToken
        )

        PushNotificationService.submitToBackend(optionalToken)
    }

    override fun onMessageReceived(optionalRemoteMessage: RemoteMessage?) {
        super.onMessageReceived(optionalRemoteMessage)
        val firebaseNotification = optionalRemoteMessage?.notification ?: return

        val title = firebaseNotification.title ?: return
        val body = firebaseNotification.body ?: return
        PushNotificationService.push(this@PushNotificationService, title, body)
    }

    companion object {
        fun push(
            context: Context,
            title: String,
            contents: String,
            overrideId: Int = (System.currentTimeMillis() % 1000).toInt(),
            @StringRes overrideChannelIdRes: Int = R.string.const_notification_channel_id_push_default
        ) {
            val mainActivity = Intent(context.applicationContext, MainActivity::class.java)
            mainActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            val mainPending = PendingIntent.getActivity(
                context.applicationContext,
                overrideId,
                mainActivity,
                PendingIntent.FLAG_ONE_SHOT
            )

            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE)
                    as NotificationManager
            val notificationBuilder = NotificationCompat.Builder(context, context.getString(overrideChannelIdRes))
                .setSmallIcon(R.drawable.dms_ic_notification_default)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(contents)
                .setContentIntent(mainPending)

            notificationManager.notify(overrideId, notificationBuilder.build())
        }

        fun submitToBackend(newToken: String?) {
            val requester = UpdateTokenNotificationRequest()
            requester.token = newToken

            DMSRestPromise(DMSRest.get(PushAPIService::class.java).updateToken(requester))
                .then {
                        _, _ -> Timber.d("FCM token has been updated successfully!") }
                .catch {
                        _, exc -> Timber.wtf(exc.toJSON()) }
                .submit()
        }
    }

}
