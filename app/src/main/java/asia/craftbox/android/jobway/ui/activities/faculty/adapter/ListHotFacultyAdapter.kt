package asia.craftbox.android.jobway.ui.activities.faculty.adapter

import android.content.Intent
import android.os.Bundle
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ItemPagerFacultyHotBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDetailActivity
import asia.craftbox.android.jobway.ui.activities.faculty.FullFacultyDetailActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.fragments.faculty.list.ListFacultyFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list.adapters
 * Date: Feb 18, 2019 - 6:25 PM
 */


class ListHotFacultyAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val items = ArrayList<FragmentItem>()
    private var onItemSelectedBlock: ((newsItem: FacultyModel) -> Unit)? = null


    fun setOnItemSelected(block: ((newsItem: FacultyModel) -> Unit)?) {
        this@ListHotFacultyAdapter.onItemSelectedBlock = block
    }


    fun updateItems(items: List<FragmentItem>) {
        this@ListHotFacultyAdapter.items.clear()
        this@ListHotFacultyAdapter.items.addAll(items)
        this@ListHotFacultyAdapter.notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return this@ListHotFacultyAdapter.items[position]
    }

    override fun getCount(): Int {
        return this@ListHotFacultyAdapter.items.count()
    }


    class FragmentItem : Fragment() {

        private var dataBinding: ItemPagerFacultyHotBinding? = null


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            this@FragmentItem.dataBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.item_pager_faculty_hot,
                container,
                false
            )
            return this@FragmentItem.dataBinding!!.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            try {
                val bundle = this@FragmentItem.arguments ?: return
                val facultyItem = bundle.getSerializable(FACULTY_MODEL) as? FacultyModel ?: return

                this@FragmentItem.dataBinding?.presenter = FragmentItemPresenter(facultyItem)
                this@FragmentItem.dataBinding?.invalidateAll()

                val onItemClicked = View.OnClickListener {view ->
                    var mActivity = this@FragmentItem.activity
                    val fFacultyActivity = Intent(mActivity, FullFacultyDetailActivity::class.java)
                    fFacultyActivity.putExtra(FullFacultyDetailActivity.FACULTY_DETAIL_BY_OBJECT, facultyItem)
                    mActivity?.startActivity(fFacultyActivity)
                }

                view.findViewById<View?>(R.id.pager_faculty_view)
                    ?.setOnClickListener(onItemClicked)
            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }


        inner class FragmentItemPresenter(val item: FacultyModel) {

            fun getCollegeType(): Spanned? {
                return null
            }

            fun getCollegeCode(): Spanned? {
                return null
            }
        }


        companion object {
            internal const val FACULTY_MODEL = "FACULTY_MODEL"

            fun create(uniItem: FacultyModel): FragmentItem {
                val instance = FragmentItem()
                instance.arguments = Bundle()
                instance.arguments?.putSerializable(FACULTY_MODEL, uniItem)
                return instance
            }
        }

    }

}