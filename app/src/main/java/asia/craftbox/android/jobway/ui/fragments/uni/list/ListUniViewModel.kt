package asia.craftbox.android.jobway.ui.fragments.uni.list

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.entities.services.UniAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list
 * Date: Feb 14, 2019 - 3:53 PM
 */


class ListUniViewModel : DMSViewModel() {

    private val itemPerPage = 10
    private var currentPage: Int = 0
    private var totalPage: Int = Int.MAX_VALUE

    private val uniPagedData by lazy {
        val dataFactory = AppData.get().getUni().getUniPagination()
        val pageListBuilder = LivePagedListBuilder(dataFactory, this@ListUniViewModel.itemPerPage)
        pageListBuilder.setBoundaryCallback(this@ListUniViewModel.onUniPagingBoundary)
        return@lazy pageListBuilder.build()
    }

    private val fetchUniListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@ListUniViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                if (this@ListUniViewModel.currentPage < (this@ListUniViewModel.totalPage - 1)) {
                    this@ListUniViewModel.request(DMSRest.get(UniAPIService::class.java).listUni(page = this@ListUniViewModel.currentPage + 1))
                        .then { _, responseData ->
                            this@ListUniViewModel.currentPage = responseData.data?.meta?.currentPage ?: 1
                            this@ListUniViewModel.totalPage = responseData.data?.meta?.totalPages ?: 1

                            responseData.data?.data?.forEach {
                                if (AppData.get().getUni().countHotUniByIdFlatten(it.id) == 0) {
                                    it.saveOrUpdate()
                                }
                            }
                        }
                        .setTaskNameResId(R.string.task_api_uni_list_retrieve)
                        .submit()
                }
            })
        return@lazy instance
    }

    private val fetchHotUniListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@ListUniViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@ListUniViewModel.request(DMSRest.get(UniAPIService::class.java).hotUni())
                    .then { _, responseData ->
                        AppData.get().getUni().cleanupHotUni()
                        responseData.data?.forEach {
                            it.hotUni = 1
                            it.saveOrUpdate()
                        }
                    }
                    .setTaskNameResId(R.string.task_api_uni_list_retrieve)
                    .submit()
            })
        return@lazy instance
    }

    private val onUniPagingBoundary = object : PagedList.BoundaryCallback<UniModel>() {

        override fun onItemAtEndLoaded(itemAtEnd: UniModel) {
            super.onItemAtEndLoaded(itemAtEnd)

            this@ListUniViewModel.fetchUniListTask.onNext(true)
        }
    }


    fun getUniPagination(): LiveData<PagedList<UniModel>> {
        return this@ListUniViewModel.uniPagedData
    }

    fun getHotUni(): LiveData<List<UniModel>> {
        return AppData.get().getUni().hotUni()
    }

    fun doFetchUniList() {
        this@ListUniViewModel.currentPage = 0
        this@ListUniViewModel.totalPage = Int.MAX_VALUE
        this@ListUniViewModel.fetchUniListTask.onNext(true)
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@ListUniViewModel.doFetchUniList()
        this@ListUniViewModel.fetchHotUniListTask.onNext(true)
    }

}
