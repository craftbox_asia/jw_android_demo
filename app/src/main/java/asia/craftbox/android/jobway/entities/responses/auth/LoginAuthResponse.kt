package asia.craftbox.android.jobway.entities.responses.auth

import asia.craftbox.android.jobway.entities.models.ProfileModel
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.responses.auth
 * Date: Jan 09, 2019 - 5:12 PM
 */


class LoginAuthResponse : DataFieldDMSRestResponse<LoginAuthResponse.Data>() {

    class Data : DMSJsonableObject {

        @SerializedName("token")
        var token: String? = null

        @SerializedName("userInfo")
        var userInfo: ProfileModel? = null

    }
}
