package asia.craftbox.android.jobway.ui.activities.main

import android.util.Log
import androidx.lifecycle.LiveData
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.entities.services.TestAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * December 2018
 */


class MainViewModel : DMSViewModel() {

    private val fetchProfileTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@MainViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@MainViewModel.request(DMSRest.get(AuthAPIService::class.java).viewProfile())
                    .then { _, data ->
                        data.data?.saveOrUpdate()
                        this@MainViewModel.fetchResumeTask.onNext(data.data?.id ?: -1)
                    }
                    .setTaskNameResId(R.string.task_api_auth_profile)
                    .submit()
            })
        return@lazy instance
    }

    private val fetchResumeTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@MainViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@MainViewModel.request(DMSRest.get(TestAPIService::class.java).getResumeItems())
                    .then { _, data ->
                        data.data?.forEach { resumeItem -> resumeItem.saveOrUpdate() }
                    }
                    .setTaskNameResId(R.string.task_api_auth_profile_resume)
                    .submit()
            })
        return@lazy instance
    }


    fun getUserProfile(): LiveData<ProfileModel> {
        val profileId = DMSPreference.default().get(JobWayConstant.Preference.AuthProfileId.name, Long::class.java)
        return AppData.get().getProfile().getProfileById(profileId ?: -1)
    }

    fun doFetchUserProfile() {
        val profileId = DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
        this@MainViewModel.fetchProfileTask.onNext(profileId)
    }

    fun getProfileId(): Long {
        return DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
    }

    fun uploadAvatar(file: File) {
        val mediaType = MediaType.parse("image/*")
        val requestFile = RequestBody.create(mediaType, file)
        if (file.isFile) {
            val part = MultipartBody.Part.createFormData( "avatar", file.name, requestFile)
            request(DMSRest.get(AuthAPIService::class.java).uploadAvatar(part))
                .setTaskNameResId(R.string.task_api_auth_update_profile)
                .then { _, data ->
                    Log.i("data", data.toString())
                    AppData.get().getProfile().updateAvatar(getProfileId(), data.urlAvatar)
                }
                .submit()
        }
    }

}
