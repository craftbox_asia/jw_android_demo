package asia.craftbox.android.jobway.ui.fragments.users.verify

import android.app.Activity
import androidx.databinding.ObservableField
import asia.craftbox.android.jobway.EventBus.PhoneVerificationFail
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.requests.auth.RegisterAuthRequest
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import com.crashlytics.android.Crashlytics
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.users.verify
 * Date: Jan 13, 2019 - 8:19 PM
 */


class VerifyUserViewModel : DMSViewModel() {

    private var isRegisteredPhone = false
    private var verificationId: String? = null
    private var validPhoneCredential: PhoneAuthCredential? = null

    private var verificationTimestamp = System.currentTimeMillis()
    val verificationRemainingSeconds = ObservableField(60)

    private var resendVerificationToken: PhoneAuthProvider.ForceResendingToken? = null
    private var registerRequest = RegisterAuthRequest()
    private var onVerificationSuccess: ((credential: PhoneAuthCredential) -> Unit)? = null


    fun setRequest(registerRequest: RegisterAuthRequest, registeredPhone: Boolean) {
        this@VerifyUserViewModel.registerRequest = registerRequest
        this@VerifyUserViewModel.isRegisteredPhone = registeredPhone
    }

    fun getRequest(): RegisterAuthRequest {
        return this@VerifyUserViewModel.registerRequest
    }

    fun setOnVerificationSuccessBlock(block: ((credential: PhoneAuthCredential) -> Unit)?) {
        this@VerifyUserViewModel.onVerificationSuccess = block
    }

    fun doResendVerificationCode(context: Activity) {
        val phoneNumber = this@VerifyUserViewModel.registerRequest.getValidPhoneNumber() ?: return
        this@VerifyUserViewModel.resetForm()

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,
            120,
            TimeUnit.SECONDS,
            context,
            this@VerifyUserViewModel.onPhoneAuthResult
        )
    }

    fun doSubmitAuth() {
        this@VerifyUserViewModel.authTask.onNext(true)
    }

    fun getVerificationId(): String? {
        return this@VerifyUserViewModel.verificationId
    }

    private val onPhoneAuthResult = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onCodeSent(verificationId: String?, forceResendToken: PhoneAuthProvider.ForceResendingToken?) {
            Timber.wtf("onCodeSent: $verificationId")
            super.onCodeSent(verificationId, forceResendToken)

            this@VerifyUserViewModel.verificationId = verificationId
            this@VerifyUserViewModel.resendVerificationToken = forceResendToken

            this@VerifyUserViewModel.verificationTimestamp = System.currentTimeMillis()
            this@VerifyUserViewModel.verificationRemainingSeconds.set(60)
        }

        override fun onCodeAutoRetrievalTimeOut(verificationId: String?) {
            Timber.wtf("onCodeAutoRetrievalTimeOut: $verificationId")
            super.onCodeAutoRetrievalTimeOut(verificationId)

            this@VerifyUserViewModel.verificationId = verificationId
        }

        override fun onVerificationCompleted(p0: PhoneAuthCredential?) {
            Timber.wtf("onVerificationCompleted: ${p0?.smsCode}")
            this@VerifyUserViewModel.validPhoneCredential = p0
            if (p0?.smsCode == null) {
                EventBus.getDefault().post(PhoneVerificationFail())
                return
            }
            p0?.let { this@VerifyUserViewModel.onVerificationSuccess?.invoke(it) }
        }

        override fun onVerificationFailed(p0: FirebaseException?) {
            Timber.wtf("onVerificationFailed: ${p0?.localizedMessage}")

            saveVerificationCode(null)
            this@VerifyUserViewModel.resetForm()

            this@VerifyUserViewModel.postCustomException(
                DMSRestPromise(Observable.just(p0)).setTaskNameResId(R.string.task_firebase_auth_verification),
                p0?.localizedMessage
            )
        }

        fun saveVerificationCode(verificationCode: String?) {
            DMSPreference.default().set(
                JobWayConstant.Preference.AuthLoginUserRequest.name,
                this@VerifyUserViewModel.registerRequest
            )
            DMSPreference.default().set(
                JobWayConstant.Preference.AuthSMSVerificationCode.name,
                verificationCode
            )
            DMSPreference.default().set(
                JobWayConstant.Preference.AuthSMSVerificationTimestamp.name,
                (System.currentTimeMillis() / 1000).toInt()
            )
        }

    }

    private val authTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@VerifyUserViewModel.compositeDisposable.add(
            instance.debounce(
                400,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@VerifyUserViewModel.registerRequest.phone = this@VerifyUserViewModel
                    .registerRequest.getValidPhoneNumber()

                if (this@VerifyUserViewModel.isRegisteredPhone) {
                    this@VerifyUserViewModel.request(
                        DMSRest.get(AuthAPIService::class.java)
                            .login(this@VerifyUserViewModel.registerRequest)

                    )
                        .then { _, res ->
                            DMSPreference.default().set(
                                JobWayConstant.Preference.AuthProfileId.name,
                                res.data?.userInfo?.id
                            )
                            DMSPreference.default().set(
                                JobWayConstant.Preference.AuthAccessToken.name,
                                res.data?.token
                            )
                            DMSRest.config
                                .updateAuthorizationToken(res.data?.token)
                            res.data?.userInfo?.saveOrUpdate()

                            Crashlytics.setUserIdentifier(res.data?.userInfo?.phone ?: "unknown")
                            Crashlytics.setUserName(
                                arrayOf(
                                    res.data?.userInfo?.firstName,
                                    res.data?.userInfo?.lastName
                                ).mapNotNull { !it.isNullOrBlank() }.joinToString(" ")
                            )
                        }
                        .setTaskNameResId(R.string.task_api_auth_login)
                        .submit()
                } else {
                    this@VerifyUserViewModel.request(
                        DMSRest.get(AuthAPIService::class.java)
                            .register(this@VerifyUserViewModel.registerRequest)
                    )
                        .then { _, _ ->
                            this@VerifyUserViewModel.isRegisteredPhone = true
                            this@VerifyUserViewModel.doSubmitAuth()
                        }
                        .setTaskNameResId(R.string.task_api_auth_register)
                        .submit()
                }
            })
        return@lazy instance
    }

    private fun resetForm() {
        this@VerifyUserViewModel.verificationId = null
        this@VerifyUserViewModel.resendVerificationToken = null

        FirebaseAuth.getInstance().signOut()
        FirebaseAuth.getInstance().setLanguageCode(Locale.getDefault().language)
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()

        this@VerifyUserViewModel.compositeDisposable.add(
            Observable.interval(
                1000,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            )
                .filter { (this@VerifyUserViewModel.verificationRemainingSeconds.get() ?: 0) > 0 }
                .subscribe {
                    val time = this@VerifyUserViewModel.verificationRemainingSeconds.get() ?: 0

                    if (time > 0) {
                        this@VerifyUserViewModel.verificationRemainingSeconds.set(time - 1)
                    }
                })
    }

    override fun lifecycleOnResume() {
        super.lifecycleOnResume()
        this@VerifyUserViewModel.verificationRemainingSeconds.set(
            ((System.currentTimeMillis() / 1000) - (this@VerifyUserViewModel.verificationTimestamp / 1000)).toInt()
        )
    }

    fun getCredential(): PhoneAuthCredential? {
        return this@VerifyUserViewModel.validPhoneCredential
    }

}
