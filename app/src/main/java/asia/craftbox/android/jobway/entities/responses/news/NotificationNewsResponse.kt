package asia.craftbox.android.jobway.entities.responses.news

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

class NotificationNewsResponse<T> : DMSRestResponse {

    @SerializedName("data")
    var data: List<T> = emptyList()

    @SerializedName("meta")
    var meta: Meta = Meta()


    class Meta : DMSJsonableObject {

        @SerializedName("current_page")
        var currentPage: Int = 1

        @SerializedName("count")
        var count: Int = 0

        @SerializedName("per_page")
        var perPage: Int = 0

        @SerializedName("total")
        var total: Int = 0

        @SerializedName("total_pages")
        var totalPages: Int = 0

        @SerializedName("links")
        var links: Link = Link()


        class Link : DMSJsonableObject {

            @SerializedName("previous")
            var previous: String? = null

            @SerializedName("next")
            var next: String? = null

        }

    }

}
