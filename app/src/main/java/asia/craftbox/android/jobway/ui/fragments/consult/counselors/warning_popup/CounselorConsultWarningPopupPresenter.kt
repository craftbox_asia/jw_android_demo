package asia.craftbox.android.jobway.ui.fragments.consult.counselors.warning_popup

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.default_consulting.CounselorConsultDefaultCounsultingFragment
import asia.craftbox.android.jobway.ui.fragments.explore.home.HomeExploreFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorConsultWarningPopupPresenter(private val fragment: CounselorConsultWarningPopupFragment) :
    BaseObservable() {

    var currentCounselorModel: CounselorModel? = null

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onExploreButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
//        mainActivity?.getFragmentHelper()?.popToRoot()
//        mainActivity?.getFragmentHelper()?.pushFragment(
//            DMSFragment.create(HomeExploreFragment::class.java)
//        )
//
        val defaultCounsultingFragment = DMSFragment.create(HomeExploreFragment::class.java)
        mainActivity?.getFragmentHelper()?.pushFragment(defaultCounsultingFragment)
    }

    fun onContinueConsultButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        val defaultCounsultingFragment = DMSFragment.create(CounselorConsultDefaultCounsultingFragment::class.java)
        mainActivity?.getFragmentHelper()?.pushFragment(defaultCounsultingFragment)
    }

}
