package asia.craftbox.android.jobway.entities.models

import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import asia.craftbox.android.jobway.JobWayApp
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.exts.toBold
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel
import kotlin.math.absoluteValue

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 27, 2019 - 3:38 PM
 */


@Entity
open class FacultyModel : AppModel() {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("fac_cd")
    var fac_cd: String? = null

    @SerializedName("featured_img")
    var featuredImg: String? = null

    @SerializedName("uni_logo")
    var uniLogo: String? = null

    @SerializedName("uni_featured_img")
    var uniFeaturedImg: String? = null


    @SerializedName("uni_name")
    var uniName: String? = null

    @SerializedName("fac_name")
    var facName: String? = null

    @SerializedName("uni_type_name")
    var uniTypeName: String? = null

    @SerializedName("uni_class_name")
    var uniClassName: String? = null

    @SerializedName("target")
    var target: Int? = null

    @SerializedName("uni_id")
    var uniId: Long? = null

    @SerializedName("benchmark")
    var benchmark: String? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("des")
    var des: String? = null

    @SerializedName("count_fac")
    var countFac: Long? = null

    @SerializedName("hot_fac")
    var hotFaculty: Int = 0

    @SerializedName("sub_fac_id")
    var subFacultyId: Int? = 0



    fun getFeatureImg(): String? {
        return this@FacultyModel.featuredImg
    }

    fun getFeatureUniImg(): String? {
        return  this@FacultyModel?.uniFeaturedImg
    }

    fun getFeatureUniLogo(): String? {
        return  this@FacultyModel?.uniLogo
    }

    fun getFacNameToString(): String {
        if (facName.isNullOrBlank())
        {
            if (name.isNullOrEmpty())
            {
                return ""
            }else{
                return  name.toString()
            }
            return ""
        }
        return  facName.toString()
    }

    fun getUniNameToString(): String {
        if (uniName.isNullOrBlank())
        {
            return  ""
        }
        return uniName!!.toString()
    }

    fun getFacCdToString(): String {
        if (fac_cd.isNullOrBlank()){
            return ""
        }
        return fac_cd.toString()
    }

    fun getFeartureImage(): String {
        if (featuredImg.isNullOrBlank())
        {
            return ""
        }
        return featuredImg.toString()
    }


    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getFaculty()
            .getFacultyByIdFlatten(this@FacultyModel.onCopyPrimaryKey())
            ?: this@FacultyModel
        mergeFields(this@FacultyModel, instance)

        instance.localUpdatedAt = System.currentTimeMillis()
        return AppData.get().getFaculty().insertOne(instance)
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<FacultyModel>() {
            override fun areItemsTheSame(oldItem: FacultyModel, newItem: FacultyModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: FacultyModel, newItem: FacultyModel): Boolean {
                return false
            }
        }
    }

}
