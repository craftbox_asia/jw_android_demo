package asia.craftbox.android.jobway.entities.requests.auth

import android.util.Patterns
import androidx.databinding.Bindable
import asia.craftbox.android.jobway.BR
import asia.craftbox.android.jobway.entities.models.ProfileModel
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.auth
 * Date: Jan 25, 2019 - 11:11 AM
 */


/**
"{
""first_name"": ""Tri"",
""last_name"": ""Le"",
""email"": ""ldtri0209@gmail.com"",
""date_of_birth"": ""2019-01-25"",
""summary"": ""La nguoi dep trai"",
""hobbies"": ""Banh bong, am nhac, thoi trang"",
""advantage"": ""Dep trai, Dep trai, Dep trai"",
""defect"": ""It noi, It noi, It noi"",
""career"": ""Tong thong Hoa Ky"",
""job_idol"": ""Nhat Vuong"",
""affect_people"": ""Daddy"",
""be_evaluate"": ""Thong minh, Dep trai"",
""be_advice"": ""Tu tin, quyet doan hon""
}"
 */
class UpdateProfileRequest : DMSRestRequest() {

    @SerializedName("first_name")
    var firstName: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.firstName)
        }

    @SerializedName("last_name")
    var lastName: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.lastName)
        }

    @SerializedName("email")
    var email: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.email)
        }

    @SerializedName("date_of_birth")
    var dateOfBirth: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.dateOfBirth)
        }

    @SerializedName("phone")
    var phone: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.phone)
        }

    @SerializedName("summary")
    var summary: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.summary)
        }

    @SerializedName("hobbies")
    var hobbies: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.hobbies)
        }

    @SerializedName("advantage")
    var advantage: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.advantage)
        }

    @SerializedName("defect")
    var defect: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.defect)
        }

    @SerializedName("career")
    var career: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.career)
        }

    @SerializedName("job_idol")
    var jobIdol: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.jobIdol)
        }

    @SerializedName("affect_people")
    var affectPeople: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.affectPeople)
        }

    @SerializedName("be_evaluate")
    var beEvaluate: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.beEvaluate)
        }

    @SerializedName("be_advice")
    var beAdvice: String? = null
        @Bindable get() = if (field.isNullOrBlank()) null else field
        set(value) {
            field = value
            this@UpdateProfileRequest.notifyPropertyChanged(BR.beAdvice)
        }


    fun updateFirstName(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.firstName = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateLastName(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.lastName = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateEmail(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.email = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updatePhone(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.phone = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateDateOfBirth(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.dateOfBirth = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateSummary(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.summary = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateHobbies(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.hobbies = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateAdvantage(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.advantage = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateDefect(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.defect = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateCareer(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.career = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateJobIdol(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.jobIdol = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateAffectPeople(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.affectPeople = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateBeAdvice(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.beAdvice = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun updateBeEvaluate(sender: CharSequence?, before: Int, after: Int, count: Int) {
        this@UpdateProfileRequest.beEvaluate = sender?.toString()
        this@UpdateProfileRequest.notifyValidation()
    }

    fun getDisplayName(): String {
        return arrayOf(
            this@UpdateProfileRequest.firstName,
            this@UpdateProfileRequest.lastName
        )
            .filterNotNull()
            .joinToString(" ")
    }

    override fun validates(): Boolean {
        return !this@UpdateProfileRequest.firstName.isNullOrBlank()
                && !this@UpdateProfileRequest.lastName.isNullOrBlank()
                && Patterns.EMAIL_ADDRESS.matcher(this@UpdateProfileRequest.email ?: "").matches()
    }

    companion object {
        fun createFromProfileModel(profileModel: ProfileModel): UpdateProfileRequest {
            val instance = UpdateProfileRequest()
            val profileResult = profileModel.getProfileResult()

            instance.firstName = profileModel.firstName
            instance.lastName = profileModel.lastName
            instance.phone = profileModel.phone
            instance.email = profileModel.email
            instance.dateOfBirth = profileModel.dateOfBirth

            instance.summary = profileModel.summary
            instance.hobbies = profileResult?.hobbies
            instance.advantage = profileResult?.advantage
            instance.defect = profileResult?.defect
            instance.career = profileResult?.career
            instance.jobIdol = profileResult?.jobIdol
            instance.affectPeople = profileResult?.affectPeople
            instance.beEvaluate = profileResult?.beEvaluate
            instance.beAdvice = profileResult?.beAdvice

            return instance
        }
    }

}
