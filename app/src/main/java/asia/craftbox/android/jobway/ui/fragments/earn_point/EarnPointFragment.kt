package asia.craftbox.android.jobway.ui.fragments.earn_point

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentEarnPointBinding
import asia.craftbox.android.jobway.entities.services.MasterService
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BaseDMSViewModel
import com.google.gson.JsonObject
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.datasources.DMSViewModelSource

class EarnPointFragment: AppMainFragment<FragmentEarnPointBinding>(R.layout.fragment_earn_point),
    DMSViewModelSource<BaseDMSViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@EarnPointFragment)[BaseDMSViewModel::class.java]
    }

    val presenter by lazy {
        EarnPointPresenter(this@EarnPointFragment)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@EarnPointFragment.dataBinding.presenter = this@EarnPointFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
        earnPointApi()
    }

    override fun initUI() {
        super.initUI()
        // optimize webview contents
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.displayZoomControls = false
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.builtInZoomControls = true
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.loadWithOverviewMode = true
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.useWideViewPort = true
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.setSupportZoom(false)
        this@EarnPointFragment.dataBinding.earnPointWebView.settings.setAppCacheEnabled(true)
        this@EarnPointFragment.dataBinding.earnPointWebView.isEnabled = false
    }

    private fun earnPointApi() {
        viewModel.request(DMSRest.get(MasterService::class.java).earnPoint())
            .then { _ , res ->
                var result = ""
                val data = res.data as JsonObject
                if (data.has("value")) {
                    result = data.get("value").asString
                }
                result.attachJWHtmlIntoWebView(this@EarnPointFragment.dataBinding.earnPointWebView)
            }
            .setTaskNameResId(R.string.task_api_earn_point_message)
            .submit()
    }
}
