package asia.craftbox.android.jobway.entities.daos

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import asia.craftbox.android.jobway.entities.models.UniModel
import com.seldatdirect.dms.core.entities.DMSDao

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.daos
 * Date: Feb 14, 2019 - 3:27 PM
 */


@Dao
interface UniDao : DMSDao<UniModel> {

    @Query("SELECT * FROM UniModel WHERE 1")
    fun getUni(): LiveData<List<UniModel>>

    @Query("SELECT * FROM UniModel WHERE hotUni = 1")
    fun hotUni(): LiveData<List<UniModel>>

    @Query("SELECT * FROM UniModel WHERE 1 LIMIT :limit")
    fun getUniLimited(limit: Int = 10): LiveData<List<UniModel>>

    @Query("SELECT * FROM UniModel WHERE UniModel.id = :id")
    fun getUniById(id: Long?): LiveData<UniModel?>

    @Query("SELECT * FROM UniModel WHERE UniModel.id = :id")
    fun getUniByIdFlatten(id: Long?): UniModel?

    @Query("SELECT * FROM UniModel ORDER By UniModel.name ASC")
    fun getUniPagination(): DataSource.Factory<Int, UniModel>

    @Query("UPDATE UniModel SET hotUni = 0 WHERE 1")
    fun cleanupHotUni()

    @Query("SELECT COUNT(*) FROM UniModel WHERE hotUni = 1 AND id = :id")
    fun countHotUniByIdFlatten(id: Long?): Int

}
