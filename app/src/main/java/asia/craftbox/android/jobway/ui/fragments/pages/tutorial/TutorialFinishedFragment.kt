package asia.craftbox.android.jobway.ui.fragments.pages.tutorial

import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentTutorialFinishedBinding
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialActivity
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialViewModel
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.tutorial
 * Date: Jan 07, 2019 - 4:11 PM
 */


class TutorialFinishedFragment :
    DMSBindingFragment<FragmentTutorialFinishedBinding>(R.layout.fragment_tutorial_finished),
    DMSViewModelSource<TutorialViewModel> {

    override val viewModel: TutorialViewModel by lazy { (this@TutorialFinishedFragment.activity as TutorialActivity).viewModel }
    private val presenter by lazy { TutorialFinishedPresenter(this@TutorialFinishedFragment) }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@TutorialFinishedFragment.dataBinding.viewModel = this@TutorialFinishedFragment.viewModel
        this@TutorialFinishedFragment.dataBinding.presenter = this@TutorialFinishedFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

}
