package asia.craftbox.android.jobway.ui.widgets

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.LayoutMainNavFooterBinding
import asia.craftbox.android.jobway.databinding.LayoutMainNavHeaderBinding
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.utils.Utils
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItems
import com.google.android.material.navigation.NavigationView

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Jan 17, 2019 - 1:47 AM
 */


class MainNavigationView : NavigationView {

    private val headerPresenter by lazy { HeaderPresenter() }

    private val headerBinding by lazy {
        val headerView = this@MainNavigationView.inflateHeaderView(R.layout.layout_main_nav_header)
        return@lazy DataBindingUtil.bind<LayoutMainNavHeaderBinding>(headerView)!!
    }

    private val footerBinding by lazy {
        val instace = DataBindingUtil.inflate<LayoutMainNavFooterBinding>(
            LayoutInflater.from(this@MainNavigationView.context),
            R.layout.layout_main_nav_footer,
            this@MainNavigationView,
            false
        )

        val footerLayout = FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        footerLayout.gravity = Gravity.BOTTOM

        this@MainNavigationView.addView(instace.root, footerLayout)
        return@lazy instace
    }

    constructor(context: Context?) : super(context) {
        this@MainNavigationView.init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        this@MainNavigationView.init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this@MainNavigationView.init()
    }


    private fun init() {
        this@MainNavigationView.headerBinding.presenter = this@MainNavigationView.headerPresenter
        this@MainNavigationView.headerBinding.invalidateAll()
        this@MainNavigationView.footerBinding.invalidateAll()
        this@MainNavigationView.inflateMenu(R.menu.menu_app_navigation)
    }

    fun setProfile(profile: ProfileModel?) {
        this@MainNavigationView.headerPresenter.profile.set(profile ?: ProfileModel())
    }

    fun setOnRequestLogout(listener: View.OnClickListener?) {
        this@MainNavigationView.footerBinding.onLogoutClicked = listener
    }


    class HeaderPresenter {

        val profile: ObservableField<ProfileModel> = ObservableField(ProfileModel())
        fun chooseAvatar(view: View?) {
            MaterialDialog(view?.context!!).show {
                title(R.string.title_choosen)
                listItems(items = listOf(
                    view?.context!!.getString(R.string.picker_source_camera),
                    view?.context!!.getString(R.string.picker_source_library)
                )
                ) { _, index, text ->
                    when(index) {
                        1 -> Utils.instance().choosePhotoFromGallary(view?.context!! as Activity)
                        0 -> Utils.instance().takePhotoFromCamera(view?.context!! as Activity)
                    }
                }
            }
        }
    }

}
