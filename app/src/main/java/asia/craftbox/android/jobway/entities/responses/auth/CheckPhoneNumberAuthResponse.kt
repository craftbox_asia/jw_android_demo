package asia.craftbox.android.jobway.entities.responses.auth

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import java.io.Serializable

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.responses.auth
 * Date: Jan 09, 2019 - 2:51 PM
 */


class CheckPhoneNumberAuthResponse : DataFieldDMSRestResponse<CheckPhoneNumberAuthResponse.Data>() {

    class Data : Serializable {

        @SerializedName("isExist")
        var isExist: Boolean = false

    }

}
