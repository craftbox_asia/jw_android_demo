package asia.craftbox.android.jobway.ui.fragments.consult.history.history

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultHistoryBinding
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.utils.Utils
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.extensions.hideKeyboard


class CounselorConsultHistoryFragment : AppMainFragment<FragmentCounselorConsultHistoryBinding>(R.layout.fragment_counselor_consult_history),
    DMSViewModelSource<CounselorConsultHistoryViewModel> {

    var textSearch: String? = ""

    override fun getTitleResId(): Int? {
        return R.string.label_history_consult
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultHistoryFragment)[CounselorConsultHistoryViewModel::class.java]
    }

    private val presenter by lazy {
        CounselorConsultHistoryPresenter(this)
    }

    override fun onDestroy() {
        if (activity?.isDestroyed == true) {
            ShareData.getInstance().releaseCurrentHistoryMessage()
        }
        super.onDestroy()
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_get_list_message
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_get_list_message) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@CounselorConsultHistoryFragment.dataBinding.viewModel = this@CounselorConsultHistoryFragment.viewModel
        this@CounselorConsultHistoryFragment.dataBinding.presenter = this@CounselorConsultHistoryFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)

        getListMessageAPI(true)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun initUI() {
        super.initUI()

        val historyRecyclerLayout = LinearLayoutManager(this.requireContext())
        historyRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.counselorConsultHistoryRecyclerView.layoutManager = historyRecyclerLayout
        dataBinding.counselorConsultHistoryRecyclerView.isNestedScrollingEnabled = false
        dataBinding.counselorConsultHistoryRecyclerView.adapter = presenter.counselorConsultHistoryAdapter

        dataBinding.refreshLayout.setOnRefreshListener {
            getListMessageAPI(true)
        }

        dataBinding.searchHistoryEditText.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                textSearch = p0.toString()
                presenter.search()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
        dataBinding.searchHistoryEditText.setOnTouchListener { view, motionEvent ->
            val DRAWABLE_RIGHT = 2
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                if (motionEvent.rawX >= (dataBinding.searchHistoryEditText.right - dataBinding.searchHistoryEditText.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    dataBinding.searchHistoryEditText.hideKeyboard()
                    textSearch = dataBinding.searchHistoryEditText.text.toString().trim()
                    presenter.search()
                    return@setOnTouchListener true
                }
            }
            return@setOnTouchListener false
        }
    }

    private fun getListMessageAPI(refresh: Boolean = false) {
        viewModel.getListMessage { data: List<MessageModel>? ->
            if (refresh) {
                presenter.historyList.clear()
                presenter.historyList.addAll(data ?: ArrayList())
                presenter.search()
                return@getListMessage
            }
            val diffList = Utils.instance().getDiffItems(presenter.historyList, data)
            if (diffList.isEmpty()) {
                return@getListMessage
            }
            presenter.historyList.addAll(diffList)
            presenter.search()
        }
    }
}
