package asia.craftbox.android.jobway.ui.fragments.faculty.search

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.EventBus.FacultyResponseEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDetailActivity
import asia.craftbox.android.jobway.ui.fragments.faculty.list.adapters.ListFacultyAdapter
import com.seldatdirect.dms.core.api.entities.DMSRestException
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.extensions.hideKeyboard
import com.seldatdirect.dms.core.extensions.showKeyboard
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_faculty_search.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.concurrent.TimeUnit

class SearchFacultyFragment : DMSBindingFragment<asia.craftbox.android.jobway.databinding.FragmentFacultySearchBinding>(R.layout.fragment_faculty_search),
    DMSViewModelSource<SearchFacultyViewModel> {
    override val viewModel by lazy {
        ViewModelProviders.of(this@SearchFacultyFragment)[SearchFacultyViewModel::class.java]
    }

    private val presenter by lazy {
        SearchFacultyPresenter()
    }


    @SuppressLint("CheckResult")
    override fun onResume() {
        super.onResume()

        Observable.timer(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .filter { this@SearchFacultyFragment.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) }
            .subscribe {
                this@SearchFacultyFragment.dataBinding.facultySearchEditText
                    .showKeyboard()
            }
    }

    override fun onPause() {
        this@SearchFacultyFragment.dataBinding.facultySearchEditText
            .hideKeyboard()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: FacultyResponseEvent) {
        faculty_search_recycler_view.adapter = ListFacultyAdapter(context!!, ArrayList(event.res))
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@SearchFacultyFragment.dataBinding.presenter = this@SearchFacultyFragment.presenter
        this@SearchFacultyFragment.dataBinding.viewModel = this@SearchFacultyFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()
        this@SearchFacultyFragment.dataBinding.facultySearchEditText.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                this@SearchFacultyFragment.dataBinding.facultySearchEditText
                    .hideKeyboard()
            }
            return@setOnKeyListener keyCode == KeyEvent.KEYCODE_ENTER
        }
    }


    inner class SearchFacultyPresenter : DMSViewModel.Listener {
        val loadingState = ObservableField<Boolean>(false)

        fun onBackButtonPressed(sender: View?) {
            this@SearchFacultyFragment.activity?.finish()
        }


        override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
            if (promise.taskNameResId == R.string.task_api_uni_search) {
                this@SearchFacultyPresenter.loadingState.set(true)
            }
        }

        override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {
            if (promise.taskNameResId == R.string.task_api_uni_search) {
                this@SearchFacultyPresenter.loadingState.set(false)
            }
        }

        override fun onAPIDidFinishedAll() {

        }

        override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {

        }

        override fun onAPIResponseError(promise: DMSRestPromise<*, *>, exception: DMSRestException) {

        }

    }


}