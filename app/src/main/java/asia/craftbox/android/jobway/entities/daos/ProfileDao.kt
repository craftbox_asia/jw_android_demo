package asia.craftbox.android.jobway.entities.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import asia.craftbox.android.jobway.entities.models.ProfileModel
import com.seldatdirect.dms.core.entities.DMSDao

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.daos
 * Date: Jan 16, 2019 - 11:44 AM
 */


@Dao
interface ProfileDao : DMSDao<ProfileModel> {

    @Query("SELECT * FROM ProfileModel WHERE ProfileModel.id = :profileId")
    fun getProfileById(profileId: Long): LiveData<ProfileModel>

    @Query("SELECT * FROM ProfileModel WHERE ProfileModel.id = :profileId")
    fun getProfileByIdFlatten(profileId: Long?): ProfileModel?

    @Query("UPDATE ProfileModel SET coin = :coin WHERE ProfileModel.id = :profileId")
    fun updateCoin(profileId: Long?, coin: Int)

    @Query("UPDATE ProfileModel SET avatar = :avatar WHERE ProfileModel.id = :profileId")
    fun updateAvatar(profileId: Long?, avatar: String)
}
