package asia.craftbox.android.jobway.entities.requests.test

import asia.craftbox.android.jobway.entities.responses.test.HOLLANDTestResponse
import asia.craftbox.android.jobway.entities.responses.test.MBTITestResponse
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest
import com.seldatdirect.dms.core.entities.DMSAnnotations

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.test
 * Date: Jan 20, 2019 - 4:21 PM
 */


class CreateTestRequest : DMSRestRequest() {

    @SerializedName("type")
    var type: String = TestType.MBTI.typeCode

    @SerializedName("data")
    var data: List<QuestionData> = emptyList()


    override fun validates(): Boolean {
        return TestType.values().any { it.typeCode == this@CreateTestRequest.type }
                && this@CreateTestRequest.data.all { it.validates() }
    }


    class QuestionData : DMSRestRequest() {

        @SerializedName("q_id")
        var questionId: Long? = null

        @SerializedName("value")
        var value: String? = null

        @DMSAnnotations.DMSJsonableExcludeField
        var mbtiQuestionValue: MBTITestResponse.Item? = null

        @DMSAnnotations.DMSJsonableExcludeField
        var hollandQuestionValue: HOLLANDTestResponse.Question? = null


        override fun validates(): Boolean {
            return this@QuestionData.questionId != null
                    && !this@QuestionData.value.isNullOrBlank()
        }

    }

    enum class TestType(val typeCode: String) {
        MBTI("MBTI"),
        Holland("HOLLAND")
    }

}
