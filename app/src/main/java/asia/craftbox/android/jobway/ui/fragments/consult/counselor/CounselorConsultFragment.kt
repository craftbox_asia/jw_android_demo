package asia.craftbox.android.jobway.ui.fragments.consult.counselor

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentConsultCounselorsBinding
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.consult.adapters.ConsultingFragmentPagerAdapter
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.master.CounselorConsultMasterFragment
import asia.craftbox.android.jobway.ui.fragments.consult.history.history.CounselorConsultHistoryFragment
import asia.craftbox.android.jobway.ui.fragments.consult.question.frequently_question.CounselorConsultQuestionFragment
import com.google.android.material.tabs.TabLayout
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import kotlinx.android.synthetic.main.tab_consult.view.*


class CounselorConsultFragment :
    AppMainFragment<FragmentConsultCounselorsBinding>(R.layout.fragment_consult_counselors),
    DMSViewModelSource<CounselorConsultViewModel> {

    private var tabLayoutBlackImageDrawables = intArrayOf(R.drawable.ic_supervisor_account_black, R.drawable.ic_history_black, R.drawable.ic_help_outline_black)
    private var tabLayoutRedImageDrawables = intArrayOf(R.drawable.ic_supervisor_account_red, R.drawable.ic_history_red, R.drawable.ic_help_outline_red)
    private var tabLayoutTitleStrings = intArrayOf(R.string.title_consult_counselor, R.string.text_history, R.string.text_requently_question)
    private var headertitleStrings = intArrayOf(R.string.title_consult_counselor, R.string.label_history_consult, R.string.text_requently_question)
    private var listFragment: List<Fragment> = listOf(CounselorConsultMasterFragment(), CounselorConsultHistoryFragment(), CounselorConsultQuestionFragment())
    private var pagerAdapter: ConsultingFragmentPagerAdapter? = null

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultFragment)[CounselorConsultViewModel::class.java]
    }

    val presenter by lazy {
        CounselorConsultPresenter(this@CounselorConsultFragment)
    }

    override fun onDestroy() {
        ShareData.getInstance().parentFragmentName = ""
        ShareData.getInstance().releaseCurrentCounselor()
        ShareData.getInstance().releaseCurrentHistoryMessage()
        ShareData.getInstance().releaseCurrentNews()
        super.onDestroy()
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@CounselorConsultFragment.dataBinding.viewModel = this@CounselorConsultFragment.viewModel
        this@CounselorConsultFragment.dataBinding.presenter = this@CounselorConsultFragment.presenter

        super.onContentViewCreated(rootView, savedInstanceState)

        val currentUser = ShareData.getInstance().getCurrentProfileModel()
        if (currentUser?.isMaster() == true) {
            listFragment = listOf(CounselorConsultHistoryFragment(), CounselorConsultQuestionFragment())
            tabLayoutBlackImageDrawables = intArrayOf(R.drawable.ic_history_black, R.drawable.ic_help_outline_black)
            tabLayoutRedImageDrawables = intArrayOf(R.drawable.ic_history_red, R.drawable.ic_help_outline_red)
            tabLayoutTitleStrings = intArrayOf(R.string.text_history, R.string.text_requently_question)
            headertitleStrings = intArrayOf(R.string.label_history_consult, R.string.text_requently_question)
        }

        setupViewPager()
        getListNotiMessageAPI()

        ShareData.getInstance().parentFragmentName = resources.getString(tabLayoutTitleStrings[0])
    }


    //region ------ API ------
    private fun getListNotiMessageAPI() {
        viewModel.getListNotiMessage()
    }

    //endegion --- API ---


    //region ------ TabLayout ------
    
    private fun setupViewPager() {
        pagerAdapter =
            ConsultingFragmentPagerAdapter(this.childFragmentManager)
        pagerAdapter!!.updateItems(listFragment)
        dataBinding.viewPager.adapter = pagerAdapter
        dataBinding.tabLayout.setupWithViewPager(dataBinding.viewPager)
        setTabSelectedListener()

        setupDataForQuestionFragment(pagerAdapter!!)

        for ((index, _) in listFragment.withIndex()) {
            setCustomViewForTabAt(index)
            if (index == 0) {
                val tab = dataBinding.tabLayout.getTabAt(0)
                setHighlightTab(tab)
            }
        }
    }

    private fun setCustomViewForTabAt(index: Int) {
        val itemLayout = LayoutInflater.from(this.activity).inflate(R.layout.tab_consult, null) as LinearLayout
        itemLayout.tab_text_view.setText(tabLayoutTitleStrings[index])
        val tab = dataBinding.tabLayout.getTabAt(index)?.setCustomView(itemLayout)
        setNormalTab(tab)
    }

    private fun setHighlightTab(tab: TabLayout.Tab?) {
        tab?.customView?.tab_image_view?.setImageResource(tabLayoutRedImageDrawables[tab.position])
        tab?.customView?.tab_text_view?.setTextColor(ContextCompat.getColor(this@CounselorConsultFragment.activity!!, R.color.color_custom_red))
        dataBinding.toolbarMain.toolbarMainTitle = resources.getString(headertitleStrings[tab?.position ?: 0])
    }

    private fun setNormalTab(tab: TabLayout.Tab?) {
        tab?.customView?.tab_image_view?.setImageResource(tabLayoutBlackImageDrawables[tab.position])
        tab?.customView?.tab_text_view?.setTextColor(ContextCompat.getColor(this@CounselorConsultFragment.activity!!, R.color.textColorSecondary))
    }

    private fun setTabSelectedListener() {
        dataBinding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                setHighlightTab(tab)
                ShareData.getInstance().parentFragmentName = resources.getString(tabLayoutTitleStrings[tab!!.position])
            }
            override fun onTabUnselected(tab:TabLayout.Tab?){
                tab?.customView?.tab_image_view?.setImageResource(tabLayoutBlackImageDrawables[tab.position])
                tab?.customView?.tab_text_view?.setTextColor(ContextCompat.getColor(this@CounselorConsultFragment.activity!!, R.color.textColorSecondary))
            }
            override fun onTabReselected(tab:TabLayout.Tab?) {

            }
        })
    }

    fun changeViewPagerCurrentItem(index: Int) {
        dataBinding.viewPager.currentItem = index
    }

    private fun setupDataForQuestionFragment(adapter: ConsultingFragmentPagerAdapter) {
        val questionFragment = adapter.getItem(listFragment.lastIndex) as CounselorConsultQuestionFragment
        questionFragment.presenter.setQuestionList(presenter.questionList)
    }

    fun addQuestionList(questionList: List<QuestionModel>) {
        presenter.questionList.addAll(questionList)
    }
    //endregion --- TabLayout ---
}
