package asia.craftbox.android.jobway.ui.fragments.pages.home

import androidx.lifecycle.LiveData
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.entities.services.NewsAPIService
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import asia.craftbox.android.jobway.entities.services.UniAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home
 * Date: Dec 28, 2018 - 10:12 PM
 */


class HomePageViewModel : DMSViewModel() {

    fun getNews(): LiveData<List<NewsModel>> {
        this@HomePageViewModel.request(DMSRest.get(NewsAPIService::class.java).hotPosts())
            .then{  _, data ->
                AppData.get().getNews().cleanupHotNews()
                data.data?.forEach {
                    it.hotNews = 1
                    it.saveOrUpdate()
                }}
            .submit()
        return AppData.get().getNews().getHotNews()
    }

    fun getUnis(): LiveData<List<UniModel>> {
        this@HomePageViewModel.request(DMSRest.get(UniAPIService::class.java).hotUni())
            .then{  _, data ->
                AppData.get().getUni().cleanupHotUni()
                data.data?.forEach {
                    it.hotUni = 1
                    it.saveOrUpdate()
                }}
            .submit()
        return AppData.get().getUni().hotUni()
    }

    // region ------ Count Unread ------

    fun getCountUnreadMessage() {
        this.request(DMSRest.get(NotificationAPIService::class.java).getCountUnreadMessage())
            .then { promise, res ->
                val count = (res.data ?: 0)
                ShareData.getInstance().countUnreadMessage = count
            }
            .catch { promise, exception ->
            }
            .doAfterResponse { it ->

            }
            .setTaskNameResId(R.string.task_api_get_count_unread_message)
            .submit()
    }

    fun getCountUnreadNews() {
        this.request(DMSRest.get(NotificationAPIService::class.java).getCountUnreadNews())
            .then { promise, res ->
                val count = (res.data ?: 0)
                ShareData.getInstance().countUnreadNews = count
            }
            .catch { promise, exception ->
            }
            .doAfterResponse { it ->

            }
            .setTaskNameResId(R.string.task_api_get_count_unread_news)
            .submit()
    }

    // endregion --- Count Unread ---
}
