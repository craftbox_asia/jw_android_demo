package asia.craftbox.android.jobway.ui.fragments.consult.history.history

import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultHistoryBinding
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history.CounselorHistoryDetailFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorConsultHistoryPresenter(private val fragment: CounselorConsultHistoryFragment) : BasePresenter<FragmentCounselorConsultHistoryBinding>(fragment) {

    var counselorConsultHistoryAdapter: CounselorConsultHistoryAdapter? = null

    var historyList: ArrayList<MessageModel> = ArrayList()
    private var searchHistoryList: ArrayList<MessageModel> = ArrayList()

    init {
        counselorConsultHistoryAdapter =
            CounselorConsultHistoryAdapter(searchHistoryList, fragment.context!!
            )
        counselorConsultHistoryAdapter?.setCallback { item, pos ->
            ShareData.getInstance().currentHistoryMessage = item
            ShareData.getInstance().isClickedNotification = false

            if (!item.isRead()) {
                fragment.viewModel.updateReadNoti(item.id!!.toString(), item.type) { success ->
                    if (!success) {
                        return@updateReadNoti
                    }
                    historyList[pos].read = 1
                    counselorConsultHistoryAdapter?.notifyItemChanged(pos)
                }
            }

            val mainActivity = fragment.activity as? MainActivity
            val historyDetailFragment = DMSFragment.create(CounselorHistoryDetailFragment::class.java)
            mainActivity?.getFragmentHelper()?.pushFragment(historyDetailFragment)
        }
    }

    fun setDrawerOpen(isOpen: Boolean) {
        (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
    }

    fun search() {
        searchHistoryList.clear()
        val filterData = historyList.filter { it.isExistInBodyOrUsername(fragment.textSearch ?: "") }
        filterData.forEach { searchHistoryList.add(it) }
        counselorConsultHistoryAdapter?.notifyDataSetChanged()
    }
}
