package asia.craftbox.android.jobway.ui.activities.test

import androidx.databinding.BaseObservable

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.test
 * Date: Jan 18, 2019 - 2:57 PM
 */


class TestPresenter(private val activity: TestActivity) : BaseObservable()