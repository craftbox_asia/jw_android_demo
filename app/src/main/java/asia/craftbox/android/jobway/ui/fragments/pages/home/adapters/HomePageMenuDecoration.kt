package asia.craftbox.android.jobway.ui.fragments.pages.home.adapters

import com.esafirm.imagepicker.view.GridSpacingItemDecoration

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home.adapters
 * Date: Dec 28, 2018 - 10:54 PM
 */


class HomePageMenuDecoration(spacing: Int) : GridSpacingItemDecoration(3, spacing, true)
