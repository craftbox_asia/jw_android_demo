package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.requests.news.BookmarkRquest
import asia.craftbox.android.jobway.entities.requests.news.IncrementNewsRequest
import asia.craftbox.android.jobway.entities.responses.PaginateDataResponse
import asia.craftbox.android.jobway.entities.responses.news.BookmarkResponse
import asia.craftbox.android.jobway.entities.responses.news.IncrementNewsResponse
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.services
 * Date: Feb 13, 2019 - 3:16 PM
 */


interface NewsAPIService {

    @GET("website/api/post")
    fun getPosts(@Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<NewsModel>>>

    @GET("website/api/post/list-bookmark-post")
    fun bookmarkPosts(@Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<NewsModel>>>

    @GET("website/api/post/hot-news")
    fun hotPosts(): Observable<DataFieldDMSRestResponse<List<NewsModel>>>

    @POST("website/api/post/increment-view")
    @Headers("Content-Type:application/json")
    fun incrementViews(@Body request: IncrementNewsRequest): Observable<IncrementNewsResponse>

    @POST("website/api/post/bookmark-post")
    @Headers("Content-Type:application/json")
    fun bookmark(@Body request: BookmarkRquest): Observable<BookmarkResponse>

    @GET("website/api/post/detail/{post_id}")
    fun getDetails(@Path("post_id") postId: Long?): Observable<DataFieldDMSRestResponse<NewsModel>>

}
