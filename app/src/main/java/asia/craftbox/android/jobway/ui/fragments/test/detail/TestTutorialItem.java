package asia.craftbox.android.jobway.ui.fragments.test.detail;

public class TestTutorialItem {

    String title;
    int imageUrl;
    String content;

    public TestTutorialItem(int imageUrl, String title, String content) {
        this.content = content;
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }
}
