package asia.craftbox.android.jobway.modules.exts

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.webkit.WebView
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.modules.exts
 * Date: Dec 28, 2018 - 10:27 PM
 */


fun String.fromHTML(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this@fromHTML, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(this@fromHTML)
    }
}

fun String.toDate(inputPattern: String = "yyyy-MM-dd"): Date? {
    return try {
        val simpleDate = SimpleDateFormat(inputPattern, Locale.US)
        simpleDate.parse(this@toDate)
    } catch (exception: Exception) {
        exception.printStackTrace()
        null
    }
}

fun String.generateJobWayHTML(backgroundColor: String? = null, fontSize: Int? = null) : String {
    var color = "rgb(248,249,251)"
    var _fontSize = 13
    if (backgroundColor != null) {
        color = backgroundColor
    }
    if (fontSize != null) {
        _fontSize = fontSize
    }
    return "<html lang='en'>" +
            "<head>" +
            "<meta charset=\"utf-8\">" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">" +
            "<style>" +
            " html, body { background-color:$color; font-size:${_fontSize}px; max-width: 100%; display: block; overflow: hidden; ; margin: 0; padding: 0; }" +
            " p, h1, h2, h3, h4, h5, h6, div { padding-left: 16px; padding-right: 16px; }" +
            " img, iframe { max-width: 100%; height: auto; }" +
            "</style>" +
            "</head>" +
            "<body>${this@generateJobWayHTML}</body>" +
            "</html>"
}

fun String.attachJWHtmlIntoWebView(webView: WebView, backgroundColor: String? = null, fontSize: Int? = null) {
    val html = this@attachJWHtmlIntoWebView.generateJobWayHTML(backgroundColor, fontSize)
    webView.settings.builtInZoomControls = true
    webView.settings.displayZoomControls = false
    webView.settings.javaScriptEnabled = true
    webView.loadDataWithBaseURL(
        "",
        html,
        "text/html", "UTF-8",
        ""
    )
}

fun String.toBold(): Spanned {
    return "<b>$this</b>".fromHTML()
}
