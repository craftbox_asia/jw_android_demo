package asia.craftbox.android.jobway.ui.activities.tutorial

import androidx.databinding.BaseObservable

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.tutorial
 * Date: Jan 02, 2019 - 5:15 PM
 */


class TutorialPresenter(private val activity: TutorialActivity) : BaseObservable() {

    internal val pagerAdapter: TutorialAdapter by lazy {
        TutorialAdapter(this@TutorialPresenter.activity.supportFragmentManager)
    }

}
