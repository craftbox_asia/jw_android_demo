package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.requests.auth.CheckInvitationCodeRequest
import asia.craftbox.android.jobway.entities.requests.auth.CheckPhoneNumberAuthRequest
import asia.craftbox.android.jobway.entities.requests.auth.RegisterAuthRequest
import asia.craftbox.android.jobway.entities.requests.auth.UpdateProfileRequest
import asia.craftbox.android.jobway.entities.responses.auth.CheckPhoneNumberAuthResponse
import asia.craftbox.android.jobway.entities.responses.auth.LoginAuthResponse
import asia.craftbox.android.jobway.entities.responses.auth.UploadAvatarResponse
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.auth
 * Date: Jan 09, 2019 - 2:39 PM
 */


interface AuthAPIService {

    @POST("auth/v1/check-phone-number")
    @Headers("Content-Type: application/json")
    fun checkPhoneNumber(@Body request: CheckPhoneNumberAuthRequest): Observable<CheckPhoneNumberAuthResponse>

    @POST("auth/v1/user/personal-code")
    @Headers("Content-Type: application/json")
    fun checkInvitationCode(@Body request: CheckInvitationCodeRequest): Observable<Unit>

    @POST("auth/v1/user/register")
    @Headers("Content-Type: application/json")
    fun register(@Body request: RegisterAuthRequest): Observable<DataFieldDMSRestResponse<ProfileModel>>

    @POST("auth/v1/login")
    @Headers("Content-Type: application/json")
    fun login(@Body request: RegisterAuthRequest): Observable<LoginAuthResponse>

    @GET("auth/v1/user/logout")
    @Headers("Content-Type: application/json")
    fun logout(): Observable<Unit>

    @GET("auth/v1/user/profile")
    @Headers("Content-Type: application/json")
    fun viewProfile(): Observable<DataFieldDMSRestResponse<ProfileModel>>

    @PUT("auth/v1/user/profile")
    @Headers("Content-Type: application/json")
    fun updateProfile(@Body request: UpdateProfileRequest): Observable<DataFieldDMSRestResponse<ProfileModel>>

    @GET("auth/v1/user/profile/{uId}")
    @Headers("Content-Type: application/json")
    fun viewProfileById(@Path("uId") uId: Int): Observable<DataFieldDMSRestResponse<ProfileModel>>

    @POST("auth/v1/user/update-avatar")
    @Multipart
    fun uploadAvatar(@Part avatar: MultipartBody.Part): Observable<UploadAvatarResponse>

    @GET("auth/v1/user/list-counselor")
    @Headers("Content-Type: application/json")
    fun listCounselor(): Observable<DataFieldDMSRestResponse<List<CounselorModel>>>

}
