package asia.craftbox.android.jobway.ui.fragments.consult.history.info_student

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.entities.services.TestAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel


class CounselorInfoStudentViewModel : DMSViewModel() {

    fun viewProfileByIdApi(uid: Int, callback: ((ProfileModel?) -> Unit)) {
        request(DMSRest.get(AuthAPIService::class.java).viewProfileById(uid))
            .then { _, responseData ->
                callback.invoke(responseData.data)
            }
            .setTaskNameResId(R.string.task_api_auth_view_profile_by_id)
            .submit()
    }

    fun getResumeItemsApi(callback: ((Boolean) -> Unit)) {
        request(DMSRest.get(TestAPIService::class.java).getResumeItems())
            .then { _, data ->
                data.data?.forEach { resumeItem -> resumeItem.saveOrUpdate() }
                callback.invoke(true)
            }
            .setTaskNameResId(R.string.task_api_auth_profile_resume)
            .submit()
    }
}
