package asia.craftbox.android.jobway.ui.fragments.pages.tutorial

import android.os.Bundle
import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentTutorialBinding
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialActivity
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialViewModel
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.tutorial
 * Date: Jan 07, 2019 - 11:11 AM
 */


class TutorialFragment : DMSBindingFragment<FragmentTutorialBinding>(R.layout.fragment_tutorial),
    DMSViewModelSource<TutorialViewModel> {

    override val viewModel: TutorialViewModel by lazy { (this@TutorialFragment.activity as TutorialActivity).viewModel }
    private val presenter by lazy { TutorialPresenter(this@TutorialFragment) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val args = this@TutorialFragment.arguments ?: return

        try {
            val titleRes = args.getInt(INTENT_TITLE_STRING_RES_INT, R.string.common_empty)
            val contentRes = args.getInt(INTENT_CONTENTS_STRING_RES_INT, R.string.common_empty)
            val coverRes = args.getInt(INTENT_COVER_DRAWABLE_RES_INT, R.string.common_empty)

            this@TutorialFragment.presenter.setTitle(titleRes)
            this@TutorialFragment.presenter.setContent(contentRes)
            this@TutorialFragment.presenter.setCover(coverRes)
        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@TutorialFragment.dataBinding.viewModel = this@TutorialFragment.viewModel
        this@TutorialFragment.dataBinding.presenter = this@TutorialFragment.presenter

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        try {
            val tutorialActivity = (this@TutorialFragment.activity as? TutorialActivity) ?: return
            this@TutorialFragment.dataBinding.tutorialPageBottomDots
                .setupWithViewPager(tutorialActivity.dataBinding.tutorialViewPager)
        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }


    companion object {

        private const val INTENT_TITLE_STRING_RES_INT: String = "INTENT_TITLE_STRING_RES_INT"
        private const val INTENT_CONTENTS_STRING_RES_INT: String = "INTENT_CONTENTS_STRING_RES_INT"
        private const val INTENT_COVER_DRAWABLE_RES_INT: String = "INTENT_COVER_DRAWABLE_RES_INT"

        fun newInstance(@StringRes title: Int, @StringRes contents: Int, @DrawableRes coverPhoto: Int): TutorialFragment {
            val instance = TutorialFragment()
            instance.arguments = if (instance.arguments != null) instance.arguments else Bundle()
            instance.arguments?.putInt(INTENT_TITLE_STRING_RES_INT, title)
            instance.arguments?.putInt(INTENT_CONTENTS_STRING_RES_INT, contents)
            instance.arguments?.putInt(INTENT_COVER_DRAWABLE_RES_INT, coverPhoto)
            return instance
        }

    }

}
