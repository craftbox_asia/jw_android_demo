package asia.craftbox.android.jobway.entities.models

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Jan 09, 2019 - 4:10 PM
 */


abstract class AppModel : DMSModel() {

    @SerializedName("id")
    var id: Long? = null

    @SerializedName("created_at")
    var createdAt: String? = null


    override fun onCopyPrimaryKey(): Long? {
        return this@AppModel.id ?: super.onCopyPrimaryKey()
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<AppModel>() {
            override fun areItemsTheSame(oldItem: AppModel, newItem: AppModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AppModel, newItem: AppModel): Boolean {
                return false
            }
        }
    }

}
