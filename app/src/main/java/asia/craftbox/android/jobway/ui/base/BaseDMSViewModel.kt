package asia.craftbox.android.jobway.ui.base

import androidx.lifecycle.LiveData
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference

open class BaseDMSViewModel : DMSViewModel() {

    val userProfile by lazy {
        val profileId = DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
        return@lazy AppData.get().getProfile().getProfileById(profileId)
    }


    fun getProfile(): LiveData<ProfileModel> {
        return this.userProfile
    }
}