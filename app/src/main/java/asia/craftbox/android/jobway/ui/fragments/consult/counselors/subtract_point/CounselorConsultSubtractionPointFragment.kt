package asia.craftbox.android.jobway.ui.fragments.consult.counselors.subtract_point

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultSubtractionPointBinding
import asia.craftbox.android.jobway.modules.exts.fromHTML
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import kotlinx.android.synthetic.main.fragment_counselor_consult_subtraction_point.*


class CounselorConsultSubtractionPointFragment : DialogFragment() {

    val presenter by lazy {
        CounselorConsultSubtractionPointPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentCounselorConsultSubtractionPointBinding.inflate(inflater, container, false)
        binding.presenter = presenter
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog?.window?.setLayout(width, height)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val format = String.format(getString(R.string.text_hello_user_name), "<b>${ShareData.getInstance().getDisplayName()}</b>")
        title_text_view.text = format.fromHTML()
        title2_text_view.text = format.fromHTML()

        val coin = ShareData.getInstance().getCoin()
        val isOk = coin >= JobWayConstant.DEFAULT_SUBTRACTION_COIN

        if (isOk) {
            agree_btn.visibility = View.VISIBLE
            contain1_ll.visibility = View.VISIBLE
            contain2_ll.visibility = View.GONE

            current_point_text_view.text = coin.toString()
            remain_point_text_view.text = (coin - JobWayConstant.DEFAULT_SUBTRACTION_COIN).toString()
        } else {
            agree_btn.visibility = View.GONE
            contain1_ll.visibility = View.GONE
            contain2_ll.visibility = View.VISIBLE
        }
    }
}
