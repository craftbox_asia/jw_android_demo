package asia.craftbox.android.jobway.ui.fragments.consult.question.detail_question

import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultDetailQuestionBinding
import asia.craftbox.android.jobway.ui.base.BasePresenter


class CounselorConsultDetailQuestionPresenter(private val fragment: CounselorConsultDetailQuestionFragment) : BasePresenter<FragmentCounselorConsultDetailQuestionBinding>(fragment) {


}
