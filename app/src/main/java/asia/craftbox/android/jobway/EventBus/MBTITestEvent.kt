package asia.craftbox.android.jobway.EventBus

import asia.craftbox.android.jobway.entities.models.ResultTestModel

open class MBTITestEvent(response: ResultTestModel){
    var response: ResultTestModel = response
}