package asia.craftbox.android.jobway.ui.activities.faculty

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper
import asia.craftbox.android.jobway.databinding.ActivityFacultyDescriptionBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.ui.fragments.faculty.detail.DetailFacultyFragment

class FacultyDescriptionDetailActivity: DMSBindingActivity<ActivityFacultyDescriptionBinding>(R.layout.activity_faculty_description),
        DMSViewModelSource<FacultyDescriptionDetailViewModel>{

    private val presenter: FacultyDescriptionDetailPresenter = FacultyDescriptionDetailPresenter(this@FacultyDescriptionDetailActivity)
    override val viewModel by lazy { ViewModelProviders.of(this@FacultyDescriptionDetailActivity)[FacultyDescriptionDetailViewModel::class.java] }

    private val fragmentUtil by lazy {
        DMSFragmentHelper(
            this@FacultyDescriptionDetailActivity.supportFragmentManager,
            R.id.faculty_description_container
        )
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@FacultyDescriptionDetailActivity.dataBinding.presenter = this@FacultyDescriptionDetailActivity.presenter
        this@FacultyDescriptionDetailActivity.dataBinding.viewModel = this@FacultyDescriptionDetailActivity.viewModel

        if (savedInstanceState == null) {
            val facObj = intent.getSerializableExtra(FACULTY_DESCRIPTION_BY_OBJECT) as FacultyModel
            this@FacultyDescriptionDetailActivity.fragmentUtil.initialRootFragment(
                fragment =  DetailFacultyFragment.newInstance(facObj)
            )
        }

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    companion object{
        val FACULTY_DESCRIPTION_BY_OBJECT = "FACULTY_DESCRIPTION_BY_OBJECT"
    }
}