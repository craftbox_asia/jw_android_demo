package asia.craftbox.android.jobway.ui.activities.test

import android.graphics.Point
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import asia.craftbox.android.jobway.EventBus.MBTITestEvent
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.models.ResultTestModel
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.entities.responses.test.HOLLANDTestResponse
import asia.craftbox.android.jobway.entities.responses.test.MBTITestResponse
import asia.craftbox.android.jobway.entities.services.TestAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.test
 * Date: Jan 18, 2019 - 2:57 PM
 */


class TestViewModel : DMSViewModel() {

    private var mbtiRequestMutable = MutableLiveData<CreateTestRequest>()
    private var hollandRequestMutable = MutableLiveData<CreateTestRequest>()

    private val currentTestTypeMutable = MutableLiveData<CreateTestRequest.TestType>()
    private val currentTestProgressMutable = MutableLiveData<Point>()


    private val requestTransformations = Transformations.switchMap(this@TestViewModel.currentTestTypeMutable) {
        return@switchMap if (it == CreateTestRequest.TestType.MBTI)
            this@TestViewModel.mbtiRequestMutable
        else
            this@TestViewModel.hollandRequestMutable
    }

    private val testResultMutable = Transformations.switchMap(this@TestViewModel.currentTestTypeMutable) {
        val userId = this@TestViewModel.getProfileId()
        return@switchMap  AppData.get().getTest().getByUserAndType(userId, it.typeCode)
    }

    private val fetchTestTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@TestViewModel.compositeDisposable.add(
            instance.debounce(
                100,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@TestViewModel.request(DMSRest.get(TestAPIService::class.java).getMBTIQuestions())
                    .setTaskNameResId(R.string.task_api_test_get_mbti)
                    .then { _, mbtiRes ->
                        mbtiRes.data?.let { d -> this@TestViewModel.mergeMBTIData(d) }

                        this@TestViewModel.request(DMSRest.get(TestAPIService::class.java).getHOLLANDQuestions())
                            .setTaskNameResId(R.string.task_api_test_get_holland)
                            .then { _, hollandRes ->
                                hollandRes.data?.let { d -> this@TestViewModel.mergeHOLLANDData(d) }
                                this@TestViewModel.setTestType(CreateTestRequest.TestType.MBTI)
                            }
                            .submit()
                    }
                    .submit()
            })
        return@lazy instance
    }

    private val submitTask by lazy {
        val instance = PublishSubject.create<CreateTestRequest>()
        this@TestViewModel.compositeDisposable.add(
            instance.throttleFirst(
                1000,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe { request ->
                this@TestViewModel.request(DMSRest.get(TestAPIService::class.java).submit(request))
                    .then { _, data ->
                        if (data.data != null) {
                            EventBus.getDefault().post(MBTITestEvent(data.data!!))
                        }
                        data.data?.let { unwrappedTest ->
                            unwrappedTest.userId = this@TestViewModel.getProfileId()
                            unwrappedTest.type = request.type
                            unwrappedTest.saveOrUpdate()
                        }
                    }
                    .setTaskNameResId(R.string.task_api_test_submit_test)
                    .submit()
            })
        return@lazy instance
    }


    private fun mergeMBTIData(questionData: MBTITestResponse) {
        val testData = CreateTestRequest()
        testData.type = CreateTestRequest.TestType.MBTI.typeCode
        testData.data = questionData.map { testItem ->
            val answerItem = CreateTestRequest.QuestionData()
            answerItem.questionId = testItem.id
            answerItem.mbtiQuestionValue = testItem
            return@map answerItem
        }

        this@TestViewModel.mbtiRequestMutable.postValue(testData)
    }

    private fun mergeHOLLANDData(questionData: HOLLANDTestResponse) {
        val questionList = questionData.flatMap { q -> q.questions }
        val testData = CreateTestRequest()
        testData.type = CreateTestRequest.TestType.Holland.typeCode
        testData.data = questionList.map { testItem ->
            val answerItem = CreateTestRequest.QuestionData()
            answerItem.questionId = testItem.id
            answerItem.hollandQuestionValue = testItem
            return@map answerItem
        }
        this@TestViewModel.hollandRequestMutable.postValue(testData)
    }


    fun setTestType(type: CreateTestRequest.TestType) {
        Timber.wtf("setTestType: $type")
        this@TestViewModel.currentTestTypeMutable.postValue(type)
    }

    fun getRequest(): LiveData<CreateTestRequest> {
        return this@TestViewModel.requestTransformations
    }

    fun getTestProgress(): LiveData<Point> {
        return this@TestViewModel.currentTestProgressMutable
    }

    fun getTestType(): LiveData<CreateTestRequest.TestType> {
        return this@TestViewModel.currentTestTypeMutable
    }

    fun getTestResult(): LiveData<ResultTestModel> {
        if (this@TestViewModel.testResultMutable?.value != null)
            EventBus.getDefault().post(MBTITestEvent(this@TestViewModel.testResultMutable?.value!!))
        return this@TestViewModel.testResultMutable
    }

    fun getProfile(): LiveData<ProfileModel> {
        return AppData.get().getProfile().getProfileById(this@TestViewModel.getProfileId())
    }

    fun getProfileId(): Long {
        return DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
    }

    fun notifyTestProgress() {
        val testData = this@TestViewModel.getRequest().value ?: return
        val totalQuestions = testData.data.count()
        val doneQuestions = testData.data.count { !it.value.isNullOrBlank() }
        val point = Point(doneQuestions, totalQuestions)
        this@TestViewModel.currentTestProgressMutable.postValue(point)
    }

    fun doSubmitTest() {
        val request = this@TestViewModel.getRequest().value ?: return
        this@TestViewModel.submitTask.onNext(request)
    }


    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@TestViewModel.doResetForm()
    }

    fun doResetForm() {
        this@TestViewModel.mbtiRequestMutable.postValue(CreateTestRequest())
        this@TestViewModel.hollandRequestMutable.postValue(CreateTestRequest())
        this@TestViewModel.currentTestProgressMutable.postValue(Point(0, 100))
        this@TestViewModel.fetchTestTask.onNext(true)
    }

}
