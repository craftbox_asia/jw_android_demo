package asia.craftbox.android.jobway.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.MediaStore
import asia.craftbox.android.jobway.entities.models.AppModel
import android.util.DisplayMetrics
import asia.craftbox.android.jobway.JobWayConstant


class Utils {
    companion object {
        fun instance(): Utils = Utils()
    }

    fun <T: AppModel>getDiffItems(oldList: List<T>, newList: List<T>?): ArrayList<T> {
        var tempList: ArrayList<T> = ArrayList()

        if (newList == null) return tempList
        for (newItem in newList) {
            var diff = true
            for (oldItem in oldList) {
                val same = T@AppModel.DIFF.areItemsTheSame(newItem, oldItem)
                if (same) {
                    diff = false
                    break
                }
            }
            if (diff) {
                tempList.add(newItem)
            }
        }
        return tempList
    }

    fun convertDpToPixel(dp: Float, context: Context): Float {
        return dp * (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun convertPixelsToDp(px: Float, context: Context): Float {
        return px / (context.resources.displayMetrics.densityDpi/ DisplayMetrics.DENSITY_DEFAULT)
    }

    fun choosePhotoFromGallary(activity: Activity) {
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        activity.startActivityForResult(galleryIntent, JobWayConstant.PICK_GALARY)
    }

    fun takePhotoFromCamera(activity: Activity) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        activity.startActivityForResult(intent, JobWayConstant.PICK_CAMERA)
    }
}