package asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter

class CounselorConsultHistoryDetailItemAdapter(private val items: List<String>, val context: Context) :
    RecyclerView.Adapter<CounselorConsultHistoryDetailItemAdapter.ViewHolder>() {

    private var onContainClickedCallback: ((item: String, pos: Int) -> Unit)? = null

    fun setCallback(callback: ((item: String, pos: Int) -> Unit)?) {
        onContainClickedCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_consult_history_detail_file, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.loadImage(item)
        holder.contain_linear_layout.setOnClickListener {
            onContainClickedCallback?.invoke(item, position)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val contain_linear_layout: LinearLayout = view.findViewById(R.id.contain_linear_layout)
        private val image_view: ImageView = view.findViewById(R.id.image_view)

        fun loadImage(file: String?) {
            ImageViewBindingAdapter.imageViewAsyncSrc(image_view, file, 0)
        }
    }
}