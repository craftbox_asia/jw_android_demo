package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.entities.responses.PaginateDataResponse
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.services
 * Date: Feb 14, 2019 - 3:50 PM
 */


interface UniAPIService {

    @GET("website/api/uni")
    fun listUni(@Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<UniModel>>>

    @GET("website/api/uni/hot-uni")
    fun hotUni(): Observable<DataFieldDMSRestResponse<List<UniModel>>>

    @GET("website/api/uni")
    fun searchUni(@Query("filter") filter: String, @Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<UniModel>>>

    @GET("website/api/uni/detail/{uni_id}")
    fun detail(@Path("uni_id") uniId: Long): Observable<DataFieldDMSRestResponse<UniModel>>

    @GET("website/api/uni/detail/{uni_id}")
    fun detailSync(@Path("uni_id") uniId: Long): Call<DataFieldDMSRestResponse<UniModel>>

}
