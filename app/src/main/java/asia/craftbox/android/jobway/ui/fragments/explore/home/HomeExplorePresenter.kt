package asia.craftbox.android.jobway.ui.fragments.explore.home

import android.content.Context
import android.content.Intent
import android.view.View
import asia.craftbox.android.jobway.databinding.FragmentExploreHomeBinding
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.activities.profile.ProfileActivity
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.explore.home
 * Date: Jan 18, 2019 - 2:51 PM
 */


class HomeExplorePresenter(private val fragment: HomeExploreFragment) : BasePresenter<FragmentExploreHomeBinding>(fragment) {

    val homePagerAdapter by lazy { HomeExploreAdapter(this@HomeExplorePresenter.fragment.requireContext()) }

    fun onTestClicked(sender: View?) {
        val mainActivity = this@HomeExplorePresenter.getMainActivity() ?: return
        mainActivity.startActivity(Intent(mainActivity, TestActivity::class.java))
    }

    fun onProfileUpdateClicked(sender: View?) {
        val mainActivity = this@HomeExplorePresenter.getMainActivity() ?: return
        mainActivity.startActivity(Intent(mainActivity, ProfileActivity::class.java))
    }

    fun onJobUpdateClicked(sender: View?) {

    }

    fun getContext(): Context? {
        return this@HomeExplorePresenter.fragment.context
    }

    fun setDrawerOpen(isOpen: Boolean) {
        (this@HomeExplorePresenter.fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
    }

    private fun getMainActivity(): MainActivity? {
        return this@HomeExplorePresenter.fragment.activity as? MainActivity
    }

}
