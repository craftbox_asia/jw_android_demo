package asia.craftbox.android.jobway.ui.fragments.news.list.adapters

import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderNewsLatestItemBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list.adapters
 * Date: Feb 13, 2019 - 4:47 PM
 */


class ListLatestNewsAdapter : PagedListAdapter<NewsModel, ListLatestNewsAdapter.ViewHolder>(NewsModel.DIFF) {

    private var onItemSelectedBlock: ((newsItem: NewsModel) -> Unit)? = null


    fun setOnItemSelected(block: ((newsItem: NewsModel) -> Unit)?) {
        this@ListLatestNewsAdapter.onItemSelectedBlock = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this@ListLatestNewsAdapter.getItem(position)?.let { unwrappedItem ->
            holder.bind(unwrappedItem, position)
        }
    }


    inner class ViewHolder(parent: ViewGroup) : DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<NewsModel>(
        parent,
        R.layout.viewholder_news_latest_item
    ) {

        private val dataBinding = DataBindingUtil.bind<ViewholderNewsLatestItemBinding>(this@ViewHolder.itemView)


        override fun bind(item: NewsModel, position: Int) {
            super.bind(item, position)
            this@ViewHolder.dataBinding?.presenter = Presenter(item, position)
            this@ViewHolder.dataBinding?.invalidateAll()
        }


        inner class Presenter(private val item: NewsModel, private val position: Int) {

            fun getNews() = this@Presenter.item

            fun getNewsNatureTime(): String? {
                return this@Presenter.item.getNatureTime(this@ViewHolder.itemView.context)
            }

            fun getCountViews(): String? {
                return this@ViewHolder.itemView.context?.getString(R.string.common_label_post_views) + ": " + this@Presenter.item.views?.toString()
            }

            fun onItemClicked(sender: View?) {
                this@ListLatestNewsAdapter.onItemSelectedBlock
                    ?.invoke(this@Presenter.item)
            }

        }

    }

}
