package asia.craftbox.android.jobway.ui.fragments.test.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import asia.craftbox.android.jobway.EventBus.MBTITestEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentTestDetailBinding
import asia.craftbox.android.jobway.entities.models.ResultTestModel
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.ui.activities.test.TestViewModel
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.test.result.ResultTestFragment
import com.google.android.material.tabs.TabLayout
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import kotlinx.android.synthetic.main.fragment_test_result.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.test.detail
 * Date: Jan 18, 2019 - 3:51 PM
 */


class DetailTestFragment : AppMainFragment<FragmentTestDetailBinding>(R.layout.fragment_test_detail),
    DMSViewModelSource<TestViewModel> {

    override val viewModel by lazy {
        (this@DetailTestFragment.activity as TestActivity).viewModel
    }

    private val presenter by lazy {
        DetailTestPresenter(this@DetailTestFragment)
    }

    lateinit var mbtiTab: TabLayout.Tab
    lateinit var hollandTab: TabLayout.Tab

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@DetailTestFragment.dataBinding.viewModel = this@DetailTestFragment.viewModel
        this@DetailTestFragment.dataBinding.presenter = this@DetailTestFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()

        this@DetailTestFragment.presenter.testAdapter.setOnUserDidUpdateAnswerListener { _, _, _ ->
            this@DetailTestFragment.viewModel.notifyTestProgress()
        }

        val testTypeTab = this@DetailTestFragment.dataBinding.testTabLayout

        mbtiTab = testTypeTab.newTab()
            .setTag(CreateTestRequest.TestType.MBTI.typeCode)
            .setText(R.string.tab_item_mbti)
        hollandTab = testTypeTab.newTab()
            .setText(R.string.tab_item_holland)
            .setTag(CreateTestRequest.TestType.Holland.typeCode)

        testTypeTab.addTab(mbtiTab)
        testTypeTab.addTab(hollandTab)

        mbtiTab.select()
        val mbtiResult: ResultTestModel? =  AppData.get().getTest().getByUserAndTypeFlatten(viewModel.getProfileId(), "MBTI")
        if (mbtiResult == null) {
            this@DetailTestFragment.viewModel.setTestType(CreateTestRequest.TestType.MBTI)
        } else{
            this@DetailTestFragment.presenter.showResult()
        }

        testTypeTab.addOnTabSelectedListener(this@DetailTestFragment.presenter)

        this@DetailTestFragment.presenter.finishButtonText.set(
            this@DetailTestFragment.requireContext().getString(
                R.string.action_finish
            )
        )
        this@DetailTestFragment.presenter.finishHintText.set(
            this@DetailTestFragment.requireContext().getString(
                R.string.hint_test_detail_bottom_finish_test
            )
        )
    }

    override fun initObservables() {
        super.initObservables()

        if ((presenter.isRetestMbti && presenter.isMbtiSelected) || (!presenter.isMbtiSelected && presenter.isRetestHolland))
            return

        this@DetailTestFragment.viewModel.getTestProgress().observe(this@DetailTestFragment, Observer { progress ->
            progress?.let {
                this@DetailTestFragment.presenter.testProgressCurrentMutable.postValue(it.x)
                this@DetailTestFragment.presenter.testProgressTotalMutable.postValue(it.y)
            }
        })

        this@DetailTestFragment.viewModel.getRequest().observe(this@DetailTestFragment, Observer {
            this@DetailTestFragment.presenter.testAdapter.updateItems(
                it.data,
                if (it.type == CreateTestRequest.TestType.MBTI.typeCode)
                    CreateTestRequest.TestType.MBTI
                else
                    CreateTestRequest.TestType.Holland
            )

            if (this@DetailTestFragment.presenter.finishButtonText.get().isNullOrBlank()) {
                this@DetailTestFragment.presenter.finishButtonText.set(
                    this@DetailTestFragment.requireContext().getString(
                        R.string.action_test_continue
                    )
                )
                this@DetailTestFragment.presenter.finishHintText.set(
                    this@DetailTestFragment.requireContext().getString(
                        R.string.hint_test_detail_bottom_continue_test
                    )
                )
            }

            this@DetailTestFragment.viewModel.notifyTestProgress()
        })
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MBTITestEvent) {
        if ((presenter.isRetestMbti && presenter.isMbtiSelected) || (!presenter.isMbtiSelected && presenter.isRetestHolland))
            return
        event.response.des?.attachJWHtmlIntoWebView(this.dataBinding.descriptionWebView, "#ffffff")
        presenter.showResult()
        test_result_share_btn.setOnClickListener { v ->
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                var shareMessage = getString(R.string.test_result_mbti).format(event.response!!.code) + event.response!!.des  + getString(R.string.share_link_app_job_way) + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "Jobway invitation"))
            } catch (e: Exception) {

            }
        }
    }

    override fun isBackable(): Boolean {
        this@DetailTestFragment.alert
            .confirm(R.string.confirm_test_cancellation) {
                this@DetailTestFragment.activity?.finish()
            }
        return false
    }
}
