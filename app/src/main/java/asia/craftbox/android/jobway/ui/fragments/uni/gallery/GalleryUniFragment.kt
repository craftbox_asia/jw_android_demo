package asia.craftbox.android.jobway.ui.fragments.uni.gallery

import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.admission
 * Date: Feb 14, 2019 - 9:43 PM
 */


class GalleryUniFragment : DMSFragment()
