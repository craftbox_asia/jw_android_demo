package asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class CounselorConsultComposeViewModel : DMSViewModel() {

    fun sendMessageAPI(
        cid: Int,
        title: String,
        description: String,
        filePathList: ArrayList<String>,
        callback: ((Boolean) -> Unit)
    ) {
        val cIDBody = RequestBody.create(MediaType.parse("multipart/form-data"), "$cid")
        val titleBody = RequestBody.create(MediaType.parse("multipart/form-data"), title)
        val bodyBody = RequestBody.create(MediaType.parse("multipart/form-data"), description)

        val multipartBodyList = ArrayList<MultipartBody.Part>()
        for (i in 0 until filePathList.size) {
            val file = File(filePathList[i])
            val mediaType = MediaType.parse("image/*")
            val requestFile = RequestBody.create(mediaType, file)
            if (file.isFile) {
                val part = MultipartBody.Part.createFormData("files[$i]", file.name, requestFile)
                multipartBodyList.add(part)
            }
        }
        if (multipartBodyList.size == 0) {
            multipartBodyList.add(MultipartBody.Part.createFormData("", ""))
        }
        request(
            DMSRest.get(NotificationAPIService::class.java).sendMessage(cIDBody, titleBody, bodyBody, multipartBodyList)
        ).then { _, responseData ->
            callback.invoke(responseData.data != null)
        }.setTaskNameResId(R.string.task_api_send_message)
            .submit()
    }
}
