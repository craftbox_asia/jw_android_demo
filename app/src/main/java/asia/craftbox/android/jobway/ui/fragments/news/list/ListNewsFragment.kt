package asia.craftbox.android.jobway.ui.fragments.news.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentNewsListBinding
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.activities.news.details.DetailsNewsActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.news.list.adapters.ListHotNewsAdapter
import asia.craftbox.android.jobway.ui.fragments.news.list.adapters.ListLatestNewsAdapter
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import timber.log.Timber


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list
 * Date: Feb 13, 2019 - 2:38 PM
 */


class ListNewsFragment : AppMainFragment<FragmentNewsListBinding>(R.layout.fragment_news_list),
    DMSViewModelSource<ListNewsViewModel> {

    private val presenter by lazy {
        ListNewsPresenter(this)
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@ListNewsFragment)[ListNewsViewModel::class.java]
    }


    override fun getTitleResId(): Int? {
        return R.string.title_news_list
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ListNewsFragment.dataBinding.presenter = this@ListNewsFragment.presenter
        this@ListNewsFragment.dataBinding.viewModel = this@ListNewsFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()

        // setup hot news
        this@ListNewsFragment.presenter.hotNewsAdapter =
            ListHotNewsAdapter(this@ListNewsFragment.childFragmentManager)
        this@ListNewsFragment.dataBinding.newsListHotViewPager.adapter = this@ListNewsFragment
            .presenter.hotNewsAdapter
        this@ListNewsFragment.dataBinding.newsListHotViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@ListNewsFragment.dataBinding.newsListHotViewPager)
            .pagerMargin(this@ListNewsFragment.requireContext().resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()

        // setup latest news
        val latestLayoutMgr = LinearLayoutManager(this@ListNewsFragment.requireContext())
        latestLayoutMgr.isAutoMeasureEnabled = true

        this@ListNewsFragment.presenter.latestNewsAdapter = ListLatestNewsAdapter()
        this@ListNewsFragment.dataBinding.newsListLatestRecycleView.adapter =
            this@ListNewsFragment.presenter.latestNewsAdapter
        this@ListNewsFragment.dataBinding.newsListLatestRecycleView.layoutManager = latestLayoutMgr
        this@ListNewsFragment.dataBinding.newsListLatestRecycleView.isNestedScrollingEnabled = false

        this@ListNewsFragment.presenter.latestNewsAdapter?.setOnItemSelected {
            (this@ListNewsFragment.activity as? MainActivity)?.let { mainActivity ->
                val newsActivity = Intent(mainActivity, DetailsNewsActivity::class.java)
                newsActivity.putExtra(DetailsNewsActivity.INTENT_DATA_NEWS_ID, it.id)
                mainActivity.startActivity(newsActivity)
            }
        }
    }

    override fun initObservables() {
        super.initObservables()

        this@ListNewsFragment.viewModel.getHotNews().observe(this@ListNewsFragment, Observer {
            val hotNewsFragments = it.map { newsItem -> ListHotNewsAdapter.FragmentItem.create(newsItem) }
            this@ListNewsFragment.presenter.hotNewsAdapter?.updateItems(hotNewsFragments)

            this@ListNewsFragment.dataBinding.newsListHotViewPager.post {
                try {
                    val pager = this@ListNewsFragment.dataBinding.newsListHotViewPager
                    val fragment = pager.adapter?.instantiateItem(pager, 0) as Fragment
                    fragment.view?.let { fragmentView -> ViewCompat.setElevation(fragmentView, 8.0f) }
                } catch (illegalStateException: Exception) {
                    Timber.e(illegalStateException.localizedMessage)
                }
            }
        })

        this@ListNewsFragment.viewModel.getLatestNews().observe(this@ListNewsFragment, Observer {
            this@ListNewsFragment.presenter.latestNewsAdapter?.submitList(it)
        })
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_news_list_retrieve,
            R.string.task_api_news_hot_list_retrieve
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (arrayOf(
                R.string.task_api_news_list_retrieve,
                R.string.task_api_news_hot_list_retrieve
            ).contains(promise.taskNameResId)
        ) {
            return
        }

        super.onAPIWillRequest(promise)
    }


    inner class ListNewsPresenter(private val fragment: ListNewsFragment): BasePresenter<FragmentNewsListBinding>(fragment) {

        var hotNewsAdapter: ListHotNewsAdapter? = null
        var latestNewsAdapter: ListLatestNewsAdapter? = null

        fun setDrawerOpen(isOpen: Boolean) {
            (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
        }

    }

}
