package asia.craftbox.android.jobway.ui.fragments.consult.question.detail_question

import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultDetailQuestionBinding
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import kotlinx.android.synthetic.main.fragment_counselor_consult_detail_question.*
import timber.log.Timber


class CounselorConsultDetailQuestionFragment : AppMainFragment<FragmentCounselorConsultDetailQuestionBinding>(R.layout.fragment_counselor_consult_detail_question) {

    override fun getTitleResId(): Int? {
        return R.string.text_requently_question
    }

    val presenter by lazy {
        CounselorConsultDetailQuestionPresenter(this@CounselorConsultDetailQuestionFragment)
    }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter

        super.onContentViewCreated(rootView, savedInstanceState)

        val fragmentBundle = arguments ?: return
        try {
            val questionModel = fragmentBundle.getSerializable(JobWayConstant.INTENT_QUESTION_MODEL)
                    as? QuestionModel ?: return
            val index = fragmentBundle.getInt(JobWayConstant.INTENT_QUESTION_MODEL_INDEX)
            loadUIFromData(questionModel, index)

        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }

    private fun loadUIFromData(questionModel: QuestionModel, index: Int) {
        question_header_text_view.text = String.format(context!!.getString(R.string.text_question_at_index), index + 1)
        question_text_view.text = questionModel.question
        question_time_text_view.text = questionModel.updatedAt

        answer_text_view.text = questionModel.answer
        answer_time_text_view.text = questionModel.updatedAt

    }
}
