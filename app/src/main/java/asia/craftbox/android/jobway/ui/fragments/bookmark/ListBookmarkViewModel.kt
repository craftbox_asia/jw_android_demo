package asia.craftbox.android.jobway.ui.fragments.bookmark

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import androidx.paging.toLiveData
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.services.NewsAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list
 * Date: Feb 13, 2019 - 2:39 PM
 */


class ListBookmarkViewModel : DMSViewModel() {

    private val newsPerPage = 10
    private var currentPage = 0
    private var totalPages = Int.MAX_VALUE
    private var lastNewId: Long = -1


    private val fetchNewsListTask by lazy {
        val instance = PublishSubject.create<Boolean>()
        this@ListBookmarkViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                if ((this@ListBookmarkViewModel.currentPage + 1) <= this@ListBookmarkViewModel.totalPages) {
                    this@ListBookmarkViewModel.request(DMSRest.get(NewsAPIService::class.java).bookmarkPosts(page = this@ListBookmarkViewModel.currentPage + 1))
                        .setTaskNameResId(R.string.task_api_news_list_retrieve)
                        .then { _, data ->
                            this@ListBookmarkViewModel.currentPage = data.data?.meta?.currentPage ?: 1
                            this@ListBookmarkViewModel.totalPages = data.data?.meta?.totalPages ?: Int.MAX_VALUE
                            data.data?.data?.forEach {
                                it.favNews = 1
                                it.saveOrUpdate()
                            }
                        }
                        .catch { _, exc ->
                            Timber.wtf(exc.original)
                        }
                        .submit()
                }
            })
        return@lazy instance
    }

    private val newsOnRequestFetching = object : PagedList.BoundaryCallback<NewsModel>() {

        override fun onItemAtEndLoaded(itemAtEnd: NewsModel) {
            super.onItemAtEndLoaded(itemAtEnd)
            if (itemAtEnd.id!!.equals(lastNewId)){
                return
            }
            lastNewId = itemAtEnd.id!!
            this@ListBookmarkViewModel.fetchNewsListTask.onNext(true)
        }

    }

    fun doFetchNews() {
        this@ListBookmarkViewModel.currentPage = 0
        this@ListBookmarkViewModel.totalPages = Int.MAX_VALUE
        AppData.get().getNews().cleanBookmarkNews()
        this@ListBookmarkViewModel.fetchNewsListTask.onNext(true)
    }

    fun getLatestNews(): LiveData<PagedList<NewsModel>> {
        val factory = AppData.get().getNews().getBookmarkNewsPaginated()
        return factory.toLiveData(
            pageSize = this@ListBookmarkViewModel.newsPerPage,
            boundaryCallback = this@ListBookmarkViewModel.newsOnRequestFetching
        )
    }

    override fun lifecycleOnCreate() {
        super.lifecycleOnCreate()
        this@ListBookmarkViewModel.doFetchNews()
    }

}
