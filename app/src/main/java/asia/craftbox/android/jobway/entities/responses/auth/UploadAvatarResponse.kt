package asia.craftbox.android.jobway.entities.responses.auth

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSJsonableObject

class UploadAvatarResponse: DMSJsonableObject {
    @SerializedName("data")
    var urlAvatar: String = ""
    @SerializedName("message")
    var message: String = ""
}