package asia.craftbox.android.jobway.ui.widgets

import android.content.Context
import android.util.AttributeSet
import androidx.cardview.widget.CardView

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Dec 28, 2018 - 10:39 PM
 */


class RectangleCardView : CardView {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.EXACTLY))
    }

}
