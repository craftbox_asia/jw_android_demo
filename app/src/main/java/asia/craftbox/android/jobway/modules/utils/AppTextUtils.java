package asia.craftbox.android.jobway.modules.utils;

import android.text.TextUtils;

import javax.annotation.Nullable;

/**
 * Created by @dphans (https://github.com/dphans)
 * <p>
 * Package: asia.craftbox.android.jobway.modules.utils
 * Date: Dec 30, 2018 - 7:18 PM
 */


public class AppTextUtils {

    @Nullable
    public static String getStringByIndex(@Nullable String text, int index) {
        if (text == null) {
            return null;
        }

        if (TextUtils.isEmpty(text)) {
            return null;
        }

        try {
            return String.valueOf(text.charAt(index));
        } catch (Exception exception) {
            exception.printStackTrace();
            return null;
        }
    }

    public static int getStringLength(@Nullable String text) {
        if (text == null) {
            return 0;
        }

        if (TextUtils.isEmpty(text)) {
            return 0;
        }

        return text.length();
    }

}
