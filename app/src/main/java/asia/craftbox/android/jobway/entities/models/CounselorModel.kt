package asia.craftbox.android.jobway.entities.models

import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import com.google.gson.annotations.SerializedName

enum class CounselorRole {
    EX, // expert
    CO
}

@Entity
class CounselorModel : AppModel() {

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    @SerializedName("address")
    var address: String? = null

    @SerializedName("role_id")
    var roleId: Int? = -1

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("role")
    var role: Role? = null

    @SerializedName("meta")
    var meta: Meta? = null

    class Role {
        @SerializedName("cd")
        var cd: String? = null

        @SerializedName("name")
        var name: String? = null
    }

    class Meta {
        @SerializedName("star")
        var star: Int? = 0

        @SerializedName("title")
        var title: String? = null

        @SerializedName("summary")
        var summary: String? = null
    }

    fun getFullName() : String {
        return "$firstName $lastName"
    }

    fun isExpert() : Boolean {
        if (role == null) return false
        if (role!!.cd == null) return false
        return (role!!.cd!!.toUpperCase() == CounselorRole.EX.toString())
    }

    fun isCo() : Boolean {
        if (role == null) return false
        if (role!!.cd == null) return false
        return (role!!.cd!!.toUpperCase() == CounselorRole.CO.toString())
    }

    fun getDescriptionJob() : String {
        return meta?.title ?: ""
    }

    fun getSummary() : String {
        return meta?.summary ?: ""
    }

    fun getStar() : Int {
        return meta?.star ?: 0
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<CounselorModel>() {
            override fun areItemsTheSame(oldItem: CounselorModel, newItem: CounselorModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: CounselorModel, newItem: CounselorModel): Boolean {
                return false
            }
        }
    }
}