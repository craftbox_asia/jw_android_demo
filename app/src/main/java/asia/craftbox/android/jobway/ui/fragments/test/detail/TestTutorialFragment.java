package asia.craftbox.android.jobway.ui.fragments.test.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import asia.craftbox.android.jobway.R;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

public class TestTutorialFragment extends DialogFragment {

    public  static String TAG = "TEST_TUTORIAL_FRAGMENT";
    public static TestTutorialFragment newInstance() {
        TestTutorialFragment frag = new TestTutorialFragment();
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dialog_test_tutorial, container);

        TextView close = view.findViewById(R.id.close_text);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        ViewPager pager = view.findViewById(R.id.view_pager);
        TestTutorialAdapter adapter = new TestTutorialAdapter(view.getContext());
        pager.setAdapter(adapter);

        SpringDotsIndicator dotsIndicator = view.findViewById(R.id.spring_dots_indicator);
        dotsIndicator.setViewPager(pager);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }
}
