package asia.craftbox.android.jobway.entities.responses.news

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.responses.news
 * Date: Feb 22, 2019 - 2:51 PM
 */


class IncrementNewsResponse : DataFieldDMSRestResponse<IncrementNewsResponse.Item>() {

    class Item : DMSJsonableObject {

        @SerializedName("coin")
        var coin: Int = 0

    }

}
