package asia.craftbox.android.jobway.ui.activities.splash

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.splash
 * Date: Jan 07, 2019 - 10:23 AM
 */


class SplashActivity : AppCompatActivity() {

    @SuppressLint("CheckResult")
    override fun onResume() {
        super.onResume()
        Observable.timer(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .filter { this@SplashActivity.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) }
            .subscribe {
                this@SplashActivity.startActivity(Intent(this@SplashActivity, MainActivity::class.java))
                this@SplashActivity.finish()
            }
    }

}
