package asia.craftbox.android.jobway.ui.fragments.pages.tutorial

import android.view.View

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.tutorial
 * Date: Jan 07, 2019 - 4:13 PM
 */


class TutorialFinishedPresenter(private val fragment: TutorialFinishedFragment) {

    fun onFinishedButtonClicked(sender: View?) {
        this@TutorialFinishedPresenter.fragment.activity?.finish()
    }

}
