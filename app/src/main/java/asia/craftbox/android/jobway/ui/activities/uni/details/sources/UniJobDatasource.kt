package asia.craftbox.android.jobway.ui.activities.uni.details.sources

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.UniFacultyModel
import asia.craftbox.android.jobway.entities.services.JobAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.entities.DMSViewModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.uni.details.sources
 * Date: Feb 26, 2019 - 5:17 PM
 */


class UniJobDatasource(private val uniId: Long) : PageKeyedDataSource<Int, UniFacultyModel>() {

    private val listener = MutableLiveData<DMSViewModel.Listener?>()


    fun setListener(block: DMSViewModel.Listener?) {
        this@UniJobDatasource.listener.postValue(block)
    }

    fun getListener(): LiveData<DMSViewModel.Listener?> {
        return this@UniJobDatasource.listener
    }


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, UniFacultyModel>) {
        DMSRestPromise(
            DMSRest.get(JobAPIService::class.java).getJobsListByUniId(
                uniId = this@UniJobDatasource.uniId,
                page = 1
            )
        )
            .doBeforeRequest { this@UniJobDatasource.getListener().value?.onAPIWillRequest(it) }
            .then { _, data ->
                val resData = data.data ?: return@then
                val jobItems = resData.data
                val paginateData = resData.meta
                val nextPage = if (paginateData.currentPage < (paginateData.totalPages - 1))
                    paginateData.currentPage + 1
                else
                    null
                callback.onResult(jobItems, null, nextPage)
            }
            .then { promise, data -> this@UniJobDatasource.getListener().value?.onAPIResponseSuccess(promise, data) }
            .catch { promise, exc -> this@UniJobDatasource.getListener().value?.onAPIResponseError(promise, exc) }
            .doAfterResponse { this@UniJobDatasource.getListener().value?.onAPIDidFinished(it) }
            .setTaskNameResId(R.string.task_api_uni_jobs_listing)
            .submit()
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, UniFacultyModel>) {
        DMSRestPromise(
            DMSRest.get(JobAPIService::class.java).getJobsListByUniId(
                uniId = this@UniJobDatasource.uniId,
                page = params.key
            )
        )
            .doBeforeRequest { this@UniJobDatasource.getListener().value?.onAPIWillRequest(it) }
            .then { _, data ->
                val resData = data.data ?: return@then
                val jobItems = resData.data
                val paginateData = resData.meta
                val nextPage = if (paginateData.currentPage < (paginateData.totalPages - 1))
                    paginateData.currentPage + 1
                else
                    null
                callback.onResult(jobItems, nextPage)
            }
            .then { promise, data -> this@UniJobDatasource.getListener().value?.onAPIResponseSuccess(promise, data) }
            .catch { promise, exc -> this@UniJobDatasource.getListener().value?.onAPIResponseError(promise, exc) }
            .doAfterResponse { this@UniJobDatasource.getListener().value?.onAPIDidFinished(it) }
            .setTaskNameResId(R.string.task_api_uni_jobs_listing)
            .submit()
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, UniFacultyModel>) {

    }

}
