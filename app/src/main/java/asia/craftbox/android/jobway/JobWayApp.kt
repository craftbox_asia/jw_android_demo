@file:Suppress("unused")

package asia.craftbox.android.jobway

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.res.Resources
import android.os.Build
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.utils.SharedPreferencesUtils
import com.crashlytics.android.Crashlytics
import com.google.firebase.auth.FirebaseAuth
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.extensions.initialCoreDMS
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.fabric.sdk.android.Fabric
import java.util.prefs.Preferences

/**
 * Created by @dphans (https://github.com/dphans)
 * December 2018
 */


class JobWayApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Fabric.with(this@JobWayApp.applicationContext, Crashlytics())

        this@JobWayApp.initialCoreDMS()
        AppData.initDefault(this@JobWayApp)
        this@JobWayApp.initialAPIServices()
        this@JobWayApp.registerNotificationChannels()
        instance = this
        res = resources
    }

    private fun registerNotificationChannels() {
        val notificationManager = this@JobWayApp.getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val pushDefaultChannel = NotificationChannel(
                this@JobWayApp.getString(R.string.const_notification_channel_id_push_default),
                this@JobWayApp.getString(R.string.notification_channel_name_push_default),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(pushDefaultChannel)
        }
    }

    private fun initialAPIServices() {
        DMSRest.config
            .updateAPIBaseURL(BuildConfig.SERVER_URL)
            .updateAuthorizationToken(
                DMSPreference.default().get(
                    JobWayConstant.Preference.AuthAccessToken.name,
                    String::class.java
                )
            )
    }


    companion object {
        lateinit var instance: Application
        lateinit var res: Resources
        fun cleanupUserData() {
            // logout from firebase
            FirebaseAuth.getInstance().signOut()
            // remove all sensitive preferences
            JobWayConstant.Preference.values()
                .filter { it.isSensitive }
                .map { it.name }
                .forEach { preferenceKey -> DMSPreference.default().set(preferenceKey, null) }
        }

    }

}
