package asia.craftbox.android.jobway.EventBus

import asia.craftbox.android.jobway.entities.responses.news.BookmarkResponse

open  class PostBookmarkEvent(res: BookmarkResponse) {
    var response: BookmarkResponse = res
}