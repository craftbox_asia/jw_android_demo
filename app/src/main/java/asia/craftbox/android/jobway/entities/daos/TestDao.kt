package asia.craftbox.android.jobway.entities.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import asia.craftbox.android.jobway.entities.models.ProfileResumeModel
import asia.craftbox.android.jobway.entities.models.ResultProfileModel
import asia.craftbox.android.jobway.entities.models.ResultTestModel
import com.seldatdirect.dms.core.entities.DMSDao

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.daos
 * Date: Jan 25, 2019 - 10:28 AM
 */


@Dao
interface TestDao : DMSDao<ResultTestModel> {

    @Query("DELETE FROM ResultTestModel WHERE 1")
    fun cleanUpResultTests()

    @Query("SELECT * FROM ResultTestModel WHERE userId = :user AND type = :typeCode")
    fun getByUserAndType(user: Long, typeCode: String): LiveData<ResultTestModel>

    @Query("SELECT * FROM ResultTestModel WHERE userId = :user AND type = :typeCode")
    fun getByUserAndTypeFlatten(user: Long, typeCode: String): ResultTestModel?

    @Query("SELECT COUNT(*) FROM ResultTestModel WHERE userId = :userId")
    fun countTestsByUserIdFlatten(userId: Long): Int

    @Query("SELECT * FROM ProfileResumeModel WHERE `key` = :resumeKey")
    fun getResumeByKey(resumeKey: String): LiveData<ProfileResumeModel>

    @Query("SELECT * FROM ProfileResumeModel WHERE `key` = :resumeKey")
    fun getResumeByKeyFlatten(resumeKey: String): ProfileResumeModel?

    @Query("SELECT * FROM ProfileResumeModel")
    fun getAllResumesFlatten(): List<ProfileResumeModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOneResume(resume: ProfileResumeModel): Long

    @Query("SELECT * FROM ResultProfileModel WHERE userId = :userId")
    fun getProfileResultByUserIdFlatten(userId: Long?): ResultProfileModel?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOneResultProfile(profileModel: ResultProfileModel): Long

    @Query("SELECT COUNT(*) FROM ResultProfileModel WHERE userId = :userId")
    fun countProfilesByUserIdFlatten(userId: Long): Int

    @Query("SELECT (SELECT COUNT(*) FROM ResultProfileModel WHERE userId = :userId) + (SELECT COUNT(*) FROM ResultTestModel WHERE userId = :userId)")
    fun countCompletedItems(userId: Long): Int

}
