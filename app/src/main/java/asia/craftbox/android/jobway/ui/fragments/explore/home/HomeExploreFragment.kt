package asia.craftbox.android.jobway.ui.fragments.explore.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentExploreHomeBinding
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.test.detail.TestTutorialFragment
import asia.craftbox.android.jobway.utils.SharedPreferencesUtils
import com.seldatdirect.dms.core.datasources.DMSViewModelSource

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.explore.home
 * Date: Jan 17, 2019 - 6:00 PM
 */


class HomeExploreFragment : AppMainFragment<FragmentExploreHomeBinding>(R.layout.fragment_explore_home),
    DMSViewModelSource<HomeExploreViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@HomeExploreFragment)[HomeExploreViewModel::class.java]
    }

    private val presenter by lazy {
        HomeExplorePresenter(this@HomeExploreFragment)
    }

    override fun getTitleResId(): Int? {
        return R.string.title_explore_home
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@HomeExploreFragment.presenter.notifyChange()
        this@HomeExploreFragment.viewModel.toString()

        this@HomeExploreFragment.dataBinding.viewModel = this@HomeExploreFragment.viewModel
        this@HomeExploreFragment.dataBinding.presenter = this@HomeExploreFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()
        this@HomeExploreFragment.dataBinding.exploreHomeViewPager.adapter =
            this@HomeExploreFragment.presenter.homePagerAdapter
        this@HomeExploreFragment.dataBinding.exploreHomeViewPager.offscreenPageLimit =
            this@HomeExploreFragment.presenter.homePagerAdapter.count
        this@HomeExploreFragment.dataBinding.exploreHomeViewPagerDots
            .setupWithViewPager(this@HomeExploreFragment.dataBinding.exploreHomeViewPager)

        //Add tutorial
        if (SharedPreferencesUtils.getBoolValue(context, TestTutorialFragment.TAG))
        {
            return
        }

        val transaction = fragmentManager?.beginTransaction()
        val previous = fragmentManager?.findFragmentByTag(TestTutorialFragment.TAG)
        if (previous != null) {
            transaction?.remove(previous)
        }
        transaction?.addToBackStack(null)

        val tutorialFragment = TestTutorialFragment.newInstance()
        tutorialFragment.show(transaction, TestTutorialFragment.TAG)
        SharedPreferencesUtils.putBoolValue(context, TestTutorialFragment.TAG, true)
    }

}
