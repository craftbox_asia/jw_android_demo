package asia.craftbox.android.jobway.ui.fragments.notification

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentNotificationBinding
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.NotificationModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.activities.news.details.DetailsNewsActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history.CounselorHistoryDetailFragment
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsFragment
import asia.craftbox.android.jobway.utils.Utils
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class NotificationFragment : AppMainFragment<FragmentNotificationBinding>(R.layout.fragment_notification),
    DMSViewModelSource<NotificationViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@NotificationFragment)[NotificationViewModel::class.java]
    }

    val presenter by lazy {
        NotificationPresenter(this@NotificationFragment)
    }

    var totalPageNotificationNews: Int = 1
    var currentPageNotificationNews: Int = 1

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_get_list_message,
            R.string.task_api_get_list_noti_message,
            R.string.task_api_update_read_noti
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_update_read_noti) {
            alert.progressShow(R.string.task_api_update_read_noti)
            return
        }
        if (promise.taskNameResId == R.string.task_api_get_list_noti_news) {
            return
        }
        if (promise.taskNameResId == R.string.task_api_get_list_noti_message) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {
        super.onAPIDidFinished(promise)
        if (promise.taskNameResId == R.string.task_api_update_read_noti) {
            alert.progressDismiss()
            return
        }
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter

        super.onContentViewCreated(rootView, savedInstanceState)

        dataBinding.closeImageView.setOnClickListener {
            val mainActivity = activity as? MainActivity
            mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
        }

        getListNotiMessageAPI()
        getListNotiNewsAPI(currentPageNotificationNews)
    }

    override fun initUI() {
        super.initUI()

        val messageRecyclerLayout = LinearLayoutManager(this.requireContext())
        messageRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.notiMessageRecyclerView.layoutManager = messageRecyclerLayout
        dataBinding.notiMessageRecyclerView.isNestedScrollingEnabled = false
        dataBinding.notiMessageRecyclerView.adapter = presenter.notificationMessageAdapter
        presenter.notificationMessageAdapter?.setCallback { messageModel, position ->
            handleWhenClickedOnMessage(messageModel, position)
        }

        val newsRecyclerLayout = LinearLayoutManager(this.requireContext())
        newsRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.notiNewsRecyclerView.layoutManager = newsRecyclerLayout
        dataBinding.notiNewsRecyclerView.isNestedScrollingEnabled = false
        dataBinding.notiNewsRecyclerView.adapter = presenter.notificationNewsAdapter
        presenter.notificationNewsAdapter?.setCallback { notificationModel, position ->
            handleWhenClickedOnNotification(notificationModel, position)
        }

        dataBinding.scrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                if (totalPageNotificationNews > currentPageNotificationNews) {
                    getListNotiNewsAPI(currentPageNotificationNews + 1)
                }
            }
        }
    }

    private fun getListNotiMessageAPI() {
        viewModel.getListNotiMessage { list ->
            val diffList = Utils.instance().getDiffItems(presenter.messageList, list)
            if (diffList.isEmpty()) {
                return@getListNotiMessage
            }
            presenter.messageList.addAll(diffList)
            presenter.notificationMessageAdapter?.notifyDataSetChanged()
        }
    }

    private fun getListNotiNewsAPI(page: Int) {
        viewModel.getListNotiNews(page) { notificationNewsResponse ->
            if (notificationNewsResponse == null) {
                return@getListNotiNews
            }
            val diffList = Utils.instance().getDiffItems(presenter.newsList, notificationNewsResponse.data)
            if (diffList.isEmpty()) {
                return@getListNotiNews
            }
            currentPageNotificationNews = notificationNewsResponse.meta.currentPage
            totalPageNotificationNews = notificationNewsResponse.meta.totalPages
            presenter.newsList.addAll(diffList)
            presenter.notificationNewsAdapter?.notifyDataSetChanged()
        }
    }

    private fun handleWhenClickedOnMessage(messageModel: MessageModel, position: Int) {
        ShareData.getInstance().currentHistoryMessage = messageModel
        if (messageModel.isRead()) {
            handleWhenReadMessage()
        } else {
            updateReadNoti(messageModel.id!!.toInt(), messageModel.type, position)
        }
    }

    private fun handleWhenClickedOnNotification(notificationModel: NotificationModel, position: Int) {
        ShareData.getInstance().currentNews = notificationModel
        if (notificationModel.isRead()) {
            handleWhenReadNews()
        } else {
            updateReadNoti(notificationModel.id!!.toInt(), notificationModel.type, position)
        }
    }

    private fun updateReadNoti(id: Int, type: String, postition: Int) {
        viewModel.updateReadNoti(id.toString(), type) { success ->
            if (!success) {
                return@updateReadNoti
            }
            if (type == JobWayConstant.TYPE_MESSAGE) {
                presenter.messageList[postition].read = 1
                presenter.notificationMessageAdapter?.notifyItemChanged(postition)

                handleWhenReadMessage()
            } else {
                presenter.newsList[postition].read = 1
                presenter.notificationNewsAdapter?.notifyItemChanged(postition)

                handleWhenReadNews()
            }
        }
    }

    // region ------ Message ------
    private fun handleWhenReadMessage() {
        val mainActivity = activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
        moveToHistoryDetailFragment()
    }

    private fun moveToHistoryDetailFragment() {
        val mainActivity = activity as? MainActivity
        ShareData.getInstance().isClickedNotification = true
        val historyDetailFragment = DMSFragment.create(CounselorHistoryDetailFragment::class.java)
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
        mainActivity?.getFragmentHelper()?.pushFragment(historyDetailFragment)
    }

    // endregion --- Message ---

    // region ------ News ------
    private fun handleWhenReadNews() {
        val mainActivity = activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
        moveToListNewsFragment()
        moveToDetailsNewsActivity()
    }

    private fun moveToListNewsFragment() {
        val mainActivity = activity as? MainActivity
        mainActivity?.getFragmentHelper()
            ?.pushFragment(fragment = DMSFragment.create(ListNewsFragment::class.java), replaceRootFragment = false)
    }

    private fun moveToDetailsNewsActivity() {
        val mainActivity = activity as? MainActivity
        val newsActivity = Intent(mainActivity, DetailsNewsActivity::class.java)
        newsActivity.putExtra(DetailsNewsActivity.INTENT_DATA_NEWS_ID, ShareData.getInstance().currentNews!!.postId.toLong())
        mainActivity?.startActivity(newsActivity)
    }
    // endregion ------ News ------
}
