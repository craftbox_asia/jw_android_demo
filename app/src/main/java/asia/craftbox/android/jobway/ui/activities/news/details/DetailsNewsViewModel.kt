package asia.craftbox.android.jobway.ui.activities.news.details

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import asia.craftbox.android.jobway.EventBus.NewsEvent
import asia.craftbox.android.jobway.EventBus.PostBookmarkEvent
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.requests.news.BookmarkRquest
import asia.craftbox.android.jobway.entities.requests.news.IncrementNewsRequest
import asia.craftbox.android.jobway.entities.services.NewsAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import org.greenrobot.eventbus.EventBus
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.news.details
 * Date: Feb 13, 2019 - 5:32 PM
 */


class DetailsNewsViewModel : DMSViewModel() {

    private val newsIdMutable = MutableLiveData<Long>()
    private val relatedPostsMutable = MutableLiveData<List<NewsModel>>()

    private val newsItemTransform = Transformations.map(this@DetailsNewsViewModel.newsIdMutable) {
        return@map AppData.get().getNews().getNewsByIdFlatten(it)
    }

    private val increaseViewsTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@DetailsNewsViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                val incrementNewsRequest = IncrementNewsRequest()
                incrementNewsRequest.id = it
                this@DetailsNewsViewModel.request(
                    DMSRest.get(NewsAPIService::class.java).incrementViews(
                        incrementNewsRequest
                    )
                )
                    .then { _, responseData ->
                        val profileId = DMSPreference.default().get(
                            JobWayConstant.Preference.AuthProfileId.name,
                            Long::class.java
                        ) ?: -1
                        AppData.get().getProfile().updateCoin(
                            profileId = profileId,
                            coin = responseData.data?.coin ?: 0
                        )
                    }
                    .setTaskNameResId(R.string.task_api_news_increment_views)
                    .submit()
            })
        return@lazy instance
    }


    private val boomarkTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@DetailsNewsViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                val bookmarRequest = BookmarkRquest()
                bookmarRequest.id = it
                this@DetailsNewsViewModel.request(
                    DMSRest.get(NewsAPIService::class.java).bookmark(
                        bookmarRequest
                    )
                )
                    .then { _, responseData ->
                        Log.i("response", responseData.toString())
                        EventBus.getDefault().post(PostBookmarkEvent(responseData))
                    }
                    .setTaskNameResId(R.string.task_api_news_increment_views)
                    .submit()
            })
        return@lazy instance
    }

    private val detailNewsSubject by lazy {
        val instance = PublishSubject.create<Long>()
        this@DetailsNewsViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe { newsId ->
                this@DetailsNewsViewModel.request(
                    DMSRest.get(NewsAPIService::class.java).getDetails(
                        postId = newsId
                    )
                )
                    .setTaskNameResId(R.string.task_api_news_detail)
                    .then { _, data ->
                        if (data.data != null)
                            EventBus.getDefault().post(NewsEvent(data.data!!))
                        this@DetailsNewsViewModel.relatedPostsMutable.postValue(
                            data.data?.relatedPost ?: emptyList()
                        )
                    }
                    .submit()
            })
        return@lazy instance
    }


    fun setupNewsWithId(newsId: Long) {
        this@DetailsNewsViewModel.newsIdMutable.postValue(newsId)
        this@DetailsNewsViewModel.detailNewsSubject.onNext(newsId)
    }

    fun doIncrementViews() {
        val newsId = this@DetailsNewsViewModel.getNewsItem().value?.id ?: return
        this@DetailsNewsViewModel.increaseViewsTask.onNext(newsId)
    }

    fun doBookmark() {
        val newsId = this@DetailsNewsViewModel.getNewsItem().value?.id ?: return
        this@DetailsNewsViewModel.boomarkTask.onNext(newsId)
    }

    fun getNewsItem(): LiveData<NewsModel?> {
        return this@DetailsNewsViewModel.newsItemTransform
    }

    fun getHotNews(): LiveData<List<NewsModel>> {
        return this@DetailsNewsViewModel.relatedPostsMutable
    }

}
