package asia.craftbox.android.jobway.ui.fragments.consult.counselors.warning_popup

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultWarningPopupBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.exts.fromHTML
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import kotlinx.android.synthetic.main.fragment_counselor_consult_warning_popup.*


class CounselorConsultWarningPopupFragment :
    AppMainFragment<FragmentCounselorConsultWarningPopupBinding>(R.layout.fragment_counselor_consult_warning_popup) {


    val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultWarningPopupFragment)[CounselorConsultWarningPopupViewModel::class.java]
    }

    val presenter by lazy {
        CounselorConsultWarningPopupPresenter(this)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        dataBinding.viewModel = viewModel
        super.onContentViewCreated(rootView, savedInstanceState)

        val counselorModel = ShareData.getInstance().currentCounselor ?: return
        loadUIFromData(counselorModel)
    }

    override fun initUI() {
        super.initUI()
    }

    private fun loadUIFromData(counselorModel: CounselorModel) {
        presenter.currentCounselorModel = counselorModel
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.nameTextView.text = counselorModel.getFullName()

        val profileId = DMSPreference.default().get(JobWayConstant.Preference.AuthProfileId.name, Long::class.java)
            ?: return
        val profile = AppData.get().getProfile().getProfileByIdFlatten(profileId) ?: return
        var name =
            String.format(context!!.getString(R.string.text_noti_counselor_warning_popup), profile.getDisplayName())
        name = name.replace("$$", "<b><font color='#006699'>", ignoreCase = true)
        name = name.replace("##", "</font></b>", ignoreCase = true)
        dataBinding.descriptionNotiTextView.text = name.fromHTML()

        var statusCount = 0
        if (!profile.isFinishedTest() && !profile.isFinishedProfile()) {
            status_count_text_view.text = String.format("%s/2", statusCount)
        } else {
            if (profile.isFinishedTest()) {
                statusCount++
                mbti_text_view.text = getText(R.string.status_explore_info_completed)
                mbti_text_view.setTextColor(ContextCompat.getColor(activity!!, R.color.color_custom_green))
                mbti_text_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle_green, 0, 0, 0);

            }
            if (profile.isFinishedProfile()) {
                statusCount++
                frofile_text_view.text = getText(R.string.status_explore_info_completed)
                frofile_text_view.setTextColor(ContextCompat.getColor(activity!!, R.color.color_custom_green))
                frofile_text_view.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle_green, 0, 0, 0);
            }
            status_count_text_view.text = String.format("%s/2", statusCount)
        }
    }
}
