package asia.craftbox.android.jobway.ui.activities.profile

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityProfileBinding
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.utils.FileUtil
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.IpCons
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import kotlinx.android.synthetic.main.activity_profile.*
import java.io.File


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.profile
 * Date: Jan 21, 2019 - 9:21 PM
 */


class ProfileActivity :
    DMSBindingActivity<ActivityProfileBinding>(R.layout.activity_profile),
    DMSViewModelSource<ProfileViewModel> {

    private val presenter by lazy { ProfilePresenter(this@ProfileActivity) }
    override val viewModel by lazy { ViewModelProviders.of(this@ProfileActivity)[ProfileViewModel::class.java] }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ProfileActivity.dataBinding.viewModel = this@ProfileActivity.viewModel
        this@ProfileActivity.dataBinding.presenter = this@ProfileActivity.presenter
        super.onContentViewCreated(rootView, savedInstanceState)

        val user = ShareData.getInstance().getCurrentProfileModel()
        val holland = AppData.get().getTest().getByUserAndTypeFlatten(
            user = user?.id ?: -1,
            typeCode = CreateTestRequest.TestType.Holland.typeCode ?: ""
        )
        val MBTI = AppData.get().getTest().getByUserAndTypeFlatten(
            user = user?.id ?: -1,
            typeCode = CreateTestRequest.TestType.MBTI.typeCode ?: ""
        )
        dataBinding.resultHollandTextview.text = holland?.code ?: ""
        dataBinding.resultMbtiTextview.text = MBTI?.code ?: ""
        dataBinding.hollandButton.setOnClickListener {
            val testActivity = Intent(this, TestActivity::class.java)
            testActivity.putExtra(
                "TestType",
                CreateTestRequest.TestType.Holland.typeCode
            )
            startActivity(testActivity)
        }
        dataBinding.mbtiButton.setOnClickListener {
            val testActivity = Intent(this, TestActivity::class.java)
            testActivity.putExtra(
                "TestType",
                CreateTestRequest.TestType.MBTI.typeCode
            )
            startActivity(testActivity)
        }
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_auth_update_profile,
            R.string.task_api_auth_profile
        )
    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {
        super.onAPIResponseSuccess(promise, responseData)

        if (promise.taskNameResId == R.string.task_api_auth_update_profile) {
            this@ProfileActivity.presenter.editingMode.set(false)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_OK) {
            return
        }

        var file: File? = null
        if(requestCode == JobWayConstant.PICK_GALARY)
        {
            val currImageURI = data?.getData()
            profile_image.setImageURI(currImageURI)
            file = FileUtil.from(this, currImageURI)
        }
        else
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            profile_image!!.setImageBitmap(thumbnail)
            file = FileUtil.fromBitmap(thumbnail)
        }

        viewModel.uploadAvatar(file)
    }

}
