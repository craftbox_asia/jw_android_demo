package asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert_bio

import android.os.Bundle
import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.default_consulting.CounselorConsultDefaultCounsultingFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.warning_popup.CounselorConsultWarningPopupFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorConsultExpertBioPresenter(private val fragment: CounselorConsultExpertBioFragment) : BaseObservable() {

    val mainActivity = fragment.activity as? MainActivity


    fun onBackButtonClicked(sender: View?) {
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onConsultButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        if (fragment.isShowWarningPopup == false) {
            val popupFragment = DMSFragment.create(CounselorConsultWarningPopupFragment::class.java)
            mainActivity?.getFragmentHelper()?.pushFragment(popupFragment)
        } else {
            val defaultCounsultingFragment = DMSFragment.create(CounselorConsultDefaultCounsultingFragment::class.java)
            mainActivity?.getFragmentHelper()?.pushFragment(defaultCounsultingFragment)
        }
    }

}
