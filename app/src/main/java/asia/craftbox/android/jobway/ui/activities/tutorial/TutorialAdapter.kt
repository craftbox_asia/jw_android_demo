package asia.craftbox.android.jobway.ui.activities.tutorial

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.fragments.pages.tutorial.TutorialFinishedFragment
import asia.craftbox.android.jobway.ui.fragments.pages.tutorial.TutorialFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.tutorial
 * Date: Jan 07, 2019 - 11:22 AM
 */


class TutorialAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private val fragments = listOf<DMSFragment>(
        TutorialFragment.newInstance(
            R.string.title_tutorial_page_i,
            R.string.content_tutorial_page_i,
            R.drawable.raw_tutorial_page_i
        ),
        TutorialFragment.newInstance(
            R.string.title_tutorial_page_ii,
            R.string.content_tutorial_page_ii,
            R.drawable.raw_tutorial_page_ii
        ),
        TutorialFragment.newInstance(
            R.string.title_tutorial_page_iii,
            R.string.content_tutorial_page_iii,
            R.drawable.raw_tutorial_page_iii
        ),
        TutorialFragment.newInstance(
            R.string.title_tutorial_page_iv,
            R.string.content_tutorial_page_iv,
            R.drawable.raw_tutorial_page_iv
        ),
        TutorialFinishedFragment()
    )


    override fun getItem(position: Int): Fragment {
        return this@TutorialAdapter.fragments[position]
    }

    override fun getCount(): Int {
        return this@TutorialAdapter.fragments.count()
    }

}
