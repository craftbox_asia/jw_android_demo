package asia.craftbox.android.jobway.ui.fragments.faculty.list.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDetailActivity
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list.adapters
 * Date: Feb 14, 2019 - 7:37 PM
 */

class ListFacultyAdapter(private val context: Context,
                         private val dataSource: ArrayList<FacultyModel>) : BaseAdapter() {

    private var mContext: Context
    var facultyList = ArrayList<FacultyModel>()
    init {
        mContext = context
        this.facultyList = dataSource
    }

    fun addMore(item:ArrayList<FacultyModel>) {
        facultyList.addAll(item)
        notifyDataSetChanged()
    }

    private var onFacultyItemClickedBlock: ((item: FacultyModel, pos: Int) -> Unit)? = null

    fun setOnUniItemClicked(block: ((item: FacultyModel, pos: Int) -> Unit)?) {
        this@ListFacultyAdapter.onFacultyItemClickedBlock = block
    }

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    //1
    override fun getCount(): Int {
        return facultyList.size
    }

    //2
    override fun getItem(position: Int): Any {
        return facultyList[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.viewholder_faculty_item, null)

        // Get detail element
        val detailTextView = rowView.findViewById(R.id.txt_faculty_name) as TextView

        // Get thumbnail element
        val thumbnailImageView = rowView.findViewById(R.id.feature_img_faculty) as RoundedImageView

        val facultyModel = getItem(position) as FacultyModel

        Glide.with(parent.context).load(facultyModel.getFeatureImg()).into(thumbnailImageView)
        detailTextView.setText(facultyModel.name)

        rowView.setOnClickListener{
            val facultyDetail = Intent(
                mContext,
                FacultyDetailActivity::class.java
            )
            facultyDetail.putExtra(FacultyDetailActivity.INTENT_DATA_FACULTY_OBJECT, facultyModel)
            mContext.startActivity(facultyDetail)
        }
        return rowView
    }
}
