package asia.craftbox.android.jobway.ui.fragments.uni.intro

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUniDetailIntroBinding
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniViewModel
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.admission
 * Date: Feb 14, 2019 - 9:43 PM
 */


class IntroUniFragment : DMSBindingFragment<FragmentUniDetailIntroBinding>(R.layout.fragment_uni_detail_intro),
    DMSViewModelSource<DetailsUniViewModel> {

    override val viewModel: DetailsUniViewModel by lazy {
        (this@IntroUniFragment.activity as DetailsUniActivity).viewModel
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@IntroUniFragment.dataBinding.viewModel = this@IntroUniFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initUI() {
        super.initUI()
        // optimize webview contents
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.displayZoomControls = false
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.builtInZoomControls = true
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.javaScriptEnabled = true
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.loadWithOverviewMode = true
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.useWideViewPort = true
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.setSupportZoom(false)
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.settings.setAppCacheEnabled(true)
        this@IntroUniFragment.dataBinding.uniDetailIntroWebView.isEnabled = false
    }

    override fun initObservables() {
        super.initObservables()
        this@IntroUniFragment.viewModel.getUni().observe(this@IntroUniFragment, Observer {
            val uniItem = it ?: return@Observer

            // render html into webview
            (uniItem.summary ?: "").attachJWHtmlIntoWebView(
                this@IntroUniFragment.dataBinding.uniDetailIntroWebView
            )
        })
    }

}
