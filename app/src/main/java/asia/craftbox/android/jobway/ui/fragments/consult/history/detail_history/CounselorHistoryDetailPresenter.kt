package asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history

import android.os.Bundle
import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose.CounselorConsultComposeFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.subtract_point.CounselorConsultSubtractionPointFragment
import asia.craftbox.android.jobway.ui.fragments.consult.history.history_image.CounselorConsultHistoryImageFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment


class CounselorHistoryDetailPresenter(private val fragment: CounselorHistoryDetailFragment) : BaseObservable() {

    var currentCounselorModel: CounselorModel? = null
    var currentMessageModel: MessageModel? = null

    var adapter: CounselorConsultHistoryDetailAdapter? = null
    var historyList: ArrayList<MessageModel> = ArrayList()

    init {
        currentMessageModel = ShareData.getInstance().currentHistoryMessage
        currentCounselorModel = ShareData.getInstance().currentCounselor

        adapter =
            CounselorConsultHistoryDetailAdapter(
                historyList, fragment.context!!
            )
        adapter?.setCallback { link ->
            val mainActivity = fragment.activity as? MainActivity
            val bundle = Bundle()
            bundle.putString(JobWayConstant.INTENT_STRING_URL, link)
            val historyDetailFragment = DMSFragment.create(CounselorConsultHistoryImageFragment::class.java, bundle)
            mainActivity?.getFragmentHelper()?.pushFragment(historyDetailFragment)
        }
    }

    fun getCurrentUserId(): Int? {
        if (ShareData.getInstance().isFromHistoryFrament(fragment.context!!)) {
            return currentMessageModel?.userId
        } else if (ShareData.getInstance().isClickedNotification) {
            return currentMessageModel?.userId
        }
        return currentCounselorModel?.id?.toInt()
    }

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onAskQuestionButtonClicked(sender: View?) {
        if (!fragment.isMaster) {
            val subtractionPointFragment = CounselorConsultSubtractionPointFragment()
            subtractionPointFragment.show(fragment.childFragmentManager, "CounselorConsultSubtractionPointFragment")
        }
        else {
            val mainActivity = fragment.activity as? MainActivity
            val composeFragment = DMSFragment.create(CounselorConsultComposeFragment::class.java)
            mainActivity?.getFragmentHelper()?.pushFragment(composeFragment)
        }
    }
}
