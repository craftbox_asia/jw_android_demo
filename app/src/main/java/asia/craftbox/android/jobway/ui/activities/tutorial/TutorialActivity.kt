package asia.craftbox.android.jobway.ui.activities.tutorial

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityTutorialBinding
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.tutorial
 * Date: Jan 02, 2019 - 5:14 PM
 */


class TutorialActivity : DMSBindingActivity<ActivityTutorialBinding>(R.layout.activity_tutorial),
    DMSViewModelSource<TutorialViewModel> {

    private val presenter by lazy { TutorialPresenter(this@TutorialActivity) }
    override val viewModel by lazy { ViewModelProviders.of(this@TutorialActivity)[TutorialViewModel::class.java] }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@TutorialActivity.dataBinding.presenter = this@TutorialActivity.presenter
        this@TutorialActivity.dataBinding.viewModel = this@TutorialActivity.viewModel

        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun onBackPressed() {
        // Keep user slide all pages before quit
        // Skip any back button pressed!
    }

    override fun initUI() {
        super.initUI()

        this@TutorialActivity.dataBinding
            .tutorialViewPager.adapter = this@TutorialActivity.presenter.pagerAdapter
    }

}
