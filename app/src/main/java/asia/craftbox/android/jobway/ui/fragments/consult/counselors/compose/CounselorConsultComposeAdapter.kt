package asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.libs.coverflow.core.Utils

class CounselorConsultComposeAdapter(private val items: ArrayList<Bitmap>, val context: Context) : BaseAdapter() {

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getView(position: Int, convertView: View?, p2: ViewGroup?): View {
        val view: View
        val viewHolder: ViewHolder

        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_consult_compose_file, null)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        val bitmap = items[position]
        viewHolder.image_view?.setImageBitmap(bitmap)
        if (view.layoutParams != null) {
            view.layoutParams.height = Utils.convertDpToPixel(100f, context).toInt()
        }
        return view
    }

    class ViewHolder(view: View?) {
        var image_view: ImageView? = null
        init {
            image_view = view?.findViewById(R.id.image_view)
        }
    }
}