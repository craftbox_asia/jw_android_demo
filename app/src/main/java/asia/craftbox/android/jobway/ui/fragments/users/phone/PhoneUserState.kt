package asia.craftbox.android.jobway.ui.fragments.users.phone

import android.content.Context
import androidx.annotation.StringRes
import asia.craftbox.android.jobway.R

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.users.phone
 * Date: Jan 15, 2019 - 10:13 AM
 */


enum class PhoneUserState(@StringRes val titleResId: Int, @StringRes val subtitleResId: Int) {
    EnterPhoneNumber(R.string.title_login_phone, R.string.hint_login_phone),
    EnterProfileInfo(R.string.title_login_user_info, R.string.hint_login_user_fullname);

    fun getTitle(context: Context?): String? {
        return context?.getString(this@PhoneUserState.titleResId)
    }

    fun getSubTitle(context: Context?): String? {
        return context?.getString(this@PhoneUserState.subtitleResId)
    }
}
