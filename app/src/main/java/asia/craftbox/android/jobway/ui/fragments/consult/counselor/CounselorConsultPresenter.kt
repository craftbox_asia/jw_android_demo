package asia.craftbox.android.jobway.ui.fragments.consult.counselor

import android.view.View
import asia.craftbox.android.jobway.databinding.FragmentConsultCounselorsBinding
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.consult.counselors
 * Date: Jan 17, 2019 - 3:07 PM
 */


class CounselorConsultPresenter(private val fragment: CounselorConsultFragment) : BasePresenter<FragmentConsultCounselorsBinding>(fragment) {

    var questionList: ArrayList<QuestionModel> = ArrayList()

    fun setDrawerOpen(isOpen: Boolean) {
        (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
    }
}
