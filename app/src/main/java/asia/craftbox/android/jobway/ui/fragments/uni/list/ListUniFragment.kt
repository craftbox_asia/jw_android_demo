package asia.craftbox.android.jobway.ui.fragments.uni.list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUniListBinding
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.uni.list.adapters.ListHotUniAdapter
import asia.craftbox.android.jobway.ui.fragments.uni.list.adapters.ListUniAdapter
import asia.craftbox.android.jobway.ui.fragments.uni.search.SearchUniFragment
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list
 * Date: Feb 14, 2019 - 3:53 PM
 */


class ListUniFragment : AppMainFragment<FragmentUniListBinding>(R.layout.fragment_uni_list),
    DMSViewModelSource<ListUniViewModel> {

    private val presenter by lazy {
        ListUniPresenter(this)
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@ListUniFragment)[ListUniViewModel::class.java]
    }

    override fun getTitleResId(): Int? {
        return R.string.title_fragment_uni_list
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ListUniFragment.dataBinding.presenter = this@ListUniFragment.presenter
        this@ListUniFragment.dataBinding.viewModel = this@ListUniFragment.viewModel
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_uni_list_retrieve
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_uni_list_retrieve) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun initUI() {
        super.initUI()

        // setup hot unis
        this@ListUniFragment.presenter.listHotUniAdapter =
            ListHotUniAdapter(this@ListUniFragment.childFragmentManager)
        this@ListUniFragment.dataBinding.uniListHotViewPager.adapter = this@ListUniFragment
            .presenter.listHotUniAdapter
        this@ListUniFragment.dataBinding.uniListHotViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@ListUniFragment.dataBinding.uniListHotViewPager)
            .pagerMargin(this@ListUniFragment.requireContext().resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()


        this@ListUniFragment.presenter.listUniAdapter = ListUniAdapter()

        val uniRecyclerLayout = LinearLayoutManager(this@ListUniFragment.requireContext())
        uniRecyclerLayout.isAutoMeasureEnabled = true

        this@ListUniFragment.dataBinding.uniListRecyclerView.layoutManager = uniRecyclerLayout
        this@ListUniFragment.dataBinding.uniListRecyclerView.isNestedScrollingEnabled = false
        this@ListUniFragment.dataBinding.uniListRecyclerView.adapter = this@ListUniFragment.presenter.listUniAdapter

        this@ListUniFragment.presenter.listUniAdapter?.setOnUniItemClicked { item, _ ->
            val uniDetail = Intent(
                this@ListUniFragment.requireContext().applicationContext,
                DetailsUniActivity::class.java
            )

            uniDetail.putExtra(DetailsUniActivity.INTENT_DATA_UNI_MODEL_ID, item.id)
            this@ListUniFragment.activity?.startActivity(uniDetail)
        }
    }

    override fun initObservables() {
        super.initObservables()

        this@ListUniFragment.viewModel.getUniPagination().observe(this@ListUniFragment, Observer {
            this@ListUniFragment.presenter.listUniAdapter?.submitList(it)
        })

        this@ListUniFragment.viewModel.getHotUni().observe(this@ListUniFragment, Observer {
            Timber.wtf("Hot uni count: ${it.count()}")

            val hotUninFragments = it.map { uniItem -> ListHotUniAdapter.FragmentItem.create(uniItem) }
            this@ListUniFragment.presenter.listHotUniAdapter?.updateItems(hotUninFragments)

            this@ListUniFragment.dataBinding.uniListHotViewPager.post {
                try {
                    val pager = this@ListUniFragment.dataBinding.uniListHotViewPager
                    val fragment = pager.adapter?.instantiateItem(pager, 0) as Fragment
                    fragment.view?.let { fragmentView -> ViewCompat.setElevation(fragmentView, 8.0f) }
                } catch (illegalStateException: Exception) {
                    Timber.e(illegalStateException.localizedMessage)
                }
            }
        })
    }

    inner class ListUniPresenter(private val fragment: ListUniFragment): BasePresenter<FragmentUniListBinding>(fragment) {
        var listHotUniAdapter: ListHotUniAdapter? = null
        var listUniAdapter: ListUniAdapter? = null


        fun setDrawerOpen(isOpen: Boolean) {
            (this@ListUniFragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
        }

        fun onSearchContainerClicked(sender: View?) {
            val fragmentHelper = this@ListUniPresenter.getMainActivity()?.getFragmentHelper() ?: return
            val sharedElement = this@ListUniFragment.dataBinding.uniListSearchContainer

            fragmentHelper.pushFragment(
                fragment = DMSFragment.create(SearchUniFragment::class.java),
                replaceRootFragment = false,
                sharedElements = mapOf(
                    (ViewCompat.getTransitionName(sharedElement)
                        ?: sharedElement::class.java.simpleName) to sharedElement
                )
            )
        }

        private fun getMainActivity(): MainActivity? {
            return this@ListUniFragment.activity as? MainActivity
        }

    }

}
