package asia.craftbox.android.jobway.entities.models

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 26, 2019 - 3:09 PM
 */


class UniFacultyModel : AppModel() {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("featured_img")
    var featuredImg: String? = null

    @SerializedName("uni_name")
    var uniName: String? = null

    @SerializedName("fac_name")
    var facName: String? = null

    @SerializedName("uni_type_name")
    var uniTypeName: String? = null

    @SerializedName("uni_class_name")
    var uniClassName: String? = null

    @SerializedName("target")
    var target: Int? = null

    @SerializedName("benchmark")
    var benchmark: String? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("fac_cd")
    var facCd: String? = null


    companion object {
        val DIFF = object : DiffUtil.ItemCallback<UniFacultyModel>() {
            override fun areItemsTheSame(oldItem: UniFacultyModel, newItem: UniFacultyModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: UniFacultyModel, newItem: UniFacultyModel): Boolean {
                return oldItem.name == newItem.name
                        && oldItem.target == newItem.target
                        && oldItem.benchmark == newItem.benchmark
            }
        }
    }

}
