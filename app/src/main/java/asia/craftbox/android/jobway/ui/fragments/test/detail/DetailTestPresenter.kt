package asia.craftbox.android.jobway.ui.fragments.test.detail

import android.content.Context
import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentTestDetailBinding
import asia.craftbox.android.jobway.entities.models.ResultTestModel
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter
import com.google.android.material.tabs.TabLayout
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import timber.log.Timber
import kotlin.math.roundToInt

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.test.detail
 * Date: Jan 19, 2019 - 7:11 PM
 */


class DetailTestPresenter(private val fragment: DetailTestFragment) : BasePresenter<FragmentTestDetailBinding>(fragment),
    TabLayout.OnTabSelectedListener {

    val testAdapter = DetailTestRecyclerAdapter()

    val testProgressVisible: ObservableField<Boolean> = ObservableField(false)

    val finishButtonText = ObservableField<String>("")
    val finishHintText = ObservableField<String>("")

    internal val testProgressCurrentMutable = MutableLiveData<Int>()
    internal val testProgressTotalMutable = MutableLiveData<Int>()

    var showTestDetail = MutableLiveData<Int>()
    var showTestResult = MutableLiveData<Int>()
    var isMbtiSelected: Boolean = true
    var isRetestMbti: Boolean = false
    var isRetestHolland: Boolean = false
    var isMbtiSubmited: Boolean = false
    var isHollandSubmited: Boolean = false

    private val testProgressPercentTransformation =
        Transformations.map(this@DetailTestPresenter.testProgressCurrentMutable) {
            val total = this@DetailTestPresenter.getTestProgressTotal().value ?: 100
            return@map try {
                "${(((it ?: 0).toFloat() / total.toFloat()) * 100.0f).roundToInt()}%"
            } catch (exception: Exception) {
                exception.printStackTrace()
                "0%"
            }
        }

    private val testProgressStatusTextTransformation =
        Transformations.map(this@DetailTestPresenter.testProgressCurrentMutable) {
            val total = this@DetailTestPresenter.testAdapter.itemCount ?: 100
            val statusText =
                this@DetailTestPresenter.fragment.requireContext().getString(R.string.hint_test_progress_status)
            return@map try {
                "$statusText: ${it ?: 0}/$total"
            } catch (exception: Exception) {
                exception.printStackTrace()
                ""
            }
        }


    fun getTestLayoutManager(): TestLayoutManager {
        return TestLayoutManager(this@DetailTestPresenter.fragment.requireContext())
    }

    fun onBackIconPressed() {
        this@DetailTestPresenter.fragment.alert
            .confirm(R.string.confirm_test_cancellation) {
                this@DetailTestPresenter.fragment.activity?.finish()
            }
    }

    fun getTestProgressCurrent(): LiveData<Int> {
        return this@DetailTestPresenter.testProgressCurrentMutable
    }

    fun getTestProgressStatusText(): LiveData<String> {
        return this@DetailTestPresenter.testProgressStatusTextTransformation
    }

    fun getTestProgressTotal(): LiveData<Int> {
        return this@DetailTestPresenter.testProgressTotalMutable
    }

    fun getTestProgressPercent(): LiveData<String> {
        return this@DetailTestPresenter.testProgressPercentTransformation
    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        val selectedTab = p0 ?: return
        val viewModel = this@DetailTestPresenter.fragment.viewModel

        when (selectedTab.tag) {
            CreateTestRequest.TestType.MBTI.typeCode -> {
                isMbtiSelected = true
                val mbtiResult:ResultTestModel? =  AppData.get().getTest().getByUserAndTypeFlatten(viewModel.getProfileId(), "MBTI")
                viewModel.setTestType(CreateTestRequest.TestType.MBTI)
                if ((mbtiResult == null || isRetestMbti) && !isMbtiSubmited) {
                    showDetail()
                } else{
                    showResult()
                }
            }
            CreateTestRequest.TestType.Holland.typeCode -> {
                isMbtiSelected = false
                val hollandResult:ResultTestModel? = AppData.get().getTest().getByUserAndTypeFlatten(viewModel.getProfileId(), "HOLLAND")
                viewModel.setTestType(CreateTestRequest.TestType.Holland)
                if ((hollandResult == null || isRetestHolland) && !isHollandSubmited){
                    showDetail()
                } else{
                    showResult()
                }
            }
        }
    }

    fun getShowTestResult() : LiveData<Int> {
        return this.showTestResult
    }

    fun getShowTestDetail() : LiveData<Int> {
        return this.showTestDetail
    }

    fun showDetail(){
        showTestResult.postValue(8)
        showTestDetail.postValue(0)
        scrollToTop()
    }

    fun showResult(){
        showTestResult.postValue(0)
        showTestDetail.postValue(8)
    }

    fun getContext(): Context? {
        return this@DetailTestPresenter.fragment.context
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {}

    override fun onTabUnselected(p0: TabLayout.Tab?) {}


    fun onSubmitButtonClicked(sender: View?) {
        if (!this@DetailTestPresenter.testAdapter.isCurrentPageValidated()) {
            this@DetailTestPresenter.fragment.alert.alert(R.string.error_test_validation)
            return
        }
        if(isMbtiSelected){
            isMbtiSubmited = true
            isRetestMbti = false
        } else {
            isRetestHolland = false
            isHollandSubmited = true
        }
        val viewModel = this@DetailTestPresenter.fragment.viewModel
        viewModel.doSubmitTest()
    }

    private fun scrollToTop() {
        try {
            val scrollView = this@DetailTestPresenter.fragment.dataBinding.testDetailNestedScrollView
            val recyclerView = this@DetailTestPresenter.fragment.dataBinding.testDetailRecyclerView
            scrollView.smoothScrollTo(0, recyclerView.y.toInt())
        } catch (exception: Exception) {
            Timber.e(exception)
        }
    }

    fun onReTestButtonClicked(sender: View?) {
        if (isMbtiSelected) {
            isRetestMbti = true
            isMbtiSubmited = false
            this@DetailTestPresenter.fragment.mbtiTab.select()
        } else {
            isRetestHolland = true
            isHollandSubmited = false
            this@DetailTestPresenter.fragment.hollandTab.select()
        }

        showDetail()
    }

    class TestLayoutManager(context: Context) : LinearLayoutManager(context) {

        override fun isAutoMeasureEnabled(): Boolean {
            return true
        }

    }

}
