package asia.craftbox.android.jobway.ui.activities.faculty.adapter

import android.os.Build
import android.text.Html
import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderFacultyDetailItemBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

class ListFacultyDetailAdapter : PagedListAdapter<FacultyModel, ListFacultyDetailAdapter.ViewHolder>(FacultyModel.DIFF) {

    private var onFacultyItemClickedBlock: ((item: FacultyModel, pos: Int) -> Unit)? = null

    fun setOnFacultyItemClicked(block: ((item: FacultyModel, pos: Int) -> Unit)?) {
        this@ListFacultyDetailAdapter.onFacultyItemClickedBlock = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this@ListFacultyDetailAdapter.getItem(position)?.let { unwrappedUni ->
            holder.bind(unwrappedUni, position)
        }
    }


    inner class ViewHolder(parent: ViewGroup) :
        DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<FacultyModel>(parent, R.layout.viewholder_faculty_detail_item) {

        private val dataBinding = DataBindingUtil.bind<ViewholderFacultyDetailItemBinding>(this@ViewHolder.itemView)


        override fun bind(item: FacultyModel, position: Int) {
            super.bind(item, position)

            this@ViewHolder.dataBinding?.presenter = Presenter(item, position)
            this@ViewHolder.dataBinding?.invalidateAll()
        }


        inner class Presenter(val item: FacultyModel, private val position: Int) {
            fun onContainerClicked(@Suppress("UNUSED_PARAMETER") sender: View?) {
                this@ListFacultyDetailAdapter.onFacultyItemClickedBlock?.invoke(
                    this@Presenter.item,
                    this@Presenter.position
                )
            }

            fun getItemFacName(): Spanned? {
                return this@Presenter.getHtml("<b>${this@Presenter.item.facName}</b>")
            }

            fun getName(): Spanned? {
                return this@Presenter.getHtml("<b>${this@Presenter.item.name}</b>")
            }

            fun getItemUniFacd(): Spanned? {
                return this@Presenter.getHtml("<b>${this@Presenter.item.fac_cd}</b>")
            }

            fun getItemUniName(): Spanned? {
                return this@Presenter.getHtml("<b>${this@Presenter.item.uniName}</b>")
            }

            private fun getHtml(source: String) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                @Suppress("DEPRECATION")
                Html.fromHtml(source)
            }

        }

    }

}