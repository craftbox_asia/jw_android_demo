package asia.craftbox.android.jobway.ui.activities.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.entities.models.ProfileResumeModel
import asia.craftbox.android.jobway.entities.requests.auth.UpdateProfileRequest
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.profile
 * Date: Jan 21, 2019 - 9:21 PM
 */


class ProfileViewModel : DMSViewModel() {

    private val profileLiveData by lazy {
        val profileId = DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
        return@lazy AppData.get().getProfile().getProfileById(profileId)
    }

    private val updateProfileTrans = Transformations.map(this@ProfileViewModel.profileLiveData) {
        return@map UpdateProfileRequest.createFromProfileModel(it)
    }

    private val resumeItemsMaped by lazy {
        return@lazy AppData.get().getTest().getAllResumesFlatten()
            .map { it.key to it }
            .toMap()
    }

    private val fetchProfileTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@ProfileViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@ProfileViewModel.request(DMSRest.get(AuthAPIService::class.java).viewProfile())
                    .then { _, data -> data.data?.saveOrUpdate() }
                    .setTaskNameResId(R.string.task_api_auth_profile)
                    .submit()
            })
        return@lazy instance
    }

    private val submitTask by lazy {
        val instance = PublishSubject.create<UpdateProfileRequest>()
        this@ProfileViewModel.compositeDisposable.add(
            instance.throttleFirst(
                1000,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe { request ->
                this@ProfileViewModel.request(DMSRest.get(AuthAPIService::class.java).updateProfile(request))
                    .setTaskNameResId(R.string.task_api_auth_update_profile)
                    .then { _, data ->
                        data.data?.id?.let { this@ProfileViewModel.fetchProfileTask.onNext(it) }
                    }
                    .submit()
            })
        return@lazy instance
    }


    fun getProfile(): LiveData<ProfileModel> {
        return this@ProfileViewModel.profileLiveData
    }

    fun uploadAvatar(file: File) {
        val mediaType = MediaType.parse("image/*")
        val requestFile = RequestBody.create(mediaType, file)
        if (file.isFile) {
            val part = MultipartBody.Part.createFormData( "avatar", file.name, requestFile)
            request(DMSRest.get(AuthAPIService::class.java).uploadAvatar(part))
                .setTaskNameResId(R.string.task_api_auth_update_profile)
                .then { _, data ->
                    Log.i("data", data.toString())
                    AppData.get().getProfile().updateAvatar(profileLiveData.value?.id, data.urlAvatar)
                }
                .submit()
        }
    }

    fun getUpdateRequest(): LiveData<UpdateProfileRequest> {
        return this@ProfileViewModel.updateProfileTrans
    }

    fun getResumeItem(key: String?): ProfileResumeModel? {
        return this@ProfileViewModel.resumeItemsMaped[key]
    }

    fun getQuestionText(key: String?, questionNumber: String?): String? {
        return "Câu $questionNumber: ${this@ProfileViewModel.getResumeItem(key)?.text ?: ""}"
    }

    fun doSubmit() {
        val request = this@ProfileViewModel.getUpdateRequest().value ?: return
        this@ProfileViewModel.submitTask.onNext(request)
    }

}
