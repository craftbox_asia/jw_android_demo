package asia.craftbox.android.jobway.entities.responses.test

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.contribs.responses.ListDMSRestResponse
import com.seldatdirect.dms.core.entities.DMSJsonableObject

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.responses.test
 * Date: Jan 18, 2019 - 11:12 AM
 */


class MBTITestResponse : ListDMSRestResponse<MBTITestResponse.Item>() {

    class Item : DMSJsonableObject {

        @SerializedName("id")
        var id: Long? = null

        @SerializedName("name")
        var name: String? = null

        @SerializedName("answers")
        var answers: List<Answer> = emptyList()


        class Answer : DMSJsonableObject {

            @SerializedName("question_id")
            var questionId: Long? = null

            @SerializedName("name")
            var name: String? = null

            @SerializedName("value")
            var value: String? = null

        }

    }

}
