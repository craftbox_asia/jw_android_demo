package asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history

import android.os.Bundle
import android.view.View
import android.widget.ScrollView
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorHistoryDetailBinding
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.consult.history.info_student.CounselorInfoStudentFragment
import asia.craftbox.android.jobway.utils.Utils
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import androidx.recyclerview.widget.DefaultItemAnimator
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.ThreadMode
import org.greenrobot.eventbus.Subscribe


class CounselorHistoryDetailFragment :
    AppMainFragment<FragmentCounselorHistoryDetailBinding>(R.layout.fragment_counselor_history_detail),
    DMSViewModelSource<CounselorConsultHistoryDetailViewModel> {

    private var currentPage: Int = 1
    private var totalPage: Int = 1
    var isMaster = false

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorHistoryDetailFragment)[CounselorConsultHistoryDetailViewModel::class.java]
    }

    val presenter by lazy {
        CounselorHistoryDetailPresenter(this)
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_get_list_history
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {

        if (promise.taskNameResId == R.string.task_api_get_list_history) {
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter

        super.onContentViewCreated(rootView, savedInstanceState)

        val isFromNotification = ShareData.getInstance().isClickedNotification
        if (isFromNotification) {
            loadUIWithMessageModel()
            return
        }
        val isFromHistoryFrament = ShareData.getInstance().isFromHistoryFrament(this.context!!)
        if (isFromHistoryFrament) {
            loadUIWithMessageModel()
        } else {
            loadUIWithCounselorModel()
        }

        isMaster = ShareData.getInstance().isCurrentUserIsMaster()

        if (isMaster) {
            dataBinding.continueAskCardView.setText(R.string.label_answer)
            dataBinding.continueAskCardView.icon = ContextCompat.getDrawable(activity!!, R.drawable.ic_reply_white_24dp)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        EventBus.getDefault().register(this)
    }
    
    override fun onDestroy() {
        ShareData.getInstance().isClickedNotification = false
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    override fun initUI() {
        super.initUI()

        val historyRecyclerLayout = LinearLayoutManager(this.requireContext())
        historyRecyclerLayout.isAutoMeasureEnabled = true
        dataBinding.recyclerView.layoutManager = historyRecyclerLayout
        dataBinding.recyclerView.isNestedScrollingEnabled = false
        dataBinding.recyclerView.adapter = presenter.adapter
        dataBinding.recyclerView.itemAnimator = null

        dataBinding.scrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (scrollY == (v.getChildAt(0).measuredHeight - v.measuredHeight)) {
                if (totalPage > currentPage) {
                    getListHistory(currentPage+1)
                }
            }
        }
    }

    private fun loadUIWithMessageModel() {
        val messageModel = presenter.currentMessageModel ?: return
        dataBinding.nameTextView.text = messageModel.userName
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, messageModel.avatar, 0)
        dataBinding.roundedImageView.setOnClickListener {
            val userId = presenter.currentMessageModel?.userId ?: return@setOnClickListener

            viewModel.viewProfileByIdApi(userId) {
                if (it != null && it.isStudent()) {
                    val infoStudentFragment = CounselorInfoStudentFragment()
                    val bundle = Bundle()
                    bundle.putSerializable("ProfileModel", it)
                    infoStudentFragment.arguments = bundle
                    infoStudentFragment.show(childFragmentManager, "CounselorConsultSubtractionPointFragment")
                }
            }
        }
        getListHistory(currentPage)
    }

    private fun loadUIWithCounselorModel() {
        val counselorModel =  presenter.currentCounselorModel ?: return
        dataBinding.nameTextView.text = counselorModel.getFullName()
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.roundedImageView.setOnClickListener {
            if (!counselorModel.isExpert() && !counselorModel.isCo()) {
                val infoStudentFragment = CounselorInfoStudentFragment()
                infoStudentFragment.show(childFragmentManager, "CounselorConsultSubtractionPointFragment")
            }
        }
        getListHistory(currentPage)
    }

    // region ------ API ------

    private fun getListHistory(page: Int) {
        alert.progressShow(R.string.task_api_get_list_history)
        viewModel.getListHistory(presenter.getCurrentUserId()!!, page) { res ->
            if (res == null) { return@getListHistory }
            val diffList = Utils.instance().getDiffItems(presenter.historyList, res.data)
            if (diffList.isEmpty()) {
                alert.progressDismiss()
                return@getListHistory
            }
            currentPage = res.meta.currentPage
            totalPage = res.meta.totalPages

            dataBinding.recyclerView.itemAnimator = null
            for (_item in diffList) {
                presenter.historyList.add(0, _item)
                dataBinding.scrollView.postDelayed({ dataBinding.scrollView.fullScroll(ScrollView.FOCUS_DOWN) }, 200)
            }
            presenter.adapter?.notifyItemRangeInserted(0, diffList.count())
            dataBinding.scrollView.postDelayed({ dataBinding.scrollView.fullScroll(ScrollView.FOCUS_DOWN) }, 500)
            dataBinding.recyclerView.itemAnimator = DefaultItemAnimator()
            alert.progressDismiss()
        }
    }

    // endregion --- API ---

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: String) {
        currentPage = 1
        presenter.historyList.clear()
        presenter.adapter?.notifyDataSetChanged()
    }
}
