package asia.craftbox.android.jobway.ui.fragments.consult.counselors.expert

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultExpertBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import kotlinx.android.synthetic.main.fragment_counselor_consult_expert.*


class CounselorConsultExpertFragment : DialogFragment() {

    val presenter by lazy {
        CounselorConsultExpertPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentCounselorConsultExpertBinding.inflate(inflater, container, false)
        binding.presenter = presenter
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog?.window?.setLayout(width, height)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val counselorModel = ShareData.getInstance().currentCounselor
        loadUIFromData(counselorModel)
        close_contain_linear_layout.setOnClickListener {
            presenter.closeButtonClicked(it)
        }
    }

    private fun loadUIFromData(counselorModel: CounselorModel?) {
        if (counselorModel == null) { return }
        presenter.currentCounselorModel = counselorModel
        ImageViewBindingAdapter.imageViewAsyncSrc(rounded_image_view, counselorModel.avatar, 0)
        title_text_view.text = counselorModel.getFullName()
        description_text_view.text = counselorModel.getDescriptionJob()
    }
}
