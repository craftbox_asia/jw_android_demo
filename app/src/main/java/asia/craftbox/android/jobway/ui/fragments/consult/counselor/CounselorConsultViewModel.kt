package asia.craftbox.android.jobway.ui.fragments.consult.counselor

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel


class CounselorConsultViewModel : DMSViewModel() {

    // region ------ Count Unread ------

    fun getListNotiMessage() {
        this.request(DMSRest.get(NotificationAPIService::class.java).getListNotiMessage())
            .then { promise, res ->

            }
            .setTaskNameResId(R.string.task_api_get_list_noti_message)
            .submit()
    }
}
