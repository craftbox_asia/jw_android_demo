package asia.craftbox.android.jobway.entities.models

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 14, 2019 - 3:25 PM
 */


@Entity
class UniModel : AppModel() {

    @SerializedName("name")
    var name: String? = null

    @SerializedName("uni_cd")
    var uniCode: String? = null

    @SerializedName("logo")
    var logo: String? = null

    @SerializedName("featured_img")
    var featureImg: String? = null

    @SerializedName("addr")
    var addr: String? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("email")
    var email: String? = null

    @SerializedName("website")
    var website: String? = null

    @SerializedName("uni_type_name")
    var uniTypeName: String? = null

    @SerializedName("uni_class_name")
    var uniClassName: String? = null

    @SerializedName("hot_uni")
    var hotUni: Int = 0


    /*
    Details
     */

    @SerializedName("uni_type_id")
    var uniTypeId: Long? = null

    @SerializedName("uni_class_id")
    var uniClassId: Long? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("faculties")
    var faculties: String? = null

    @SerializedName("recruitment")
    var recruitment: String? = null


    fun getItemUniCode(context: Context?): Spanned? {
        val ctx = context ?: return null
        val localText = ctx.getString(R.string.label_uni_uni_code)
        return this@UniModel.getHtml("$localText: <b>${this@UniModel.uniCode}</b>")
    }

    fun getItemUniClassName(context: Context?): Spanned? {
        val ctx = context ?: return null
        val localText = ctx.getString(R.string.label_uni_name)
        val combineUniTypeNLevel : String? = this@UniModel.uniTypeName + " - " + this@UniModel.uniClassName
        return this@UniModel.getHtml("$localText: <b>$combineUniTypeNLevel</b>")
    }

    private fun getHtml(source: String) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
    } else {
        @Suppress("DEPRECATION")
        Html.fromHtml(source)
    }

    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getUni().getUniByIdFlatten(this@UniModel.onCopyPrimaryKey())
            ?: this@UniModel
        DMSModel.mergeFields(this@UniModel, instance)

        instance.localUpdatedAt = System.currentTimeMillis()
        return AppData.get().getUni().insertOne(instance)
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<UniModel>() {
            override fun areItemsTheSame(oldItem: UniModel, newItem: UniModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: UniModel, newItem: UniModel): Boolean {
                return oldItem.localUpdatedAt == newItem.localUpdatedAt
                        && oldItem.id == newItem.id
            }

        }
    }

}
