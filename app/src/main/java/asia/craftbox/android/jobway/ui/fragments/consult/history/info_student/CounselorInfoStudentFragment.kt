package asia.craftbox.android.jobway.ui.fragments.consult.history.info_student

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.databinding.FragmentCounselorInfoStudentBinding
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import kotlinx.android.synthetic.main.fragment_counselor_info_student.*


class CounselorInfoStudentFragment : DialogFragment(), DMSViewModelSource<CounselorInfoStudentViewModel> {

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorInfoStudentFragment)[CounselorInfoStudentViewModel::class.java]
    }

    val presenter by lazy {
        CounselorInfoStudentPresenter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentCounselorInfoStudentBinding.inflate(inflater, container, false)
        binding.presenter = presenter
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog?.window?.setLayout(width, height)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val profileModel = arguments?.getSerializable("ProfileModel") ?: return
        loadUIFromData(profileModel as ProfileModel)

        val jobWayRecyclerLayout = LinearLayoutManager(this.requireContext())
        jobWayRecyclerLayout.isAutoMeasureEnabled = true
        recycler_view.layoutManager = jobWayRecyclerLayout
        recycler_view.isNestedScrollingEnabled = false

    }

    private fun loadUIFromData(profileModel: ProfileModel) {
        val messageModel = ShareData.getInstance().currentHistoryMessage ?: return
        viewModel.getResumeItemsApi() {
//            val resultProfileModel = AppData.get().getTest().getProfileResultByUserIdFlatten(messageModel.userId.toLong())

            presenter.parseDataForTitleInfo0(profileModel)
            presenter.parseDataForOtherTitleInfo(profileModel)

            recycler_view.adapter = presenter.adapter
            presenter.adapter?.notifyDataSetChanged()


        }

    }
}
