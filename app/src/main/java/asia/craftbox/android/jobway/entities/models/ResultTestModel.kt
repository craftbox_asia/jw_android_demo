package asia.craftbox.android.jobway.entities.models

import android.content.Context
import androidx.room.Entity
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSAnnotations
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Jan 21, 2019 - 11:38 AM
 */


@Entity
class ResultTestModel : AppModel() {

    @DMSAnnotations.DMSJsonableExcludeField
    var userId: Long? = null

    @DMSAnnotations.DMSJsonableExcludeField
    var type: String? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("img_url")
    var imgUrl: String? = null

    @SerializedName("short_description")
    var shortDescription: String? = null

    @SerializedName("des")
    var des: String? = null


    fun stringifyTestResult(context: Context?): String? {
        return this@ResultTestModel.code ?: "-"
    }

    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getTest().getByUserAndTypeFlatten(
            user = this@ResultTestModel.userId ?: -1,
            typeCode = this@ResultTestModel.type ?: ""
        ) ?: this@ResultTestModel

        DMSModel.mergeFields(this@ResultTestModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()

        return AppData.get().getTest().insertOne(instance)
    }

}
