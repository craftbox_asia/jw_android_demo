package asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose

import android.graphics.Bitmap
import android.view.View
import android.widget.Toast
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import org.greenrobot.eventbus.EventBus




class CounselorConsultComposePresenter(private val fragment: CounselorConsultComposeFragment) : BaseObservable() {

    var adapter: CounselorConsultComposeAdapter? = null
    var bitmapList: ArrayList<Bitmap> = ArrayList()
    var filePathList: ArrayList<String> = ArrayList()

    init {
        adapter = CounselorConsultComposeAdapter(bitmapList, fragment.context!!)
    }

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onSendQuestionButtonClicked(sender: View?) {
        val cid = fragment.cID
        if (cid == -1) {
            val mainActivity = fragment.activity as? MainActivity
            mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
            return
        }
        val title = fragment.dataBinding.titleEditText.text.toString().trim()
        val description = fragment.dataBinding.descriptionEditText.text.toString().trim()
        val context = fragment.context
        if (context == null || title.isEmpty()) {
            Toast.makeText(context!!, context.getString(R.string.msg_require_input_title), Toast.LENGTH_LONG).show()
            return
        }
        if (description.isEmpty()) {
            Toast.makeText(context, context.getString(R.string.msg_require_input_description), Toast.LENGTH_LONG).show()
            return
        }
        fragment.viewModel.sendMessageAPI(cid, title, description, filePathList) { success ->
            if (success) {
                EventBus.getDefault().post("SentMessage")
                val isMaster = ShareData.getInstance().isCurrentUserIsMaster()
                if (!isMaster) {
                    ShareData.getInstance().subtractAndUpdateCoin()
                }
                val mainActivity = fragment.activity as? MainActivity
                mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
            }
        }
    }

    fun onOpenCameraOrAlbumButtonClicked(sender: View?) {
        fragment.showPictureDialog()
    }
}
