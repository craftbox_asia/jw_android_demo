package asia.craftbox.android.jobway.ui.fragments.faculty.fulldetails

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityUniDetailBinding
import asia.craftbox.android.jobway.databinding.FragmentFullFacultyDetailBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDescriptionDetailActivity
import asia.craftbox.android.jobway.ui.activities.uni.details.DetailsUniActivity
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.bumptech.glide.Glide
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import kotlinx.android.synthetic.main.fragment_full_faculty_detail.*

class FullFacultyDetailFragment: AppMainFragment<FragmentFullFacultyDetailBinding>(R.layout.fragment_full_faculty_detail),
    DMSViewModelSource<FullFacultyDetailViewModel>{

    private var facObj: FacultyModel = FacultyModel()
    private var uniRequest = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val facId = arguments!!.getLong(FULL_FACULTY_LONG, -1)
        if (facId.compareTo(-1) != 0) {
            uniRequest = true
            this@FullFacultyDetailFragment.viewModel.setupWithFacultyId(facId)
        } else{
            facObj = arguments!!.getSerializable(FULL_FACULTY_OBJ) as FacultyModel
            this@FullFacultyDetailFragment.viewModel.setupWithFacultyId(facObj.id!!)
        }

    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        //Init Fragment
        super.onContentViewCreated(rootView, savedInstanceState)
        this@FullFacultyDetailFragment.dataBinding.viewModel = this@FullFacultyDetailFragment.viewModel
        this@FullFacultyDetailFragment.dataBinding.presenter = this@FullFacultyDetailFragment.presenter
        if (uniRequest)
            return
    }

    fun initUIControl(response: FacultyModel){
        facObj = response
        if(!facObj.getFeartureImage().isNullOrBlank())
            context?.let { Glide.with(it).load(facObj.getFeartureImage()).into(full_faculty_image_view) }
        job_name.text = facObj.name
        label_job_code.text = facObj.fac_cd
        label_uni_name.text = facObj.uniName
        label_uni_type.text = facObj.uniTypeName
        if(facObj.target != null)
            label_uni_target.text = facObj.target.toString()
        intro_general.text = facObj.name
        intro_general.setOnClickListener { view: View? ->
            var intent = Intent(this@FullFacultyDetailFragment.context, FacultyDescriptionDetailActivity::class.java)
            intent.putExtra(FacultyDescriptionDetailActivity.FACULTY_DESCRIPTION_BY_OBJECT, facObj)
            this@FullFacultyDetailFragment.context?.startActivity(intent)
        }

        text_uni_content_layout.setOnClickListener { view: View? ->
            var intent = Intent(this@FullFacultyDetailFragment.context, DetailsUniActivity::class.java)
            intent.putExtra(DetailsUniActivity.INTENT_DATA_UNI_MODEL_ID, facObj.uniId)
            this@FullFacultyDetailFragment.context?.startActivity(intent)
        }
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.displayZoomControls = false
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.builtInZoomControls = true
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.javaScriptEnabled = true
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.loadWithOverviewMode = true
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.useWideViewPort = true
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.setSupportZoom(false)
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.settings.setAppCacheEnabled(true)
        this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint.isEnabled = false

        val benchmark = facObj.benchmark ?: ""
        benchmark.attachJWHtmlIntoWebView(this@FullFacultyDetailFragment.dataBinding.webviewStandarPoint)
    }

    override val viewModel by lazy {
        ViewModelProviders.of(this@FullFacultyDetailFragment)[FullFacultyDetailViewModel::class.java]
    }

    val presenter by lazy {
        FullFacultyDetailPresenter(this@FullFacultyDetailFragment)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initUI() {
        super.initUI()
        // optimize webview contents
        if (uniRequest)
            return
        initUIControl(facObj)
        loadSummaryWeb(facObj)
    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {
        super.onAPIResponseSuccess(promise, responseData)
        var data = responseData as DataFieldDMSRestResponse<FacultyModel>
        if (data.data != null ) {
            initUIControl(data.data!!)
            loadSummaryWeb(data.data!!)
        }
    }

    fun loadSummaryWeb(data: FacultyModel ){
        if (data != null ) {
            initUIControl(data!!)

            var summary: String = ""
            if(!data?.summary.isNullOrEmpty()){
                summary = data?.summary!!
            }
            else if(!data?.des.isNullOrEmpty()){
                summary = data?.des!!
            }
            if (summary.isNullOrEmpty())
                return

            // optimize webview contents
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.displayZoomControls = false
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.builtInZoomControls = true
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.javaScriptEnabled = true
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.loadWithOverviewMode = true
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.useWideViewPort = true
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.setSupportZoom(false)
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.settings.setAppCacheEnabled(true)
            this@FullFacultyDetailFragment.dataBinding.webviewDescription.isEnabled = false
            summary.attachJWHtmlIntoWebView(this@FullFacultyDetailFragment.dataBinding.webviewDescription)
        }
    }

    companion object{
        val FULL_FACULTY_OBJ = "FULL_FACULTY_OBJ"
        val FULL_FACULTY_LONG = "FULL_FACULTY_LONG"

        fun newInstance(obj: FacultyModel): DMSFragment {
            var fragment = FullFacultyDetailFragment()
            var args = Bundle()
            args.putSerializable(FULL_FACULTY_OBJ, obj)
            fragment.arguments = args
            return  fragment
        }

        fun newInstance(obj: Long): DMSFragment {
            var fragment = FullFacultyDetailFragment()
            var args = Bundle()
            args.putLong(FULL_FACULTY_LONG, obj)
            fragment.arguments = args
            return  fragment
        }
    }
}
