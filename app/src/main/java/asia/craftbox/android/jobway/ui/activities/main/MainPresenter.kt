package asia.craftbox.android.jobway.ui.activities.main

import android.view.MenuItem
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.fragments.bookmark.ListBookmarkNewsFragment
import asia.craftbox.android.jobway.ui.fragments.consult.counselor.CounselorConsultFragment
import asia.craftbox.android.jobway.ui.fragments.earn_point.EarnPointFragment
import asia.craftbox.android.jobway.ui.fragments.explore.home.HomeExploreFragment
import asia.craftbox.android.jobway.ui.fragments.faculty.list.ListFacultyFragment
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsFragment
import asia.craftbox.android.jobway.ui.fragments.pages.home.HomePageFragment
import asia.craftbox.android.jobway.ui.fragments.uni.list.ListUniFragment
import com.google.android.material.navigation.NavigationView
import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * December 2018
 */


class MainPresenter(private val activity: MainActivity) : NavigationView.OnNavigationItemSelectedListener {

    private val toolbarIsCollapsed = MutableLiveData<Boolean>()


    fun getToolbarIsCollapsed(): LiveData<Boolean> {
        return this@MainPresenter.toolbarIsCollapsed
    }

    fun getIsDrawerOpened(): Boolean {
        return this@MainPresenter.activity.dataBinding
            .mainDrawerLayout
            .isDrawerOpen(this@MainPresenter.activity.dataBinding.mainNavigationView)
    }

    fun setDrawerOpen(isOpen: Boolean) {
        val drawerLayout = this@MainPresenter.activity.dataBinding.mainDrawerLayout
        val navigationView = this@MainPresenter.activity.dataBinding.mainNavigationView

        when {
            isOpen && !drawerLayout.isDrawerOpen(navigationView) -> {
                drawerLayout.openDrawer(navigationView)
            }
            !isOpen && drawerLayout.isDrawerOpen(navigationView) -> {
                drawerLayout.closeDrawer(navigationView)
            }
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        this@MainPresenter.setDrawerOpen(false)
        return when (menuItem.itemId) {
            R.id.app_nav_item_homepage -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(HomePageFragment::class.java),
                    replaceRootFragment = true
                )
                false
            }
            R.id.app_nav_item_day_news -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(ListNewsFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_news_saved -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(ListBookmarkNewsFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_consulting -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(CounselorConsultFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_explore_self -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(HomeExploreFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_college_info -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(ListUniFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_job_info -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    fragment = DMSFragment.create(ListFacultyFragment::class.java),
                    replaceRootFragment = false
                )
                false
            }
            R.id.app_nav_item_earn_points -> {
                this@MainPresenter.activity.getFragmentHelper().pushFragment(
                    DMSFragment.create(EarnPointFragment::class.java)
                )
                false
            }
            else -> true
        }
    }

    internal fun setTopBarIsCollapsed(isCollapsed: Boolean) {
        val currentState = this@MainPresenter.getToolbarIsCollapsed().value ?: false

        if (isCollapsed != currentState) {
            this@MainPresenter.toolbarIsCollapsed.postValue(isCollapsed)
        }
    }

}
