package asia.craftbox.android.jobway.ui.fragments.news.list.adapters

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ItemPagerNewsHotBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.ui.activities.news.details.DetailsNewsActivity
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsFragment
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Jan 16, 2019 - 4:57 PM
 */


class ListHotNewsAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val items = ArrayList<FragmentItem>()
    private var onItemSelectedBlock: ((newsItem: NewsModel) -> Unit)? = null


    fun setOnItemSelected(block: ((newsItem: NewsModel) -> Unit)?) {
        this@ListHotNewsAdapter.onItemSelectedBlock = block
    }


    fun updateItems(items: List<FragmentItem>) {
        this@ListHotNewsAdapter.items.clear()
        this@ListHotNewsAdapter.items.addAll(items)
        this@ListHotNewsAdapter.notifyDataSetChanged()
    }

    override fun getItem(position: Int): Fragment {
        return this@ListHotNewsAdapter.items[position]
    }

    override fun getCount(): Int {
        return this@ListHotNewsAdapter.items.count()
    }


    class FragmentItem : Fragment() {

        private var dataBinding: ItemPagerNewsHotBinding? = null
        private val presenter = FragmentItemPresenter()


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            this@FragmentItem.dataBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.item_pager_news_hot,
                container,
                false
            )
            return this@FragmentItem.dataBinding!!.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            this@FragmentItem.dataBinding?.presenter = this@FragmentItem.presenter
            super.onViewCreated(view, savedInstanceState)

            try {
                val bundle = this@FragmentItem.arguments ?: return
                val newsItem = bundle.getSerializable(NEWS_MODEL) as? NewsModel ?: return

                val onItemClicked = View.OnClickListener {
                    val newsActivity = Intent(view.context, DetailsNewsActivity::class.java)
                    newsActivity.putExtra(DetailsNewsActivity.INTENT_DATA_NEWS_ID, newsItem.id)
                    view.context.startActivity(newsActivity)
                }

                view.findViewById<View?>(R.id.pager_news_latest_card_view)
                    ?.setOnClickListener(onItemClicked)

                this@FragmentItem.presenter.title.set(newsItem.title)
                this@FragmentItem.presenter.subTitle.set(newsItem.getNatureTime(view.context))
                this@FragmentItem.presenter.countView.set(view.context?.getString(R.string.common_label_post_views) + ": " + newsItem.views?.toString())
                this@FragmentItem.presenter.imageSource.set(newsItem.featureImg ?: "")
            } catch (exception: Exception) {
                Timber.e(exception)
            }
        }


        class FragmentItemPresenter {

            val title = ObservableField<String>()
            val subTitle = ObservableField<String>()
            val countView = ObservableField<String>()
            val imageSource = ObservableField<String>()

        }


        companion object {
            internal const val NEWS_MODEL = "NEWS_MODEL"

            fun create(newsItem: NewsModel): FragmentItem {
                val instance = FragmentItem()
                instance.arguments = Bundle()
                instance.arguments?.putSerializable(NEWS_MODEL, newsItem)
                return instance
            }
        }

    }

}
