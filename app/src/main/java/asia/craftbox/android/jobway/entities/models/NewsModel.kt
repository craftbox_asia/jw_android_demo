package asia.craftbox.android.jobway.entities.models

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.TypeConverter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.exts.timeAgo
import asia.craftbox.android.jobway.modules.exts.toDate
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 13, 2019 - 3:11 PM
 */


@Entity
class NewsModel : AppModel() {

    @SerializedName("sts")
    var sts: String? = null

    @SerializedName("is_deleted")
    var deleted: Int = 0

    @SerializedName("category")
    var category: String? = null

    @SerializedName("ctgry_id")
    var ctgryId: Long? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("meta_title")
    var metaTitle: String? = null

    @SerializedName("slug")
    var slug: String? = null

    @SerializedName("des")
    var des: String? = null

    @SerializedName("content")
    var content: String? = null

    @SerializedName("tags")
    var tags: NewsTags = NewsTags()

    @SerializedName("att_image_alt")
    var attImageAlt: String? = null

    @SerializedName("is_host_news")
    var hotNews: Int = 0

    @SerializedName("views")
    var views: Int = 0

    @SerializedName("type")
    var type: String? = null

    @SerializedName("is_fav")
    var isFav: Boolean? = null

    @SerializedName("is_fav_news")
    var favNews: Int = 0

    @SerializedName("related_post")
    @Ignore
    var relatedPost: List<NewsModel> = emptyList()

    @SerializedName("featured_img")
    var featureImg: String? = null


    fun getNatureTime(context: Context?): String? {
        val ctx = context ?: return null
        val publishTimeString = this@NewsModel.createdAt ?: return null
        val publishDate = publishTimeString.toDate("yyyy-MM-dd kk:mm:ss") ?: return null
        return publishDate.timeAgo(ctx)
    }

    fun getViews(context: Context?): Spanned? {
        val ctx = context ?: return null
        val views = this@NewsModel.views
        val viewText = ctx.getString(R.string.common_label_post_views)
        val htmlText =
            "<span style='color: #8E8E8E;'>$viewText: </span><strong style='color: #5A5A5A;'>$views</strong>"
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(htmlText, Html.FROM_HTML_MODE_LEGACY)
        } else {
            @Suppress("DEPRECATION")
            Html.fromHtml(htmlText)
        }
    }



    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getNews().getNewsByIdFlatten(this@NewsModel.onCopyPrimaryKey())
            ?: this@NewsModel

        DMSModel.mergeFields(this@NewsModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()

        return AppData.get().getNews().insertOne(instance)
    }


    companion object {
        val DIFF = object : DiffUtil.ItemCallback<NewsModel>() {
            override fun areItemsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: NewsModel, newItem: NewsModel): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.localUpdatedAt == newItem.localUpdatedAt
            }

        }
    }


    class NewsTags : ArrayList<String>() {

        class Converter {

            @TypeConverter
            fun tagsToObject(tags: String?): NewsTags {
                val tagsList = NewsTags()

                if (tags.isNullOrBlank()) return tagsList

                tagsList.addAll(
                    tags.split(",").map { it.trim().capitalize() }
                )

                return tagsList
            }

            @TypeConverter
            fun objectToTags(`object`: NewsTags?): String {
                return `object`?.joinToString(",") ?: ""
            }

        }

    }

}
