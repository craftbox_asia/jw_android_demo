package asia.craftbox.android.jobway.EventBus

import asia.craftbox.android.jobway.entities.models.ProfileModel

open class ProfileEvent(profile: ProfileModel) {
    var profile: ProfileModel = profile
}