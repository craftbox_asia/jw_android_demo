package asia.craftbox.android.jobway.ui.fragments.pages.tutorial

import android.graphics.drawable.Drawable
import android.view.View
import androidx.core.content.ContextCompat
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.ui.activities.tutorial.TutorialActivity
import timber.log.Timber

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.tutorial
 * Date: Jan 07, 2019 - 11:12 AM
 */


class TutorialPresenter(private val fragment: TutorialFragment) {

    private var titleField = R.string.common_empty
    private var contentField = R.string.common_empty
    private var coverField = R.drawable.ic_launcher_foreground


    fun setTitle(titleRes: Int) {
        this@TutorialPresenter.titleField = titleRes
    }

    fun setContent(contentRes: Int) {
        this@TutorialPresenter.contentField = contentRes
    }

    fun setCover(coverRes: Int) {
        this@TutorialPresenter.coverField = coverRes
    }

    fun getTitle(): String? {
        return try {
            this@TutorialPresenter.fragment.getString(this@TutorialPresenter.titleField)
        } catch (resourceNotFound: Exception) {
            Timber.e(resourceNotFound)
            null
        }
    }

    fun getContent(): String? {
        return try {
            this@TutorialPresenter.fragment.getString(this@TutorialPresenter.contentField)
        } catch (resourceNotFound: Exception) {
            Timber.e(resourceNotFound)
            null
        }
    }

    fun getCover(): Drawable? {
        return try {
            ContextCompat.getDrawable(
                this@TutorialPresenter.fragment.requireContext(),
                this@TutorialPresenter.coverField
            )
        } catch (resourceNotFound: Exception) {
            Timber.e(resourceNotFound)
            null
        }
    }

    fun onSlideButtonClicked(sender: View?) {
        if (!this@TutorialPresenter.fragment.isAdded) {
            return
        }

        val tutorialActivity = this@TutorialPresenter.fragment.activity as? TutorialActivity ?: return
        val currentItem = tutorialActivity.dataBinding.tutorialViewPager.currentItem
        val totalItems = tutorialActivity.dataBinding.tutorialViewPager.adapter?.count ?: 0

        if (currentItem + 1 <= (totalItems - 1)) {
            tutorialActivity.dataBinding.tutorialViewPager.setCurrentItem(currentItem + 1, true)
        }
    }

}
