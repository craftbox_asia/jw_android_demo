package asia.craftbox.android.jobway.ui.widgets

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import kotlin.math.roundToInt

/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.widgets
 * Date: Dec 30, 2018 - 3:00 PM
 */


class LoginLogoImageView : AppCompatImageView {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val originalWidth = 553.0f
        val originalHeight = 248.0f
        val imageRatio = (originalWidth / originalHeight)
        val currentWidth = MeasureSpec.getSize(widthMeasureSpec).toFloat()
        val imageHeight = (currentWidth / imageRatio).roundToInt()
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(imageHeight, MeasureSpec.EXACTLY))
    }

}
