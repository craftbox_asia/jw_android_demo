package asia.craftbox.android.jobway.ui.activities.main

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.JobWayApp
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityMainBinding
import asia.craftbox.android.jobway.entities.services.AuthAPIService
import asia.craftbox.android.jobway.services.PushNotificationService
import asia.craftbox.android.jobway.ui.activities.login.LoginActivity
import asia.craftbox.android.jobway.ui.activities.splash.SplashActivity
import asia.craftbox.android.jobway.ui.fragments.pages.home.HomePageFragment
import asia.craftbox.android.jobway.utils.FileUtil
import asia.craftbox.android.jobway.utils.SharedPreferencesUtils
import com.google.firebase.iid.FirebaseInstanceId
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper
import com.seldatdirect.dms.core.utils.permission.DMSPermission
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import kotlinx.android.synthetic.main.layout_main_nav_header.*
import java.io.File

/**
 * Created by @dphans (https://github.com/dphans)
 * December 2018
 */


class MainActivity : DMSBindingActivity<ActivityMainBinding>(R.layout.activity_main),
    DMSViewModelSource<MainViewModel> {

    override val viewModel by lazy { ViewModelProviders.of(this@MainActivity)[MainViewModel::class.java] }

    internal val presenter by lazy { MainPresenter(this@MainActivity) }

    private val fragmentUtil by lazy {
        DMSFragmentHelper(
            this@MainActivity.supportFragmentManager,
            R.id.main_container
        )
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        // back to login screen when missing access token
        val authAccessToken =
            DMSPreference.default().get(
            JobWayConstant.Preference.AuthAccessToken.name,
            String::class.java
        )
        if (authAccessToken.isNullOrBlank()) {
            this@MainActivity.startActivity(Intent(this@MainActivity, LoginActivity::class.java))
            this@MainActivity.finish()
            return
        }
        DMSRest.config.updateAuthorizationToken(authAccessToken)

        this@MainActivity.dataBinding.viewModel = this@MainActivity.viewModel
        this@MainActivity.dataBinding.presenter = this@MainActivity.presenter

        if (savedInstanceState == null) {
            this@MainActivity.fragmentUtil.initialRootFragment(
                DMSFragment.create(HomePageFragment::class.java)
            )
        }

        super.onContentViewCreated(rootView, savedInstanceState)

        this@MainActivity.submitFCMIfNeeded()
        this@MainActivity.viewModel.doFetchUserProfile()

        this@MainActivity.checkPermissionIfNeeded()



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }

        var file: File? = null
        if(requestCode == JobWayConstant.PICK_GALARY)
        {
            val currImageURI = data?.getData()
            home_avatar.setImageURI(currImageURI)
            file = FileUtil.from(this, currImageURI)
        }
        else
        {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            home_avatar!!.setImageBitmap(thumbnail)
            file = FileUtil.fromBitmap(thumbnail)
        }
        viewModel.uploadAvatar(file)
    }

    override fun onBackPressed() {
        if (this@MainActivity.presenter.getIsDrawerOpened()) {
            this@MainActivity.presenter.setDrawerOpen(false)
            return
        }

        if (!this@MainActivity.fragmentUtil.isCurrentFragmentBackable()) {
            return
        }

        if (this@MainActivity.fragmentUtil.popFragmentIfNeeded()) {
            return
        }

        super.onBackPressed()
    }

    override fun onTitleChanged(title: CharSequence?, color: Int) {
        super.onTitleChanged(title, color)
        this@MainActivity.dataBinding.toolbarTitle = title?.toString()
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_auth_logout
        )
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (grantResults.none { PackageManager.PERMISSION_GRANTED == it }) {
            this@MainActivity.alert.alert(R.string.error_local_permissions_missing) {
                this@MainActivity.finish()
            }
        }
    }

    override fun initUI() {
        super.initUI()

        this@MainActivity.dataBinding.mainNavigationView.setOnRequestLogout(View.OnClickListener {
            this@MainActivity.viewModel.request(DMSRest.get(AuthAPIService::class.java).logout())
                .doAfterResponse {
                    JobWayApp.cleanupUserData()
                    SharedPreferencesUtils.clearAll(this@MainActivity)
                    this@MainActivity.startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                    this@MainActivity.finish()
                }
                .setTaskNameResId(R.string.task_api_auth_logout)
                .submit()
        })

        /*
        this@MainActivity.dataBinding.mainAppbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, offset ->
            this@MainActivity.presenter.setTopBarIsCollapsed(
                Math.abs(offset) - (appBarLayout.totalScrollRange) == 0
            )
        })
        */

        this@MainActivity.dataBinding.mainNavigationView
            .setNavigationItemSelectedListener(this@MainActivity.presenter)
    }

    override fun initObservables() {
        super.initObservables()

        this@MainActivity.viewModel.getUserProfile().observe(this@MainActivity, Observer {
            this@MainActivity.dataBinding.mainNavigationView.setProfile(it)
        } )
    }

    fun getFragmentHelper(): DMSFragmentHelper {
        return this@MainActivity.fragmentUtil
    }

    private fun submitFCMIfNeeded() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
            DMSPreference.default().set(
                JobWayConstant.Preference.PrefPushNotificationToken.name,
                it.token
            )
            PushNotificationService.submitToBackend(it.token)
        }
    }

    private fun checkPermissionIfNeeded() {
        val permissions = this@MainActivity.permission.getMissingPermissions()
            .filter { DMSPermission.DAMAGE_PERMISSIONS.contains(it) }

        if (permissions.isEmpty()) {
            return
        }

        this@MainActivity.permission.requestPermission(
            this@MainActivity,
            permissions
        )
    }
}
