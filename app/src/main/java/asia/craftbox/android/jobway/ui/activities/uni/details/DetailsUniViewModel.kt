package asia.craftbox.android.jobway.ui.activities.uni.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.UniFacultyModel
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.entities.services.UniAPIService
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.ui.activities.uni.details.sources.UniJobDatasource
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.uni.details
 * Date: Feb 14, 2019 - 8:36 PM
 */


class DetailsUniViewModel : DMSViewModel() {

    private val uniIdMutable = MutableLiveData<Long>()

    private val uniItemTransform = Transformations.switchMap(this@DetailsUniViewModel.uniIdMutable) {
        this@DetailsUniViewModel.doFetchUni()
        return@switchMap AppData.get().getUni().getUniById(it)
    }

    private val fetchUniTask by lazy {
        val instance = PublishSubject.create<Long>()
        this@DetailsUniViewModel.compositeDisposable.add(
            instance.debounce(
                300,
                TimeUnit.MILLISECONDS,
                AndroidSchedulers.mainThread()
            ).subscribe {
                this@DetailsUniViewModel.request(DMSRest.get(UniAPIService::class.java).detail(it))
                    .then { _, responseData ->
                        responseData.data?.saveOrUpdate()
                    }
                    .setTaskNameResId(R.string.task_api_uni_detail_retrieve)
                    .submit()
            })
        return@lazy instance
    }

    private val fetchJobListExecutor = Executors.newFixedThreadPool(5)
    private val fetchJobListPageSize = 10
    private var fetchJobDataFactory: ListJobDataFactory? = null
    private val fetchJobPagedList = Transformations.switchMap(this@DetailsUniViewModel.uniIdMutable) { uniId ->
        this@DetailsUniViewModel.fetchJobDataFactory = ListJobDataFactory(uniId)
        Timber.wtf(this@DetailsUniViewModel.fetchJobDataFactory.toString())

        val config = PagedList.Config.Builder()
            .setPageSize(this@DetailsUniViewModel.fetchJobListPageSize)
            .setEnablePlaceholders(false)
            .build()

        return@switchMap LivePagedListBuilder(this@DetailsUniViewModel.fetchJobDataFactory!!, config)
            .setFetchExecutor(this@DetailsUniViewModel.fetchJobListExecutor)
            .setInitialLoadKey(1)
            .build()
    }


    fun setupUniWithId(uniId: Long) {
        this@DetailsUniViewModel.uniIdMutable.postValue(uniId)
    }

    fun getUniId(): LiveData<Long> {
        return this@DetailsUniViewModel.uniIdMutable
    }

    fun getUni(): LiveData<UniModel?> {
        return this@DetailsUniViewModel.uniItemTransform
    }

    fun doFetchUni() {
        this@DetailsUniViewModel.getUniId().value
            ?.let(this@DetailsUniViewModel.fetchUniTask::onNext)
    }

    fun getJobsList(): LiveData<PagedList<UniFacultyModel>> {
        return this@DetailsUniViewModel.fetchJobPagedList
    }

    fun getJobsDatasource(): LiveData<UniJobDatasource>? {
        return this@DetailsUniViewModel.fetchJobDataFactory?.getDatasource()
    }


    inner class ListJobDataFactory(private val uniId: Long) : DataSource.Factory<Int, UniFacultyModel>() {

        private val datasourceMutable = MutableLiveData<UniJobDatasource>()

        override fun create(): DataSource<Int, UniFacultyModel> {
            val datasource = UniJobDatasource(this@ListJobDataFactory.uniId)
            this@ListJobDataFactory.datasourceMutable.postValue(datasource)
            return datasource
        }

        fun getDatasource(): LiveData<UniJobDatasource> {
            return this@ListJobDataFactory.datasourceMutable
        }

        override fun toString(): String {
            return "DataFactory with uniId = ${this@ListJobDataFactory.uniId}"
        }

    }

}
