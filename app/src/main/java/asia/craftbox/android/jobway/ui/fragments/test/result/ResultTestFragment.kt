package asia.craftbox.android.jobway.ui.fragments.test.result

import android.content.Intent
import android.os.Bundle
import android.view.View
import asia.craftbox.android.jobway.EventBus.MBTITestEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentTestResultBinding
import asia.craftbox.android.jobway.entities.requests.test.CreateTestRequest
import asia.craftbox.android.jobway.modules.exts.attachJWHtmlIntoWebView
import asia.craftbox.android.jobway.ui.activities.test.TestActivity
import asia.craftbox.android.jobway.ui.activities.test.TestViewModel
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.google.android.material.tabs.TabLayout
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import kotlinx.android.synthetic.main.fragment_test_result.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.test.result
 * Date: Jan 18, 2019 - 3:51 PM
 */


class ResultTestFragment :
    AppMainFragment<FragmentTestResultBinding>(R.layout.fragment_test_result),
    DMSViewModelSource<TestViewModel>, TabLayout.OnTabSelectedListener {
    private var testType: String? = null

    private val presenter by lazy {
        ResultTestPresenter(this@ResultTestFragment)
    }

    override val viewModel: TestViewModel by lazy {
        (this@ResultTestFragment.activity as TestActivity).viewModel
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@ResultTestFragment.presenter.notifyChange()
        this@ResultTestFragment.dataBinding.viewModel = this@ResultTestFragment.viewModel
        this@ResultTestFragment.dataBinding.presenter = this@ResultTestFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun onAPIDidFinishedAll() {
        super.onAPIDidFinishedAll()
        if (!testType.isNullOrBlank()) {
            if (testType == CreateTestRequest.TestType.MBTI.typeCode) {
                viewModel.setTestType(CreateTestRequest.TestType.MBTI)
            } else if (testType == CreateTestRequest.TestType.Holland.typeCode) {
                viewModel.setTestType(CreateTestRequest.TestType.Holland)
            }
        }
    }
    override fun initUI() {
        super.initUI()

        val testTypeTab = dataBinding.testTabLayout
        val mbtiTab = testTypeTab.newTab()
            .setTag(CreateTestRequest.TestType.MBTI.typeCode)
            .setText(R.string.tab_item_mbti)
        val hollandTab = testTypeTab.newTab()
            .setText(R.string.tab_item_holland)
            .setTag(CreateTestRequest.TestType.Holland.typeCode)
        testTypeTab.addTab(mbtiTab)
        testTypeTab.addTab(hollandTab)
        testTypeTab.addOnTabSelectedListener(this)

        val bundle = this.arguments
        if (bundle == null || bundle.isEmpty) {
            mbtiTab.select()
            return
        }
        testType = bundle.getString("TestType")
        if (!testType.isNullOrBlank()) {
            dataBinding.headerToolbar.visibility = View.GONE
            dataBinding.headerAppCompatImageView.visibility = View.GONE
            dataBinding.containTabLayoutCardView.visibility = View.GONE
            //dataBinding.reStatusButton.visibility = View.GONE
        }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: MBTITestEvent) {
        event.response.des?.attachJWHtmlIntoWebView(this.dataBinding.descriptionWebView, "#ffffff")
        test_result_share_btn.setOnClickListener { v ->
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                var shareMessage = getString(R.string.test_result_mbti).format(event.response!!.code) + event.response!!.des  + getString(R.string.share_link_app_job_way) + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "Jobway invitation"))
            } catch (e: Exception) {

            }
        }
    }

    override fun isBackable(): Boolean {
        if (!this@ResultTestFragment.isAdded) return true
        this@ResultTestFragment.activity?.finish()
        return false
    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        val selectedTab = p0 ?: return

        when (selectedTab.tag) {
            CreateTestRequest.TestType.MBTI.typeCode -> {
                viewModel.setTestType(CreateTestRequest.TestType.MBTI)
            }
            CreateTestRequest.TestType.Holland.typeCode -> {
                viewModel.setTestType(CreateTestRequest.TestType.Holland)
            }
        }
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {}

    override fun onTabUnselected(p0: TabLayout.Tab?) {}

}
