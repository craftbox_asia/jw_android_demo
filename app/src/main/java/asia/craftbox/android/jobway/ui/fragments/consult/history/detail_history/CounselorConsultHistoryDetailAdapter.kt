package asia.craftbox.android.jobway.ui.fragments.consult.history.detail_history

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.modules.exts.toBold
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.utils.Utils
import com.makeramen.roundedimageview.RoundedImageView
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter

class CounselorConsultHistoryDetailAdapter(private val items: ArrayList<MessageModel>, val context: Context) :
    RecyclerView.Adapter<CounselorConsultHistoryDetailAdapter.ViewHolder>() {

    private var onContainClickedCallback: ((link: String) -> Unit)? = null
    private var currentUserId: Long? = 0

    init {
        currentUserId = ShareData.getInstance().getCurrentProfileModel()?.id
    }

    fun setCallback(callback: ((link: String) -> Unit)?) {
        onContainClickedCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (viewType == 1) {
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_consult_history_right_detail, parent, false))
        }
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_consult_history_left_detail, parent, false))
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        if (item.userId.toLong() == currentUserId) {
            return 2
        }
        return 1
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.setContext(context)
        holder.setFiles(item.files)
        holder.setupAdapter()
        holder.loadAvatar(item.avatar)
        holder.username_text_view.text = item.userName?.toBold()
        holder.title_text_view.text = item.title?.toBold()
        holder.time_text_view.text = item.getNatureTime(context)
        holder.description_text_view.text = item.body
        holder.getAdapter()?.setCallback { link, _ ->
            onContainClickedCallback?.invoke(link)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val rounded_image_view: RoundedImageView = view.findViewById(R.id.rounded_image_view)
        val username_text_view: TextView = view.findViewById(R.id.username_text_view)
        val time_text_view: TextView = view.findViewById(R.id.time_text_view)
        val title_text_view: TextView = view.findViewById(R.id.title_text_view)
        val description_text_view: TextView = view.findViewById(R.id.description_text_view)

        val recyclerView: RecyclerView = view.findViewById(R.id.recycler_view)
        private var adapter: CounselorConsultHistoryDetailItemAdapter? = null

        private var context: Context? = null
        private var files: List<String> = ArrayList<String>()

        fun loadAvatar(avatar: String?) {
            ImageViewBindingAdapter.imageViewAsyncSrc(rounded_image_view, avatar, 0)
        }

        fun setContext(_context: Context) {
            context = _context
        }

        fun setFiles(_file: List<String>?) {
            if (_file.isNullOrEmpty()) {
                recyclerView.visibility = View.INVISIBLE
                files = ArrayList<String>()
            } else {
                recyclerView.visibility = View.VISIBLE
                recyclerView.layoutParams.height = Utils.instance().convertDpToPixel(100f, context!!).toInt()
                files = _file
            }
        }

        fun getAdapter(): CounselorConsultHistoryDetailItemAdapter? {
            return adapter
        }

        fun setupAdapter() {
            val recyclerLayout = LinearLayoutManager(context!!, RecyclerView.HORIZONTAL, false)
            recyclerLayout.isAutoMeasureEnabled = true
            recyclerView.layoutManager = recyclerLayout
            recyclerView.isNestedScrollingEnabled = false
            adapter = CounselorConsultHistoryDetailItemAdapter(files, context!!)
            recyclerView.adapter = adapter
        }
    }
}