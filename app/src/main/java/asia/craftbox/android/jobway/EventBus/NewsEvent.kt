package asia.craftbox.android.jobway.EventBus

import asia.craftbox.android.jobway.entities.models.NewsModel

open class NewsEvent(news: NewsModel) {
    var news: NewsModel = news
}