package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.NotificationModel
import asia.craftbox.android.jobway.entities.responses.news.NotificationNewsResponse
import asia.craftbox.android.jobway.entities.responses.notification.NotificationMessagesResponse
import com.google.gson.JsonObject
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*


interface NotificationAPIService {

    @GET("notification/v1/get-count-unread-message")
    @Headers("Content-Type: application/json")
    fun getCountUnreadMessage(): Observable<DataFieldDMSRestResponse<Int>>

    @GET("notification/v1/get-count-unread-news")
    @Headers("Content-Type: application/json")
    fun getCountUnreadNews(): Observable<DataFieldDMSRestResponse<Int>>

    @GET("notification/v1/get-list-message")
    @Headers("Content-Type: application/json")
    fun getListMessage(): Observable<DataFieldDMSRestResponse<List<MessageModel>>>

    @GET("notification/v1/get-list-noti-message")
    @Headers("Content-Type: application/json")
    fun getListNotiMessage(): Observable<DataFieldDMSRestResponse<List<MessageModel>>>

    @GET("notification/v1/get-list-noti-news")
    @Headers("Content-Type: application/json")
    fun getListNotiNews(@Query("page") page: Int = 1, @Query("limit") limit: Int = 10): Observable<DataFieldDMSRestResponse<NotificationNewsResponse<NotificationModel>>>

    @GET("notification/v1/get-list-history/{user_id}")
    @Headers("Content-Type: application/json")
    fun getListHistory(@Path("user_id") userId: Int, @Query("page") page: Int = 1, @Query("limit") limit: Int = 50): Observable<DataFieldDMSRestResponse<NotificationMessagesResponse<MessageModel>>>

    @POST("notification/v1/send-message")
    @Multipart
    fun sendMessage(@Part("c_id") c_id: RequestBody,
                    @Part("title") title: RequestBody,
                    @Part("body") body: RequestBody,
                    @Part files: ArrayList<MultipartBody.Part>): Observable<DataFieldDMSRestResponse<JsonObject>>

    @PUT("notification/v1/update-read-noti")
    fun updateReadNoti(@Body body: HashMap<String, String>): Observable<DataFieldDMSRestResponse<Any>>
}
