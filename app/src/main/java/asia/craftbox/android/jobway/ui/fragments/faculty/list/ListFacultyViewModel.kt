package asia.craftbox.android.jobway.ui.fragments.faculty.list

import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import asia.craftbox.android.jobway.EventBus.FacultyResponseEvent
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.services.JobAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel
import org.greenrobot.eventbus.EventBus


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.uni.list
 * Date: Feb 14, 2019 - 3:53 PM
 */


class ListFacultyViewModel : DMSViewModel() {

    val itemPerPage = 5
    private var currentPage: Int = 0
    private var totalPage: Int = Int.MAX_VALUE
    var loadingData: Boolean = false

    fun fetchFacultyTask(progressBar: ProgressBar ) {
        progressBar.visibility = VISIBLE
        loadingData = true
        if (this@ListFacultyViewModel.currentPage < (this@ListFacultyViewModel.totalPage)) {
            this@ListFacultyViewModel.request(DMSRest.get(JobAPIService::class.java).getJobsListPaginate(page = this@ListFacultyViewModel.currentPage + 1))
                .then { _, responseData ->
                    this@ListFacultyViewModel.currentPage = responseData.data?.meta?.currentPage ?: 1
                    this@ListFacultyViewModel.totalPage = responseData.data?.meta?.totalPages ?: 1
                    EventBus.getDefault().post(FacultyResponseEvent(responseData.data?.data!!))
                    loadingData = false
                    progressBar.visibility = GONE
                }
                .setTaskNameResId(R.string.task_api_faculty_list_retrieve)
                .submit()
        }
    }
    fun doFetchFacultyList(progressBar: ProgressBar) {
        this@ListFacultyViewModel.currentPage = 0
        this@ListFacultyViewModel.totalPage = Int.MAX_VALUE
        fetchFacultyTask(progressBar)
    }
}
