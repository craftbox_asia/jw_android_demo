package asia.craftbox.android.jobway.ui.fragments.faculty.search

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import asia.craftbox.android.jobway.R
import com.seldatdirect.dms.core.utils.fragment.DMSFragmentHelper

class SearchFacultyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_faculty)
        this@SearchFacultyActivity.fragmentUtil.initialRootFragment(
            fragment = SearchFacultyFragment()
        )
    }

    private val fragmentUtil by lazy {
        DMSFragmentHelper(
            this@SearchFacultyActivity.supportFragmentManager,
            R.id.faculty_search_container
        )
    }
}