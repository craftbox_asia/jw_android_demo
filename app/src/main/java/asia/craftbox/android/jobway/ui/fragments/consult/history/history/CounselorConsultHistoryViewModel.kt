package asia.craftbox.android.jobway.ui.fragments.consult.history.history

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel


class CounselorConsultHistoryViewModel : DMSViewModel() {

    fun getListMessage(callback: ((List<MessageModel>?) -> Unit)) {
        this.request(DMSRest.get(NotificationAPIService::class.java).getListMessage())
            .then { _, responseData ->
                callback.invoke(responseData.data ?: ArrayList())
            }
            .setTaskNameResId(R.string.task_api_get_list_message)
            .submit()
    }

    fun refreshListHistory(callback: ((List<MessageModel>?) -> Unit)) {
        getListMessage(callback)
    }

    fun updateReadNoti(id: String, type: String, callback: ((Boolean) -> Unit)) {
        val params: HashMap<String, String> = HashMap()
        params["id"] = id
        params["type"] = type
        this.request(DMSRest.get(NotificationAPIService::class.java).updateReadNoti(params))
            .then { _, responseData ->
                val data = responseData.data?.toString() ?: ""
                callback(data.isNotEmpty())
            }.setTaskNameResId(R.string.task_api_update_read_noti)
            .submit()
    }
}
