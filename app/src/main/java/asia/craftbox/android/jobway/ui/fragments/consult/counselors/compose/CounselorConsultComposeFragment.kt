package asia.craftbox.android.jobway.ui.fragments.consult.counselors.compose

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultComposeBinding
import asia.craftbox.android.jobway.modules.exts.resize
import asia.craftbox.android.jobway.modules.exts.save
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.extensions.hideKeyboard
import com.seldatdirect.dms.core.utils.databinding.ImageViewBindingAdapter
import java.io.IOException


class CounselorConsultComposeFragment :
    AppMainFragment<FragmentCounselorConsultComposeBinding>(R.layout.fragment_counselor_consult_compose),
    DMSViewModelSource<CounselorConsultComposeViewModel> {

    private val GALLERY = 100
    private val CAMERA = 101

    var cID: Int = -1

    override val viewModel by lazy {
        ViewModelProviders.of(this@CounselorConsultComposeFragment)[CounselorConsultComposeViewModel::class.java]
    }

    val presenter by lazy {
        CounselorConsultComposePresenter(this)
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_send_message
        )
    }

    override fun onAPIWillRequest(promise: DMSRestPromise<*, *>) {
        if (promise.taskNameResId == R.string.task_api_send_message) {
            alert.progressShow(R.string.task_api_send_message)
            return
        }
        super.onAPIWillRequest(promise)
    }

    override fun onAPIDidFinished(promise: DMSRestPromise<*, *>) {
        super.onAPIDidFinished(promise)
        alert.progressDismiss()
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        dataBinding.presenter = presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun initUI() {
        super.initUI()

        val isFromHistoryFrament = ShareData.getInstance().isFromHistoryFrament(this.context!!)
        if (isFromHistoryFrament) {
            loadUIWithMessageModel()
        } else {
            loadUIWithCounselorModel()
        }
    }

    override fun onDestroy() {
        dataBinding.titleEditText.hideKeyboard()
        dataBinding.descriptionEditText.hideKeyboard()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_CANCELED || data == null) {
            return
        }
        if (requestCode == GALLERY) {
            val contentURI = data.data
            try {
                var bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, contentURI)
                bitmap = bitmap.resize(1000f, 1000f)
                presenter.bitmapList.add(bitmap)
                val filePath = bitmap.save(context!!)
                presenter.filePathList.add(filePath)
                presenter.adapter?.notifyDataSetChanged()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (requestCode == CAMERA) {
            var bitmap = data.extras!!.get("data") as Bitmap
            bitmap = bitmap.resize(1000f, 1000f)
            presenter.bitmapList.add(bitmap)
            val filePath = bitmap.save(context!!)
            presenter.filePathList.add(filePath)
            presenter.adapter?.notifyDataSetChanged()
        }
    }

    private fun loadUIWithCounselorModel() {
        val counselorModel = ShareData.getInstance().currentCounselor ?: return
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, counselorModel.avatar, 0)
        dataBinding.nameTextView.text = counselorModel.getFullName()
        dataBinding.jobTextView.text = counselorModel.getDescriptionJob()
        dataBinding.gridView.adapter = presenter.adapter

        cID = counselorModel.id?.toInt() ?: -1
    }

    private fun loadUIWithMessageModel() {
        val messageModel = ShareData.getInstance().currentHistoryMessage ?: return
        ImageViewBindingAdapter.imageViewAsyncSrc(dataBinding.roundedImageView, messageModel.avatar, 0)
        dataBinding.nameTextView.text = messageModel.userName
        dataBinding.jobTextView.text = ""
        dataBinding.gridView.adapter = presenter.adapter

        cID = messageModel.userId
    }
    // region ------ Camera + Album ------
    fun showPictureDialog() {
        val pictureDialog = AlertDialog.Builder(this.activity)
        val pictureDialogItems = arrayOf("Select photo from gallery", "Capture photo from camera")
        pictureDialog.setItems(pictureDialogItems) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallary()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    // endregion --- Camera + Album ---
}
