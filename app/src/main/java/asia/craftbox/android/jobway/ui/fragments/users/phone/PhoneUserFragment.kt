package asia.craftbox.android.jobway.ui.fragments.users.phone

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentUserPhoneBinding
import asia.craftbox.android.jobway.entities.responses.auth.CheckPhoneNumberAuthResponse
import asia.craftbox.android.jobway.ui.activities.login.LoginActivity
import asia.craftbox.android.jobway.ui.fragments.users.verify.VerifyUserFragment
import com.seldatdirect.dms.core.api.processors.DMSRestPromise
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.fragment.DMSBindingFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.users.phone
 * Date: Jan 13, 2019 - 8:16 PM
 */


class PhoneUserFragment : DMSBindingFragment<FragmentUserPhoneBinding>(R.layout.fragment_user_phone),
    DMSViewModelSource<PhoneUserViewModel> {

    private val presenter by lazy { Presenter() }
    override val viewModel by lazy { ViewModelProviders.of(this@PhoneUserFragment)[PhoneUserViewModel::class.java] }


    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@PhoneUserFragment.dataBinding.viewModel = this@PhoneUserFragment.viewModel
        this@PhoneUserFragment.dataBinding.presenter = this@PhoneUserFragment.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun apiAllowBehaviourWithTaskNameResIds(): List<Int> {
        return listOf(
            R.string.task_api_auth_check_phone_number
        )
    }

    override fun onAPIResponseSuccess(promise: DMSRestPromise<*, *>, responseData: Any?) {
        if (promise.taskNameResId == R.string.task_api_auth_check_phone_number) {
            val fragmentHelper = (this@PhoneUserFragment.activity as? LoginActivity)?.getFragmentHelper() ?: return
            val registerRequest = this@PhoneUserFragment.viewModel.getPhoneRequest()

            val response = responseData as? CheckPhoneNumberAuthResponse ?: return
            val isAccountRegistered = response.data?.isExist ?: false

            if (isAccountRegistered) {
                val verificationBundle = Bundle()
                verificationBundle.putSerializable(VerifyUserFragment.INTENT_USER_REGISTER_REQUEST, registerRequest)
                verificationBundle.putBoolean(VerifyUserFragment.INTENT_IS_REGISTERED, isAccountRegistered)

                val verificationFragment = DMSFragment.create(VerifyUserFragment::class.java, verificationBundle)
                fragmentHelper.pushFragment(verificationFragment)
                return
            }
            return
        }

        super.onAPIResponseSuccess(promise, responseData)
    }


    inner class Presenter {

        fun doVerifyPhoneNumber() {
            val fragmentHelper = (this@PhoneUserFragment.activity as? LoginActivity)?.getFragmentHelper() ?: return
            val registerRequest = this@PhoneUserFragment.viewModel.getPhoneRequest()

            if (!registerRequest.validates()) {
                return
            }

            val verificationBundle = Bundle()

            verificationBundle.putSerializable(VerifyUserFragment.INTENT_USER_REGISTER_REQUEST, registerRequest)
            verificationBundle.putBoolean(VerifyUserFragment.INTENT_IS_REGISTERED, false)

            val verificationFragment = DMSFragment.create(VerifyUserFragment::class.java, verificationBundle)
            fragmentHelper.pushFragment(verificationFragment)
        }

    }

}
