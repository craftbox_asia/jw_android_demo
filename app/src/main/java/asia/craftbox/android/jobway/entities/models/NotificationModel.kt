package asia.craftbox.android.jobway.entities.models

import android.content.Context
import androidx.recyclerview.widget.DiffUtil
import androidx.room.Entity
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.modules.exts.timeAgo
import asia.craftbox.android.jobway.modules.exts.toDate
import com.google.gson.annotations.SerializedName

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 13, 2019 - 3:11 PM
 */


@Entity
class NotificationModel : AppModel() {

    @SerializedName("title")
    var title: String? = null

    @SerializedName("des")
    var des: String? = null

    @SerializedName("body")
    var body: String? = null

    @SerializedName("path")
    var path: String? = null

    @SerializedName("post_id")
    var postId: Int = -1

    @SerializedName("avatar")
    var avatar: String? = null

    @SerializedName("user_id")
    var userId: Int = -1

    @SerializedName("read")
    var read: Int = 0

    @SerializedName("files")
    val files: List<String>? = null

    var type = JobWayConstant.TYPE_NEWS

    fun isRead(): Boolean {
        return read != 0
    }

    fun getNatureTime(context: Context?): String? {
        val ctx = context ?: return null
        val publishTimeString = createdAt ?: return null
        val publishDate = publishTimeString.toDate("yyyy-MM-dd kk:mm:ss") ?: return null
        return publishDate.timeAgo(ctx)
    }

    companion object {
        val DIFF = object : DiffUtil.ItemCallback<NotificationModel>() {
            override fun areItemsTheSame(oldItem: NotificationModel, newItem: NotificationModel): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: NotificationModel, newItem: NotificationModel): Boolean {
                return oldItem.id == newItem.id
                        && oldItem.localUpdatedAt == newItem.localUpdatedAt
            }

        }
    }
}
