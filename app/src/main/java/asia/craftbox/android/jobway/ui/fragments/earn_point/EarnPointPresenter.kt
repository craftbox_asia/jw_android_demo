package asia.craftbox.android.jobway.ui.fragments.earn_point

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.databinding.FragmentEarnPointBinding
import asia.craftbox.android.jobway.ui.activities.faculty.FacultyDescriptionDetailActivity
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter

class EarnPointPresenter (private val fragment: EarnPointFragment) : BasePresenter<FragmentEarnPointBinding>(fragment) {

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }
}