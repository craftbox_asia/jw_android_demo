package asia.craftbox.android.jobway.ui.fragments.consult.question.frequently_question

import androidx.databinding.ObservableField
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultQuestionBinding
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.consult.adapters.CounselorConsultQuestionAdapter


class CounselorConsultQuestionPresenter(private val fragment: CounselorConsultQuestionFragment) : BasePresenter<FragmentCounselorConsultQuestionBinding>(fragment) {

    val loadingState = ObservableField<Boolean>(false)

    val counselorConsultQuestionAdapter by lazy {
        CounselorConsultQuestionAdapter(
            searchQuestionList,
            fragment.context!!
        )
    }

    private var questionList: ArrayList<QuestionModel> = ArrayList()
    private var searchQuestionList: ArrayList<QuestionModel> = ArrayList<QuestionModel>()

    fun setDrawerOpen(isOpen: Boolean) {
        (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
    }

    fun setQuestionList(questionList: ArrayList<QuestionModel>) {
        this.questionList = questionList
    }

    fun search() {
        searchQuestionList.clear()

        val filterData = questionList.filter { it.question?.contains(fragment.textSearch, ignoreCase = true) ?: false }
        filterData.forEach { searchQuestionList.add(it) }
        counselorConsultQuestionAdapter.notifyDataSetChanged()
    }
}
