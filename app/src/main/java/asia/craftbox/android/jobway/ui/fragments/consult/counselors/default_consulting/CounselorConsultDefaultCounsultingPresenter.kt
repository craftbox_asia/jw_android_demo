package asia.craftbox.android.jobway.ui.fragments.consult.counselors.default_consulting

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselors.subtract_point.CounselorConsultSubtractionPointFragment
import timber.log.Timber


class CounselorConsultDefaultCounsultingPresenter(private val fragment: CounselorConsultDefaultCounsultingFragment) : BaseObservable() {

    var currentCounselorModel: CounselorModel? = null

    fun onBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun onAskQuestionButtonClicked(sender: View?) {
        val subtractionPointFragment = CounselorConsultSubtractionPointFragment()
        subtractionPointFragment.show(fragment.childFragmentManager, "CounselorConsultSubtractionPointFragment")
    }

    fun onContinueConsultButtonClicked(sender: View?) {
        Timber.d("onContinueConsultButtonClicked")
    }

}
