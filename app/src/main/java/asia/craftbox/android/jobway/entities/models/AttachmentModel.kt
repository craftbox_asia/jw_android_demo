package asia.craftbox.android.jobway.entities.models

import androidx.room.Entity
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSModel

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.models
 * Date: Feb 13, 2019 - 3:14 PM
 */


@Entity
class AttachmentModel : AppModel() {

    @SerializedName("post_id")
    var postId: Long? = null

    @SerializedName("path")
    var path: String? = null

    @SerializedName("data")
    var data: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("updated_at")
    var updatedAt: String? = null


    override fun saveOrUpdate(): Long? {
        super.saveOrUpdate()

        val instance = AppData.get().getNews().getAttachmentByIdFlatten(this@AttachmentModel.onCopyPrimaryKey())
            ?: this@AttachmentModel

        DMSModel.mergeFields(this@AttachmentModel, instance)
        instance.localUpdatedAt = System.currentTimeMillis()

        return AppData.get().getNews().insertOneAttachment(instance)
    }

}
