package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.entities.responses.PaginateDataResponse
import com.google.gson.JsonObject
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface MasterService {

    @GET("master-service/v1/faqs")
    fun getFaqs(): Observable<DataFieldDMSRestResponse<PaginateDataResponse<QuestionModel>>>

    @GET("master-service/v1/general/by-key?key=RUL_SCR")
    fun earnPoint(): Observable<DataFieldDMSRestResponse<JsonObject>>
}