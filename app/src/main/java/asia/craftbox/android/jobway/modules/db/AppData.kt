package asia.craftbox.android.jobway.modules.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import asia.craftbox.android.jobway.entities.daos.*
import asia.craftbox.android.jobway.entities.models.*

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.modules.db
 * Date: Jan 09, 2019 - 4:18 PM
 */


@Database(
    version = 19,
    exportSchema = false,
    entities = [
        ProfileModel::class,
        ResultTestModel::class,
        ProfileResumeModel::class,
        ResultProfileModel::class,
        NewsModel::class,
        AttachmentModel::class,
        UniModel::class,
        FacultyModel::class
    ]
)
@TypeConverters(
    NewsModel.NewsTags.Converter::class
)
abstract class AppData : RoomDatabase() {

    abstract fun getProfile(): ProfileDao
    abstract fun getTest(): TestDao
    abstract fun getNews(): NewsDao
    abstract fun getUni(): UniDao
    abstract fun getFaculty(): FacultyDao


    companion object {

        private var instance: AppData? = null

        fun initDefault(context: Context) {
            if (this@Companion.instance != null) {
                return
            }

            this@Companion.instance = Room.databaseBuilder(
                context.applicationContext,
                AppData::class.java,
                context.applicationContext.packageName
            )
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .build()
        }

        fun get(): AppData {
            return this@Companion.instance ?: throw Exception("AppData doesn't initialized already!")
        }
    }

}
