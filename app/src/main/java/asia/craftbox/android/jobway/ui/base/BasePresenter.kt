package asia.craftbox.android.jobway.ui.base

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LiveData
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.notification.NotificationFragment
import com.seldatdirect.dms.core.ui.fragment.DMSFragment
import com.seldatdirect.dms.core.utils.preference.DMSPreference
import timber.log.Timber

open class BasePresenter<T : ViewDataBinding>(private val fragment: AppMainFragment<T>) : BaseObservable() {

    fun notificationButtonClicked(sender: View?) {
        Timber.d("notificationButtonClicked")
        val mainActivity = fragment.activity as? MainActivity
        val notificationFragment = DMSFragment.create(NotificationFragment::class.java)
        mainActivity?.getFragmentHelper()?.pushFragment(notificationFragment)
    }

    fun toolbarMainOnBackButtonClicked(sender: View?) {
        val mainActivity = fragment.activity as? MainActivity
        mainActivity?.getFragmentHelper()?.popFragmentIfNeeded()
    }

    fun getNotificationCount(): Int? {
        return ShareData.getInstance().getCountUnreadMessageAndNews()
    }

    private val userProfileMutable by lazy {
        val userId = DMSPreference.default().get(
            JobWayConstant.Preference.AuthProfileId.name,
            Long::class.java
        ) ?: -1
        return@lazy AppData.get().getProfile().getProfileById(userId)
    }

    fun getUserProfile(): LiveData<ProfileModel> {
        return this.userProfileMutable
    }
}