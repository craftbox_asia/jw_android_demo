package asia.craftbox.android.jobway.ui.fragments.notification

import asia.craftbox.android.jobway.databinding.FragmentNotificationBinding
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.NotificationModel
import asia.craftbox.android.jobway.ui.base.BasePresenter


class NotificationPresenter(private val fragment: NotificationFragment) :
    BasePresenter<FragmentNotificationBinding>(fragment) {

    var notificationMessageAdapter: NotificationMessageAdapter? = null
    var notificationNewsAdapter: NotificationNewsAdapter? = null

    var messageList: ArrayList<MessageModel> = ArrayList()
    var newsList: ArrayList<NotificationModel> = ArrayList()

    init {
        notificationMessageAdapter =
            NotificationMessageAdapter(
                messageList,
                fragment.context!!
            )
        notificationNewsAdapter =
            NotificationNewsAdapter(
                newsList,
                fragment.context!!
            )
    }
}
