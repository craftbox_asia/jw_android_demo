package asia.craftbox.android.jobway.entities.services

import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.entities.models.UniFacultyModel
import asia.craftbox.android.jobway.entities.responses.PaginateDataResponse
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.services
 * Date: Feb 26, 2019 - 3:04 PM
 */


interface JobAPIService {

    @GET("website/api/job")
    fun getJobsListPaginate(@Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<FacultyModel>>>

    @GET("website/api/job/{fc_id}")
    fun getJobsListByFacultyIdPaginate(@Path("fc_id") fcId: Long = 1, @Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<FacultyModel>>>

    @GET("website/api/job/uni-faculties/{uni_id}")
    fun getJobsListByUniId(@Path("uni_id") uniId: Long, @Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<UniFacultyModel>>>

    @GET("website/api/job/{job_id}")
    fun getJobDetails(@Path("job_id") jobId: Long): Observable<Unit>

    @GET("website/api/job/detail/{fc_id}")
    fun getJobByFacultyId(@Path("fc_id") fcId: Long): Observable<DataFieldDMSRestResponse<FacultyModel>>

    @GET("website/api/job/hot-faculties/{id}")
    fun getHotFaculties(@Path("id") id: Long): Observable<DataFieldDMSRestResponse<List<FacultyModel>>>

    @GET("website/api/job")
    fun searchFaculty(@Query("filter") filter: String, @Query("page") page: Int = 1): Observable<DataFieldDMSRestResponse<PaginateDataResponse<FacultyModel>>>

}
