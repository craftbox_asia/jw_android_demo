package asia.craftbox.android.jobway.ui.fragments.consult.counselors.master

import android.view.View
import asia.craftbox.android.jobway.databinding.FragmentCounselorConsultMasterBinding
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.QuestionModel
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.base.BasePresenter
import asia.craftbox.android.jobway.ui.fragments.consult.counselor.CounselorConsultFragment
import asia.craftbox.android.jobway.ui.fragments.consult.adapters.CounselorConsultQuestionAdapter


class CounselorConsultMasterPresenter(private val fragment: CounselorConsultMasterFragment) : BasePresenter<FragmentCounselorConsultMasterBinding>(fragment) {

    var counselorConsultJobWayAdapter: CounselorConsultJobWayAdapter? = null
    var counselorConsultMasterAdapter: CounselorConsultJobWayAdapter? = null
    var counselorConsultQuestionAdapter: CounselorConsultQuestionAdapter? = null

    var coList: ArrayList<CounselorModel> = ArrayList()
    var exList: ArrayList<CounselorModel> = ArrayList()
    var questionList: ArrayList<QuestionModel> = ArrayList()

    init {
        counselorConsultJobWayAdapter =
            CounselorConsultJobWayAdapter(
                coList,
                fragment.context!!
            )
        counselorConsultMasterAdapter =
            CounselorConsultJobWayAdapter(
                exList,
                fragment.context!!
            )
        counselorConsultQuestionAdapter =
            CounselorConsultQuestionAdapter(
                questionList,
                fragment.context!!
            )
    }

    fun setDrawerOpen(isOpen: Boolean) {
        (fragment.activity as? MainActivity)?.presenter?.setDrawerOpen(isOpen)
    }

    fun loadMoreButtonClicked(sender: View?) {
        val parent = fragment.parentFragment as CounselorConsultFragment
        parent.changeViewPagerCurrentItem(2)
    }
}
