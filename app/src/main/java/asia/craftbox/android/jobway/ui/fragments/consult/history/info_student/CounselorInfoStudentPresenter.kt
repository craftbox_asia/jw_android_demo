package asia.craftbox.android.jobway.ui.fragments.consult.history.info_student

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import com.google.gson.JsonObject
import com.google.gson.Gson




class CounselorInfoStudentPresenter(private val fragment: CounselorInfoStudentFragment) : BaseObservable() {

    var adapter: CounselorInfoStudentAdapter? = null

    var titlesInfo0: ArrayList<String> = arrayListOf("Họ tên", "Năm sinh", "Mô tả ngắn về bạn")
    var descriptionInfo0: ArrayList<String> = arrayListOf()

    var titlesInfo1: ArrayList<String> = arrayListOf("1. Bạn hiểu về bạn")
    var descriptionInfo1: ArrayList<String> = arrayListOf("")

    var titlesInfo2: ArrayList<String> = arrayListOf("2. Người khác hiểu về bạn")
    var descriptionInfo2: ArrayList<String> = arrayListOf("")

    var titlesInfo3: ArrayList<String> = arrayListOf("3. Kết quả  nghiệm của bạn", "Trắc nghiệm tính cách (MBTI)", "Trắc nghiệm nghề nghiệp (Holland)")
    var descriptionInfo3: ArrayList<String> = arrayListOf("")

    var totalTitles = arrayListOf<ArrayList<String>>()
    var totalDescriptions = arrayListOf<ArrayList<String>>()

    init {
        adapter = CounselorInfoStudentAdapter(totalTitles, totalDescriptions, fragment.context!!)
    }

    fun parseDataForTitleInfo0(profileModel: ProfileModel) {
        descriptionInfo0.add(profileModel.getDisplayName())
        descriptionInfo0.add(profileModel.dateOfBirth?:"")
        descriptionInfo0.add(profileModel.summary?:"")
    }

    fun parseDataForOtherTitleInfo(profileModel: ProfileModel) {
        val currentUserResults =  profileModel.userResults
        val currentUserProfileJsonObject = Gson().fromJson(currentUserResults?.profile?.toJSON(), JsonObject::class.java)
        val profileModelList = AppData.get().getTest().getAllResumesFlatten()

        for (item in profileModelList) {
            val key = item.key?.toLowerCase()
            val value = currentUserProfileJsonObject[key]
            if (key?.contains("be_") == true) {
                titlesInfo2.add(item.text?:"")
                descriptionInfo2.add(value.asString)
            } else {
                titlesInfo1.add(item.text?:"")
                descriptionInfo1.add(value.asString)
            }
        }
        descriptionInfo3.add(currentUserResults?.mbti?.code?: "Còn thiếu")
        descriptionInfo3.add(currentUserResults?.holland?.code?: "Còn thiếu")

        totalTitles = arrayListOf(titlesInfo0, titlesInfo1, titlesInfo2, titlesInfo3)
        totalDescriptions = arrayListOf(descriptionInfo0, descriptionInfo1, descriptionInfo2, descriptionInfo3)
        adapter = CounselorInfoStudentAdapter(totalTitles, totalDescriptions, fragment.context!!)
    }

    fun closeButtonClicked(sender: View?) {
        fragment.dismiss()
    }
}
