package asia.craftbox.android.jobway.ui.activities.profile

import android.Manifest
import android.app.DatePickerDialog
import android.view.View
import android.widget.Toast
import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.modules.exts.toDate
import asia.craftbox.android.jobway.utils.Utils
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.ItemListener
import com.afollestad.materialdialogs.list.listItems
import com.esafirm.imagepicker.features.ImagePicker
import com.seldatdirect.dms.core.extensions.formatToString
import java.util.*
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import asia.craftbox.android.jobway.JobWayConstant


/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.activities.profile
 * Date: Jan 21, 2019 - 9:21 PM
 */


@Suppress("UNUSED_PARAMETER")
class ProfilePresenter(private val activity: ProfileActivity) : BaseObservable() {

    val editingMode = ObservableField(false)


    fun onBackButtonClicked(sender: View?) {
        this@ProfilePresenter.activity.finish()
    }

    fun onFinishButtonClicked(sender: View?) {
        val isEditingMode = this@ProfilePresenter.editingMode.get() ?: false

        if (isEditingMode) {
            this@ProfilePresenter.activity.viewModel.doSubmit()
            return
        }

        this@ProfilePresenter.editingMode.set(!isEditingMode)
    }

    fun displayDateOfBirthDialog() {
        val updateRequest = this@ProfilePresenter.activity.viewModel.getUpdateRequest().value ?: return
        val calendarInstance = Calendar.getInstance()
        calendarInstance.time = updateRequest.dateOfBirth?.toDate("yyyy-MM-dd") ?: Date()

        var calendarDialog: DatePickerDialog? = DatePickerDialog(
            this@ProfilePresenter.activity,
            { _, year, month, dayOfMonth ->
                calendarInstance.set(Calendar.YEAR, year)
                calendarInstance.set(Calendar.MONTH, month)
                calendarInstance.set(Calendar.DAY_OF_MONTH, dayOfMonth)

                updateRequest.updateDateOfBirth(calendarInstance.time.formatToString("yyyy-MM-dd"), 0, 0, 0)
            },
            calendarInstance.get(Calendar.YEAR),
            calendarInstance.get(Calendar.MONTH),
            calendarInstance.get(Calendar.DAY_OF_MONTH)
        )

        calendarDialog?.setOnDismissListener { calendarDialog = null }
        calendarDialog?.show()
    }

    fun onHintClicked(key: String?) {
        val hintMessage = this@ProfilePresenter.activity.viewModel.getResumeItem(key)?.hint ?: return
        this@ProfilePresenter.activity.getAlertHelper().alert(hintMessage)
    }

    fun onProfilePhotoClicked(sender: View?) {
        MaterialDialog(activity).show {
            title(R.string.title_choosen)
            listItems(items = listOf(
                    this@ProfilePresenter.activity.getString(R.string.picker_source_camera),
                    this@ProfilePresenter.activity.getString(R.string.picker_source_library)
                )
            ) { _, index, text ->
                when(index) {
                    1 -> Utils.instance().choosePhotoFromGallary(activity)
                    0 -> Utils.instance().takePhotoFromCamera(activity)
                }
            }
        }
    }

}
