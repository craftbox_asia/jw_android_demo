package asia.craftbox.android.jobway.ui.fragments.pages.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.FragmentPageHomeBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import asia.craftbox.android.jobway.entities.models.UniModel
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import asia.craftbox.android.jobway.ui.activities.main.MainActivity
import asia.craftbox.android.jobway.ui.fragments.consult.counselor.CounselorConsultFragment
import asia.craftbox.android.jobway.ui.fragments.explore.home.HomeExploreFragment
import asia.craftbox.android.jobway.ui.base.AppMainFragment
import asia.craftbox.android.jobway.ui.fragments.faculty.list.ListFacultyFragment
import asia.craftbox.android.jobway.ui.fragments.uni.list.ListUniFragment
import asia.craftbox.android.jobway.ui.widgets.NewsHomePageAdapter
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import android.content.Intent
import android.net.Uri
import asia.craftbox.android.jobway.EventBus.ProfileEvent
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsFragment
import asia.craftbox.android.jobway.ui.fragments.news.list.ListNewsViewModel
import asia.craftbox.android.jobway.ui.fragments.uni.list.ListUniViewModel
import kotlinx.android.synthetic.main.layout_home_invite_friend.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


/**
 * Created by @dphans (https://github.com/dphans)
 *
 * Package: asia.craftbox.android.jobway.ui.fragments.pages.home
 * Date: Dec 28, 2018 - 10:11 PM
 */


class HomePageFragment : AppMainFragment<FragmentPageHomeBinding>(R.layout.fragment_page_home),
    DMSViewModelSource<HomePageViewModel> {

    override val viewModel: HomePageViewModel by lazy { ViewModelProviders.of(this@HomePageFragment)[HomePageViewModel::class.java] }
    private val presenter by lazy { HomePagePresenter(this@HomePageFragment) }
    private var shareContent: String = ""
    var profile: ProfileModel? = null
    override fun getTitleResId(): Int? {
        return null
    }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@HomePageFragment.dataBinding.viewModel = this@HomePageFragment.viewModel
        this@HomePageFragment.dataBinding.presenter = this@HomePageFragment.presenter

        super.onContentViewCreated(rootView, savedInstanceState)

        // update CountUnreadMessage
        viewModel.getCountUnreadMessage()
        viewModel.getCountUnreadNews()
    }

    override fun onAPIDidFinishedAll() {
        super.onAPIDidFinishedAll()
        dataBinding.toolbarMain.toolbarMainNotificationCounts = ShareData.getInstance().getCountUnreadMessageAndNews()
    }

    override fun initUI() {
        super.initUI()

        // setup news items
        this@HomePageFragment.presenter.newsAdapter =
            NewsHomePageAdapter(this@HomePageFragment.childFragmentManager)
        this@HomePageFragment.dataBinding.pageHomeNewsViewPager.adapter = this@HomePageFragment
            .presenter.newsAdapter
        this@HomePageFragment.dataBinding.pageHomeNewsViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@HomePageFragment.dataBinding.pageHomeNewsViewPager)
            .pagerMargin(this@HomePageFragment.requireContext().resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()
        ListNewsViewModel().doFetchNews()

        // setup edu info items
        this@HomePageFragment.presenter.eduInfoAdapter =
            NewsHomePageAdapter(this@HomePageFragment.childFragmentManager)
        this@HomePageFragment.dataBinding.pageHomeEduInfoViewPager.adapter = this@HomePageFragment
            .presenter.eduInfoAdapter
        this@HomePageFragment.dataBinding.pageHomeEduInfoViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@HomePageFragment.dataBinding.pageHomeEduInfoViewPager)
            .pagerMargin(this@HomePageFragment.requireContext().resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()
        ListUniViewModel().doFetchUniList()

        // handle main menu click
        this@HomePageFragment.presenter.mainMenuAdapter.setOnMenuClickListener {
            if (!this@HomePageFragment.isAdded) return@setOnMenuClickListener
            val mainActivity = this@HomePageFragment.activity as? MainActivity
                ?: return@setOnMenuClickListener
            when (it.menuId) {
                R.id.main_item_consulting -> {
                    mainActivity.getFragmentHelper().pushFragment(
                        create(CounselorConsultFragment::class.java)
                    )
                }
                R.id.main_item_news -> {
                    mainActivity.getFragmentHelper().pushFragment(
                        create(ListNewsFragment::class.java)
                    )
                }
                R.id.main_item_explore_self -> {
                    mainActivity.getFragmentHelper().pushFragment(
                        create(HomeExploreFragment::class.java)
                    )
                }
                R.id.main_item_uni_info -> {
                    mainActivity.getFragmentHelper().pushFragment(
                        create(ListUniFragment::class.java)
                    )
                }
                R.id.main_item_job -> {
                    mainActivity.getFragmentHelper().pushFragment(
                        create(ListFacultyFragment::class.java)
                    )
                }
                else -> null
            } ?: return@setOnMenuClickListener
        }

        this@HomePageFragment.view?.findViewById<View?>(R.id.home_tool_explore_self)?.setOnClickListener {
            (this@HomePageFragment.activity as? MainActivity)?.let { mainActivity ->
                mainActivity.getFragmentHelper().pushFragment(
                    fragment = create(HomeExploreFragment::class.java),
                    replaceRootFragment = false
                )
            }
        }

        this@HomePageFragment.view?.findViewById<View?>(R.id.home_tool_rewards)?.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                var shareMessage = shareContent + ": https://jobway.vn"+ "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "Jobway invitation"))
            } catch (e: Exception) {

            }
        }

        this@HomePageFragment.view?.findViewById<View?>(R.id.hot_phone_call)?.setOnClickListener {
            try {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.setData(Uri.parse("tel:" + getString(R.string.value_support_phone_primary)));
                startActivity(callIntent);
            } catch (e: Exception) {

            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (this@HomePageFragment.activity as? MainActivity)
            ?.dataBinding
            ?.toolbarDisabledHeaderCover = false
    }


    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onMessageEvent(event: ProfileEvent) {
        profile = event.profile
        val nameMerged = arrayOf(profile?.firstName, profile?.lastName).joinToString(" ")
        shareContent = getString(
            R.string.share_content_to_get_rewards,
            nameMerged,
            profile?.personalCode
        )
        personal_code_text.text = profile?.personalCode
    }

    override fun initObservables() {
        super.initObservables()

        this@HomePageFragment.viewModel.getNews().observe(this@HomePageFragment, Observer {
            this@HomePageFragment.presenter.newsAdapter?.updateItems(it.map {
                NewsHomePageAdapter.FragmentItem.create(
                    it.title ?: "",
                    it.views,
                    it.getNatureTime(this@HomePageFragment.requireContext()) ?: "",
                    it.featureImg ?: "",
                    NewsModel::class.java.simpleName,
                    it.id ?: -1,
                    false,
                    null
                )
            })
        })
        this@HomePageFragment.dataBinding.pageHomeNewsPagerContainer
            .setOverlapEnabled(true)

        this@HomePageFragment.viewModel.getUnis().observe(this@HomePageFragment, Observer {
            this@HomePageFragment.presenter.eduInfoAdapter?.updateItems(it.map {
                NewsHomePageAdapter.FragmentItem.create(
                    it.name ?: "",
                      -1,
                    it.addr ?: "",
                    it.featureImg ?: "",
                    UniModel::class.java.simpleName,
                    it.id ?: -1,
                    true,
                    it
                )
            })
        })
        this@HomePageFragment.dataBinding.pageHomeEduInfoPagerContainer
            .setOverlapEnabled(true)
    }
}
