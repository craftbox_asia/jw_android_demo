package asia.craftbox.android.jobway.entities.responses.news

import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.entities.DMSJsonableObject
import com.seldatdirect.dms.core.api.contribs.responses.DataFieldDMSRestResponse
@Entity
class BookmarkResponse : DMSJsonableObject{
    @SerializedName("data")
    var data: Boolean = false
    @SerializedName("message")
    var message: String = ""
}