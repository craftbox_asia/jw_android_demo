package asia.craftbox.android.jobway.ui.fragments.notification

import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.NotificationModel
import asia.craftbox.android.jobway.entities.responses.news.NotificationNewsResponse
import asia.craftbox.android.jobway.entities.services.NotificationAPIService
import asia.craftbox.android.jobway.modules.sharedata.ShareData
import com.seldatdirect.dms.core.api.DMSRest
import com.seldatdirect.dms.core.entities.DMSViewModel

class NotificationViewModel : DMSViewModel() {

    fun getListNotiMessage(callback: ((List<MessageModel>?) -> Unit)) {
        this.request(DMSRest.get(NotificationAPIService::class.java).getListNotiMessage())
            .then { promise, res ->
                callback.invoke(res.data ?: ArrayList())
            }
            .setTaskNameResId(R.string.task_api_get_list_noti_message)
            .submit()
    }

    fun getListNotiNews(page: Int, callback: ((NotificationNewsResponse<NotificationModel>?) -> Unit)) {
        this.request(DMSRest.get(NotificationAPIService::class.java).getListNotiNews(page))
            .then { promise, responseData ->
                callback.invoke(responseData.data)
            }
            .setTaskNameResId(R.string.task_api_get_list_noti_news)
            .submit()
    }

//    fun getListMessage(callback: ((List<MessageModel>?) -> Unit)) {
//        this.request(DMSRest.get(NotificationAPIService::class.java).getListMessage())
//            .then { _, responseData ->
//                callback.invoke(responseData.data ?: ArrayList())
//            }
//            .setTaskNameResId(R.string.task_api_get_list_message)
//            .submit()
//    }

    fun updateReadNoti(id: String, type: String, callback: ((Boolean) -> Unit)) {
        val params: HashMap<String, String> = HashMap()
        params["id"] = id
        params["type"] = type
        this.request(DMSRest.get(NotificationAPIService::class.java).updateReadNoti(params))
            .then { _, responseData ->
                val data = responseData.data?.toString() ?: ""
                if (data.isNotEmpty()) {
                    getCountUnreadNews(callback)
                } else {
                    callback(false)
                }

            }.setTaskNameResId(R.string.task_api_update_read_noti)
            .submit()
    }

    fun getCountUnreadNews(callback: ((Boolean) -> Unit)) {
        this.request(DMSRest.get(NotificationAPIService::class.java).getCountUnreadNews())
            .then { promise, res ->
                val count = (res.data ?: 0)
                ShareData.getInstance().countUnreadNews = count
                callback(true)
            }
            .catch { promise, exception ->
                callback(false)
            }
            .setTaskNameResId(R.string.task_api_get_count_unread_news)
            .submit()
    }

}
