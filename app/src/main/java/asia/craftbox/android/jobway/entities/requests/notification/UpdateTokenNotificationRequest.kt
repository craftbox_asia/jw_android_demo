package asia.craftbox.android.jobway.entities.requests.notification

import com.google.gson.annotations.SerializedName
import com.seldatdirect.dms.core.api.entities.DMSRestRequest

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.entities.requests.notification
 * Date: Jan 18, 2019 - 10:59 AM
 */


class UpdateTokenNotificationRequest : DMSRestRequest() {

    @SerializedName("token")
    var token: String? = null

    @SerializedName("device")
    var device: Int = 1


    override fun validates(): Boolean {
        return !this@UpdateTokenNotificationRequest.token.isNullOrBlank()
                && device == 1
    }

}
