package asia.craftbox.android.jobway.ui.activities.faculty

import android.view.View
import androidx.databinding.BaseObservable
import asia.craftbox.android.jobway.ui.activities.faculty.adapter.ListFacultyDetailAdapter
import asia.craftbox.android.jobway.ui.activities.faculty.adapter.ListHotFacultyAdapter

class FacultyDetailPresenter(private val activity: FacultyDetailActivity) : BaseObservable() {


    var listFacultyAdapter: ListFacultyDetailAdapter? = null
    var listHotFacultyAdapter: ListHotFacultyAdapter? = null

    fun onBackButtonClicked(sender: View?) {
        this@FacultyDetailPresenter.activity.finish()
    }

    fun getNotificationCount(): Int? {
        return 0
    }
}