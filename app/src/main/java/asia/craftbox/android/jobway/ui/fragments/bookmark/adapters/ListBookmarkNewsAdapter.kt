package asia.craftbox.android.jobway.ui.fragments.bookmark.adapters

import android.text.Spanned
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ViewholderBookmarkNewsItemBinding
import asia.craftbox.android.jobway.entities.models.NewsModel
import com.seldatdirect.dms.core.utils.recycleradapters.DMSSingleModelRecyclerAdapter

/**
 * Created by @dphans (https://github.com/dphans)
 * ==============================================
 * Package: asia.craftbox.android.jobway.ui.fragments.news.list.adapters
 * Date: Feb 13, 2019 - 4:47 PM
 */


class ListBookmarkNewsAdapter : PagedListAdapter<NewsModel, ListBookmarkNewsAdapter.ViewHolder>(NewsModel.DIFF) {

    private var onItemSelectedBlock: ((newsItem: NewsModel) -> Unit)? = null


    fun setOnItemSelected(block: ((newsItem: NewsModel) -> Unit)?) {
        this@ListBookmarkNewsAdapter.onItemSelectedBlock = block
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        this@ListBookmarkNewsAdapter.getItem(position)?.let { unwrappedItem ->
            holder.bind(unwrappedItem, position)
        }
    }


    inner class ViewHolder(parent: ViewGroup) : DMSSingleModelRecyclerAdapter.DMSSingleModelViewHolder<NewsModel>(
        parent,
        R.layout.viewholder_bookmark_news_item
    ) {

        private val dataBinding = DataBindingUtil.bind<ViewholderBookmarkNewsItemBinding>(this@ViewHolder.itemView)


        override fun bind(item: NewsModel, position: Int) {
            super.bind(item, position)
            this@ViewHolder.dataBinding?.presenter = Presenter(item, position)
            this@ViewHolder.dataBinding?.invalidateAll()
        }


        inner class Presenter(private val item: NewsModel, private val position: Int) {

            fun getNews() = this@Presenter.item

            fun getNewsNatureTime(): String? {
                return this@Presenter.item.getNatureTime(this@ViewHolder.itemView.context)
            }

            fun getCountViews(): String? {
                return this@Presenter.item.views?.toString()
            }

            fun onItemClicked(sender: View?) {
                this@ListBookmarkNewsAdapter.onItemSelectedBlock
                    ?.invoke(this@Presenter.item)
            }
        }

    }

}
