package asia.craftbox.android.jobway.ui.activities.faculty

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.databinding.ActivityFacultyDetailBinding
import asia.craftbox.android.jobway.entities.models.FacultyModel
import asia.craftbox.android.jobway.libs.coverflow.CoverFlow
import asia.craftbox.android.jobway.ui.activities.faculty.adapter.ListFacultyDetailAdapter
import asia.craftbox.android.jobway.ui.activities.faculty.adapter.ListHotFacultyAdapter
import com.seldatdirect.dms.core.datasources.DMSViewModelSource
import com.seldatdirect.dms.core.ui.activity.DMSBindingActivity
import kotlinx.android.synthetic.main.activity_faculty_detail.*
import timber.log.Timber

class FacultyDetailActivity : DMSBindingActivity<ActivityFacultyDetailBinding>(R.layout.activity_faculty_detail),
    DMSViewModelSource<FacultyDetailViewModel> {

    private var facObj: FacultyModel = FacultyModel()
    private val presenter by lazy { FacultyDetailPresenter(this@FacultyDetailActivity) }
    override val viewModel by lazy { ViewModelProviders.of(this@FacultyDetailActivity)[FacultyDetailViewModel::class.java] }

    override fun onContentViewCreated(rootView: View, savedInstanceState: Bundle?) {
        this@FacultyDetailActivity.dataBinding.viewModel = this@FacultyDetailActivity.viewModel
        this@FacultyDetailActivity.dataBinding.presenter = this@FacultyDetailActivity.presenter
        super.onContentViewCreated(rootView, savedInstanceState)
    }

    override fun isRequiredUserPressBackButtonTwiceToExit(): Boolean {
        return false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            facObj = this@FacultyDetailActivity.intent.getSerializableExtra(INTENT_DATA_FACULTY_OBJECT) as FacultyModel
            this@FacultyDetailActivity.viewModel.setupFacultyWithId(facObj?.id!!)
            this@FacultyDetailActivity.viewModel.getFacultyPagination().observe(this@FacultyDetailActivity, Observer {
                this@FacultyDetailActivity.presenter.listFacultyAdapter?.submitList(it)
            })
            faculty_button_desciption.text = facObj?.name
            faculty_button_desciption.setOnClickListener(clickListener)
        } catch (exception: Exception) {
            Timber.e(exception.localizedMessage)
        }
    }

    val clickListener = View.OnClickListener {view ->
        var intentFacultyDescription = Intent(this@FacultyDetailActivity, FacultyDescriptionDetailActivity::class.java)
        intentFacultyDescription.putExtra(FacultyDescriptionDetailActivity.FACULTY_DESCRIPTION_BY_OBJECT, facObj)
        this@FacultyDetailActivity.startActivity(intentFacultyDescription)
    }

    override fun initUI() {
        super.initUI()
        var intent = Intent(this@FacultyDetailActivity, FullFacultyDetailActivity::class.java)
        //setup hot
        this@FacultyDetailActivity.presenter.listHotFacultyAdapter =
            ListHotFacultyAdapter(supportFragmentManager)
        this@FacultyDetailActivity.dataBinding.facultyDetailListHotViewPager.adapter = this@FacultyDetailActivity
            .presenter.listHotFacultyAdapter
        this@FacultyDetailActivity.dataBinding.facultyDetailListHotViewPager.offscreenPageLimit = 3
        CoverFlow.Builder().with(this@FacultyDetailActivity.dataBinding.facultyDetailListHotViewPager)
            .pagerMargin(this@FacultyDetailActivity.resources.getDimensionPixelSize(R.dimen.app_dimen_overlapse_size).toFloat())
            .scale(0.2f)
            .spaceSize(0f)
            .build()
        this@FacultyDetailActivity.presenter.listHotFacultyAdapter?.setOnItemSelected {
            intent.putExtra(FullFacultyDetailActivity.FACULTY_DETAIL_BY_OBJECT, it)
            this@FacultyDetailActivity.startActivity(intent)
        }

        //setup list fact
        this@FacultyDetailActivity.presenter.listFacultyAdapter = ListFacultyDetailAdapter()

        val uniRecyclerLayout = LinearLayoutManager(this@FacultyDetailActivity.baseContext)
        uniRecyclerLayout.isAutoMeasureEnabled = true

        this@FacultyDetailActivity.dataBinding.facultyDetailListRecyclerView.layoutManager = uniRecyclerLayout
        this@FacultyDetailActivity.dataBinding.facultyDetailListRecyclerView.isNestedScrollingEnabled = false
        this@FacultyDetailActivity.dataBinding.facultyDetailListRecyclerView.adapter =
            this@FacultyDetailActivity.presenter.listFacultyAdapter

        this@FacultyDetailActivity.presenter.listFacultyAdapter?.setOnFacultyItemClicked { item, _ ->
            intent.putExtra(FullFacultyDetailActivity.FACULTY_DETAIL_BY_OBJECT, item)
            this@FacultyDetailActivity.startActivity(intent)
        }
    }

    override fun initObservables() {
        super.initObservables()

        this@FacultyDetailActivity.viewModel.getHotFaculties().observe(this@FacultyDetailActivity, Observer {
            Timber.wtf("Hot faculty count: ${it.count()}")

            val hotFacultyFragments = it.map { uniItem -> ListHotFacultyAdapter.FragmentItem.create(uniItem) }
            this@FacultyDetailActivity.presenter.listHotFacultyAdapter?.updateItems(hotFacultyFragments)

            this@FacultyDetailActivity.dataBinding.facultyDetailListHotViewPager.post {
                try {
                    val pager = this@FacultyDetailActivity.dataBinding.facultyDetailListHotViewPager
                    val fragment = pager.adapter?.instantiateItem(pager, 0) as Fragment
                    fragment.view?.let { fragmentView -> ViewCompat.setElevation(fragmentView, 8.0f) }
                } catch (illegalStateException: Exception) {
                    Timber.e(illegalStateException.localizedMessage)
                }
            }
        })
    }

    companion object{
        const val INTENT_DATA_FACULTY_OBJECT: String = "INTENT_DATA_FACULTY_OBJECT"
    }
}