package asia.craftbox.android.jobway.modules.sharedata

import android.content.Context
import asia.craftbox.android.jobway.JobWayConstant
import asia.craftbox.android.jobway.R
import asia.craftbox.android.jobway.entities.models.CounselorModel
import asia.craftbox.android.jobway.entities.models.MessageModel
import asia.craftbox.android.jobway.entities.models.NotificationModel
import asia.craftbox.android.jobway.entities.models.ProfileModel
import asia.craftbox.android.jobway.modules.db.AppData
import com.seldatdirect.dms.core.utils.preference.DMSPreference

class ShareData private constructor() {

    var parentFragmentName = ""
    var currentCounselor: CounselorModel? = null
    var currentHistoryMessage: MessageModel? = null
    var currentNews: NotificationModel? = null

    var countUnreadMessage: Int = 0
    var countUnreadNews: Int = 0

    var isClickedNotification = false

    init {

    }

    private object Holder {
        val INSTANCE = ShareData()
    }

    companion object {
        @JvmStatic
        fun getInstance(): ShareData {
            return Holder.INSTANCE
        }
    }

    fun getCountUnreadMessageAndNews(): Int {
        return countUnreadMessage + countUnreadNews
    }

    // region ------ release ------
    fun releaseCurrentCounselor() {
        currentCounselor = null
    }

    fun releaseCurrentHistoryMessage() {
        currentHistoryMessage = null
    }

    fun releaseCurrentNews() {
        currentNews = null
    }
    // endregion --- release ---


    // region ------ Common ------

    fun isFromHistoryFrament(context: Context): Boolean {
        val parentName = ShareData.getInstance().parentFragmentName
        return (parentName == context.getString(R.string.text_history))
    }

    // endregion --- Common ---

    // region ------ Profile ------

    fun getCurrentProfileModel(): ProfileModel? {
        val profileId = DMSPreference.default().get(JobWayConstant.Preference.AuthProfileId.name, Long::class.java)
        return AppData.get().getProfile().getProfileByIdFlatten(profileId ?: -1)
    }

    fun getDisplayName(): String {
        val userProfile = getCurrentProfileModel()
        return userProfile?.getDisplayName() ?: ""
    }

    fun getCoin(): Int {
        val userProfile = getCurrentProfileModel()
        return userProfile?.coin ?: 0
    }

    fun canSendMessage(): Boolean {
        val userProfile = getCurrentProfileModel() ?: return false
        val currentCoin = userProfile.coin
        return currentCoin >= JobWayConstant.DEFAULT_SUBTRACTION_COIN
    }

    fun subtractAndUpdateCoin(): Boolean {
        val userProfile = getCurrentProfileModel() ?: return false
        val currentCoin = userProfile.coin
        if (currentCoin < JobWayConstant.DEFAULT_SUBTRACTION_COIN) {
            return false
        }
        AppData.get().getProfile().updateCoin(
            profileId = userProfile.id,
            coin = (currentCoin - JobWayConstant.DEFAULT_SUBTRACTION_COIN)
        )
        return true
    }

    fun isCurrentUser(_userId: Int): Boolean {
        val userProfile = getCurrentProfileModel()
        return userProfile?.id == _userId.toLong()
    }

    fun isCurrentUserIsMaster(): Boolean {
        val profile = getCurrentProfileModel()
        return profile?.isMaster() == true
    }
    // endregion --- Profile ---
}