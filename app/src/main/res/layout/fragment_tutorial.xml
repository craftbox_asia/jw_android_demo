<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="viewModel"
            type="asia.craftbox.android.jobway.ui.activities.tutorial.TutorialViewModel" />

        <variable
            name="presenter"
            type="asia.craftbox.android.jobway.ui.fragments.pages.tutorial.TutorialPresenter" />

    </data>

    <FrameLayout
        android:id="@+id/tutorial_page_container"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="#505050"
        android:paddingTop="@dimen/dms_spacing_large">

        <com.google.android.material.card.MaterialCardView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_margin="@dimen/dms_spacing_small"
            android:clipChildren="true"
            android:clipToPadding="true"
            android:fitsSystemWindows="true"
            app:cardBackgroundColor="#F8F9FB"
            app:cardCornerRadius="@dimen/app_dimen_card_corner"
            app:cardElevation="@dimen/dms_spacing_small"
            app:cardMaxElevation="@dimen/dms_spacing_small">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical">

                <FrameLayout
                    android:id="@+id/tutorial_page_cover_container"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent">

                    <androidx.appcompat.widget.AppCompatImageView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:adjustViewBounds="true"
                        android:scaleType="centerCrop"
                        android:src="@{ presenter.cover }"
                        tools:src="@drawable/raw_tutorial_page_i" />

                    <androidx.appcompat.widget.AppCompatImageView
                        android:layout_width="100dp"
                        android:layout_height="30dp"
                        android:layout_margin="@dimen/dms_spacing_medium"
                        android:src="@drawable/raw_tutorial_logo" />
                </FrameLayout>

                <androidx.appcompat.widget.AppCompatImageView
                    android:id="@+id/tutorial_page_cloud"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_gravity="bottom"
                    android:adjustViewBounds="true"
                    android:scaleType="centerCrop"
                    android:src="@drawable/ic_tutorial_cloud"
                    app:layout_constraintBottom_toBottomOf="@id/tutorial_page_cover_container"
                    android:layout_marginBottom="0dp"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent" />

                <androidx.appcompat.widget.AppCompatImageView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:adjustViewBounds="true"
                    android:alpha=".5"
                    android:scaleType="centerCrop"
                    android:src="@drawable/vector_background_tutorial_footer"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent" />

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/dms_spacing_xlarge"
                    android:layout_marginEnd="@dimen/dms_spacing_xlarge"
                    android:layout_marginBottom="@dimen/dms_spacing_large"
                    android:layout_marginTop="@dimen/dms_spacing_medium"
                    android:orientation="vertical"
                    app:layout_constraintTop_toBottomOf="@id/tutorial_page_cover_container">

                    <TextView
                        fontPath="@string/dms_font_arimo_bold"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:text="@{ presenter.title }"
                        android:textAlignment="center"
                        android:textColor="@color/colorPrimaryDark"
                        android:textSize="@dimen/dms_textsize_medium"
                        tools:text="@string/title_tutorial_page_i"
                        tools:textStyle="bold" />

                    <TextView
                        android:id="@+id/tutorial_page_subtitle"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/dms_spacing_medium"
                        android:text="@{ presenter.content }"
                        android:textAlignment="center"
                        android:textColor="?android:textColorSecondary"
                        tools:text="@string/content_tutorial_page_i" />
                </LinearLayout>

                <com.google.android.material.tabs.TabLayout
                    android:id="@+id/tutorial_page_bottom_dots"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_gravity="center_horizontal"
                    android:background="@android:color/transparent"
                    app:layout_constraintBottom_toTopOf="@id/tutorial_slide_button"
                    app:tabBackground="@drawable/state_tutorial_paging_tabs"
                    app:tabGravity="center"
                    app:tabIndicatorHeight="0dp" />

                <com.google.android.material.button.MaterialButton
                    android:id="@+id/tutorial_slide_button"
                    style="@style/Widget.MaterialComponents.Button.OutlinedButton.Icon"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/dms_spacing_xlarge"
                    android:layout_marginEnd="@dimen/dms_spacing_xlarge"
                    android:layout_marginBottom="@dimen/dms_spacing_medium"
                    android:layoutDirection="rtl"
                    android:onClick="@{ presenter::onSlideButtonClicked }"
                    android:paddingStart="@dimen/dms_spacing_xxlarge"
                    android:paddingTop="@dimen/dms_spacing_medium"
                    android:paddingEnd="@dimen/dms_spacing_xxlarge"
                    android:paddingBottom="@dimen/dms_spacing_medium"
                    android:text="@string/action_slide_to_next_tutorial"
                    android:textAppearance="@style/TextAppearance.AppCompat.Small"
                    app:cornerRadius="@dimen/dms_spacing_small"
                    app:icon="@drawable/ic_tutorial_slide"
                    app:layout_constraintBottom_toBottomOf="parent"
                    app:layout_constraintEnd_toEndOf="parent"
                    app:layout_constraintStart_toStartOf="parent"
                    app:strokeColor="#C1EFFF" />

            </androidx.constraintlayout.widget.ConstraintLayout>

        </com.google.android.material.card.MaterialCardView>

    </FrameLayout>

</layout>
